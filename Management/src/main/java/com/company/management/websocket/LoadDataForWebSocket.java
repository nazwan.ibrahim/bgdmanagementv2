package com.company.management.websocket;

import com.company.management.entity.ProductPrice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import io.jmix.core.security.SystemAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoadDataForWebSocket {

	@Autowired
	private DataManager dataManager;

	@Autowired
	private SystemAuthenticator systemAuthenticator;

	private final ObjectMapper mapper = new ObjectMapper();

	public String getSupplierAndBursaPrice() throws JsonProcessingException {
		return systemAuthenticator.withSystem(() -> {
			try{
				ProductPrice supplierPrice = this.dataManager.load(ProductPrice.class)
						.query("select e from ProductPrice e where e.type.code = :code and e.type.typeGroup.code = :typeCode")
						.parameter("code", "A01")
						.parameter("typeCode", "A")
						.fetchPlan("webSocketProductPrice")
						.one();

				ProductPrice bursaPrice = this.dataManager.load(ProductPrice.class)
						.query("select e from ProductPrice e where e.type.code = :code and e.type.typeGroup.code = :typeCode")
						.parameter("code", "A02")
						.parameter("typeCode", "A")
						.fetchPlan("webSocketProductPrice")
						.one();


				// construct a JSON message with some data
				Map<String, Object> data = new HashMap<>();
//				data.put("supplierName", supplierPrice.getType().getName());
//				data.put("supplierCode", supplierPrice.getType().getCode());
//				data.put("supplierSellPrice", supplierPrice.getSellPrice());
//				data.put("supplierBuyPrice", supplierPrice.getBuyPrice());
//				data.put("supplierTime", new Date().toString());
				data.put("bursaName",bursaPrice.getType().getName());
				data.put("bursaCode",bursaPrice.getType().getCode());
				data.put("bursaSellPrice",bursaPrice.getSellPrice());
				data.put("bursaBuyPrice", bursaPrice.getBuyPrice());
				data.put("bursaTime", new Date().toString());
				String json = mapper.writeValueAsString(data);

				return json;

			}catch (Exception ex){
				ex.getLocalizedMessage();
			}
			return "user not authenticated";
		});
	}
}
