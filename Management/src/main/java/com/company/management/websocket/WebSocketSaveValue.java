package com.company.management.websocket;

import com.company.management.entity.ProductPrice;

import com.company.management.listener.DataInitializer;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.security.SystemAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class WebSocketSaveValue {
	private static final Logger log = LoggerFactory.getLogger(WebSocketSaveValue.class);
	@Autowired
	private DataManager dataManager;

	@Autowired
	private SystemAuthenticator systemAuthenticator;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private DataInitializer dataInitializer;

	@Authenticated
	@ManagedOperation
	public Boolean saveValue(BigDecimal companybuybd, BigDecimal companysellbd){
		return systemAuthenticator.withSystem(() -> {
			try{
				ProductPrice productPrice = this.dataManager.load(ProductPrice.class)
						.query("select e from ProductPrice e where e.type.code = :code")
						.parameter("code","A01")
						.fetchPlan("webSocketProductPrice")
						.one();
				productPrice.setBuyPrice(companybuybd);
				productPrice.setSellPrice(companysellbd);
				dataManager.save(productPrice);
			}catch (Exception ex) {
//				log.info(ex.getMessage());
			}
			return true;
		});
	}

	public void callBackWebSocket(){
		dataInitializer.BGDWebSocketPrice();
	}


}
