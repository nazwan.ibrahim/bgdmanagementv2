package com.company.management.websocket;

import com.company.management.listener.DataInitializer;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BGDWebSocket extends WebSocketClient {

	private static final Logger log = LoggerFactory.getLogger(BGDWebSocket.class);

	private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	private final WebSocketSaveValue webSocketSaveValue;

	public BGDWebSocket(URI serverUri, WebSocketSaveValue webSocketSaveValue) {
		super(serverUri);
		this.webSocketSaveValue = webSocketSaveValue;
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		log.info("\nConnected to server: " + getURI());
	}

	@Override
	public void onMessage(String message) {

		Gson gson = new Gson();
		JsonObject jsonObject = gson.fromJson(message, JsonObject.class);
		String a = "a";

		String getPriceSell = String.valueOf(jsonObject.get("data").getAsJsonArray());
		List<JsonObject> jsonObjectCompanySell = new ArrayList<>();
		JsonArray jsonObjectArray = gson.fromJson(getPriceSell, JsonArray.class);

		String companysell = jsonObjectArray.get(0).getAsJsonObject().get("companysell").getAsString();
		String companybuy = jsonObjectArray.get(0).getAsJsonObject().get("companybuy").getAsString();

		BigDecimal companysellbd = new BigDecimal(companysell);
		BigDecimal companybuybd = new BigDecimal(companybuy);

		webSocketSaveValue.saveValue(companybuybd, companysellbd);
//		log.info("Received message: {}" , message);
//		scheduler.scheduleAtFixedRate(() -> {
//				log.info("Received message: {}" , message);
//		}, 0, 3600, TimeUnit.SECONDS); // send a message every 1 hour
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		log.info("Disconnected from server: " + reason);
		log.info("Reconnected back to the websocket after 1 second");
		setConnectionLostTimeout(1000);
		webSocketSaveValue.callBackWebSocket();
	}

	@Override
	public void onError(Exception ex) {
//		log.error("Error", ex);
	}

}
