package com.company.management.websocket;

import com.company.management.api.ProductController;
import com.company.management.entity.ProductPrice;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import io.jmix.core.security.SystemAuthenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.PongMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class ServerWebSocketHandler extends TextWebSocketHandler {

	@Autowired
	private DataManager dataManager;
	private static final Logger log = LoggerFactory.getLogger(ServerWebSocketHandler.class);

	private final Set<WebSocketSession> sessions = new CopyOnWriteArraySet<>();
	private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	private final ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private ProductController productController;

	@Autowired
	private SystemAuthenticator systemAuthenticator;
	@Autowired
	private LoadDataForWebSocket loadDataForWebSocket;


	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		String topic = session.getUri().getPath().split("/")[2];

		sessions.add(session);
		super.afterConnectionEstablished(session);

		log.info("BGD WebSocket Session Start + :" + session);

	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
//		log.info("Server connection closed: {}", status);
//		sessions.remove(session);
		sessions.remove(session);
		super.afterConnectionClosed(session, status);

		log.info("BGD WebSocket Session End {}", session + "\n" +
				"Status " + status);
	}

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

		// Check if the message is a heartbeat message.
		if (message.getPayload().equals("ping")) {
			// Send a heartbeat response back to the client.
			session.sendMessage(new TextMessage("pong"));
			log.info("BGD WebSocket Session Message Payload {}", message.getPayload());
		} else if(message.getPayload().equals("price")) {
			String json1 = loadDataForWebSocket.getSupplierAndBursaPrice();
			session.sendMessage(new TextMessage(json1));
			// Handle the message as usual.
//			super.handleTextMessage(session, message);

		}else{
			session.sendMessage(new TextMessage(message.getPayload()));
			log.info("BGD WebSocket Session Message Payload {}", message.getPayload() + "\n" +
					"Session : " + session);
		}

	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) {
		log.info("Server transport error: {} \n Session : {}", exception.getMessage(),session);
	}

	//	@Scheduled(fixedRate = 1000)
	@PostConstruct
	public void startSendingMessages() {
//		Timer timer = new Timer();
//		timer.schedule(new TimerTask() {
		scheduler.scheduleAtFixedRate(() -> {
			try {
				Map<String, Object> data = new HashMap<>();
				data.put("time", new Date().toString());
				String json = mapper.writeValueAsString(data);
				// send the message to all connected sessions
				for (WebSocketSession session : sessions) {
//					if (session.isOpen()) {
//						String json1 = loadDataForWebSocket.getSupplierAndBursaPrice();
//						session.sendMessage(new TextMessage(json1));
//						log.info("Bursa WebSocket Sending data {}", json1);
//					} else {
//						session.sendMessage(new TextMessage("pong"));
//						log.info("Bursa WebSocket Send pong to client");
//					}
					if (session.isOpen()) {
						String json1 = loadDataForWebSocket.getSupplierAndBursaPrice();
						if(json1.isEmpty())
						{
							session.sendMessage(new TextMessage("ping"));
							log.info("Bursa WebSocket Send pong to client");
						}else{
							session.sendMessage(new TextMessage(json1));
//							log.info("Bursa WebSocket Sending data {}",json1);
						}
					}
				}
			} catch (Exception e) {
				log.error("Error", e);
			}
		}, 0, 1, TimeUnit.SECONDS); // send a message every 10 second
	}
//		}, 0, 30000);


	@Override
	protected void handlePongMessage(WebSocketSession session, PongMessage message) throws Exception {

		super.handlePongMessage(session, message);
		log.info("BGD WebSocket Send Pong Message : {}\n" +
				"Session : {}",message,session);
	}
}
