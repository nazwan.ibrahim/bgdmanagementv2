package com.company.management.api;

import com.company.management.entity.TransactionHistoriesView;
import io.jmix.core.DataManager;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ScheduleController {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(ScheduleController.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private DataManager dataManager;

	List<TransactionHistoriesView> historiesViewList;

	public List<TransactionHistoriesView> getHistoriesViewList(Date date) {
		historiesViewList = new ArrayList<TransactionHistoriesView>();
		try{
			historiesViewList = this.dataManager.load(TransactionHistoriesView.class)
					.query("select d from TransactionHistoriesView where @dateEquals(d.createdDate, :param, user_timezone)")
					.parameter("param",date)
					.list();
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
		return historiesViewList;
	}

	public void generateFinanceReport() {
		Date yesterdayDate = Date.from(LocalDate.now().minusDays(1).atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());

		if(getHistoriesViewList(yesterdayDate).size() > 0) {
			generateFeeInterface(historiesViewList);
			generateCashTransactionInterface(historiesViewList);
			generateBGDInterface(historiesViewList);
		}
	}
	public void generateFeeInterface(List<TransactionHistoriesView> historiesViews) {}

	public void generateCashTransactionInterface(List<TransactionHistoriesView> historiesViews) {}

	public void generateBGDInterface(List<TransactionHistoriesView> historiesViews){}


}
