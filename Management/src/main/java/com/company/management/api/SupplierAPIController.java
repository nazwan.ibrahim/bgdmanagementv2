package com.company.management.api;

import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/supplierAPI")
public class SupplierAPIController {

	@Autowired
	private TimeSource timeSource;
	@Autowired
	private DataManager dataManager;

	@Autowired
	private Environment environment;

	@GetMapping(value = "/getProductPrice")
	public ResponseEntity getProductPrice() {

		try {
//			String url = "https://migasit.ace2u.com/mbbgtp.php?";
			String version = "version=1.0M&";
			String merchant_id = "merchant_id=Bursa@UAT&";
			String action = "action=price_acesell&";
			String product = "product=DG-999-9&";
			String currency = "currency=MYR&";
			String reference = "reference=BURSA_TESTING&";
			LocalDateTime currentDateTime = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			String stringtimeStamp = "timestamp=";
			String timeStamp = currentDateTime.format(formatter);
			String and = "&";
			String key = environment.getProperty("ace_secretKey");

			String cryptString = version + merchant_id + action + product + currency + reference + stringtimeStamp + timeStamp + and + key;

			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(cryptString.getBytes());
			byte[] digestSHA = md.digest();
			StringBuffer sb = new StringBuffer();
			for (byte b : digestSHA) {
				sb.append(String.format("%02x", b & 0xff));
			}

			String stringDisgest = "digest=";
			String digest = String.valueOf(sb);

			String completeURL = environment.getProperty("migasit.ace2u") + version + merchant_id + action + product + currency + reference + stringtimeStamp + timeStamp + and + stringDisgest + digest;

			URL apiUrl = new URL(completeURL);
			HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			StringBuffer response = null;
			if (responseCode == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = connection.getInputStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				String inputLine;
				response = new StringBuffer();

				while ((inputLine = bufferedReader.readLine()) != null) {
					response.append(inputLine);
				}
				bufferedReader.close();

			} else {  // handle the error response here

			}

			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}
}
