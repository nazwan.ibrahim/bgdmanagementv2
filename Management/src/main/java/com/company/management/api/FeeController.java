package com.company.management.api;

import com.company.management.entity.FeesManagement;
import com.company.management.form.FeeManagementForm;
import io.jmix.core.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fee")
public class FeeController {

	@Autowired
	private DataManager dataManager;

	@PutMapping("/transaction")
	public ResponseEntity setFeeTransaction(String productCode, @RequestBody FeeManagementForm form) {
		try {
			return ResponseEntity.ok("Fee Of Transaction Updated");
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}
}