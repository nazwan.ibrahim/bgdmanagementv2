package com.company.management.api;

import com.company.management.app.EmailBean;
import com.company.management.entity.TransactionHistoriesView;
import com.company.management.entity.UserPermission;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.wallet.*;
import io.jmix.core.DataManager;
import io.jmix.email.EmailException;
import io.jmix.email.EmailInfo;
import io.jmix.email.Emailer;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;
import io.jmix.emailtemplates.exception.ReportParameterTypeChangedException;
import io.jmix.emailtemplates.exception.TemplateNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/emailNotification")
public class EmailNotificationController {
	private static final Logger log = LoggerFactory.getLogger(EmailNotificationController.class);
	@Autowired
	private Emailer emailer;
	@Inject
	EmailTemplates emailTemplates;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private EmailBean emailBean;

	@GetMapping("cashTopupEmail")
	public ResponseEntity cashTopUpEmail(String fullName, BigDecimal amount, String timestamp,
	                                     BigDecimal feeCharge, BigDecimal feeAmount, BigDecimal taxCharge,
	                                     BigDecimal taxAmount, String totalAmount, String email){


		try{
			EmailTemplate emailTemplate = emailTemplates.buildFromTemplate("cash_topup")
				.setTo(email)
				.setSubject("Cash Top Up Confirmation")
				.build();
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("fullName",fullName);
			params.put("amount",amount);
			params.put("timestamp",timestamp);
			params.put("feeCharge",feeCharge);
			params.put("feeAmount",feeAmount);
			params.put("taxCharge",taxCharge);
			params.put("taxAmount",taxAmount);
			params.put("totalAmount",totalAmount);

			EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);
			emailer.sendEmail(emailInfo);
			return ResponseEntity.status(HttpStatus.OK).body("email sent");
		}
		catch (TemplateNotFoundException | EmailException | ReportParameterTypeChangedException e) {
			log.error("Error sending email due to :", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
		}
	}

	@GetMapping("cashWithdrawEmail")
	public ResponseEntity cashWithdrawEmail(String fullName, BigDecimal amount, String timestamp,
	                                     BigDecimal feeCharge, BigDecimal feeAmount, BigDecimal taxCharge,
	                                     BigDecimal taxAmount, String totalAmount, String email){
		try{
			EmailTemplate emailTemplate = emailTemplates.buildFromTemplate("cash_withdraw")
					.setTo(email)
					.setSubject("Cash Withdraw Confirmation")
					.build();
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("fullName",fullName);
			params.put("amount",amount);
			params.put("timestamp",timestamp);
			params.put("feeCharge",feeCharge);
			params.put("feeAmount",feeAmount);
			params.put("taxCharge",taxCharge);
			params.put("taxAmount",taxAmount);
			params.put("totalAmount",totalAmount);

			EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);
			emailer.sendEmail(emailInfo);
			return ResponseEntity.status(HttpStatus.OK).body("email sent");
		}
		catch (TemplateNotFoundException | EmailException | ReportParameterTypeChangedException e) {
			log.error("Error sending email due to :", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
		}
	}


	public List<TransactionHistoriesView> getInvestorTransactionHistoriesViewList(String referenceNo) {
		return this.dataManager.load(TransactionHistoriesView.class)
				.query("select e from TransactionHistoriesView e " +
						"where e.reference = :reference " +
						"and e.accOwner <> :accOwner1 " +
						"and e.accOwner <> :accOwner2")
				.parameter("reference", referenceNo)
				.parameter("accOwner1", "BgdAdmin")
				.parameter("accOwner2", "AceAdmin")
				.list();
	}

	public UserProfile getUserProfile (String username) {
		return this.dataManager.load(UserProfile.class)
				.query("select e from UserProfile e " +
						"where e.username = :username")
				.parameter("username", username)
				.one();
	}

	public UserPermission getUserPermission (String username) {
		try {
			return this.dataManager.load(UserPermission.class)
					.query("select u from UserPermission u " +
							"where u.user.username = :username")
					.parameter("username", username).one();
		}
		catch (Exception e){
			return null;
		}
	}

	public BigDecimal getAmountFromTransactionHistory(List<TransactionHistoriesView> historiesViewList, String transactionCode, String walletCode) {
		return historiesViewList.stream().filter(f -> f.getTransactionCode().equals(transactionCode) && f.getWalletCode().equals(walletCode)).toList().size() > 0
				&& historiesViewList.stream().filter(f -> f.getTransactionCode().equals(transactionCode) && f.getWalletCode().equals(walletCode)).toList().get(0).getAmount() != null
				? historiesViewList.stream().filter(f -> f.getTransactionCode().equals(transactionCode) && f.getWalletCode().equals(walletCode)).toList().get(0).getAmount()
				: BigDecimal.ZERO;
	}

	@PostMapping("cashTopup")
	public ResponseEntity cashTopup(String referenceNo) {


		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);
			UserProfile userProfile = getUserProfile(historiesViewList.get(0).getAccOwner());

			UserPermission userPermission = getUserPermission(historiesViewList.get(0).getAccOwner());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			BigDecimal topupAmount = getAmountFromTransactionHistory(historiesViewList, "F01", "E01");
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F01", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalAmount = topupAmount.add(fee).add(tax);

			return emailBean.generateEmailCashTopupConfirmation(userProfile.getEmail(), userProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					topupAmount.abs(), fee.abs(), tax.abs(), totalAmount.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("cashWithdrawal")
	public ResponseEntity cashWithdrawal(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);
			UserProfile userProfile = getUserProfile(historiesViewList.get(0).getAccOwner());

			UserPermission userPermission = getUserPermission(historiesViewList.get(0).getAccOwner());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			BigDecimal withdrawal = getAmountFromTransactionHistory(historiesViewList, "F02", "E01");
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F02", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalAmount = withdrawal.add(fee).add(tax);

			return emailBean.generateEmailCashWithdrawalConfirmation(userProfile.getEmail(), userProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					withdrawal.abs(), fee.abs(), tax.abs(), totalAmount.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("goldPurchase")
	public ResponseEntity goldPurchase(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);
			UserProfile userProfile = getUserProfile(historiesViewList.get(0).getAccOwner());

			UserPermission userPermission = getUserPermission(historiesViewList.get(0).getAccOwner());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			BigDecimal quantity = getAmountFromTransactionHistory(historiesViewList, "F05", "E02");
			BigDecimal purchaseAmount = getAmountFromTransactionHistory(historiesViewList, "F05", "E01");
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F05", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalPurchase = purchaseAmount.add(fee).add(tax);
			BigDecimal price_per_gram = historiesViewList.stream().filter(f -> f.getReference().equals(referenceNo)).toList().get(0).getPrice();

			return emailBean.generateEmailGoldPurchase(userProfile.getEmail(), userProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					referenceNo, historiesViewList.get(0).getReceiptNumber(), price_per_gram.abs(), quantity.abs(), purchaseAmount.abs(), fee.abs(), tax.abs(), totalPurchase.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("goldSale")
	public ResponseEntity goldSale(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);
			UserProfile userProfile = getUserProfile(historiesViewList.get(0).getAccOwner());

			UserPermission userPermission = getUserPermission(historiesViewList.get(0).getAccOwner());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			BigDecimal quantity = getAmountFromTransactionHistory(historiesViewList, "F06", "E02");
			BigDecimal saleAmount = getAmountFromTransactionHistory(historiesViewList, "F06", "E01");
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F06", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalReceive = saleAmount.add(fee).add(tax);
			BigDecimal price_per_gram = historiesViewList.stream().filter(f -> f.getReference().equals(referenceNo)).toList().get(0).getPrice();

			return emailBean.generateEmailGoldSale(userProfile.getEmail(), userProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					referenceNo, historiesViewList.get(0).getReceiptNumber(), price_per_gram.abs(), quantity.abs(), saleAmount.abs(), fee.abs(), tax.abs(), totalReceive.abs());

//			return ResponseEntity.ok("Email send");
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("goldRedeem")
	public ResponseEntity goldRedeem(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);
			UserProfile userProfile = getUserProfile(historiesViewList.get(0).getAccOwner());

			UserPermission userPermission = getUserPermission(historiesViewList.get(0).getAccOwner());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

//			List<RedeemInfo> redeemInfo = this.dataManager.load(RedeemInfo.class)
//					.query("select e RedeemInfo e " +
//							"where e.transaction.reference = :reference")
//					.parameter("reference", referenceNo)
//					.list();
			List<RedeemInfo> redeemInfo = this.dataManager.load(RedeemInfo.class)
					.query("from RedeemInfo a, TransactionAcct b " +
							"where a.transaction = b " +
							"and b.reference = :reference")
					.parameter("reference", referenceNo)
					.list();

			String address = redeemInfo.get(0).getAddress();
			String[] parts = address.split(",");
			for (int i = 0; i < parts.length; i++) {
				parts[i] = parts[i].trim();
			}

			String deliverTo = redeemInfo.get(0).getName();
			String phoneNo = redeemInfo.get(0).getPhone();
			String address1 = parts[0];
			String address2 = parts[1];
			String postcode = parts[2];
			String city = parts[3];
			String state = parts[4];
			String reason = redeemInfo.get(0).getReason();

//			Integer bilCoins = (int) (getAmountFromTransactionHistory(historiesViewList, "F04", "E02").intValue() / 4.25);
			Integer bilCoins = redeemInfo.size();
			BigDecimal courierFee = getAmountFromTransactionHistory(historiesViewList, "F09/B03", "E01");
			BigDecimal mintingFee = getAmountFromTransactionHistory(historiesViewList, "F09/B05", "E01");
			BigDecimal takafulFee = getAmountFromTransactionHistory(historiesViewList, "F09/B04", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal total = courierFee.add(mintingFee).add(takafulFee);



			return emailBean.generateEmailGoldRedemption(userProfile.getEmail(), userProfile.getFullName(), historiesViewList.get(0).getCreatedDate(), referenceNo,
					deliverTo, phoneNo, address1, address2, postcode, city, state, reason,
					bilCoins, courierFee.abs(), mintingFee.abs(), takafulFee.abs(), tax.abs(), total.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("goldTransfer")
	public ResponseEntity goldTransfer(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);

			List<TransferInfoView> transferInfo = this.dataManager.load(TransferInfoView.class)
					.query("select e from TransferInfoView e " +
							"where e.reference = :reference")
					.parameter("reference", referenceNo)
					.list();
			UserProfile senderUserProfile = getUserProfile(transferInfo.get(0).getSender());
			UserProfile receiverUserProfile = getUserProfile(transferInfo.get(0).getReceiver());

			UserPermission userPermission = getUserPermission(senderUserProfile.getUsername());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}


			String receiver = receiverUserProfile.getFullName();
			String reason = transferInfo.get(0).getReason();
			BigDecimal pricePerGram = historiesViewList.get(0).getPrice();
//			BigDecimal quantity = getAmountFromTransactionHistory(historiesViewList, "F03", "E02");
			BigDecimal quantity = transferInfo.get(0).getAmount();
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F03", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalPaid = fee.add(tax);

			return emailBean.generateEmailGoldTransfer(senderUserProfile.getEmail(), senderUserProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					receiver, reason, pricePerGram, quantity.abs(), fee.abs(), tax.abs(), totalPaid.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("goldReceive")
	public ResponseEntity goldReceive(String referenceNo) {

		try{
			List<TransactionHistoriesView> historiesViewList = getInvestorTransactionHistoriesViewList(referenceNo);

			List<TransferInfoView> transferInfo = this.dataManager.load(TransferInfoView.class)
					.query("select e from TransferInfoView e " +
							"where e.reference = :reference")
					.parameter("reference", referenceNo)
					.list();
			UserProfile senderUserProfile = getUserProfile(transferInfo.get(0).getSender());
			UserProfile receiverUserProfile = getUserProfile(transferInfo.get(0).getReceiver());

			UserPermission userPermission = getUserPermission(receiverUserProfile.getUsername());
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			String sender = senderUserProfile.getFullName();
			String reason = transferInfo.get(0).getReason();
			BigDecimal pricePerGram = historiesViewList.get(0).getPrice();
			BigDecimal quantity = transferInfo.get(0).getAmount();
			BigDecimal fee = getAmountFromTransactionHistory(historiesViewList, "F09/F03", "E01");
			BigDecimal tax = getAmountFromTransactionHistory(historiesViewList, "F09/02", "E01");
			BigDecimal totalPaid = fee.add(tax);

			return emailBean.generateEmailGoldReceived(receiverUserProfile.getEmail(), receiverUserProfile.getFullName(), historiesViewList.get(0).getCreatedDate(),
					sender, reason, quantity.abs());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("accountUpgradeSuccess")
	public ResponseEntity accountUpgradeSuccess(String username) {
		try{
			UserProfile userProfile = getUserProfile(username);

			UserPermission userPermission = getUserPermission(username);
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			return emailBean.generateEmailAccountUpgradeSuccess(userProfile.getFullName(), userProfile.getEmail());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("accountUpgradeFailed")
	public ResponseEntity accountUpgradeFailed(String username) {
		try{
			UserProfile userProfile = getUserProfile(username);

			UserPermission userPermission = getUserPermission(username);
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			return emailBean.generateEmailAccountUpgradeFailed(userProfile.getFullName(), userProfile.getEmail());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

	@PostMapping("accountUpgradePending")
	public ResponseEntity accountUpgradePending(String username) {
		try{
			UserProfile userProfile = getUserProfile(username);

			UserPermission userPermission = getUserPermission(username);
			if(userPermission==null || !userPermission.getAllowEmailNotification()){
				throw new RuntimeException("Permission not allow");
			}

			return emailBean.generateEmailAccountUpgradePending(userProfile.getFullName(), userProfile.getEmail());

		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}
}
