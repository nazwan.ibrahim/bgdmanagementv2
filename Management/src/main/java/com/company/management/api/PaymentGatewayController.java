package com.company.management.api;

import com.google.gson.JsonObject;
import org.springframework.core.env.Environment;
import net.minidev.json.JSONArray;
import netscape.javascript.JSObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/PaymentAPI/v3.0")
public class PaymentGatewayController {

    @Autowired
    private Environment env;

    @PostMapping("token")
    public ResponseEntity token(@RequestHeader("ClientID") String ClientID, @RequestHeader("Authorization") String Authorization,
                                @RequestParam String grant_type, @RequestParam String scope) {
        JsonObject jsonObject = new JsonObject();
        String response = "{\"message\": \"ok}";
        if(grant_type.equals(env.getProperty("paymentGateway_grant_type")) && scope.equals(env.getProperty("paymentGateway_scope"))) {
            jsonObject.addProperty("access_token", "cewrvcewrvrewvcarravcreverv");
            jsonObject.addProperty("token_type", "Bearer");
            jsonObject.addProperty("expires_in", 3600);
            jsonObject.addProperty("scope", "resource.WRITEresource.READ");

            response = "{\"id\": 123, \"name\": \"John Doe\", \"age\": 30}";
            response = jsonObject.getAsString();

            return ResponseEntity.ok(response);
        } else {
            jsonObject.addProperty("message", "unauthorized");
            response = "{\"message\": \"unauthorized}";
            response = jsonObject.getAsString();

            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }


    }

    @PostMapping("GetAccountEnquiry")
    public ResponseEntity getAccountEnquiry (@RequestHeader("Authorization") String Authorization, @RequestHeader("AmBank-Signature") String AmBankSignature,
                                             @RequestHeader("AmBank-Timestamp") String AmBankTimestamp, @RequestHeader("Content-Type") String ContentType,
                                             @RequestHeader("Accept") String Accept, @RequestHeader("srcRefNo") String srcRefNo,
                                             @RequestHeader("Channel-Token") String ChannelToken, @RequestHeader("Channel-APIKey") String ChannelAPIKey) {
        JsonObject jsonObject = new JsonObject();
        String response = "{\"message\": \"ok}";

        return ResponseEntity.ok(response);
    }

}
