package com.company.management.api;

import com.company.management.entity.*;
import com.company.management.entity.wallet.SequenceNumber;
import com.company.management.form.*;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Component
@RestController
@RequestMapping("/management")
public class ManagementController {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(ManagementController.class);
	@Autowired
	private DataManager dataManager;

	@Autowired
	private TimeSource timeSource;


	SimpleDateFormat formatter = new SimpleDateFormat("EEEE");
	SimpleDateFormat monthformatter = new SimpleDateFormat("MMMM");
	SimpleDateFormat yearformatter = new SimpleDateFormat("YYYY");

	@GetMapping(value = "/getTransferMotive")
	public ResponseEntity getTransferMotive() {
		try{
			List<TransferMotiveForm> transferMotiveForms = new ArrayList<>();

			List<RtType> rtType  = this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.typeGroup.code = :code")
					.parameter("code","TM")
					.list();

			rtType.stream().forEach(rtType1 -> {
				TransferMotiveForm transferMotiveForm1 = new TransferMotiveForm();
				transferMotiveForm1.setName(rtType1.getName());
				transferMotiveForm1.setCode(rtType1.getCode());
				transferMotiveForm1.setDescription(rtType1.getDescription());
				transferMotiveForm1.setTypeGroup(rtType1.getTypeGroup().getName());

				transferMotiveForms.add(transferMotiveForm1);
			});

			return ResponseEntity.status(HttpStatus.OK).body(transferMotiveForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getRedeemMotive")
	public ResponseEntity getTRedeemMotive() {
		try{
			List<RedeemMotiveForm> redeemMotiveFormList = new ArrayList<>();

			List<RtType> rtType  = this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.typeGroup.code = :code")
					.parameter("code","RR")
					.list();

			rtType.stream().forEach(rtType1 -> {
				RedeemMotiveForm redeemMotiveForm = new RedeemMotiveForm();
				redeemMotiveForm.setName(rtType1.getName());
				redeemMotiveForm.setCode(rtType1.getCode());
				redeemMotiveForm.setDescription(rtType1.getDescription());
				redeemMotiveForm.setTypeGroup(rtType1.getTypeGroup().getName());

				redeemMotiveFormList.add(redeemMotiveForm);
			});

			return ResponseEntity.status(HttpStatus.OK).body(redeemMotiveFormList);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getPriceGraph7Days")
	public ResponseEntity getPriceGraph7Days() throws ParseException {

		try{

			List<GraphSevenDaysForm> graphSevenDaysForms = new ArrayList<>();

			List<Sevendaysgraph> productPriceHistories = dataManager.load(Sevendaysgraph.class)
					.query("select s from Sevendaysgraph s order by s.priceTs asc")
					.list();

			for (Sevendaysgraph productPriceHistories1 : productPriceHistories) {
				String dayName = formatter.format(productPriceHistories1.getPriceTs());
				GraphSevenDaysForm graphSevenDaysForm1 = new GraphSevenDaysForm();

				switch (dayName) {
					case "Sunday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					case "Monday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					case "Tuesday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					case "Wednesday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					case "Thursday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					case "Friday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
					default -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(productPriceHistories1.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(productPriceHistories1.getSellPrice());
					}
				}
				graphSevenDaysForms.add(graphSevenDaysForm1);
			}

			return ResponseEntity.status(HttpStatus.OK).body(graphSevenDaysForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}

	@GetMapping(value = "/getPriceGraph1Month")
	public ResponseEntity getPriceGraph1Month() throws ParseException {

		try{

			List<GraphSevenDaysForm> graphSevenDaysForms = new ArrayList<>();

			List<Onemonthgraph> onemonthgraphs = dataManager.load(Onemonthgraph.class)
					.query("select s from Onemonthgraph s order by s.priceTs asc")
					.list();

			for (Onemonthgraph onemonthgraph : onemonthgraphs) {
				String dayName = formatter.format(onemonthgraph.getPriceTs());
				GraphSevenDaysForm graphSevenDaysForm1 = new GraphSevenDaysForm();

				switch (dayName) {
					case "Sunday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));

						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Monday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Tuesday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Wednesday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Thursday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Friday" -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
					default -> {
						graphSevenDaysForm1.setDateName(dayName);
						graphSevenDaysForm1.setDate(onemonthgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(onemonthgraph.getSellPrice()));
						graphSevenDaysForm1.setPrice(number.setScale(6, RoundingMode.UP));
					}
				}
				graphSevenDaysForms.add(graphSevenDaysForm1);
			}

			return ResponseEntity.status(HttpStatus.OK).body(graphSevenDaysForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}


	@GetMapping(value = "/getPriceGraph6Months")
	public ResponseEntity getPriceGraph6Months() throws ParseException {
		try{
			List<GraphMonthsForm> graphMonthsForms = new ArrayList<>();

			List<Sixmonthsgraph> sixmonthsgraphs = dataManager.load(Sixmonthsgraph.class)
					.query("select s from Sixmonthsgraph s order by s.priceTs asc")
					.list();

			for (Sixmonthsgraph sixmonthsgraph : sixmonthsgraphs) {
				String monthName = monthformatter.format(sixmonthsgraph.getPriceTs());
				GraphMonthsForm graphMonthsForm = new GraphMonthsForm();

				switch (monthName) {
					case "January" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "February" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "March" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "April" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "May" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Jun" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "July" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "August" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "September" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "October" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "November" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					default -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(sixmonthsgraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(sixmonthsgraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
				}
				graphMonthsForms.add(graphMonthsForm);
			}
			return ResponseEntity.status(HttpStatus.OK).body(graphMonthsForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}

	@GetMapping(value = "/getPriceGraph1year")
	public ResponseEntity getPriceGraph1year() throws ParseException {
		try{
			List<GraphMonthsForm> graphMonthsForms = new ArrayList<>();

			List<Oneyeargraph> oneyeargraphs = dataManager.load(Oneyeargraph.class)
					.query("select s from Oneyeargraph s order by s.priceTs asc")
					.list();

			for (Oneyeargraph oneyeargraph : oneyeargraphs) {
				String monthName = monthformatter.format(oneyeargraph.getPriceTs());
				GraphMonthsForm graphMonthsForm = new GraphMonthsForm();

				switch (monthName) {
					case "January" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "February" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "March" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "April" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "May" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "Jun" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "July" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "August" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "September" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "October" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					case "November" -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
					default -> {
						graphMonthsForm.setMonthName(monthName);
						graphMonthsForm.setDate(oneyeargraph.getPriceTs().toString());
						BigDecimal number = new BigDecimal(String.valueOf(oneyeargraph.getSellPrice()));
						graphMonthsForm.setPrice(number.setScale(6, RoundingMode.UP));
					}
				}
				graphMonthsForms.add(graphMonthsForm);
			}
			return ResponseEntity.status(HttpStatus.OK).body(graphMonthsForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}

	@GetMapping(value = "/getPriceGraph3years")
	public ResponseEntity getPriceGraph3years() throws ParseException {
		try {

			List<GraphYearForm> graphYearForms = new ArrayList<>();

			List<Threeyearsgraph> threeyearsgraphs = dataManager.load(Threeyearsgraph.class)
					.query("select s from Threeyearsgraph s order by s.priceTs asc")
					.list();


			for (Threeyearsgraph threeyearsgraph : threeyearsgraphs) {
				GraphYearForm graphYearForm = new GraphYearForm();
				String yearName = yearformatter.format(threeyearsgraph.getPriceTs());
				graphYearForm.setYearName(yearName);
				graphYearForm.setDate(threeyearsgraph.getPriceTs().toString());
				BigDecimal number = new BigDecimal(String.valueOf(threeyearsgraph.getSellPrice()));
				graphYearForm.setPrice(number.setScale(6, RoundingMode.UP));
				graphYearForms.add(graphYearForm);
			}

			return ResponseEntity.status(HttpStatus.OK).body(graphYearForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}

	@GetMapping(value = "/getPriceGraph5years")
	public ResponseEntity getPriceGraph5years() throws ParseException {
		try {
			List<GraphYearForm> graphYearForms = new ArrayList<>();

			List<Fiveyearsgraph> fiveyearsgraphs = dataManager.load(Fiveyearsgraph.class)
					.query("select s from Fiveyearsgraph s order by s.priceTs asc")
					.list();
			for (Fiveyearsgraph fiveyearsgraph : fiveyearsgraphs) {
				GraphYearForm graphYearForm = new GraphYearForm();
				String yearName = yearformatter.format(fiveyearsgraph.getPriceTs());
				graphYearForm.setYearName(yearName);
				graphYearForm.setDate(fiveyearsgraph.getPriceTs().toString());
				BigDecimal number = new BigDecimal(String.valueOf(fiveyearsgraph.getSellPrice()));
				graphYearForm.setPrice(number.setScale(6, RoundingMode.UP));

				graphYearForms.add(graphYearForm);
			}

			return ResponseEntity.status(HttpStatus.OK).body(graphYearForms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}


	@GetMapping(value = "/getPriceGraph")
	public ResponseEntity getPriceGraph(@RequestParam int days) throws ParseException {
		try {


			// Get current timestamp
			Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

			// Convert to LocalDate
			LocalDate currentDate = currentTimestamp.toLocalDateTime().toLocalDate();

			// Subtract 30 days
			LocalDate dateLocalFrom = currentDate.minusDays(days);

			// Convert to a Date object
			Date dateFrom = Date.from(dateLocalFrom.atStartOfDay(ZoneId.systemDefault()).toInstant());



			System.out.println("DATE FROM : "+ dateFrom);

			List<MarketplacePriceGraph> marketplacePriceGraphs = dataManager.load(MarketplacePriceGraph.class)
					.query("select m from MarketplacePriceGraph m where @dateAfter(m.date, :dateAfter)")
					.parameter("dateAfter",dateFrom)
					.list();

			return ResponseEntity.status(HttpStatus.OK).body(marketplacePriceGraphs);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
		}
	}

	public String generateSeqNo(String code, String length) {
		Date dateToday = Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());

		Integer seqNo = 0;
		List<SequenceNumber> currentSeqNo = this.dataManager.load(SequenceNumber.class)
				.query("select e from SequenceNumber e " +
						"where e.code = :code and " +
						"e.seqDate = :dateToday")
				.parameter("code", code)
				.parameter("dateToday", dateToday)
				.list();

		if(currentSeqNo.size() > 0) {
			seqNo = currentSeqNo.get(0).getSeqNo() + 1;

			currentSeqNo.get(0).setSeqNo(seqNo);
			this.dataManager.save(currentSeqNo.get(0));

		} else {
			seqNo = 1;

			SequenceNumber sequenceNo = this.dataManager.create(SequenceNumber.class);
			sequenceNo.setCode(code);
			sequenceNo.setSeqDate(dateToday);
			sequenceNo.setSeqNo(seqNo);
			this.dataManager.save(sequenceNo);
		}
		String referenceNo = code + new SimpleDateFormat("yyyyMMdd").format(getDateToday()) + String.format("%0"+length+"d", seqNo);
		log.info("SEC NUMBER GENERATED: ");
		log.info(referenceNo);

		return referenceNo;

	}

	public Date getDateToday() {
		return Date.from(LocalDate.now().atStartOfDay(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
	}

	public Date getDateTimeNow() {
		return Date.from(LocalDateTime.now().atZone(ZoneId.of("Asia/Kuala_Lumpur")).toInstant());
	}
}
