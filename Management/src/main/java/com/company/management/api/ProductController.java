package com.company.management.api;

import com.company.management.entity.*;
import com.company.management.entity.registrationdb.UserTier;
import com.company.management.form.*;
import io.jmix.core.DataManager;
import io.jmix.core.security.SystemAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;

@Component
@RestController
@RequestMapping("/product")
public class ProductController {
	@Autowired
	private DataManager dataManager;

	@Autowired
	private SystemAuthenticator systemAuthenticator;

	private final String transactionType = "T";

	public Product getProduct(String productCode) {
		return this.dataManager.load(Product.class)
				.query("select e from Product e where e.code = :code")
				.parameter("code", productCode).one();
	}

	public RtType getRtType(String rtypeString){
		return this.dataManager.load(RtType.class)
				.query("select e from Product e where e.code = :code")
				.parameter("code", rtypeString)
				.one();
	}

	public List<ProductPrice> getListProductPrice() {
		return this.dataManager.load(ProductPrice.class)
				.query("select e from ProductPrice e where e.type.code = :code")
				.parameter("code", transactionType)
				.list();
	}

	public ProductManagement getProductMarginOne(String productCode, String rtTypeCode) {
		return this.dataManager.load(ProductManagement.class)
				.query("select e from ProductManagement e where e.product.code = :productCode " +
						"and e.typeID.code = :typeCode")
				.parameter("productCode", productCode)
				.parameter("typeCodeazon", rtTypeCode).one();

	}

	public FeesManagement getFeeManagement(String productCode) {
		return this.dataManager.load(FeesManagement.class)
				.query("select e from FeesManagement e where e.type.code = :productCode")
				.parameter("productCode", productCode).one();
	}


//		@MessageMapping("/message")
		@GetMapping(value = "/getProductPrice")
		public ResponseEntity getProductPrice() {
			systemAuthenticator.withSystem(() -> {
				try {
					List<ProductPriceForm> forms = new ArrayList<>();
					List<ProductPriceDetailForm> productPriceDetailFormList = new ArrayList<>();

					List<ProductPrice> productPriceList = this.dataManager.load(ProductPrice.class)
							.query("select e from ProductPrice e where e.type.typeGroup.code = :code")
							.parameter("code", "A")
							//					.fetchPlan(FetchPlan.BASE)
							.list();

					productPriceList.forEach(productPrice -> {
						ProductPriceForm form = new ProductPriceForm();
						form.setName(productPrice.getType().getName());
						form.setCode(productPrice.getType().getCode());
						form.setBuyPrice(productPrice.getBuyPrice());
						form.setSellPrice(productPrice.getSellPrice());
						forms.add(form);
					});

//				template.convertAndSend("/supplierPriceWebSocket",forms);
					return ResponseEntity.status(HttpStatus.OK).body(forms);
				} catch (Exception ex) {
					return ResponseEntity.badRequest().body("{\"message\":\"" + ex.getMessage() + "\"}");
				}
			});
			return null;
		}

	@GetMapping("/getProductMargin")
	public ResponseEntity getProductMargin(String codeProduct, String codeType) {
		try {
			ProductMarginForm form = new ProductMarginForm();
			ProductManagement productMargin = getProductMarginOne(codeProduct,codeType);

			form.setProduct(productMargin.getProduct().getName());
			form.setDescription(productMargin.getDescription());
			form.setRtUom(productMargin.getUom().getCode());
			form.setType(productMargin.getType().getName());
			form.setValue(productMargin.getValue());


			return ResponseEntity.status(HttpStatus.OK).body(form);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getFeeManagement")
	public ResponseEntity getFeeManagement() {
		try{

			List<FeeManagementForm> forms = new ArrayList<>();

			List<FeesManagement> feesManagements = this.dataManager.load(FeesManagement.class)
					.query("select e from FeesManagement e")
					.list();
			feesManagements.forEach(feesManagement -> {
				FeeManagementForm feeManagementForm = new FeeManagementForm();
				feeManagementForm.setType(feesManagement.getType().getName());
				feeManagementForm.setCode(feesManagement.getType().getCode());
				feeManagementForm.setCharge(feesManagement.getCharge());
				feeManagementForm.setUom(feesManagement.getUom().getCode());

				forms.add(feeManagementForm);
			});
			return ResponseEntity.status(HttpStatus.OK).body(forms);
		}catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}

	}

	@PutMapping("/setFeeOfProduct")
	public ResponseEntity setFeeOfProduct(@RequestBody FeeManagementForm form) {
		try {
			FeesManagement feesManagement = getFeeManagement(form.getCode());
			feesManagement.getType().setCode(form.getType());
			feesManagement.getUom().setCode(form.getUom());
			feesManagement.setCharge(form.getCharge());
			this.dataManager.save(feesManagement);
			return ResponseEntity.ok("Fee Of Product Updated");
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}


	@GetMapping(value = "/getBursaRevenue")
	public ResponseEntity getBursaRevenue() {

		try{
			List<FeesForm> forms = new ArrayList<>();

			List<RevenueManagement> revenueManagementList = this.dataManager.load(RevenueManagement.class)
					.query("select e from RevenueManagement e " +
//							"where e.product.code = :code " +
							"where e.active = :active " +
							"and e.status.code = :statusCode" +
							" order by e.uom.code asc")
//					.parameter("code", "BGD")
					.parameter("active", true)
					.parameter("statusCode","G04")
//					.fetchPlan("bursaRevenueManagement")
					.list();

			revenueManagementList.forEach(revenueManagement -> {

				FeesForm form = new FeesForm();
				form.setFeeTypeCode(revenueManagement.getType().getCode());
				form.setFeeType(revenueManagement.getType().getName());
				form.setSetupUomCode(revenueManagement.getUom().getCode());
				form.setSetupUom(revenueManagement.getUom().getName());

				if(revenueManagement.getActive().equals(true)) {
					form.setSetupFees(revenueManagement.getCharge());
				} else {
					form.setSetupFees(BigDecimal.ZERO);
				}
				form.setMinFees(revenueManagement.getMinRM());
				form.setMaxFees(revenueManagement.getMaxRM());
				forms.add(form);
			});

			return ResponseEntity.status(HttpStatus.OK).body(forms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getBursaCostRecovery")
	public ResponseEntity getBursaCostRecovery() {

		try{
//			List<BursaCostRecovery> forms = new ArrayList<>();
			List<FeesForm> forms = new ArrayList<>();

			List<CostRevoceryManagement> costRevoceryManagementList = this.dataManager.load(CostRevoceryManagement.class)
					.query("select e from CostRevoceryManagement e " +
//							"where e.product.code = :code " +
							"where e.active = :active " +
							"and e.status.code= :statusCode " +
							"order by e.uom.code asc ")
//					.parameter("code", "BGD")
					.parameter("active",true)
					.parameter("statusCode","G04")
//					.fetchPlan("bursaRevenueManagement")
					.list();

			costRevoceryManagementList.forEach(costRecovery -> {
				FeesForm form = new FeesForm();
				form.setFeeTypeCode(costRecovery.getType().getCode());
				form.setFeeType(costRecovery.getType().getName());
				form.setSetupUomCode(costRecovery.getUom().getCode());
				form.setSetupUom(costRecovery.getUom().getName());

				if(costRecovery.getActive().equals(true)) {
					form.setSetupFees(costRecovery.getCharge());
				} else {
					form.setSetupFees(BigDecimal.ZERO);
				}
				form.setMinFees(costRecovery.getMinRM());
				form.setMaxFees(costRecovery.getMaxRM());
				forms.add(form);
			});

			return ResponseEntity.status(HttpStatus.OK).body(forms);
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getTierManagement")
	public ResponseEntity getTierManagement(String code){
		try {
			TierManagementForm tierManagementForm = new TierManagementForm();

			List<TierThresholdForm> tierThresholdFormList = new ArrayList<>();

			List<TierRequirementDoc> requirementDocList = new ArrayList<>();

			TierManagement tierManagement = this.dataManager.load(TierManagement.class)
					.query("select e from TierManagement e where e.code = :code")
					.parameter("code", code)
					.one();
			tierManagementForm.setCode(tierManagement.getCode());
			tierManagementForm.setCategory(tierManagement.getName());

			List<TierThreshold> tierThreshold = this.dataManager.load(TierThreshold.class)
					.query("select e from TierThreshold e where e.tierManagement = :tierManagement1")
					.parameter("tierManagement1", tierManagement)
					.list();

			tierThreshold.stream().forEach(tierThreshold1 -> {
				TierThresholdForm tierThresholdForm1 = new TierThresholdForm();
				tierThresholdForm1.setType(tierThreshold1.getType().getName());
				tierThresholdForm1.setTypeCode(tierThreshold1.getType().getCode());
				tierThresholdForm1.setMinValue(tierThreshold1.getMinValue());
				tierThresholdForm1.setMaxValue(tierThreshold1.getMaxValue());
				tierThresholdForm1.setUom(tierThreshold1.getUom().getName());
				tierThresholdForm1.setUomCode(tierThreshold1.getUom().getCode());
				tierThresholdForm1.setFrequent(tierThreshold1.getFrequent().getName());
				tierThresholdForm1.setFrequentCode(tierThreshold1.getFrequent().getCode());
				tierThresholdFormList.add(tierThresholdForm1);
			});
			tierManagementForm.setTierThresholdForm(tierThresholdFormList);

			List<SupportDoc> supportDoc = this.dataManager.load(SupportDoc.class)
							.query("select e from SupportDoc e where e.tierManagement = :tierManagement1")
									.parameter("tierManagement1",tierManagement)
											.list();

			supportDoc.stream().forEach(supportDoc1 -> {
				TierRequirementDoc tierRequirementDoc = new TierRequirementDoc();
				tierRequirementDoc.setCode(supportDoc1.getType().getCode());
				tierRequirementDoc.setRequirementDoc(supportDoc1.getType().getName());
				requirementDocList.add(tierRequirementDoc);
			});
			tierManagementForm.setTierRequirementDoc(requirementDocList);


			return ResponseEntity.status(HttpStatus.OK).body(tierManagementForm);
		}catch (Exception ex){
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getUserTierManagement")
	public ResponseEntity getUserTierManagement(String username){
		try {
			UserTier userTierRegistration = this.dataManager.load(UserTier.class)
					.query( "select e from UserTierRegistration e " +
									 "where e.user.username = :username1 " +
									 "order by e.createdDate desc")
					.parameter("username1", username)
					.one();

			UserTierManagement userTierManagement = new UserTierManagement();

			userTierManagement.setUserUsername(userTierRegistration.getUser().getUsername());
			userTierManagement.setUserFullName(userTierRegistration.getUser().getFullName());
			userTierManagement.setUserEmail(userTierRegistration.getUser().getEmail());
			userTierManagement.setUserTierCode(userTierRegistration.getTier().getCode());
			userTierManagement.setUserTierName(userTierRegistration.getTier().getName());
			userTierManagement.setUserTierDesc(userTierRegistration.getTier().getDescription());
			userTierManagement.setUserCreatedBy(userTierRegistration.getCreatedBy());
			userTierManagement.setUserCreatedDate(userTierRegistration.getCreatedDate());
			userTierManagement.setIdentificationNo(userTierRegistration.getUser().getIdentificationNo());
			userTierManagement.setIdentificationCode(userTierRegistration.getUser().getIdentificationType().getCode());
			userTierManagement.setIdentificationName(userTierRegistration.getUser().getIdentificationType().getName());
			userTierManagement.setIdentificationDesc(userTierRegistration.getUser().getIdentificationType().getDescription());
			userTierManagement.setReference(userTierRegistration.getReference());
			userTierManagement.setUserAccountStatusCode(userTierRegistration.getUser().getActStatus().getCode());
			userTierManagement.setUserAccountStatusName(userTierRegistration.getUser().getActStatus().getName());
			userTierManagement.setUserAccountStatusDesc(userTierRegistration.getUser().getActStatus().getDescription());
//

			return ResponseEntity.status(HttpStatus.OK).body(userTierManagement);
		}catch (Exception ex){
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}

	@GetMapping(value = "/getUserTierManagementList")
	public ResponseEntity getUserTierManagementList(String username){
		try {

			List<UserTierManagement> userTierManagementList = new ArrayList<>();

			List<UserTier> userTierRegistration = this.dataManager.load(UserTier.class)
					.query("select e from UserTierRegistration e where e.user.username= :username1 order by e.createdDate Desc")
//					.fetchPlan("getUserTierManagement")
					.parameter("username1",username)
					.list();

			userTierRegistration.stream().forEach(userTierRegistration1 -> {
				UserTierManagement userTierManagement = new UserTierManagement();

				userTierManagement.setUserUsername(userTierRegistration1.getUser().getUsername());
				userTierManagement.setUserFullName(userTierRegistration1.getUser().getFullName());
				userTierManagement.setUserEmail(userTierRegistration1.getUser().getEmail());
				userTierManagement.setUserTierCode(userTierRegistration1.getTier().getCode());
				userTierManagement.setUserTierName(userTierRegistration1.getTier().getName());
				userTierManagement.setUserTierDesc(userTierRegistration1.getTier().getDescription());
				userTierManagement.setUserCreatedBy(userTierRegistration1.getCreatedBy());
				userTierManagement.setUserCreatedDate(userTierRegistration1.getCreatedDate());
				userTierManagement.setIdentificationNo(userTierRegistration1.getUser().getIdentificationNo());
				userTierManagement.setIdentificationCode(userTierRegistration1.getUser().getIdentificationType().getCode());
				userTierManagement.setIdentificationName(userTierRegistration1.getUser().getIdentificationType().getName());
				userTierManagement.setIdentificationDesc(userTierRegistration1.getUser().getIdentificationType().getDescription());
				userTierManagement.setReference(userTierRegistration1.getReference());
				userTierManagement.setUserAccountStatusCode(userTierRegistration1.getUser().getActStatus().getCode());
				userTierManagement.setUserAccountStatusName(userTierRegistration1.getUser().getActStatus().getName());
				userTierManagement.setUserAccountStatusDesc(userTierRegistration1.getUser().getActStatus().getDescription());

				userTierManagementList.add(userTierManagement);
			});

			return ResponseEntity.status(HttpStatus.OK).body(userTierManagementList);
		}catch (Exception ex){
			return ResponseEntity.badRequest().body("{\"message\":\""+ex.getMessage()+"\"}");
		}
	}
}


