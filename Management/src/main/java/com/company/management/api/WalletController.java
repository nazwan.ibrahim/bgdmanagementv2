package com.company.management.api;

import com.company.management.entity.*;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.wallet.*;
import com.company.management.form.*;
import io.jmix.core.DataManager;
import io.jmix.core.Entity;
import io.jmix.core.LoadContext;
import io.jmix.core.TimeSource;
import io.jmix.core.repository.Query;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.CurrentAuthentication;
import org.apache.commons.math3.dfp.DfpField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.eclipse.persistence.jpa.jpql.utility.CollectionTools.list;

@RestController
@RequestMapping("/wallet")
public class WalletController {


    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(WalletController.class);
    static Logger logger = LogManager.getLogger(WalletController.class.getName());
    @Autowired
    private DataManager dataManager;

    @Autowired
    private TimeSource timeSource;

    @Autowired
    private CurrentAuthentication currentAuthentication;
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    @Autowired
    private ManagementController managementController;

    @Autowired
    private ScheduleController scheduleController;

    public static String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(CHARACTERS.length());
            sb.append(CHARACTERS.charAt(index));
        }
        return sb.toString();
    }


    public ResponseDTO errorResponseHandler(ResponseDTO responseDTO,String exMessage,String transactionCode ){
        String errorCode=null;
        String errorDescription= null;
        if(exMessage.equals("RCE002")){
            errorCode=exMessage;
            errorDescription="Balance Insufficient";
        } else if (exMessage.equals("RCE003")) {
            errorCode=exMessage;
            errorDescription="Invalid Account";
        } else{
            errorCode="RCE999";
            errorDescription=exMessage;
        }
        responseDTO.setDate(timeSource.now().toLocalDateTime());
        responseDTO.setReference(null);
        responseDTO.setStatus("G02");
        responseDTO.setResponseCode(errorCode);
        responseDTO.setDescription(errorDescription);
        responseDTO.setTransactionType(transactionCode);
        responseDTO.setSuccess(false);
        logger.error(exMessage);
        return responseDTO;
    }

    @GetMapping("initTransfer")
    public ResponseEntity initTransfer(String acctNumberSender, String acctNumberReceiver, BigDecimal amount, String statusCode, String reason) {
        log.info("INITTRANSFER: called by: " + currentAuthentication.getAuthentication().getName());
        ResponseEntity tierChecking = tierThresholdChecking("F03", acctNumberSender, amount);
        if(!tierChecking.getStatusCode().is2xxSuccessful()) {
            return tierChecking;
        }

        List<WalletTransferForm> forms = new ArrayList<>();
        WalletTransferForm form = new WalletTransferForm();
        form.setAcctNumberSender(acctNumberSender);
        form.setAcctNumberReceiver(acctNumberReceiver);
        form.setAmount(amount);
        form.setStatusCode(statusCode);
        form.setReason(reason);
        forms.add(form);

        return initTransfer(forms);
    }
    public ResponseEntity initTransfer(@RequestBody List<WalletTransferForm> forms){

        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try {

            forms.forEach(walletTransferForm -> {
                BigDecimal chargeFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                BigDecimal buyPrice= BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                BigDecimal taxFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
//                walletTransferForm.setReference(UUID.randomUUID().toString());
//                walletTransferForm.setReason(managementController.generateSeqNo("F03", "6"));

                ProductPrice productPriceSell = this.dataManager.load(ProductPrice.class)
                        .query("select e from ProductPrice e where e.type.code = :code")
                        .parameter("code","A02")
                        .fetchPlan("webSocketProductPrice")
                        .one();

                walletTransferForm.setBursaSellPrice(productPriceSell.getSellPrice().setScale(2,RoundingMode.HALF_UP));
                walletTransferForm.setStatusCode(walletTransferForm.getStatusCode());

                Optional<AccountBgd> accountBgdReceiver = this.dataManager.load(AccountBgd.class)
                        .query( "select a from AccountBgd a " +
                                "where a.acctNumber = :acctNumber1")
                        .parameter("acctNumber1", walletTransferForm.getAcctNumberReceiver()).optional();

                //unnecessary because checking balance will be done after init
//                AccountBgd accountBgdSender= this.dataManager.load(AccountBgd.class)
//                        .query( "select a from AccountBgd a " +
//                                "where a.acctNumber = :acctNumber1")
//                        .parameter("acctNumber1", walletTransferForm.getAcctNumberSender()).one();

                if(accountBgdReceiver.isEmpty()){
                    throw  new RuntimeException("RCE003");
                }

                UserProfile receiverProfile = this.dataManager.load(UserProfile.class)
                        .query( "select u from UserProfile u " +
                                "where u.username = :username1")
                        .parameter("username1", accountBgdReceiver.get().getAccOwner()).one();

//                receiverProfile.get setUserTier(null); // SET usertier null because this causing return response api  error
                walletTransferForm.setReceiverProfile(receiverProfile);
//                log.info("INITTRANSFER: from: Account no.: " + accountBgdSender.getAcctNumber().toString() + " Owner: " + accountBgdSender.getAccOwner());
                log.info("INITTRANSFER: to: Account no.: " + accountBgdReceiver.get().getAcctNumber() + " Owner: " + accountBgdReceiver.get().getAccOwner());
                log.info("INITTRANSFER: Amount: " + walletTransferForm.getAmount());


                //Checking will be done after init
//                AccountWalletView walletViewAssetSender = this.dataManager.load(AccountWalletView.class)
//                        .query( "select a from AccountWalletView a " +
//                                "where a.acctNumber = :acctNumber1")
//                        .parameter("acctNumber1", accountBgdSender.getAcctNumber())
//                        .one();

//                if(walletTransferForm.getAmount().compareTo(walletViewAssetSender.getAvailableAmount()) > 0) {
//                    try {
//                        throw new Exception("Amount insufficient due to amount:"+ walletTransferForm.getAmount() + " > wallet_balance:"+walletViewAssetSender.getAvailableAmount());
//                    } catch (Exception ex) {
//                        throw new RuntimeException(ex.getMessage());
//                    }
//                }

                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0).setScale(2,RoundingMode.HALF_UP));
                //calculated fee
                RevenueManagement revenueManagement = this.dataManager.load(RevenueManagement.class)
                        .query(   "select r from RevenueManagement r " +
                                "where r.type.code = :typeCode1 " +
                                "and r.active = :active")
                        .parameter("typeCode1", "C04") //Transfer fee (Sender)
                        .parameter("active", true)
                        .one();

                List<FeesForm> feesForms = new ArrayList<>();
                FeesForm feesForm = new FeesForm();
                if(revenueManagement.getActive().equals(true)) { // add condition min fee apply when transfer more that RM200
//                    FeesForm feesForm = new FeesForm();
                    feesForm.setFeeTypeCode("F09/F03");
                    feesForm.setFeeType(revenueManagement.getType().getName());
                    feesForm.setSetupFees(revenueManagement.getCharge());
                    feesForm.setSetupUomCode(revenueManagement.getUom().getCode());
                    feesForm.setSetupUom(revenueManagement.getUom().getName());

                    if(revenueManagement.getUom().getCode().equals("J04")) { //percentage
                        ProductPrice productPrice = this.dataManager.load(ProductPrice.class)
                                .query( "select p from ProductPrice p " +
                                        "where p.product.code = :productCode1 " +
                                        "and p.type.code = :typeCode1")
                                .parameter("productCode1", "BGD")
                                .parameter("typeCode1", "A02") //Bursa Price
                                .one();
                        chargeFee = walletTransferForm.getAmount().setScale(6,RoundingMode.HALF_UP).multiply(productPrice.getSellPrice().setScale(2,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP)
                                .multiply(revenueManagement.getCharge().divide(new BigDecimal("100"),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                        //Check if value of transfer is less then RM 200, will not charge
                        BigDecimal transferValue = walletTransferForm.getAmount().setScale(6,RoundingMode.HALF_UP).multiply(productPrice.getSellPrice().setScale(2,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                        if(transferValue.compareTo(BigDecimal.valueOf(200))<0){
                            chargeFee= BigDecimal.valueOf(0);
                        }
                        if(revenueManagement.getMinRM() != null && chargeFee.compareTo(revenueManagement.getMinRM()) < 0) {
                            chargeFee = revenueManagement.getMinRM();
                        } else if(revenueManagement.getMaxRM() != null && chargeFee.compareTo(revenueManagement.getMaxRM()) > 0) {
                            chargeFee = revenueManagement.getMaxRM();
                        }

//                        taxFee = chargeFee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
                    else if(revenueManagement.getUom().getCode().equals("J01")) //rm fixed
                    {
                        chargeFee = revenueManagement.getCharge().setScale(2,RoundingMode.HALF_UP);
//                        taxFee = chargeFee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }

                    //calculated tax
                    if(revenueManagement.getTypeTax() != null && !revenueManagement.getTypeTax().getCode().equals("TAX00")) {
                        taxFee = chargeFee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
                    }

                    totalFee.getAndSet(totalFee.get().add(chargeFee));
                    totalFee.getAndSet(totalFee.get().add(taxFee));

                    feesForm.setChargeFees(chargeFee);
                    feesForm.setChargeUomCode("J01");
                    feesForm.setChargeUom("RM");
                    feesForms.add(feesForm);

                    FeesForm taxFeesForm = new FeesForm();
                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(revenueManagement.getValueTax());
                    taxFeesForm.setSetupUom(revenueManagement.getUomTax().getName());
                    taxFeesForm.setSetupUomCode(revenueManagement.getUomTax().getCode());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");
                    taxFeesForm.setChargeFees(taxFee);

                    feesForms.add(taxFeesForm);
                    walletTransferForm.setFeesForms(feesForms);

                }

//                AccountBgd accountBgdSenderCash = getWallet(accountBgdSender.getAccOwner(),"E01");

                //checking will be done after init
//                AccountWalletView walletViewCashSender = this.dataManager.load(AccountWalletView.class)
//                        .query( "select a from AccountWalletView a " +
//                                "where a.acctNumber = :acctNumber1")
//                        .parameter("acctNumber1", accountBgdSenderCash.getAcctNumber())
//                        .one();

//                log.info("INITTRANSFER: Total fees: " + totalFee.get());

//                log.info("INITTRANSFER: " + walletViewCashSender.getAccountOwner().toString());
                //CHECK BALANCE
                //Checking will be done after init
//                if(totalFee.get().compareTo(walletViewCashSender.getAvailableAmount()) > 0) {
//                    logger.error("Amount cash insufficient due to amount:"+ totalFee.get() + " > wallet_balance:"+walletViewCashSender.getAvailableAmount());
//                    throw new RuntimeException("RCE002");
//                    }
            });

            return ResponseEntity.ok(forms);

        } catch (Exception ex) {

            responseDTO = errorResponseHandler(responseDTO,ex.getMessage(),"F03" );
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }



    @GetMapping("initRedeem")
    public ResponseEntity initRedeem(String acctNumber, BigDecimal unit, String name, String phone, String address, String address2, String postcode, String city, String state, String remarks, String reason) {
        logger.info("INITREDEEM: called by: "+currentAuthentication.getAuthentication().getName());

        List<WalletRedeemForm> forms = new ArrayList<>();
        WalletRedeemForm form = new WalletRedeemForm();
        form.setAcctNumber(acctNumber);
        form.setUnit(unit);
        form.setName(name);
        form.setPhone(phone);
        form.setAddress(address);
        form.setAddress2(address2);
        form.setPostcode(postcode);
        form.setCity(city);
        form.setState(state);
        form.setReason(reason);
        form.setRemarks(remarks);
        forms.add(form);

        return initRedeem(forms);
    }
    public ResponseEntity initRedeem(@RequestBody List<WalletRedeemForm> forms) {

        log.info("INITREDEEM: called");
        try {
            forms.forEach(e -> {

//                e.setReference(UUID.randomUUID().toString());
//                e.setReference(managementController.generateSeqNo("F04", "6"));
                e.setWeight(e.getUnit().multiply(BigDecimal.valueOf(4.25)).setScale(6,RoundingMode.HALF_UP));

                // get available serial number
//                e.setSerialNumber(generateRandomString(10)); // dummy serial number


                AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                        .query( "select a from AccountWalletView a " +
                                "where a.acctNumber = :acctNumber1")
                        .parameter("acctNumber1", e.getAcctNumber())
                        .one();

                AccountBgd accountBgdCash= getWallet(walletView.getAccountOwner(),"E01");

                log.info("INITREDEEM: " + walletView.getAccountOwner());
                if(e.getWeight().compareTo(walletView.getAvailableAmount()) > 0) {
                    try {
//                        throw new Exception("{\"message\":\"Amount exceed limit\"}");
                        throw new Exception("Amount insufficient due to amount:"+ e.getWeight() + " > wallet_balance:"+walletView.getAvailableAmount());
                    } catch (Exception ex) {
                        throw new RuntimeException(ex.getMessage());
                    }
                }

                ProductPrice productPriceSell = this.dataManager.load(ProductPrice.class)
                        .query("select e from ProductPrice e where e.type.code = :code")
                        .parameter("code","A02")
                        .fetchPlan("webSocketProductPrice")
                        .one();

                e.setBursaSellPrice(productPriceSell.getSellPrice().setScale(2,RoundingMode.HALF_UP));


                // fees charge section
                List<CostRevoceryManagement> costRecoveryManagements = this.dataManager.load(CostRevoceryManagement.class)
                        .query( "select c from CostRevoceryManagement c " +
                                "where c.active = :active1")
                        .parameter("active1", true).list();


                // set courier charge
                CostRevoceryManagement courierCost = costRecoveryManagements.stream()
                        .filter(f -> f.getType().getCode().equals("B03")).collect(Collectors.toList()).get(0);


                // set takaful charge
                CostRevoceryManagement takafulCost = costRecoveryManagements.stream()
                        .filter(f -> f.getType().getCode().equals("B04")).collect(Collectors.toList()).get(0);

                // set minting charge
                CostRevoceryManagement mintingCost = costRecoveryManagements.stream()
                        .filter(f -> f.getType().getCode().equals("B05")).collect(Collectors.toList()).get(0);


                AtomicReference<BigDecimal> totalFee = new AtomicReference<>(BigDecimal.valueOf(0).setScale(2,RoundingMode.HALF_UP));


                List<FeesForm> feesForms = new ArrayList<>();
                if(courierCost != null) {
                    FeesForm feesForm = new FeesForm();
                    feesForm.setFeeType(courierCost.getType().getName());
                    feesForm.setFeeTypeCode(courierCost.getType().getCode());
                    feesForm.setSetupFees(courierCost.getCharge());
                    feesForm.setSetupUom(courierCost.getUom().getName());
                    feesForm.setSetupUomCode(courierCost.getUom().getCode());
                    feesForm.setChargeUomCode(courierCost.getUom().getCode());

                    if(courierCost.getUom().getCode().equals("J01")) { //charge in RM
                        feesForm.setChargeFees(courierCost.getCharge().setScale(2,RoundingMode.HALF_UP));
                        totalFee.getAndSet(totalFee.get().add(feesForm.getChargeFees()));
                        feesForm.setChargeUom("RM");
                        feesForm.setChargeUomCode("J01");
                        log.info("INITREDEEM: minting cost setup:  " + feesForm.getSetupFees() + " (" + feesForm.getSetupUom() + ") ");
                        log.info("INITREDEEM: courier cost : " + feesForm.getChargeFees());
                    }
                    else{
                        throw new RuntimeException("CHARGE IN PERCENT NOT SUPPORTED");
                    }


                    feesForms.add(feesForm);
                    FeesForm taxFeesForm = new FeesForm();
//                    taxFeesForm.setFeeTypeCode("TAX01");
//                    taxFeesForm.setFeeType("SST");
//                    taxFeesForm.setSetupFees(courierCost.getValueTax());
//                    taxFeesForm.setSetupUom(courierCost.getUomTax().getName());
//                    taxFeesForm.setChargeUomCode("J01");
//                    taxFeesForm.setChargeUom("RM");
//                    BigDecimal addtax =  feesForm.getSetupFees().subtract((taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100))));
//                    taxFeesForm.setChargeFees(addtax);
                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(courierCost.getValueTax());
                    taxFeesForm.setSetupUom(courierCost.getUomTax().getName());
                    taxFeesForm.setSetupUomCode(courierCost.getUomTax().getCode());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");

                    //assuming all sst is always in percentage
                    //Add sst calculation
//                    BigDecimal taxChargeFee = feesForm.getChargeFees().multiply(taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    BigDecimal taxChargeFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                    if(courierCost.getTypeTax() != null && !courierCost.getTypeTax().getCode().equals("TAX00")) {
                        taxChargeFee = feesForm.getChargeFees().multiply(courierCost.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
                    taxFeesForm.setChargeFees(taxChargeFee.setScale(2,RoundingMode.HALF_UP));
                    log.info("INITREDEEM: minting cost sst setup: " + taxFeesForm.getSetupFees() + " (" + taxFeesForm.getSetupUom() + ") ");
                    log.info("INITREDEEM: courier cost sst : " + taxChargeFee);

                    totalFee.getAndSet(totalFee.get().add(taxFeesForm.getChargeFees()));

                    feesForms.add(taxFeesForm);
                }

                if(takafulCost != null) {
                    FeesForm feesForm = new FeesForm();
                    feesForm.setFeeType(takafulCost.getType().getName());
                    feesForm.setFeeTypeCode(takafulCost.getType().getCode());
                    feesForm.setSetupFees(takafulCost.getCharge());
                    feesForm.setSetupUom(takafulCost.getUom().getName());
                    feesForm.setSetupUomCode(takafulCost.getUom().getCode());
                    feesForm.setChargeUomCode(takafulCost.getUom().getCode());

                    if(takafulCost.getUom().getCode().equals("J01")) { //charge in RM
                        feesForm.setChargeFees(takafulCost.getCharge().multiply(e.getUnit()).setScale(2,RoundingMode.HALF_UP));
                        totalFee.getAndSet(totalFee.get().add(feesForm.getChargeFees()));

                        feesForm.setChargeUom("RM");
                        feesForm.setChargeUomCode("J01");
                        log.info("INITREDEEM: minting cost setup:  " + feesForm.getSetupFees() + " (" + feesForm.getSetupUom() + ") ");
                        log.info("INITREDEEM: takaful cost : " + feesForm.getChargeFees());
                    }
                    else{
                        throw new RuntimeException("CHARGE IN PERCENT NOT SUPPORTED");
                    }


                    feesForms.add(feesForm);

                    FeesForm taxFeesForm = new FeesForm();
//                    taxFeesForm.setFeeTypeCode("TAX01");
//                    taxFeesForm.setFeeType("SST");
//                    taxFeesForm.setSetupFees(takafulCost.getValueTax());
//                    taxFeesForm.setSetupUom(takafulCost.getUomTax().getName());
//                    taxFeesForm.setChargeUomCode("J01");
//                    taxFeesForm.setChargeUom("RM");
//                    BigDecimal addtax =  feesForm.getSetupFees().subtract((taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100))));
//                    taxFeesForm.setChargeFees(addtax);
                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(takafulCost.getValueTax());
                    taxFeesForm.setSetupUom(takafulCost.getUomTax().getName());
                    taxFeesForm.setSetupUomCode(takafulCost.getUomTax().getCode());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");

                    //assuming all sst is always in percentage
//                    BigDecimal taxChargeFee = feesForm.getChargeFees().multiply(taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    BigDecimal taxChargeFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                    if(takafulCost.getTypeTax() != null && !takafulCost.getTypeTax().getCode().equals("TAX00")) {
                        taxChargeFee = feesForm.getChargeFees().multiply(takafulCost.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
                    taxFeesForm.setChargeFees(taxChargeFee.setScale(2,RoundingMode.HALF_UP));
                    log.info("INITREDEEM: minting cost sst setup: " + taxFeesForm.getSetupFees() + " (" + taxFeesForm.getSetupUom() + ") ");
                    log.info("INITREDEEM: takaful cost sst : " + taxChargeFee);

                    totalFee.getAndSet(totalFee.get().add(taxFeesForm.getChargeFees()));
                    feesForms.add(taxFeesForm);
                }

                if(mintingCost != null) {
                    FeesForm feesForm = new FeesForm();
                    feesForm.setFeeType(mintingCost.getType().getName());
                    feesForm.setFeeTypeCode(mintingCost.getType().getCode());
                    feesForm.setSetupFees(mintingCost.getCharge());
                    feesForm.setSetupUom(mintingCost.getUom().getName());
                    feesForm.setSetupUomCode(mintingCost.getUom().getCode());
                    feesForm.setChargeUomCode(mintingCost.getUom().getCode());

                    if(mintingCost.getUom().getCode().equals("J01")) { //charge in RM
                        feesForm.setChargeFees(mintingCost.getCharge().multiply(e.getUnit()).setScale(2,RoundingMode.HALF_UP));
                        totalFee.getAndSet(totalFee.get().add(feesForm.getChargeFees()));
                        feesForm.setChargeUom("RM");
                        feesForm.setChargeUomCode("J01");
                        log.info("INITREDEEM: minting cost setup:  " + feesForm.getSetupFees() + " (" + feesForm.getSetupUom() + ") ");
                        log.info("INITREDEEM: minting cost : " + feesForm.getChargeFees());
                    }
                    else{
                        throw new RuntimeException("CHARGE IN PERCENT NOT SUPPORTED");
                    }

                    feesForms.add(feesForm);

                    FeesForm taxFeesForm = new FeesForm();
//                    taxFeesForm.setFeeTypeCode("TAX01");
//                    taxFeesForm.setFeeType("SST");
//                    taxFeesForm.setSetupFees(mintingCost.getValueTax());
//                    taxFeesForm.setSetupFees(BigDecimal.ZERO);
//                    taxFeesForm.setSetupUom(mintingCost.getUomTax().getName());
//                    taxFeesForm.setChargeUomCode("J01");
//                    taxFeesForm.setChargeUom("RM");
//                    BigDecimal addtax =  feesForm.getSetupFees().subtract((taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100))));
//                    taxFeesForm.setChargeFees(addtax);
                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(mintingCost.getValueTax());
                    taxFeesForm.setSetupUom(mintingCost.getUomTax().getName());
                    taxFeesForm.setSetupUomCode(mintingCost.getUomTax().getCode());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");

                    //assuming all sst is always in percentage
//                    BigDecimal taxChargeFee = feesForm.getChargeFees().multiply(taxFeesForm.getSetupFees().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    BigDecimal taxChargeFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                    if(mintingCost.getTypeTax() != null && !mintingCost.getTypeTax().getCode().equals("TAX00")) {
                        taxChargeFee = feesForm.getChargeFees().multiply(mintingCost.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
                    taxFeesForm.setChargeFees(taxChargeFee.setScale(2,RoundingMode.HALF_UP));
                    log.info("INITREDEEM: minting cost sst setup: " + taxFeesForm.getSetupFees() + " (" + taxFeesForm.getSetupUom() + ") ");
                    log.info("INITREDEEM: minting cost sst : " + taxChargeFee);

                    totalFee.getAndSet(totalFee.get().add(taxFeesForm.getChargeFees()));
                    feesForms.add(taxFeesForm);
                }
                e.setFeesForms(feesForms);


                log.info("INITREDEEM: Cash wallet number: " + accountBgdCash.getAcctNumber());


                //  COMMENT OUT checking BECASUSE FRONT END NEED TO GET THIS VALUE TO VALIDATE LATER after init
//                AccountWalletView walletViewCash = this.dataManager.load(AccountWalletView.class)
//                        .query( "select a from AccountWalletView a " +
//                                "where a.acctNumber = :acctNumber1")
//                        .parameter("acctNumber1", accountBgdCash.getAcctNumber())
//                        .one();

//                if(totalFee.get().compareTo(walletViewCash.getAvailableAmount()) > 0) {
//                    try {
////                        throw new Exception("{\"message\":\"Amount exceed limit\"}");
//                        throw new Exception("Amount insufficient due to amount:"+  totalFee.get()+ " > wallet_balance:"+walletViewCash.getAvailableAmount());
//                    } catch (Exception ex) {
//                        throw new RuntimeException(ex.getMessage());
//                    }
//                }
            });
            return ResponseEntity.ok(forms);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("initTopup")
    public ResponseEntity initTopup(String acctNumber, BigDecimal amount, String transactionTypeCode, String walletTypeCode , String channelCode , String currency ,String customerName ) {

        ResponseEntity tierChecking = tierThresholdChecking(transactionTypeCode, acctNumber, amount);
        if(!tierChecking.getStatusCode().is2xxSuccessful()) {
            return tierChecking;
        }

        List<WalletInitTopupForm> forms = new ArrayList<>();
        WalletInitTopupForm form = new WalletInitTopupForm();
        form.setAcctNumber(acctNumber);
        form.setAmount(amount.setScale(2,RoundingMode.HALF_UP));
        form.setTransactionTypeCode(transactionTypeCode);
        form.setWalletTypeCode(walletTypeCode);
        form.setChannelCode(channelCode);
        form.setBicCode(null);
        form.setCustomerName(customerName);
        form.setCurrency(currency);
        forms.add(form);

        return initTopup(forms);
    }
    public ResponseEntity initTopup(@RequestBody List<WalletInitTopupForm> forms) {
        try {
            CostRevoceryManagement costRecovery = this.dataManager.load(CostRevoceryManagement.class)
                    .query("select e from CostRevoceryManagement e " +
                            "where e.type.code = :code " +
                            "and e.active = :active")
                    .parameter("code", "B01")
                    .parameter("active", true)
                    .one();
            forms.forEach(e -> {
                List<FeesForm> feesForms = new ArrayList<>();
                FeesForm transactionFeesForm = new FeesForm();
                FeesForm taxFeesForm = new FeesForm();

                //amount total that user need to deducted from their bank
                e.setTotalAmount(e.getAmount().setScale(2,RoundingMode.HALF_UP));
                if(costRecovery.getActive()) {
                    transactionFeesForm.setFeeTypeCode("F09/F01");
                    transactionFeesForm.setFeeType(getTransactionType("F09/F01").getName());
                    transactionFeesForm.setSetupFees(costRecovery.getCharge());
                    transactionFeesForm.setSetupUom(costRecovery.getUom().getName());
                    transactionFeesForm.setSetupUomCode(costRecovery.getUom().getCode());
                    transactionFeesForm.setChargeUom("RM");
                    transactionFeesForm.setChargeUomCode("J01");

                    if(costRecovery.getUom().getCode().equals("J01")) { //cash
                        transactionFeesForm.setChargeFees(costRecovery.getCharge().setScale(2,RoundingMode.HALF_UP));
                    }
                    else if(costRecovery.getUom().getCode().equals("J04")) {
                        transactionFeesForm.setChargeFees(e.getAmount().multiply(costRecovery.getCharge().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP));
//                        if(transactionFeesForm.getChargeFees().compareTo(new BigDecimal("0.10")) < 0) {
//                            transactionFeesForm.setChargeFees(BigDecimal.valueOf(0.10));
//                        } else if (transactionFeesForm.getChargeFees().compareTo(new BigDecimal("1.00")) > 0) {
//                            transactionFeesForm.setChargeFees(BigDecimal.valueOf(1.00));
//                        }
                        if(costRecovery.getMinRM() != null && transactionFeesForm.getChargeFees().compareTo(costRecovery.getMinRM()) < 0) {
                            transactionFeesForm.setChargeFees(costRecovery.getMinRM());
                        } else if(costRecovery.getMaxRM() != null && transactionFeesForm.getChargeFees().compareTo(costRecovery.getMaxRM()) > 0) {
                            transactionFeesForm.setChargeFees(costRecovery.getMaxRM());
                        }
                    }

                    BigDecimal totalChargeFee = transactionFeesForm.getChargeFees();
//                    transactionFeesForm.setChargeFees(e.getAmount().subtract(totalChargeFee));
                    //Add sst calculation
                    BigDecimal taxFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                    if(costRecovery.getTypeTax() != null && !costRecovery.getTypeTax().getCode().equals("TAX00")) {
                        taxFee = transactionFeesForm.getChargeFees().multiply(costRecovery.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
//                    transactionFeesForm.setChargeFees(taxFee);

                    feesForms.add(transactionFeesForm);




                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(costRecovery.getValueTax());
                    taxFeesForm.setSetupUom(costRecovery.getUomTax().getName());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");
                    taxFeesForm.setChargeFees(taxFee);
                    feesForms.add(taxFeesForm);

                    //Amount that will deposited into cash wallet user, the fee part will go to bursa
//                    e.setAmount(e.getAmount().subtract(totalChargeFee).subtract(taxFee));
                    e.setTotalAmount(e.getTotalAmount().subtract(totalChargeFee).subtract(taxFee));
                }
                e.setFeesForms(feesForms);
            });
            return ResponseEntity.ok(forms);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("initWithdraw")
    public ResponseEntity initWithdraw(String acctNumber, BigDecimal amount, String transactionTypeCode, String walletTypeCode) {

        ResponseEntity tierChecking = tierThresholdChecking(transactionTypeCode, acctNumber, amount);
        if(!tierChecking.getStatusCode().is2xxSuccessful()) {
            return tierChecking;
        }

        List<WalletForm> forms = new ArrayList<>();
        WalletForm form = new WalletForm();
        form.setAcctNumber(acctNumber);
        form.setAmount(amount.setScale(2,RoundingMode.HALF_UP));
        form.setTransactionTypeCode(transactionTypeCode);
        form.setWalletTypeCode(walletTypeCode);
        forms.add(form);

        return initWithdraw(forms);
    }
    public ResponseEntity initWithdraw(@RequestBody List<WalletForm> forms) {
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try {
            CostRevoceryManagement costRecovery = this.dataManager.load(CostRevoceryManagement.class)
                    .query("select e from CostRevoceryManagement e " +
                            "where e.type.code = :code " +
                            "and e.active = :active")
                    .parameter("code", "B02")
                    .parameter("active", true)
                    .one();

            forms.forEach(e -> {


                List<FeesForm> feesForms = new ArrayList<>();
                FeesForm transactionFeesForm = new FeesForm();
                FeesForm taxFeesForm = new FeesForm();

                //amount total that user need to deducted from their cash wallet
                e.setTotalAmount(e.getAmount().setScale(2,RoundingMode.HALF_UP));
                if(costRecovery.getActive()) {
                    transactionFeesForm.setFeeTypeCode("F09/F02");
                    transactionFeesForm.setFeeType(getTransactionType("F09/F02").getName());
                    transactionFeesForm.setSetupFees(costRecovery.getCharge());
                    transactionFeesForm.setSetupUom(costRecovery.getUom().getName());
                    transactionFeesForm.setSetupUomCode(costRecovery.getUom().getCode());
                    transactionFeesForm.setChargeUom("RM");
                    transactionFeesForm.setChargeUomCode("J01");

                    if(costRecovery.getUom().getCode().equals("J01")) { //cash
                        transactionFeesForm.setChargeFees(costRecovery.getCharge());
                    }
                    else if(costRecovery.getUom().getCode().equals("J04")) {
                        transactionFeesForm.setChargeFees(e.getAmount().multiply(costRecovery.getCharge().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP));
                        if(costRecovery.getMinRM() != null && transactionFeesForm.getChargeFees().compareTo(costRecovery.getMinRM()) < 0) {
                            transactionFeesForm.setChargeFees(costRecovery.getMinRM());
                        } else if(costRecovery.getMaxRM() != null && transactionFeesForm.getChargeFees().compareTo(costRecovery.getMaxRM()) > 0) {
                            transactionFeesForm.setChargeFees(costRecovery.getMaxRM());
                        }
                    }

                    BigDecimal chargeFee = transactionFeesForm.getChargeFees().setScale(2,RoundingMode.HALF_UP);
//                    BigDecimal taxFee = transactionFeesForm.getChargeFees().multiply(costRecovery.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
//                    transactionFeesForm.setChargeFees(taxFee);

                    //Add sst calculation
                    BigDecimal taxFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                    if(costRecovery.getTypeTax() != null && !costRecovery.getTypeTax().getCode().equals("TAX00")) {
                        taxFee = transactionFeesForm.getChargeFees().multiply(costRecovery.getValueTax().divide(BigDecimal.valueOf(100),6,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                    }
                    feesForms.add(transactionFeesForm);
//                    e.setTotalAmount(e.getTotalAmount().add(transactionFeesForm.getChargeFees()));
                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(costRecovery.getValueTax());
                    taxFeesForm.setSetupUom(costRecovery.getUomTax().getName());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");
                    taxFeesForm.setChargeFees(taxFee);
                    feesForms.add(taxFeesForm);

                    // Amount that will be paid into the bank ... without fee
                    e.setTotalAmount(e.getTotalAmount().subtract(chargeFee).subtract(taxFee));
//                    e.setAmount(e.getAmount().setScale(2,RoundingMode.HALF_UP).subtract(chargeFee).subtract(taxFee));


                    //Comment out because will do checking after init
//                    AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
//                            .query( "select a from AccountWalletView a " +
//                                    "where a.acctNumber = :acctNumber1")
//                            .parameter("acctNumber1", e.getAcctNumber())
//                            .one();

//                    if(e.getTotalAmount().compareTo(walletView.getAvailableAmount()) > 0) {
////                        e.setTotalAmount(e.getAmount());
////
////                        feesForms.forEach(f -> {
////                            e.setAmount(e.getAmount().subtract(f.getChargeFees()));
////                        });
//                        logger.error("Amount insufficient due to amount:"+ e.getTotalAmount() + " > wallet_balance:"+walletView.getAvailableAmount());
//                        throw new RuntimeException("RCE002");
//                    }
                }
                e.setFeesForms(feesForms);
            });
            return ResponseEntity.ok(forms);
        } catch (Exception ex) {
            responseDTO = errorResponseHandler(responseDTO,ex.getMessage(),"F02" );
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("initPurchase")
    public ResponseEntity initPurchase(BigDecimal totalPrice, BigDecimal quantity, String inputType, String reference) {
        List<WalletPurchaseForm> forms = new ArrayList<>();
        WalletPurchaseForm form = new WalletPurchaseForm();
        form.setTotalPrice(totalPrice!=null? totalPrice.setScale(2,RoundingMode.HALF_UP):BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP));
        form.setQuantity(quantity!= null? quantity.setScale(6,RoundingMode.HALF_UP):BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP));
//        form.setType(type);
        form.setInputType(inputType);
        form.setReferenceId(reference);
//        form.setPriceRequestId(priceRequestId);
//        form.setPrice(price);
        forms.add(form);

        return initPurchase(forms);
    }
    public ResponseEntity initPurchase(@RequestBody List<WalletPurchaseForm> forms) {
        try {
            forms.forEach(e -> {

                Boolean feeActive = true;
                String taxType = "SST";

//                BigDecimal tradingFee = BigDecimal.valueOf(2);
                BigDecimal fee = BigDecimal.ZERO;

                PriceQuery priceQuery = dataManager.load(PriceQuery.class)
                        .query("select p from PriceQuery p " +
                                "where p.reference = :reference")
                        .parameter("reference", e.getReferenceId() )
                        .one();

                BigDecimal price = null;
                if(priceQuery.getBursaPrice()==null || priceQuery.getTransactionCode()==null){
                    throw new RuntimeException("Price not found in priceQuery table");
                }
                else{
                    price = priceQuery.getBursaPrice().setScale(2,RoundingMode.HALF_UP);
                    e.setPrice(price);
                    //need to remap because trading service using differet code which is S05 and S06
                    if(priceQuery.getTransactionCode().equals("F05")){
                        e.setType("S05");
                    }
                    else if(priceQuery.getTransactionCode().equals("F06")){
                        e.setType("S06");
                    }
                    else{
                        throw new RuntimeException("Invalid transactionCode");
                    }

                }
//                    BigDecimal price = e.getPrice().setScale(2, RoundingMode.HALF_UP);



//                BigDecimal totalPrice = BigDecimal.ZERO;
//                BigDecimal quantity = BigDecimal.ZERO;
                BigDecimal netPrice = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);
                BigDecimal totalPrice = e.getTotalPrice().setScale(2, RoundingMode.HALF_UP);
                BigDecimal quantity = e.getQuantity().setScale(6, RoundingMode.HALF_UP);
//                BigDecimal netPrice = e.getNetPrice().setScale(2, RoundingMode.HALF_EVEN);
                BigDecimal taxFee = BigDecimal.ZERO.setScale(2,RoundingMode.HALF_UP);

                List<FeesForm> feesForms = new ArrayList<>();
                FeesForm transactionFeesForm = new FeesForm();
                FeesForm taxFeesForm = new FeesForm();

                if(e.getType().equals("S05")) { // buy
                    RevenueManagement revenueManagement = this.dataManager.load(RevenueManagement.class)
                            .query(   "select r from RevenueManagement r " +
                                    "where r.type.code = :typeCode1 " +
                                    "and r.active = :active")
                            .parameter("typeCode1", "C01") //Transaction fee (buy/sell)
                            .parameter("active", true)
                            .one();

                    RtType typeUOM = this.dataManager.load(RtType.class)
                            .query( "select r.uom from RevenueManagement r " +
                                    "where r.type.code = :typeCode1")
                            .parameter("typeCode1", "C01")
                            .one();
                    if(e.getInputType().equals("S21")) { // buy by cash

                        if(revenueManagement.getActive()) {
                            if(typeUOM.getCode().equals("J04")) {
                                fee = e.getTotalPrice().multiply(revenueManagement.getCharge().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                                if(revenueManagement.getMinRM() != null && fee.compareTo(revenueManagement.getMinRM()) < 0) {
                                    fee = revenueManagement.getMinRM();
                                } else if(revenueManagement.getMaxRM() != null && fee.compareTo(revenueManagement.getMaxRM()) > 0) {
                                    fee = revenueManagement.getMaxRM();
                                }
//                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                            } else {
                                fee = revenueManagement.getCharge().setScale(2,RoundingMode.HALF_UP);
//                                taxFee = revenueManagement.getValueTax().setScale(2,RoundingMode.HALF_UP);
                            }

                            //calculated tax
                            if(revenueManagement.getTypeTax() != null && !revenueManagement.getTypeTax().getCode().equals("TAX00")) {
                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
                            }
                        }

                        totalPrice = e.getTotalPrice().setScale(2, RoundingMode.HALF_UP);
                        quantity = (totalPrice.subtract(fee).subtract(taxFee)).setScale(2,RoundingMode.HALF_UP).divide(price,6, RoundingMode.HALF_UP);
                        netPrice = totalPrice.subtract(fee).subtract(taxFee).setScale(2,RoundingMode.HALF_UP);
                    }
                    else { //buy by gram
                        if(revenueManagement.getActive()) {
                            if(typeUOM.getCode().equals("J04")) {
                                fee = e.getQuantity().multiply(e.getPrice()).multiply(revenueManagement.getCharge().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                                if(revenueManagement.getMinRM() != null && fee.compareTo(revenueManagement.getMinRM()) < 0) {
                                    fee = revenueManagement.getMinRM();
                                } else if(revenueManagement.getMaxRM() != null && fee.compareTo(revenueManagement.getMaxRM()) > 0) {
                                    fee = revenueManagement.getMaxRM();
                                }
//                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                            } else {
                                fee = revenueManagement.getCharge().setScale(2,RoundingMode.HALF_UP);
//                                taxFee = revenueManagement.getValueTax().setScale(2,RoundingMode.HALF_UP);
                            }

                            //calculated tax
                            if(revenueManagement.getTypeTax() != null && !revenueManagement.getTypeTax().getCode().equals("TAX00")) {
                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
                            }
                        }

                        quantity = e.getQuantity().setScale(6,RoundingMode.HALF_UP);
//                        totalPrice = (quantity.multiply(price)).add(fee);
//                        netPrice = totalPrice.subtract(fee);

                        totalPrice = (quantity.multiply(price)).setScale(2, RoundingMode.HALF_UP).add(fee).add(taxFee).setScale(2,RoundingMode.HALF_UP);
                        netPrice = totalPrice.subtract(fee).subtract(taxFee).setScale(2,RoundingMode.HALF_UP);
                    }

                    transactionFeesForm.setFeeTypeCode("F09/F05");
                    transactionFeesForm.setFeeType(getTransactionType("F09/F05").getName());
                    transactionFeesForm.setSetupFees(revenueManagement.getCharge());
                    transactionFeesForm.setSetupUom(revenueManagement.getUom().getName());
                    transactionFeesForm.setChargeUomCode("J01");
                    transactionFeesForm.setChargeUom("RM");
                    transactionFeesForm.setChargeFees(fee);
                    feesForms.add(transactionFeesForm);

                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(revenueManagement.getValueTax());
                    taxFeesForm.setSetupUom(revenueManagement.getUomTax().getName());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");
                    taxFeesForm.setChargeFees(taxFee);
                    feesForms.add(taxFeesForm);

                    e.setFeesForms(feesForms);

                }
                else if(e.getType().equals("S06")) { //sell
                    RevenueManagement revenueManagement = this.dataManager.load(RevenueManagement.class)
                            .query(   "select r from RevenueManagement r " +
                                    "where r.type.code = :typeCode1")
                            .parameter("typeCode1", "C02") //Transaction fee (buy/sell)
                            .one();

                    RtType typeUOM = this.dataManager.load(RtType.class)
                            .query( "select r.uom from RevenueManagement r " +
                                    "where r.type.code = :typeCode1")
                            .parameter("typeCode1", "C02")
                            .one();
                    if(e.getInputType().equals("S21")) { //sell by cash
                        if(revenueManagement.getActive()) {
                            if(typeUOM.getCode().equals("J04")) {
                                fee= e.getTotalPrice().multiply(revenueManagement.getCharge().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
//                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                            } else {
                                fee = revenueManagement.getCharge().setScale(2,RoundingMode.HALF_UP);
//                                taxFee = revenueManagement.getValueTax().setScale(2,RoundingMode.HALF_UP);
                            }

                            //calculated tax
                            if(revenueManagement.getTypeTax() != null && !revenueManagement.getTypeTax().getCode().equals("TAX00")) {
                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
                            }
                        }


                        totalPrice =e.getTotalPrice().setScale(2,RoundingMode.HALF_UP);
                        quantity = (totalPrice.add(fee).add(taxFee)).setScale(2,RoundingMode.HALF_UP).divide(price,6,RoundingMode.HALF_UP);

                        netPrice = totalPrice.add(fee).add(taxFee).setScale(2,RoundingMode.HALF_UP);
                    } else { //sell by gram
                        if(revenueManagement.getActive()) {
                            if(typeUOM.getCode().equals("J04")) {
                                fee = e.getQuantity().multiply(e.getPrice()).multiply(revenueManagement.getCharge().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
//                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);
                            } else {
                                fee = revenueManagement.getCharge().setScale(2,RoundingMode.HALF_UP);
//                                taxFee = revenueManagement.getValueTax().setScale(2,RoundingMode.HALF_UP);
                            }

                            //calculated tax
                            if(revenueManagement.getTypeTax() != null && !revenueManagement.getTypeTax().getCode().equals("TAX00")) {
                                taxFee = fee.multiply(revenueManagement.getValueTax().divide(BigDecimal.valueOf(100), 6, RoundingMode.HALF_UP)).setScale(2, RoundingMode.HALF_UP);
                            }
                        }


                        quantity =  e.getQuantity().setScale(6,RoundingMode.HALF_UP);
                        totalPrice = quantity.multiply(price).setScale(2, RoundingMode.HALF_UP).subtract(fee).subtract(taxFee).setScale(2,RoundingMode.HALF_UP);
                        netPrice = totalPrice.add(fee).add(taxFee).setScale(2,RoundingMode.HALF_UP);
                    }

                    transactionFeesForm.setFeeTypeCode("F09/F06");
                    transactionFeesForm.setFeeType(getTransactionType("F09/F06").getName());
                    transactionFeesForm.setSetupFees(revenueManagement.getCharge());
                    transactionFeesForm.setSetupUom(typeUOM.getName());
                    transactionFeesForm.setChargeUomCode("J01");
                    transactionFeesForm.setChargeUom("RM");
                    transactionFeesForm.setChargeFees(fee);
                    feesForms.add(transactionFeesForm);

                    taxFeesForm.setFeeTypeCode("TAX01");
                    taxFeesForm.setFeeType("SST");
                    taxFeesForm.setSetupFees(revenueManagement.getValueTax());
                    taxFeesForm.setSetupUom(typeUOM.getName());
                    taxFeesForm.setChargeUomCode("J01");
                    taxFeesForm.setChargeUom("RM");
                    taxFeesForm.setChargeFees(taxFee);
                    feesForms.add(taxFeesForm);

                    e.setFeesForms(feesForms);
                }
                e.setQuantity(quantity);
                e.setTotalPrice(totalPrice);
                e.setNetPrice(netPrice);
            });

            return ResponseEntity.ok(forms);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @GetMapping("walletProfitLoss")
    public ResponseEntity walletProfitLoss(String accountNo) {
        try {

            AccountWalletView walletView = this.dataManager.load(AccountWalletView.class)
                    .query( "select a from AccountWalletView a " +
                            "where a.acctNumber = :acctNumber")
                    .parameter("acctNumber", accountNo)
                    .one();

            List<TransactionHistoriesView> transactionList = this.dataManager.load(TransactionHistoriesView.class)
                    .query( "select t from TransactionHistoriesView t " +
                            "where t.accOwner = :accOwner1")
                    .parameter("accOwner1", walletView.getAccountOwner())
                    .list();



            BigDecimal goldOwn = walletView.getAvailableAmount();
//            BigDecimal totalGoldBuy = this.dataManager.load(TransactionHistoriesView.class)
//                    .query("select sum(a.amount) from TransactionHistoriesView a " +
//                            "where a.accOwner = :accOwner and " +
//                            "a.walletCode = :walletCode and " +
//                            "a.transactionCode = :transactionCode " +
//                            "and a.statusCode = :statusCode ")
//                    .parameter("accOwner", walletView.getAccountOwner())
//                    .parameter("walletCode", "E02")
//                    .parameter("transactionCode", "F05")
//                    .parameter("statusCode", "G01")
//                    .one().getAmount();
//            Query queryTotalGoldBuy = dataManager.createNativeQuery(sqlQueryTotalGoldBuy)

            return ResponseEntity.ok("OK");
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }


    @GetMapping("tierThresholdChecking")
    public ResponseEntity tierThresholdChecking(String code, String account, BigDecimal newAmount) {
        ResponseDTO responseDTO = dataManager.create(ResponseDTO.class);
        try {
            BigDecimal totalAmount;
            Boolean proceed = null;
            UserDetails userDetails = currentAuthentication.getUser();
            //ini punca hardcode tier takkan upgrade
            String tierCode = "T01";

            TierThreshold threshold = this.dataManager.load(TierThreshold.class)
                    .query( "select t from TierThreshold t " +
                            "where t.tierManagement.code = :tierManagementCode1 " +
                            "and t.type.code = :typeCode1")
                    .parameter("tierManagementCode1", tierCode)
                    .parameter("typeCode1", code).one();

            //check collective amount
            if(threshold.getFrequent().getCode().equals("M01")) { //daily
                //need to change to TransactionHistoryDays
                List<TransactionHistoryMonth> transaction = this.dataManager.load(TransactionHistoryMonth.class)
                        .query( "select t from TransactionHistoryMonth t " +
                                "where t.acctNumber = :acctNumber1 " +
                                "and t.transcode = :transcode1")
                        .parameter("acctNumber1", account)
                        .parameter("transcode1", code).list();

                totalAmount = newAmount.add(transaction.size() > 0 ? transaction.get(0).getSumamount() : BigDecimal.ZERO);

	            proceed = totalAmount.compareTo(threshold.getMaxValue()) <= 0;
            }else if(threshold.getFrequent().getCode().equals("M02")) { //weekly
                //need to change to TransactionHistoryWeek
                List<TransactionHistoryMonth> transaction = this.dataManager.load(TransactionHistoryMonth.class)
                        .query( "select t from TransactionHistoryMonth t " +
                                "where t.acctNumber = :acctNumber1 " +
                                "and t.transcode = :transcode1")
                        .parameter("acctNumber1", account)
                        .parameter("transcode1", code).list();

                totalAmount = newAmount.add(transaction.size() > 0 ? transaction.get(0).getSumamount() : BigDecimal.ZERO);

	            proceed = totalAmount.compareTo(threshold.getMaxValue()) <= 0;
            }else if(threshold.getFrequent().getCode().equals("M03")) { //monthly
//                int month = LocalDate.now().getMonthValue();
//                int year = LocalDate.now().getYear();
//                BigDecimal getAmount = this.dataManager.load(TransactionAcct.class)
//                        .query( "select sum(t.amount) from TransactionAcct t " +
//                                "where t.account.acctNumber = :accountAcctNumber1 " +
//                                "and t.transType.code = :transTypeCode1 " +
//                                "and EXTRACT(MONTH FROM t.createdDate) = :month " +
//                                "and EXTRACT(YEAR FROM t.createdDate) = :year")
//                        .parameter("accountAcctNumber1", account)
//                        .parameter("transTypeCode1", code)
//                        .parameter("month", month)
//                        .parameter("year", year)
//                        .list()
//                        .get(0) != null ? (BigDecimal) list().get(0) : BigDecimal.ZERO;

                List<TransactionHistoryMonth> transaction = this.dataManager.load(TransactionHistoryMonth.class)
                        .query( "select t from TransactionHistoryMonth t " +
                                "where t.acctNumber = :acctNumber1 " +
                                "and t.transcode = :transcode1")
                        .parameter("acctNumber1", account)
                        .parameter("transcode1", code).list();

                totalAmount = newAmount.add(transaction.size() > 0 ? transaction.get(0).getSumamount() : BigDecimal.ZERO);

	            proceed = totalAmount.compareTo(threshold.getMaxValue()) <= 0;
            } else if(threshold.getFrequent().getCode().equals("M04")) { //yearly

                List<TransactionHistoryYear> transaction = this.dataManager.load(TransactionHistoryYear.class)
                        .query( "select t from TransactionHistoryYear t " +
                                "where t.acctNumber = :acctNumber1 " +
                                "and t.transcode = :transcode1")
                        .parameter("acctNumber1", account)
                        .parameter("transcode1", code).list();

                totalAmount = newAmount.add(transaction.size() > 0 ? transaction.get(0).getSumamount() : BigDecimal.ZERO);

	            proceed = totalAmount.compareTo(threshold.getMaxValue()) <= 0;
            }

            if(Boolean.TRUE.equals(proceed)) {
                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(null);
                responseDTO.setStatus("G01");
                responseDTO.setResponseCode("RCS001");
                responseDTO.setDescription("OK");
                responseDTO.setTransactionType(code);
                responseDTO.setSuccess(true);
                return ResponseEntity.ok(responseDTO);

            } else {
                responseDTO.setDate(timeSource.now().toLocalDateTime());
                responseDTO.setReference(null);
                responseDTO.setStatus("G02");
                responseDTO.setResponseCode("RCE001");
                responseDTO.setDescription("Amount Exceed Limit Monthly");
                responseDTO.setTransactionType(code);
                responseDTO.setSuccess(false);
                return ResponseEntity.badRequest().body(responseDTO);
            }

        } catch (Exception ex) {
	        responseDTO.setDate(timeSource.now().toLocalDateTime());
            responseDTO.setReference(null);
            responseDTO.setStatus("G02");
            responseDTO.setResponseCode("RCE999");
            responseDTO.setDescription(ex.getMessage());
            responseDTO.setTransactionType(code);
            responseDTO.setSuccess(false);
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("getWalletAccount")
    public ResponseEntity getWalletAccount(String code, String email, String phone) {
        try {
            UserProfile userProfile = this.dataManager.load(UserProfile.class)
                    .query( "select u from UserProfile u " +
                            "where u.email = :email1 " +
                            "or u.phoneNumber = :phoneNumber1")
                    .parameter("email1", email)
                    .parameter("phoneNumber1", phone).one();

            AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
                    .query( "select a from AccountBgd a " +
                            "where a.walletType.code = :walletTypeCode1 " +
                            "and a.accOwner = :accOwner1")
                    .parameter("walletTypeCode1", code)
                    .parameter("accOwner1", userProfile.getUsername())
                    .one();

            return ResponseEntity.ok("{\"accountNumber\":" +accountBgd.getAcctNumber()+ "}");

        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("{\"message\":\"Account Not Found\"}");
        }
    }

    @GetMapping("getRedeemSN")
    public ResponseEntity getRedeemSN(String serialNumber) {
        try{
            PurchaseRecordProduct purchaseRecordProduct = this.dataManager.load(PurchaseRecordProduct.class)
                    .query("select p from PurchaseRecordProduct p " +
                            "where p.serial_number = :serial_number1")
                    .parameter("serial_number1", serialNumber)
                    .one();

            purchaseRecordProduct.setIsRedeem(true);
            this.dataManager.save(purchaseRecordProduct);


            return ResponseEntity.ok("{\"Ok\"}");
        }catch (Exception ex){
            return ResponseEntity.badRequest().body("{\"message\":\"Redeem Serial Number Not Found :\"}"+ex.getMessage());
        }
    }

    private TransactionType TX_TYPE_ADMINFEE;
    private TransStatus TX_STATUS_COMPLETED;


    @Authenticated
    @ManagedOperation
    @EventListener
    public void onApplicationStarted(ApplicationStartedEvent event) {


        TX_TYPE_ADMINFEE = getTransactionType("F09/01");

        TX_STATUS_COMPLETED = getTransactionStatus("G01");

    }
    public TransactionType getTransactionType(String code) {
        return this.dataManager.load(TransactionType.class)
                .query("select e from TransactionType e where e.code = :code")
                .parameter("code", code).one();
    }

    public TransStatus getTransactionStatus(String code) {
        return this.dataManager.load(TransStatus.class)
                .query("select e from TransStatus e where e.code = :code")
                .parameter("code", code).one();
    }

    public AccountBgd getWallet(String username,String typeCode){

        return dataManager.load(AccountBgd.class)
                .query("select a from AccountBgd a " +
                        "where a.accOwner = :accOwner " +
                        "and a.walletType.code = :walletTypeCode")
                .parameter("accOwner", username)
                .parameter("walletTypeCode", typeCode)
                .one();
    }

    public BigDecimal getAdminPercentFee(){
        return  dataManager.loadValue(
                        "select COALESCE(e.charge, 0) from RevenueManagement e " +
                        "where e.type.code = :code", BigDecimal.class)
                .parameter("code", "C10")
                .one();
    }

    public BigDecimal getGoldBuyPrice(){
        return dataManager.loadValue(
                        "select COALESCE(p.buyPrice, 0) from ProductPrice p " +
                                "where p.type.code = :typeCode",BigDecimal.class)
                .parameter("typeCode", "A02")
                .one();
    }


    public void chargeAdministrationFee(){
        log.info("Charging administrative fee to all applicable users....");

        List<AdminFeeUser> adminFeeUsers= dataManager.load(AdminFeeUser.class)
                .query("select a from AdminFeeUser a")
                .list();

        for (AdminFeeUser a: adminFeeUsers){

            if(a.getGoldAmount().compareTo(BigDecimal.valueOf(0))>0) {

                TransactionAcct newTx = dataManager.create(TransactionAcct.class);
                newTx.setTransType(TX_TYPE_ADMINFEE);
                newTx.setTransStatus(TX_STATUS_COMPLETED);

                BigDecimal totalChargeableGold = getAdminPercentFee().multiply(a.getGoldAmount().setScale(6,RoundingMode.HALF_UP)).setScale(6,RoundingMode.HALF_UP);

                BigDecimal totalAdminFeeCash = totalChargeableGold.multiply(getGoldBuyPrice().setScale(2,RoundingMode.HALF_UP)).setScale(2,RoundingMode.HALF_UP);

                if(totalAdminFeeCash.compareTo(a.getCashAmount())<=0){
                    // enough cash , minus fee from cash balance
                    newTx.setAccount(getWallet(a.getAccOwner(), "E01"));
                    newTx.setAmount(totalAdminFeeCash);
                }
                else{
                    //Not enough cash, minus fee from gold balance instead
                    newTx.setAccount(getWallet(a.getAccOwner(), "E02"));
                    newTx.setAmount(totalChargeableGold);
                }

                String reference_id = UUID.randomUUID().toString();
                newTx.setReference(reference_id);

                dataManager.save(newTx);

            }
            else{
                // NO GOLD, HENCE NO FEE CHARGE
            }
        }
    }

    @GetMapping("getGenerateFinanceReport")
    public ResponseEntity getGenerateFinanceReport() {
        try {
            scheduleController.generateFinanceReport();
            return ResponseEntity.ok("Ok");

        } catch (Exception ex) {
            return ResponseEntity.badRequest().body("{\"message\":\"Account Not Found\"}");
        }
    }
}
