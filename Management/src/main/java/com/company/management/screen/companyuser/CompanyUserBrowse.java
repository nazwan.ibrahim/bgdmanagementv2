package com.company.management.screen.companyuser;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.CompanyUser;

@UiController("CompanyUser.browse")
@UiDescriptor("company-user-browse.xml")
@LookupComponent("companyUsersTable")
public class CompanyUserBrowse extends StandardLookup<CompanyUser> {
}