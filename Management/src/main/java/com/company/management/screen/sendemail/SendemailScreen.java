package com.company.management.screen.sendemail;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("SendemailScreen")
@UiDescriptor("sendEmail-screen.xml")
public class SendemailScreen extends Screen {
}