package com.company.management.screen.usertierregistration;

import com.company.management.entity.registrationdb.UserTier;
import io.jmix.ui.screen.*;

@UiController("UserTierRegistration.browse")
@UiDescriptor("user-tier-registration-browse.xml")
@LookupComponent("userTierRegistrationsTable")
public class UserTierRegistrationBrowse extends StandardLookup<UserTier> {
}