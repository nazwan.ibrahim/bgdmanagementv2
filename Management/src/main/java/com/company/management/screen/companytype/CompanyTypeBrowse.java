package com.company.management.screen.companytype;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.CompanyType;

@UiController("CompanyType.browse")
@UiDescriptor("company-type-browse.xml")
@LookupComponent("companyTypesTable")
public class CompanyTypeBrowse extends StandardLookup<CompanyType> {
}