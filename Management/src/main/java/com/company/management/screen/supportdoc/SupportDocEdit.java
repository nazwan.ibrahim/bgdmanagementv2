package com.company.management.screen.supportdoc;

import io.jmix.ui.screen.*;
import com.company.management.entity.SupportDoc;

@UiController("SupportDoc.edit")
@UiDescriptor("support-doc-edit.xml")
@EditedEntityContainer("supportDocDc")
public class SupportDocEdit extends StandardEditor<SupportDoc> {
}