package com.company.management.screen.rttypegroup;

import io.jmix.ui.screen.*;
import com.company.management.entity.RtTypeGroup;

@UiController("RtTypeGroup.edit")
@UiDescriptor("rt-type-group-edit.xml")
@EditedEntityContainer("rtTypeGroupDc")
public class RtTypeGroupEdit extends StandardEditor<RtTypeGroup> {
}