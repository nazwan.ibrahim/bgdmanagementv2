package com.company.management.screen.revenuemanagement;

import com.company.management.screen.usertier.InvestorReportEdit;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.LayoutClickNotifier;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@UiController("RevenueManagementV2.browse")
@UiDescriptor("revenue-managementV2-browse.xml")
@LookupComponent("revenueActiveTable")
public class RevenueManagementV2Browse extends StandardLookup<RevenueManagement> {

    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private Table<RevenueManagement> revenueActiveTable;
    @Autowired
    private Table<RevenueManagement> revenuePendingTable;
    @Autowired
    private CollectionLoader<RevenueManagement> revenueActiveDl;
    @Autowired
    private CollectionLoader<RevenueManagement> revenueLogsDl;
    @Autowired
    private CollectionLoader<RevenueManagement> revenuePendingDl;

    @Subscribe
    public void onBeforeShow(final BeforeShowEvent event) {
        revenueActiveDl.setParameter("activeCode", "G04");
        revenueActiveDl.setParameter("marginBuyCode", "C05");
        revenueActiveDl.setParameter("marginSellCode", "C06");
        revenueActiveDl.load();

        revenuePendingDl.setParameter("pendingCode", "G05");
        revenuePendingDl.setParameter("approvedCode", "G06");
        revenuePendingDl.setParameter("marginBuyCode2", "C05");
        revenuePendingDl.setParameter("marginSellCode2", "C06");
        revenuePendingDl.load();

        revenueLogsDl.setParameter("marginBuyCode3", "C05");
        revenueLogsDl.setParameter("marginSellCode3", "C06");
        revenueLogsDl.load();
    }
    @Subscribe("revenueActiveTable.edit")
    public void onRevenueActiveTableEdit(final Action.ActionPerformedEvent event) {
        RevenueManagementCreateNewEdit revenueManagementCreateNewEdit = screenBuilders.screen(this)
                .withScreenClass(RevenueManagementCreateNewEdit.class)
                .withAfterCloseListener(afterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        revenueManagementCreateNewEdit.setEntityToEdit(Objects.requireNonNull(revenueActiveTable.getSingleSelected()));
        revenueManagementCreateNewEdit.show();
    }

    @Subscribe("revenuePendingTable.view")
    public void onRevenuePendingTableView(final Action.ActionPerformedEvent event) {
        RevenueManagementApprovalPageEdit revenueManagementApprovalPageEdit = screenBuilders.screen(this)
                .withScreenClass(RevenueManagementApprovalPageEdit.class)
                .withAfterCloseListener(afterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        revenueManagementApprovalPageEdit.setEntityToEdit(Objects.requireNonNull(revenuePendingTable.getSingleSelected()));
        revenueManagementApprovalPageEdit.show();
    }

    @Install(to = "revenueActiveTable", subject = "styleProvider")
    private String revenueActiveTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "revenuePendingTable", subject = "styleProvider")
    private String revenuePendingTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "revenueLogsTable", subject = "styleProvider")
    private String revenueLogsTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

}