package com.company.management.screen.notificationtype;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationType;

@UiController("NotificationType.edit")
@UiDescriptor("notification-type-edit.xml")
@EditedEntityContainer("notificationTypeDc")
public class NotificationTypeEdit extends StandardEditor<NotificationType> {
}