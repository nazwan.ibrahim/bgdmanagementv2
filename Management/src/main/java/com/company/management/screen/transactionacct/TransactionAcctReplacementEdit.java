package com.company.management.screen.transactionacct;

import com.company.management.app.ActivityLogBean;
import com.company.management.entity.*;
import com.company.management.entity.wallet.RedeemInfo;
import com.company.management.entity.wallet.RedeemTracker;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.upload.TemporaryStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Text;

import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@UiController("TransactionAcctReplacement.edit")
@UiDescriptor("transaction-acct-replacement-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class TransactionAcctReplacementEdit extends StandardEditor<TransactionAcct> {
    @Autowired
    private CollectionContainer<RedeemInfo> redeemInfoesDc;
    @Autowired
    private CollectionLoader<RedeemInfo> redeemInfoesDl;
    @Autowired
    private InstanceContainer<TransactionAcct> transactionAcctDc;
    @Autowired
    private TextField<String> quantity;
    @Autowired
    private TextField<String> name;
    @Autowired
    private TextField<String> createdDate;
    @Autowired
    private TextField<String> reason;
    @Autowired
    private TextField<String> phoneNo;
    @Autowired
    private TextArea<String> address;
    @Autowired
    private TextField<String> reference;
    @Autowired
    private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
    @Autowired
    private CollectionContainer<TransactionHistoriesView> transactionHistoriesViewsDc;
    @Autowired
    private TextField<String> courierFee;
    @Autowired
    private TextField<String> mintingFee;
    @Autowired
    private TextField<String> takafulFee;
    @Autowired
    private CollectionLoader<RedeemTracker> redeemTrackersDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesDl;
    @Autowired
    private UiComponents uiComponents;
    @Named("uploadFileTable.remove")
    private RemoveAction<ActivityLogFile> uploadFileTableRemove;
    @Autowired
    private Downloader downloader;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private TemporaryStorage temporaryStorage;
    @Autowired
    private Notifications notifications;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private InstanceContainer<ActivityLog> activityLogDc;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc;
    @Autowired
    private InstanceLoader<ActivityLog> activityLogDl;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private EntityComboBox<RtType> activityLogDesc;
    @Autowired
    private TextArea<String> remarksFieldId;
    @Autowired
    private ActivityLogBean activityLogBean;
    @Autowired
    private Form validateForm;
    @Autowired
    private ScreenValidation screenValidation;
    @Autowired
    private GroupTable<RedeemInfo> serialNoTable;
    static Logger logger = LogManager.getLogger(TransactionAcctReplacementEdit.class);

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        redeemInfoesDl.setParameter("transaction1", transactionAcctDc.getItem());
        redeemInfoesDl.load();
        quantity.setValue(String.valueOf(redeemInfoesDc.getItems().size()));
        name.setValue(redeemInfoesDc.getItems().get(0).getName());
        createdDate.setValue(redeemInfoesDc.getItems().get(0).getCreatedDate().toString());
        phoneNo.setValue(redeemInfoesDc.getItems().get(0).getPhone());
        reason.setValue(redeemInfoesDc.getItems().get(0).getReason());
        address.setValue(redeemInfoesDc.getItems().get(0).getAddress());
        reference.setValue(redeemInfoesDc.getItems().get(0).getReference());

        transactionHistoriesViewsDl.setParameter("reference1", transactionAcctDc.getItem().getReference());
        transactionHistoriesViewsDl.setParameter("accOwner1", "BgdAdmin");
        transactionHistoriesViewsDl.load();
        transactionHistoriesViewsDc.getItems().forEach(transactionHistoriesView -> {
            if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B05")){
                mintingFee.setValue(transactionHistoriesView.getAmount().abs().toString());
            }
            if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B03")){
                courierFee.setValue(transactionHistoriesView.getAmount().abs().toString());
            }
            if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B04")){
                takafulFee.setValue(transactionHistoriesView.getAmount().abs().toString());
            }
        });

        redeemTrackersDl.setParameter("transactionId1", transactionAcctDc.getItem());
        redeemTrackersDl.load();

        rtTypesDl.setParameter("code1", "DRR01");
        rtTypesDl.setParameter("code2", "DRR02");
        rtTypesDl.load();

        try{
            activityLogDl.setParameter("refEntity1", "TransactionAcct");
            activityLogDl.setParameter("refData1", transactionAcctDc.getItem().getId().toString());
            activityLogDl.load();

            activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
            activityLogFilesDl.load();
        }catch(Exception e){
            logger.error(e.getMessage());
        }
        uploadMultipleFile();
    }
    @Install(to = "serialNoTable.serialNumber", subject = "columnGenerator")
    private Component serialNoTableSerialNumberColumnGenerator(final RedeemInfo redeemInfo) {
        TextField<String> textField = uiComponents.create(TextField.TYPE_STRING);
        textField.setEditable(true);
        textField.setRequired(true);
        return textField;
    }

    @Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        Button btn = uiComponents.create(Button.class);
        btn.setCaption("Delete");
        btn.setId("deleteBtn");
        btn.addClickListener(clickEvent -> {
            uploadFileTableRemove.execute();
        });
        return btn;
    }
    @Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
    private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }
    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
//                activityLogFile.setActivityLog(activityLog);
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
//                activityLogFile.setAction(rtType("ACT02"));

                activityLogFilesDc.getMutableItems().add(activityLogFile);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }
    private RtType rtType(String code){
        String query = "select e from RtType e where e.code =:code";
        return this.dataManager.load(RtType.class)
                .query(query)
                .parameter("code", code)
                .one();
    }

    @Subscribe("saveBtn")
    public void onSaveBtnClick(final Button.ClickEvent event) {
        ValidationErrors errorsForm = screenValidation.validateUiComponents(validateForm);
        if (!errorsForm.isEmpty() || activityLogFilesDc.getItems().isEmpty()) {
//            screenValidation.showValidationErrors(this, errorsForm);
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.BOTTOM_RIGHT)
                    .withCaption("Please fill in all the Mandatory fields.")
                    .show();
        }
//        else if(activityLogFilesDc.getItems().isEmpty()){
//            notifications.create()
//                    .withContentMode(ContentMode.HTML)
//                    .withType(Notifications.NotificationType.ERROR)
//                    .withPosition(Notifications.Position.MIDDLE_CENTER)
//                    .withCaption("Upload document is required")
//                    .show();
//        }
        else{
            dialogs.createOptionDialog()
                    .withCaption("Replacement Edit Confirmation")
                    .withContentMode(ContentMode.HTML)
                    .withMessage("Click Confirm or Cancel button to proceed.")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withPrimary(true)
                                    .withCaption("Confirm")
                                    .withHandler(e -> saveData()),
                            new DialogAction(DialogAction.Type.NO)
                                    .withCaption("Cancel")
                    )
                    .show();
        }
    }
    private void saveData(){
        //insert new row into transaction acc
        TransactionAcct transactionAcctEntityDc = this.dataManager.load(TransactionAcct.class).id(transactionAcctDc.getItem().getId()).one();
        TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
        String[] ignoreProperties = {"id", "reference"};
        transactionAcct.setReference(transactionAcctEntityDc.getReference() + "/01");
        BeanUtils.copyProperties(transactionAcctEntityDc, transactionAcct, ignoreProperties);
        this.dataManager.save(transactionAcct);
        //insert new row(s) into redeem info
        redeemInfoesDc.getMutableItems().forEach(redeemInfo -> {
            RedeemInfo redeemInfoEntityDc = this.dataManager.load(RedeemInfo.class).id(redeemInfo.getId()).one();
            RedeemInfo redeemInfo1 = this.dataManager.create(RedeemInfo.class);
            String[] ignorePropertiesRedeemInfo = {"id", "serialNumber", "transaction"};
            redeemInfo1.setTransaction(transactionAcct);
            redeemInfo1.setSerialNumber(redeemInfo.getSerialNumber());
            BeanUtils.copyProperties(redeemInfoEntityDc, redeemInfo1, ignorePropertiesRedeemInfo);
            this.dataManager.save(redeemInfo1);
        });
        activityLogBean.makerActivity("MENU07", "LOG07-01",
            "Replacement Reason: " + activityLogDesc.getValue().getName(),
            "TransactionAcct", transactionAcctDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());

         notifications.create()
                .withContentMode(ContentMode.HTML)
                .withType(Notifications.NotificationType.HUMANIZED)
                .withPosition(Notifications.Position.MIDDLE_CENTER)
                .withCaption("Replacement Successfully Updated")
                .show();
        closeWithDiscard();
    }

    @Install(to = "serialNoTable.previousSerialNumber", subject = "columnGenerator")
    private Component serialNoTablePreviousSerialNumberColumnGenerator(final RedeemInfo redeemInfo) {
        Label<String> label = uiComponents.create(Label.TYPE_STRING);
        label.setValue(redeemInfo.getSerialNumber());
        return label;
    }
}