package com.company.management.screen.revenuemanagement;

import com.company.management.screen.revenuefeereport.MarginConfigurationApproverEdit;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@UiController("MarginConfiguration.browse")
@UiDescriptor("margin-configuration-browse.xml")
@LookupComponent("marginActiveTable")
public class MarginConfigurationBrowse extends StandardLookup<RevenueManagement> {
    @Autowired
    private CollectionLoader<RevenueManagement> marginActiveDl;
    @Autowired
    private CollectionLoader<RevenueManagement> marginPendingDl;
    @Autowired
    private CollectionLoader<RevenueManagement> marginLogsDl;
    @Autowired
    private Table<RevenueManagement> marginActiveTable;
    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private Table<RevenueManagement> marginPendingTable;

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        marginActiveDl.setParameter("activeCode", "G04");
        marginActiveDl.setParameter("marginBuyCode", "C05");
        marginActiveDl.setParameter("marginSellCode", "C06");
        marginActiveDl.load();

        marginPendingDl.setParameter("pendingCode", "G05");
        marginPendingDl.setParameter("approvedCode", "G06");
        marginPendingDl.setParameter("marginBuyCode2", "C05");
        marginPendingDl.setParameter("marginSellCode2", "C06");
        marginPendingDl.load();

        marginLogsDl.setParameter("marginBuyCode3", "C05");
        marginLogsDl.setParameter("marginSellCode3", "C06");
        marginLogsDl.load();
    }

    @Subscribe("marginActiveTable.edit")
    public void onMarginActiveTableEdit(final Action.ActionPerformedEvent event) {
        MarginConfigurationMakerEdit marginConfigurationMakerEdit = screenBuilders.screen(this)
                .withScreenClass(MarginConfigurationMakerEdit.class)
                .withAfterCloseListener(afterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        marginConfigurationMakerEdit.setEntityToEdit(Objects.requireNonNull(marginActiveTable.getSingleSelected()));
        marginConfigurationMakerEdit.show();
    }

    @Subscribe("marginPendingTable.view")
    public void onMarginPendingTableView(final Action.ActionPerformedEvent event) {
        MarginConfigurationApproverEdit marginConfigurationApproverEdit = screenBuilders.screen(this)
                .withScreenClass(MarginConfigurationApproverEdit.class)
                .withAfterCloseListener(afterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        marginConfigurationApproverEdit.setEntityToEdit(Objects.requireNonNull(marginPendingTable.getSingleSelected()));
        marginConfigurationApproverEdit.show();
    }

    @Install(to = "marginActiveTable", subject = "styleProvider")
    private String marginActiveTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "marginPendingTable", subject = "styleProvider")
    private String marginPendingTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "marginLogsTable", subject = "styleProvider")
    private String marginLogsTableStyleProvider(RevenueManagement revenueManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (revenueManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

}