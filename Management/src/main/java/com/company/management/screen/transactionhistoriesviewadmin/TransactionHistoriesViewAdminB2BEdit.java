package com.company.management.screen.transactionhistoriesviewadmin;

import io.jmix.ui.component.Component;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionHistoriesViewAdmin;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@UiController("TransactionHistoriesViewAdminB2B.edit")
@UiDescriptor("transaction-histories-view-admin-B2B-edit.xml")
@EditedEntityContainer("transactionHistoriesViewAdminDc")
public class TransactionHistoriesViewAdminB2BEdit extends StandardEditor<TransactionHistoriesViewAdmin> {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(TransactionHistoriesViewAdminB2BEdit.class);
	private BigDecimal purchasePrice;
	private BigDecimal stockGram;
	private BigDecimal stockPrice;
	@Autowired
	private CollectionLoader<TransactionHistoriesViewAdmin> transactionHistoriesViewAdminsDl;
	@Autowired
	private InstanceLoader<TransactionHistoriesViewAdmin> transactionHistoriesViewAdminDl;


	public BigDecimal getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(BigDecimal purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public BigDecimal getStockGram() {
		return stockGram;
	}

	public void setStockGram(BigDecimal stockGram) {
		this.stockGram = stockGram;
	}

	public BigDecimal getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(BigDecimal stockPrice) {
		this.stockPrice = stockPrice;
	}

	public TransactionHistoriesViewAdmin getTransactionHistoriesViewAdmin() {
		return transactionHistoriesViewAdmin;
	}

	public void setTransactionHistoriesViewAdmin(TransactionHistoriesViewAdmin transactionHistoriesViewAdmin) {
		this.transactionHistoriesViewAdmin = transactionHistoriesViewAdmin;
	}

	public TransactionHistoriesViewAdmin transactionHistoriesViewAdmin;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		try {
			transactionHistoriesViewAdminsDl.setParameter("reference1", getTransactionHistoriesViewAdmin().getReference());
			transactionHistoriesViewAdminsDl.setParameter("uom1", "Gram");
			transactionHistoriesViewAdminsDl.load();
		}catch (Exception ex)
		{
			log.info(ex.getCause().getMessage());
		}
	}


	@Install(to = "purchaseRecordProductsTable.purchasePrice", subject = "columnGenerator")
	private Component purchaseRecordProductsTablePurchasePriceColumnGenerator(TransactionHistoriesViewAdmin transactionHistoriesViewAdmin) {
		return new Table.PlainTextCell(getPurchasePrice().toPlainString());
	}

	@Install(to = "purchaseRecordProductsTable.stockGram", subject = "columnGenerator")
	private Component purchaseRecordProductsTableStockGramColumnGenerator(TransactionHistoriesViewAdmin transactionHistoriesViewAdmin) {
		return new Table.PlainTextCell(getStockGram().toPlainString());
	}

	@Install(to = "purchaseRecordProductsTable.stockRM", subject = "columnGenerator")
	private Component purchaseRecordProductsTableStockRMColumnGenerator(TransactionHistoriesViewAdmin transactionHistoriesViewAdmin) {
		return new Table.PlainTextCell(getStockPrice().toPlainString());
	}
}