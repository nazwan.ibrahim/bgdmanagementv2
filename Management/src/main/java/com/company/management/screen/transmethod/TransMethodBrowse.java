package com.company.management.screen.transmethod;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransMethod;

@UiController("TransMethod.browse")
@UiDescriptor("trans-method-browse.xml")
@LookupComponent("transMethodsTable")
public class TransMethodBrowse extends StandardLookup<TransMethod> {
}