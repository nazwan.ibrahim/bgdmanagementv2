package com.company.management.screen.revenuemanagement;

import com.company.management.entity.Product;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;


@UiController("BursaRevenueManagement.browse")
@UiDescriptor("bursa-revenue-management-browse.xml")
@LookupComponent("revenueManagementsTable")
public class BursaRevenueManagementBrowse extends StandardLookup<RevenueManagement> {

	@Autowired
	private CollectionContainer<RevenueManagement> revenueManagementsDc;
	@Autowired
	private Notifications notifications;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<RevenueManagement> revenueManagementsDl;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private CollectionContainer<RtType> rtTypesStatusDc;

	Set<RevenueManagement> revenueManagementSet1;
	private RtType type;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private CollectionLoader<RevenueManagement> revenueManagementsDl1;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		loadTable();
	}

	@Subscribe("revenueManagementsTable.edit")
	public void onRevenueManagementsTableEdit(Action.ActionPerformedEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Revenue Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void saveData(){
		revenueManagementsDc.getItems().forEach(revenueManagement -> {
			dataManager.save(revenueManagement);
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated")
				.show();
		revenueManagementsDl.load();
	}

	@Subscribe("makerBtn")
	public void onMakerBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Revenue Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> makerRequest()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void makerRequest(){
		revenueManagementSet1.forEach(revenueManagement -> {
			RevenueManagement revenueManagement1 = this.dataManager.create(RevenueManagement.class);
			revenueManagement1.setStatus(rtType("G05"));
			revenueManagement1.setCharge(revenueManagement.getCharge());
			revenueManagement1.setProduct(revenueManagement.getProduct());
			revenueManagement1.setType(revenueManagement.getType());
			revenueManagement1.setTypeTax(revenueManagement.getTypeTax());
			revenueManagement1.setUom(revenueManagement.getUom());
			revenueManagement1.setUomTax(revenueManagement.getUomTax());
			revenueManagement1.setActive(false);
			revenueManagement1.setValueTax(revenueManagement.getValueTax());
			dataManager.save(revenueManagement1);
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated\nPending for Approval")
				.show();

		loadTable();
	}

	private void checkerApprovedRequest(){
		List<RevenueManagement> revenueManagements = this.dataManager.load(RevenueManagement.class)
						.query("select e from RevenueManagement e where e.status.code = :code")
								.parameter("code","G05")
									.fetchPlan("bursaRevenueManagement")
										.list();
		revenueManagements.forEach(revenueManagement -> {
				if (revenueManagement.getStatus().getCode().equals("G05")) {
					type = revenueManagement.getType();
					deArchivedPrevious();
					revenueManagement.setStatus(rtType("G01"));
					revenueManagement.setApprovedBy(currentAuthentication.getUser().getUsername());
					revenueManagement.setApprovedDate(timeSource.currentTimestamp());
					revenueManagement.setActive(true);
					this.dataManager.save(revenueManagement);
				}
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated")
				.show();

		loadTable();
	}

	private void checkerRejectRequest(){
		List<RevenueManagement> revenueManagements = this.dataManager.load(RevenueManagement.class)
				.query("select e from RevenueManagement e where e.status.code = :code")
				.parameter("code","G05")
				.fetchPlan("bursaRevenueManagement")
				.list();
		revenueManagements.forEach(revenueManagement -> {
			if (revenueManagement.getStatus().getCode().equals("G05")) {
				type = revenueManagement.getType();
				deArchivedPrevious();
				revenueManagement.setActive(true);
				revenueManagement.setStatus(rtType("G07"));
				revenueManagement.setApprovedBy(currentAuthentication.getUser().getUsername());
				revenueManagement.setApprovedDate(timeSource.currentTimestamp());
				this.dataManager.save(revenueManagement);
			}
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated")
				.show();

		loadTable();
	}

	@Subscribe("checkerBtn")
	public void onCheckerBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Revenue Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> checkerApprovedRequest()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Reject")
								.withHandler(e-> checkerRejectRequest())
				)
				.show();
	}
	@Subscribe("revenueManagementsTable")
	public void onRevenueManagementsTableSelection(final Table.SelectionEvent<RevenueManagement> event) {
	revenueManagementSet1 = event.getSelected();
	}

	private void deArchivedPrevious(){
		String query = "select e from RevenueManagement e where e.type = :type order by e.createdDate desc";
		List<RevenueManagement> revenueManagementList = this.dataManager.load(RevenueManagement.class)
				.query(query)
				.parameter("type", type)
				.fetchPlan("bursaRevenueManagement")
				.list();

		revenueManagementList.forEach(revenueManagement1 -> {
				if(revenueManagement1.getStatus().getCode().equals("G01")){
					revenueManagement1.setStatus(rtType("G15"));
					this.dataManager.save(revenueManagement1);
				}
		});
	}

	private RtType rtType(String code){
		String query = "select e from RtType e where e.code = :code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code",code)
				.one();
	}

	private void loadTable(){
		revenueManagementsDl1.setParameter("statusCode1","G05");
		revenueManagementsDl1.load();

		revenueManagementsDl.setParameter("statusCode1","G01");
		revenueManagementsDl.load();
	}
}