package com.company.management.screen.costrevocerymanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.CostRevoceryManagement;

@UiController("CostRevoceryManagement.browse")
@UiDescriptor("cost-revocery-management-browse.xml")
@LookupComponent("costRevoceryManagementsTable")
public class CostRevoceryManagementBrowse extends StandardLookup<CostRevoceryManagement> {
}