package com.company.management.screen.sapfee1;

import io.jmix.core.TimeSource;
import io.jmix.ui.component.Button;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.SapFee1;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

@UiController("SapFee1.browse")
@UiDescriptor("sap-fee1-browse.xml")
@LookupComponent("sapFee1sTable")
public class SapFee1Browse extends StandardLookup<SapFee1> {
	private static final Logger log = LoggerFactory.getLogger(SapFee1Browse.class);
	DecimalFormat dfmillion = new DecimalFormat("000000000000.00");
	@Autowired
	private TimeSource timeSource;

	BigDecimal sumAmountnSst = BigDecimal.ZERO;

	BigDecimal sumAllTrans = BigDecimal.ZERO;
	@Autowired
	private CollectionContainer<SapFee1> sapFee1sDc;

	@Subscribe("importText")
	public void onImportTextClick(final Button.ClickEvent event) throws FileNotFoundException {
		String dateFormat = "yyyyMM";
		String dateFormatTxt = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat dateSimpleformatterTxt = new SimpleDateFormat(dateFormatTxt);
		String path = String.format("C:\\Users\\amir\\Downloads\\FEE_%s.txt", dateSimpleformatter.format(timeSource.currentTimestamp()));
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalSst = BigDecimal.ZERO;
//		String path="C:\\Users\\amir\\Downloads\\test.txt";
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
			sapFee1sDc.getMutableItems().forEach(a -> {

				String getDate = dateSimpleformatterTxt.format(a.getTransactionDate());
				String getCreatedDate = getDate != null ? getDate : "";
				String getTypeCode = a.getTypeFee() != null ? a.getTypeFee(): "";
				BigDecimal getAmount = a.getAmount() != null ? a.getAmount() : BigDecimal.ZERO;
				BigDecimal getSst = a.getSst().abs() != null ? a.getSst() : BigDecimal.ZERO;
				BigDecimal amountAddSst = getAmount.add(getSst);
				sumAmountnSst= sumAmountnSst.add(amountAddSst);
				try {
					csvPrinter.printRecord(getCreatedDate,getTypeCode,getAmount.setScale(2),getSst.setScale(2),amountAddSst.setScale(2));
				} catch (IOException e) {
//					log.error("IOException Error :"+e);
					log.error("IOException Error :" + e);
				}

			});

			for(SapFee1 sapFee1 : sapFee1sDc.getItems()){
				totalAmount = totalAmount.add(sapFee1.getAmount());
				totalSst = totalSst.add(sapFee1.getSst());
			}
			sumAllTrans = (totalAmount.add(totalSst)).add(sumAmountnSst);
			csvPrinter.printRecord(null,null,totalAmount.setScale(2),totalSst.setScale(2),sumAllTrans.setScale(2));
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
		}
	}

	@Subscribe("importCash")
	public void onImportCashClick(final Button.ClickEvent event) throws FileNotFoundException {
		String dateFormat = "yyyyMM";
		String dateFormatTxt = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat dateSimpleformatterTxt = new SimpleDateFormat(dateFormatTxt);
		String path = String.format("C:\\Users\\amir\\Downloads\\CASH_%s.txt", dateSimpleformatter.format(timeSource.currentTimestamp()));
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalSst = BigDecimal.ZERO;

//		String path="C:\\Users\\amir\\Downloads\\test.txt";
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
			sapFee1sDc.getMutableItems().forEach(a -> {
				String getDate = dateSimpleformatterTxt.format(a.getTransactionDate());
				String getCreatedDate = getDate != null ? getDate : "";
				String getTypeCode = a.getTypeFee() != null ? a.getTypeFee(): "";
				BigDecimal getAmount = a.getAmount() != null ? a.getAmount() : BigDecimal.ZERO;
				BigDecimal getSst = a.getSst().abs() != null ? a.getSst() : BigDecimal.ZERO;
				BigDecimal amountAddSst = getAmount.add(getSst);
				sumAmountnSst= sumAmountnSst.add(amountAddSst);
				try {
					csvPrinter.printRecord(getCreatedDate,getTypeCode,getAmount.setScale(2),getSst.setScale(2),amountAddSst.setScale(2));
				} catch (IOException e) {
//					log.error("IOException Error :"+e);
					log.error("IOException Error :" + e);
				}

			});

			for(SapFee1 sapFee1 : sapFee1sDc.getItems()){
				totalAmount = totalAmount.add(sapFee1.getAmount());
				totalSst = totalSst.add(sapFee1.getSst());
			}
			csvPrinter.printRecord(null,null,totalAmount.setScale(2),totalSst.setScale(2));
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
		}
	}
}