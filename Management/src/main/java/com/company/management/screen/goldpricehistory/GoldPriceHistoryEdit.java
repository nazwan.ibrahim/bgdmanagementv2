package com.company.management.screen.goldpricehistory;

import io.jmix.ui.screen.*;
import com.company.management.entity.GoldPriceHistory;

@UiController("GoldPriceHistory.edit")
@UiDescriptor("gold-price-history-edit.xml")
@EditedEntityContainer("goldPriceHistoryDc")
public class GoldPriceHistoryEdit extends StandardEditor<GoldPriceHistory> {
}