package com.company.management.screen.deliveryrecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecord;

@UiController("DeliveryRecord.edit")
@UiDescriptor("delivery-record-edit.xml")
@EditedEntityContainer("deliveryRecordDc")
public class DeliveryRecordEdit extends StandardEditor<DeliveryRecord> {
}