package com.company.management.screen.investorgoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.InvestorGoldWalletStatement;

@UiController("InvestorGoldWalletStatement.browse")
@UiDescriptor("investor-gold-wallet-statement-browse.xml")
@LookupComponent("investorGoldWalletStatementsTable")
public class InvestorGoldWalletStatementBrowse extends StandardLookup<InvestorGoldWalletStatement> {
}