package com.company.management.screen.b2bgoldselling;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionAcct;
import com.company.management.entity.wallet.TransactionType;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.component.FileStorageUploadField;
import io.jmix.ui.component.SingleFileUploadField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

@UiController("B2BGoldSellingSubsequent.edit")
@UiDescriptor("b2b-gold-selling-subsequent-edit.xml")
@EditedEntityContainer("b2BGoldSellingDc")
public class B2BGoldSellingSubsequentEdit extends StandardEditor<B2BGoldSelling> {

	@Autowired
	private InstanceLoader<B2BGoldSelling> b2BGoldSellingDl;
	@Autowired
	private CollectionLoader<B2BGoldSellingProduct> b2BGoldSellingProductsDl;
	@Autowired
	private InstanceContainer<B2BGoldSelling> b2BGoldSellingDc;
	@Autowired
	private CollectionLoader<B2BGoldSellingDoc> b2BGoldSellingDocsDl;
	@Autowired
	private Button commit;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<B2BGoldSellingProduct> b2BGoldSellingProductsDc;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	private BigDecimal total = BigDecimal.ZERO;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private Dialogs dialogs;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		b2BGoldSellingDl.load();
		b2BGoldSellingProductsDl.setParameter("b2bGoldSelling1",b2BGoldSellingDc.getItem());
		b2BGoldSellingProductsDl.load();
		b2BGoldSellingDocsDl.setParameter("b2bGoldSelling1",b2BGoldSellingDc.getItem());
		b2BGoldSellingDocsDl.load();
		commit.setVisible(false);
		commit.setEnabled(false);
	}

//	@Subscribe("uploadPurchaseForm")
//	public void onUploadPurchaseFormFileUploadSucceed(SingleFileUploadField.FileUploadSucceedEvent event) {
//		commit.setEnabled(true);
//		commit.setVisible(true);
//	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveConfirmData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveConfirmData(){
		getEditedEntity().setType(loadType("GT03"));
		getEditedEntity().setStatus(loadType("G01"));
		dataManager.save(getEditedEntity());
		createNewSetData();

		b2BGoldSellingProductsDc.getItems().forEach(b2BGoldSellingProduct -> {
			dataManager.save(b2BGoldSellingProduct);
		});
		if (uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		closeWithDiscard();
	}

	private RtType loadType(String code) {
		return this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", code)
				.one();
	}

	private void createNewSetData() {
		B2BGoldSellingProduct b2BGoldSellingProduct = this.dataManager.create(B2BGoldSellingProduct.class);
		b2BGoldSellingProduct.setB2bGoldSelling(getEditedEntity());
		this.dataManager.save(b2BGoldSellingProduct);

		B2BGoldSellingDoc b2BGoldSellingDoc = this.dataManager.create(B2BGoldSellingDoc.class);
		b2BGoldSellingDoc.setB2bGoldSelling(getEditedEntity());
		this.dataManager.save(b2BGoldSellingDoc);

	}

	private void saveUploadDoc() {
		B2BGoldSellingDoc bGoldSellingDoc = this.dataManager.create(B2BGoldSellingDoc.class);
		bGoldSellingDoc.setB2bGoldSelling(b2BGoldSellingDc.getItem());
		bGoldSellingDoc.setFileUpload(uploadPurchaseForm.getValue());
		bGoldSellingDoc.setName(uploadPurchaseForm.getFileName());
		bGoldSellingDoc.setCreatedBy(currentAuthentication.getAuthentication().getName());
		bGoldSellingDoc.setCreatedDate(timeSource.currentTimestamp());
		this.dataManager.save(bGoldSellingDoc);
	}

	@Install(to = "b2BGoldSellingProductsTable.sub_total", subject = "valueProvider")
	private Object b2BGoldSellingProductsTableSub_totalValueProvider(B2BGoldSellingProduct b2BGoldSellingProduct) {
		if(b2BGoldSellingProduct.getPrice() != null & b2BGoldSellingProduct.getQuantity() != null) {
			BigDecimal price = b2BGoldSellingProduct.getPrice();
			b2BGoldSellingProduct.setSub_total(price);
			return price;
		}else{
			return null;
		}
	}

	@Subscribe("b2BGoldSellingProductsTable.add")
	public void onB2BGoldSellingProductsTableAdd(Action.ActionPerformedEvent event) {
		B2BGoldSellingProduct b2BGoldSellingProduct = this.dataManager.create(B2BGoldSellingProduct.class);
		b2BGoldSellingProduct.setB2bGoldSelling(b2BGoldSellingDc.getItem());
		b2BGoldSellingProduct.setType(loadType("GT03"));
		b2BGoldSellingProductsDc.getMutableItems().add(b2BGoldSellingProduct);
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveData(){
		getEditedEntity().setType(loadType("GT03"));
		dataManager.save(getEditedEntity());
		createNewSetData();
		addtogoldWallet();
		getEditedEntity().setPhysical_stock(total);
		this.dataManager.save(getEditedEntity());
		b2BGoldSellingProductsDc.getItems().forEach(b2BGoldSellingProduct -> {
			dataManager.save(b2BGoldSellingProduct);
		});
		if (uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		close(StandardOutcome.DISCARD);
	}


	private void addtogoldWallet(){
		AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
				.query("select a from AccountBgd a " +
						"where a.accOwner = :accOwner1 " +
						"and a.walletType.code = :walletTypeCode1").parameter("accOwner1", "AceAdmin").parameter("walletTypeCode1", "E02").one();

		TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);
		TransStatus transStatus = this.dataManager.load(TransStatus.class)
				.query("select t from TransStatus t " +
						"where t.code = :code1").parameter("code1", "G01")
				.one();

		transactionAcct.setAccount(accountBgd);

		Integer qty = b2BGoldSellingProductsDc.getItem().getQuantity();
		BigDecimal goldGram = BigDecimal.valueOf(1000);
		total = goldGram.multiply(BigDecimal.valueOf(qty));

		transactionAcct.setTransType(type("F01"));
		String reference = managementController.generateSeqNo("BGDF01","6");
		transactionAcct.setReference(reference);
		transactionAcct.setAmount(total.negate());

		transactionAcct.setTransStatus(transStatus);
		transactionAcct.setCreatedBy(accountBgd.getAccOwner());
		transactionAcct.setCreatedDate(new Date());

		this.dataManager.save(transactionAcct);
	}

	private TransactionType type(String code){
		return this.dataManager.load(TransactionType.class)
				.query("select t from TransactionType t " +
						"where t.code = :code1").parameter("code1", code)
				.one();
	}
}