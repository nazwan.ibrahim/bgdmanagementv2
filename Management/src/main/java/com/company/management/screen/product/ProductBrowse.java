package com.company.management.screen.product;

import io.jmix.ui.screen.*;
import com.company.management.entity.Product;

@UiController("Product.browse")
@UiDescriptor("product-browse.xml")
@LookupComponent("table")
public class ProductBrowse extends MasterDetailScreen<Product> {
}