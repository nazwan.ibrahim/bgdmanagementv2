package com.company.management.screen.transferinfo;

import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransferInfo;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("TransferInfo.browse")
@UiDescriptor("transfer-info-browse.xml")
@LookupComponent("transferInfoesTable")
public class TransferInfoBrowse extends StandardLookup<TransferInfo> {
	@Autowired
	private CollectionLoader<TransferInfo> transferInfoesDl;
	@Autowired
	private CollectionContainer<TransferInfo> transferInfoesDc;
	@Autowired
	private GroupTable<TransferInfo> transferInfoesTable;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		transferInfoesDl.setParameter("transAcctTransTypeCode1", "F03");
		transferInfoesDl.load();

	}
}