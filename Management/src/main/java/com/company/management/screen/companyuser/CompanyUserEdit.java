package com.company.management.screen.companyuser;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.CompanyUser;

@UiController("CompanyUser.edit")
@UiDescriptor("company-user-edit.xml")
@EditedEntityContainer("companyUserDc")
public class CompanyUserEdit extends StandardEditor<CompanyUser> {
}