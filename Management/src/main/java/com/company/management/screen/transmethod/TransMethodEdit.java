package com.company.management.screen.transmethod;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransMethod;

@UiController("TransMethod.edit")
@UiDescriptor("trans-method-edit.xml")
@EditedEntityContainer("transMethodDc")
public class TransMethodEdit extends StandardEditor<TransMethod> {
}