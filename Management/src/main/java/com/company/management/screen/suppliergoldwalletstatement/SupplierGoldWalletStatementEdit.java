package com.company.management.screen.suppliergoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.SupplierGoldWalletStatement;

@UiController("SupplierGoldWalletStatement.edit")
@UiDescriptor("supplier-gold-wallet-statement-edit.xml")
@EditedEntityContainer("supplierGoldWalletStatementDc")
public class SupplierGoldWalletStatementEdit extends StandardEditor<SupplierGoldWalletStatement> {
}