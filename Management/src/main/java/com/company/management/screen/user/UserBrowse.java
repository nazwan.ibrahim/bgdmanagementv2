package com.company.management.screen.user;

import com.company.management.entity.User;
import com.company.management.screen.changepassword.ChangePassword;
import com.company.management.screen.resourceroleentity.ResourceRoleEntityBrowse;
import com.company.management.screen.roleassignmententity.RoleAssignmentEntityBrowse;
import io.jmix.core.DataManager;
import io.jmix.securityui.screen.changepassword.ChangePasswordDialog;
import io.jmix.ui.Notifications;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.Screens;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@UiController("User.browse")
@UiDescriptor("user-browse.xml")
@LookupComponent("usersTable")
@Route("users")
public class UserBrowse extends StandardLookup<User> {
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<User> usersTable;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private CollectionLoader<User> usersDl;
	@Autowired
	private Screens screens;
	@Autowired
	Notifications notifications;

	boolean currentPasswordRequired = false;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		usersDl.setParameter("system1",true);
		usersDl.load();
	}

	@Subscribe("usersTable.edit")
	public void onUsersTableEdit(Action.ActionPerformedEvent event) {

		UserEdit userEdit = screenBuilders.screen(this)
		.withScreenClass(UserEdit.class)
		.withAfterCloseListener(afterScreenCloseEvent -> {
			getScreenData().loadAll();
		})
		.build();
		userEdit.setEntityToEdit(Objects.requireNonNull(usersTable.getSingleSelected()));
		userEdit.setDisablePassword("disablePassword");
		userEdit.show();
	}

	@Subscribe("usersTable.create")
	public void onUsersTableCreate(Action.ActionPerformedEvent event) {
		User user = this.dataManager.create(User.class);
		UserEdit userEdit = screenBuilders.screen(this)
				.withScreenClass(UserEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		userEdit.setEntityToEdit(user);
		userEdit.setDisablePassword("enablePassword");
		userEdit.show();
	}

	@Subscribe("usersTable.edit2")
	public void onUsersTableEdit2(Action.ActionPerformedEvent event) {
		RoleAssignmentEntityBrowse userEdit = screenBuilders.screen(this)
				.withScreenClass(RoleAssignmentEntityBrowse.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		userEdit.setUser(usersTable.getSingleSelected());
		userEdit.show();
	}

	@Subscribe("usersTable.changePassword")
	public void onUsersTableChangePassword(Action.ActionPerformedEvent event) {
		screens.create(ChangePassword.class)
				.withUsername(Objects.requireNonNull(usersTable.getSingleSelected()).getUsername())
				.withCurrentPasswordRequired(currentPasswordRequired)
				.show();
	}

	@Subscribe("usersTable.showRoleAssignments")
	public void onUsersTableShowRoleAssignments(final Action.ActionPerformedEvent event) {
		RoleAssignmentEntityBrowse userEdit = screenBuilders.screen(this)
				.withScreenClass(RoleAssignmentEntityBrowse.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		userEdit.setUser(usersTable.getSingleSelected());
		userEdit.show();
	}
}