package com.company.management.screen.suppliercashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.SupplierCashWalletStatement;

@UiController("SupplierCashWalletStatement.edit")
@UiDescriptor("supplier-cash-wallet-statement-edit.xml")
@EditedEntityContainer("supplierCashWalletStatementDc")
public class SupplierCashWalletStatementEdit extends StandardEditor<SupplierCashWalletStatement> {
}