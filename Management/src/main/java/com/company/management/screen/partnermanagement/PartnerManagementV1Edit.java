package com.company.management.screen.partnermanagement;

import com.company.management.entity.RtType;
import io.jmix.ui.component.HasValue;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.PartnerManagement;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("PartnerManagementV1.edit")
@UiDescriptor("partner-management-v1-edit.xml")
@EditedEntityContainer("partnerManagementDc")
public class PartnerManagementV1Edit extends StandardEditor<PartnerManagement> {
	@Autowired
	private CollectionLoader<RtType> rtTypesDl;
	@Autowired
	private InstanceLoader<PartnerManagement> partnerManagementDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		partnerManagementDl.load();
		rtTypesDl.setParameter("typeGroupCode1","D");
		rtTypesDl.load();
	}



}