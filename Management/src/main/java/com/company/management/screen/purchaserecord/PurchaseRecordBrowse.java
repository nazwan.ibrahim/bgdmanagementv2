package com.company.management.screen.purchaserecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseRecord;

@UiController("PurchaseRecord.browse")
@UiDescriptor("purchase-record-browse.xml")
@LookupComponent("purchaseRecordsTable")
public class PurchaseRecordBrowse extends StandardLookup<PurchaseRecord> {
}