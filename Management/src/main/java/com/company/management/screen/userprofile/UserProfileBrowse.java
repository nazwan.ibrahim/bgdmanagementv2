package com.company.management.screen.userprofile;


import com.company.management.entity.ActivityLog;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.registrationdb.UserTier;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

@UiController("UserProfile.browse")
@UiDescriptor("user-profile-browse.xml")
@LookupComponent("userTier1Table")
public class UserProfileBrowse extends StandardLookup<UserProfile> {
	private static final Logger log = LoggerFactory.getLogger(UserProfileBrowse.class);
	@Autowired
	private ScreenBuilders screenBuilders;
	String typeUser = "freeAccount";
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;
	@Autowired
	private Environment environment;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Table<UserTier> userProfilesTable3;
	@Autowired
	private CollectionLoader<UserTier> userTiersDl;
	@Autowired
	private CollectionLoader<UserTier> userTiersDl1;
	@Autowired
	private GroupTable<UserTier> userTier1Table;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		userTiersDl1.setParameter("tierCode1","T00");
		userTiersDl1.setParameter("currentTier1",true);
		userTiersDl1.load();
		activityLogsDl.setParameter("moduleCode1","MDL04-01");
		activityLogsDl.load();

	}

	@Subscribe("freeTab")
	public void onFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("freeAccount2")){
			userTiersDl1.setParameter("tierCode1","T00");
			userTiersDl1.setParameter("currentTier1",true);
			userTiersDl1.load();
			typeUser = "freeAccount";
		}else {
			userTiersDl.setParameter("tierCode1","T01");
			userTiersDl.setParameter("currentTier1",true);
//			userTiersDl.setParameter("currentTier1",false);
			userTiersDl.load();
			activityLogsDl.setParameter("moduleCode1","MDL04-01");
			activityLogsDl.load();
			typeUser = "tradableAccount";
		}

	}

	public void pageNavigation(UserProfile userProfile){
		UserProfileEdit userProfileEdit = screenBuilders.screen(this)
				.withScreenClass(UserProfileEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					if(typeUser.equals("freeAccount")) {
						typeUser = "freeAccount";
					}else{
						typeUser = "tradableAccount";
					}
					userTiersDl1.setParameter("tierCode1","T00");
					userTiersDl1.setParameter("currentTier1",true);
					userTiersDl1.load();
					userTiersDl.setParameter("tierCode1","T01");
					userTiersDl.setParameter("currentTier1",true);
//
					userTiersDl.load();
				})
				.build();
		userProfileEdit.setTypeUser(typeUser);
		userProfileEdit.setEntityToEdit(userProfile);
		userProfileEdit.show();
	}

//	@Subscribe("userProfilesTable3.view")
//	public void onUserProfilesTable3View(Action.ActionPerformedEvent event) {
//		UserProfileFreeEdit userProfileFreeEdit = screenBuilders.screen(this)
//				.withScreenClass(UserProfileFreeEdit.class)
//				.withAfterCloseListener(userProfileFreeEditAfterScreenCloseEvent -> {
//						getScreenData().loadAll();
//				})
//			.build();
//		userProfileFreeEdit.show();
//	}



//	@Subscribe("userProfilesTable3.edit")
//	public void onUserProfilesTable3Edit(Action.ActionPerformedEvent event) {
//		UserProfile userProfilesTableTradableSingleSelected = userProfilesTable3.getSingleSelected().getUser();
//		pageNavigation(userProfilesTableTradableSingleSelected);
//	}

	@Subscribe("userProfilesTable3.view")
	public void onUserProfilesTable3View(Action.ActionPerformedEvent event) {
		UserProfile userProfilesTableTradableSingleSelected = userProfilesTable3.getSingleSelected().getUser();
		pageNavigation(userProfilesTableTradableSingleSelected);
	}

	@Subscribe("userTier1Table.view")
	public void onUserTier1TableView(Action.ActionPerformedEvent event) {
		UserProfile userProfilesTableTradableSingleSelected = userTier1Table.getSingleSelected().getUser();
		pageNavigation(userProfilesTableTradableSingleSelected);
	}

	@Install(to = "userProfilesTable3.[user.identificationNo]", subject = "columnGenerator")
	private Component userProfilesTable3UserIdentificationNoColumnGenerator(UserTier userTier) {
		String decrypt = cipherEncryptionManagement.decrypt(userTier.getUser().getIdentificationNo(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userTier1Table.[user.identificationNo]", subject = "columnGenerator")
	private Component userTier1TableUserIdentificationNoColumnGenerator(UserTier userTier) {
		String decrypt = cipherEncryptionManagement.decrypt(userTier.getUser().getIdentificationNo(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userProfilesTable3.[user.phoneNumber]", subject = "columnGenerator")
	private Component userProfilesTable3UserPhoneNumberColumnGenerator(UserTier userTier) {
		String decrypt = cipherEncryptionManagement.decrypt(userTier.getUser().getPhoneNumber(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userProfilesTable3", subject = "styleProvider")
	private String userProfilesTable3StyleProvider(UserTier userTier, String property) {

		if (property == null) {

		} else if (property.equals("verification.overalVerificationResult.name")) {
			switch (userTier.getVerification().getOveralVerificationResult().getCode()) {
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "onboardingActivityLog", subject = "styleProvider")
	private String onboardingActivityLogStyleProvider(ActivityLog activityLog, String property) {

		if (property == null) {

		} else if (property.equals("verification.overal_verification_result.name")) {
			switch (activityLog.getStatus().getCode()) {
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}



	@Install(to = "userTier1Table.[user.phoneNumber]", subject = "columnGenerator")
	private Component userTier1TableUserPhoneNumberColumnGenerator1(UserTier userTier) {
		String decrypt = cipherEncryptionManagement.decrypt(userTier.getUser().getPhoneNumber(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userProfilesTable3.[user.phoneNumber]", subject = "columnGenerator")
	private Component userProfilesTable3UserPhoneNumberColumnGenerator1(UserTier userTier) {
		String decrypt = cipherEncryptionManagement.decrypt(userTier.getUser().getPhoneNumber(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}
}