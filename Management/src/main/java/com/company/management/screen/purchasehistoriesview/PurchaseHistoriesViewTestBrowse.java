package com.company.management.screen.purchasehistoriesview;

import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseHistoriesView;

@UiController("PurchaseHistoriesTestView.browse")
@UiDescriptor("purchase-histories-view-test-browse.xml")
@LookupComponent("purchaseHistoriesViewsTable")
public class PurchaseHistoriesViewTestBrowse extends StandardLookup<PurchaseHistoriesView> {
}