package com.company.management.screen.userprofile;

import com.company.management.entity.ActivityLog;
import com.company.management.entity.User;
import com.company.management.entity.registrationdb.UserTier;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import io.jmix.ui.ScreenBuilders;
import com.company.management.entity.registrationdb.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Objects;

@UiController("InvestorAccountManagement.browse")
@UiDescriptor("investor-account-management-browse.xml")
@LookupComponent("investorAccountManagementTable")
public class InvestorAccountManagementBrowse extends StandardLookup<UserProfile> {
    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private Table<UserProfile> investorAccountManagementTable;
    @Autowired
    private CipherEncryptionManagement cipherEncryptionManagement;
    @Autowired
    private Environment environment;
    @Autowired
    private CollectionLoader<ActivityLog> activityLogDl;

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        activityLogDl.setParameter("refEntity1", "UserProfile");
        activityLogDl.load();
    }

//    @Subscribe("investorAccountManagementTable.view")
//    public void onUserProfilesTableView(Action.ActionPerformedEvent event) {
//        InvestorAccountManagementEdit investorAccountManagementEdit = screenBuilders.screen(this)
//                .withScreenClass(InvestorAccountManagementEdit.class)
//                .withAfterCloseListener(investorAccountManagementEditAfterScreenCloseEvent -> {
//                    getScreenData().loadAll();
//                })
//                .build();
//        investorAccountManagementEdit.setEntityToEdit(Objects.requireNonNull(investorAccountManagementTable.getSingleSelected()));
//        investorAccountManagementEdit.show();
//    }



    @Install(to = "investorAccountManagementTable.identificationNo", subject = "columnGenerator")
    private Component investorAccountManagementTableIdentificationNoColumnGenerator(UserProfile userProfile) {
        String decrypt = cipherEncryptionManagement.decrypt(userProfile.getIdentificationNo(),environment.getProperty("secret_key"));
        if(decrypt != null) {
            return new Table.PlainTextCell(decrypt);
        }else{
            return new Table.PlainTextCell("-");
        }
    }

    @Install(to = "investorAccountManagementTable.phoneNumber", subject = "columnGenerator")
    private Component investorAccountManagementTablePhoneNumberColumnGenerator(UserProfile userProfile) {
        String decrypt = cipherEncryptionManagement.decrypt(userProfile.getPhoneNumber(),environment.getProperty("secret_key"));
        if(decrypt != null) {
            return new Table.PlainTextCell(decrypt);
        }else{
            return new Table.PlainTextCell("-");
        }    }

    @Install(to = "investorAccountManagementTable", subject = "styleProvider")
    private String investorAccountManagementTableStyleProvider(UserProfile userProfile, String property) {

        if (property == null) {

        } else if (property.equals("actStatus")) {
            switch (userProfile.getActStatus().getCode()) {
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "activityLogTable", subject = "styleProvider")
    private String activityLogTableStyleProvider(ActivityLog activityLog, String property) {
        if (property == null) {

        } else if (property.equals("status.name")) {
            switch (activityLog.getStatus().getCode()) {
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }



    @Subscribe("investorAccountManagementTable.edit")
    public void onInvestorAccountManagementTableEdit(final Action.ActionPerformedEvent event) {
        InvestorAccountManagementEdit investorAccountManagementEdit = screenBuilders.screen(this)
                .withScreenClass(InvestorAccountManagementEdit.class)
                .withAfterCloseListener(investorAccountManagementEditAfterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        investorAccountManagementEdit.setEntityToEdit(Objects.requireNonNull(investorAccountManagementTable.getSingleSelected()));
        investorAccountManagementEdit.show();
    }
}