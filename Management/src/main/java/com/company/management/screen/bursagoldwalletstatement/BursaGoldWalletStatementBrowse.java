package com.company.management.screen.bursagoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.BursaGoldWalletStatement;

@UiController("BursaGoldWalletStatement.browse")
@UiDescriptor("bursa-gold-wallet-statement-browse.xml")
@LookupComponent("bursaGoldWalletStatementsTable")
public class BursaGoldWalletStatementBrowse extends StandardLookup<BursaGoldWalletStatement> {
}