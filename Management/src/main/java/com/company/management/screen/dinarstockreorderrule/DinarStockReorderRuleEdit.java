package com.company.management.screen.dinarstockreorderrule;

import io.jmix.ui.Dialogs;
import io.jmix.ui.component.Button;
import io.jmix.ui.screen.*;
import com.company.management.entity.DinarStockReorderRule;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("DinarStockReorderRule.edit")
@UiDescriptor("dinar-stock-reorder-rule-edit.xml")
@EditedEntityContainer("dinarStockReorderRuleDc")
public class DinarStockReorderRuleEdit extends StandardEditor<DinarStockReorderRule> {

	@Autowired
	private Dialogs dialogs;

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		dialogs.createMessageDialog()
				.withCaption("Save Successful")
				.withMessage("Your Progress is successfully saved")
				.show();
	}
}