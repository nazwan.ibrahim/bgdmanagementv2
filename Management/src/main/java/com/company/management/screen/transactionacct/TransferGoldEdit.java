package com.company.management.screen.transactionacct;

import com.company.management.api.ManagementController;
import com.company.management.app.ActivityLogBean;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.ActivityLogFile;
import com.company.management.entity.RtType;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.wallet.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.*;

@UiController("TransferGoldEdit.edit")
@UiDescriptor("TransferGoldEdit.xml")
public class TransferGoldEdit extends Screen {

	private static final Logger log = LoggerFactory.getLogger(TransferGoldEdit.class);
	private String reference;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl;
	@Autowired
	private EntityComboBox<AccountBgd> accountNo;
	@Autowired
	private TextField<String> supplierName;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl1;
	@Autowired
	private TextField<String> uom;
	@Autowired
	private InstanceContainer<ActivityLog> activityLogDc;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<ActivityLogFile> activityLogFilesDl;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private TabSheet transferCash;
	@Autowired
	private TextField<BigDecimal> amountField;
	@Autowired
	private Notifications notifications;
	@Autowired
	private TextField<BigDecimal> amountField1;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private ActivityLogBean activityLogBean;
	@Autowired
	private EntityComboBox<AccountBgd> investorID;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private TemporaryStorage temporaryStorage;
	@Autowired
	private Downloader downloader;
	@Autowired
	private CollectionContainer<AccountBgd> accountBgdsDc1;
	@Autowired
	private TextField<String> investorName;
	@Autowired
	private TextArea<String> remarksFieldId;
	@Autowired
	private CollectionContainer<ActivityLogFile> activityLogFilesDc;
	@Named("uploadFileTable.remove")
	private RemoveAction<ActivityLogFile> uploadFileTableRemove;
	@Named("uploadFileTable1.remove")
	private RemoveAction<ActivityLogFile> uploadFileTable1Remove;
	@Autowired
	private TextField<String> uom1;
	@Autowired
	private FileMultiUploadField fileUpload1;
	@Autowired
	private CollectionLoader<RtType> rtTypesDl;
	private TransactionAcct transactionAcctSender;
	private AccountBgd accountBgdReceiver;
	@Autowired
	private EntityComboBox<RtType> reasonTf;
	@Autowired
	private EntityComboBox<RtType> reasonTf1;
	@Autowired
	private TextArea<String> remarksFieldId1;
	@Autowired
	private EntityComboBox<AccountBgd> investorNameID;

	public String getAccountOwner() {
		return accountOwner;
	}
	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}
	private String accountOwner;
	@Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
	private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
		Button btn = uiComponents.create(Button.class);
		btn.setCaption("Delete");
		btn.addClickListener(clickEvent -> {
			uploadFileTableRemove.execute();
		});
		return btn;
	}

	@Install(to = "uploadFileTable1.deleteFileUpload1", subject = "columnGenerator")
	private Component uploadFileTable1DeleteFileUpload1ColumnGenerator(final ActivityLogFile activityLogFile) {
		Button btn = uiComponents.create(Button.class);
		btn.setCaption("Delete");
		btn.addClickListener(clickEvent -> {
			temporaryStorage.deleteFile(activityLogFile.getId());
			activityLogFilesDc.getMutableItems().remove(activityLogFile);
		});
		return btn;
	}

	@Subscribe
	public void onBeforeShow(final BeforeShowEvent event) {
		if(getAccountOwner().equals("AceAdmin")) {
			accountBgdsDl.setParameter("accOwner1", "BgdAdmin");
		}else if(getAccountOwner().equals("BgdAdmin")) {
			accountBgdsDl.setParameter("accOwner1","AceAdmin");
		}else {
			String query = "select e from AccountBgd e where e.accOwner <> :accOwner1 and e.accOwner <> :accOwner2 and e.walletType.code = :walletTypeCode1";
			accountBgdsDl.setQuery(query);
			accountBgdsDl.setParameter("accOwner1","AceAdmin");
			accountBgdsDl.setParameter("accOwner2","BgdAdmin");
			transferCash.getTab("supplierCash").setVisible(false);
		}
		accountBgdsDl.setParameter("walletTypeCode1","E02");
		accountBgdsDl.load();
		accountNo.setValue(accountBgdsDl.getContainer().getItems().get(0));
		supplierName.setValue(accountBgdsDl.getContainer().getItems().get(0).getAccOwner());
		accountBgdsDl1.setParameter("walletTypeCode1","E02");
		accountBgdsDl1.setParameter("regTypeCode1","D02");
		accountBgdsDl1.load();

		rtTypesDl.setParameter("typeGroupCode1","TR");
		rtTypesDl.load();
	}

	@Subscribe
	public void onInit(final InitEvent event) {
		uom.setValue(rtType("J02").getName());
	}

	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
		activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
		uploadMultipleFile();
	}

	private TransactionType type (String code){
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
		return this.dataManager.load(TransactionType.class)
				.query(query).parameter("code1", code)
				.one();
	}

	private RtType rtType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Transfer Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> checkBeforeSave()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void checkBeforeSave(){
		if(transferCash.getSelectedTab().getName().equals("supplierCash")){
			if(!activityLogFilesDc.getItems().isEmpty() & !remarksFieldId.isEmpty() & !reasonTf.isEmpty()){
				transferGold();
			}else{
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.ERROR)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Please fill in all the blanks")
						.show();
			}
		}else{
			if(!activityLogFilesDc.getItems().isEmpty() & !remarksFieldId1.isEmpty() & !reasonTf1.isEmpty()){
				transferGoldInvestor();
			}else{
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.ERROR)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Please fill in all the blanks")
						.show();
			}
		}
	}

	private void transferGold(){
		if(amountField.getValue() != null & Objects.nonNull(accountNo.getValue())) {
			deductFromBursa();
			addToOtherSupplier();
			saveTransferInfo();

			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.HUMANIZED)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Successfully Transfer")
					.show();
		}else {
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Not Successfully Transfer")
					.show();
		}
		close(StandardOutcome.CLOSE);
	}

	private void transferGoldInvestor(){
		if(amountField1.getValue() != null & Objects.nonNull(investorID.getValue())) {
			deductFromBursa();
			addToOtherInvestor();
			saveTransferInfo();

			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.HUMANIZED)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Successfully Transfer")
					.show();
		}else {
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Not Successfully Transfer")
					.show();
		}
		close(StandardOutcome.CLOSE);
	}

	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
				activityLogFile.setActivityLog(activityLogDc.getItem());
				activityLogFile.setName(fileName);
				activityLogFile.setFileUpload(fileRef);
				activityLogFile.setAction(rtType("ACT01"));

				activityLogFilesDc.getMutableItems().add(activityLogFile);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());


		fileUpload1.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload1.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
				activityLogFile.setActivityLog(activityLogDc.getItem());
				activityLogFile.setName(fileName);
				activityLogFile.setFileUpload(fileRef);
				activityLogFile.setAction(rtType("ACT01"));

				activityLogFilesDc.getMutableItems().add(activityLogFile);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload1.getUploadsMap().values())
					.show();
		});
		fileUpload1.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}

	private void deductFromBursa(){
		String queryaccountBgd = "select a from AccountBgd a " +
				"where a.accOwner = :accOwner1 and a.walletType.code = :code1";
		AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
				.query(queryaccountBgd)
				.parameter("accOwner1", getAccountOwner())
				.parameter("code1", "E02")
				.one();

		TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);
		String querytransStatus = "select t from TransStatus t " +
				"where t.code = :code1";
		TransStatus transStatus = this.dataManager.load(TransStatus.class)
				.query(querytransStatus).parameter("code1", "G05/02")
				.one();

		transactionAcct.setAccount(accountBgd);
		if(transferCash.getSelectedTab().getName().equals("supplierCash")) {
			transactionAcct.setAmount(amountField.getValue().negate());
		}else {
			transactionAcct.setAmount(amountField1.getValue().negate());
		}
		transactionAcct.setTransStatus(transStatus);
		transactionAcct.setTransType(type("F03"));
		reference = managementController.generateSeqNo("BGDF03","6");
		transactionAcct.setReference(reference);
		transactionAcct.setCreatedBy(accountBgd.getAccOwner());
		transactionAcct.setCreatedDate(new Date());

		transactionAcctSender = transactionAcct;
		this.dataManager.save(transactionAcct);

		if(getAccountOwner().equals("BgdAdmin")) {
			activityLogBean.makerActivity("MDL03-D01-E02", "LOG03-F03", "Reference No = " +reference,
					"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
		} else if(getAccountOwner().equals("AceAdmin")) {
			activityLogBean.makerActivity("MDL03-D04-E02", "LOG03-F03", "Reference No = " +reference,
					"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
		}
	}

	private void addToOtherSupplier(){
		String queryaccountBgd1 = "select a from AccountBgd a " +
				"where a.acctNumber = :acctNumber1";
		AccountBgd accountBgd1 = this.dataManager.load(AccountBgd.class)
				.query(queryaccountBgd1).parameter("acctNumber1", accountNo.getValue().getAcctNumber()).one();

		TransactionAcct transactionAcct1 =this.dataManager.create(TransactionAcct.class);
		String querytransStatus1 = "select t from TransStatus t " +
				"where t.code = :code1";
		TransStatus transStatus1 = this.dataManager.load(TransStatus.class)
				.query(querytransStatus1).parameter("code1", "G05/02")
				.one();

		transactionAcct1.setAccount(accountBgd1);
		accountBgdReceiver = accountBgd1;
		transactionAcct1.setAmount(amountField.getValue());
		transactionAcct1.setTransStatus(transStatus1);
		transactionAcct1.setTransType(type("F03/E02-02"));
//		String reference1 = managementController.generateSeqNo("BGDF03","6");
		transactionAcct1.setReference(reference);
		transactionAcct1.setCreatedBy(accountBgd1.getAccOwner());
		transactionAcct1.setCreatedDate(new Date());

		this.dataManager.save(transactionAcct1);
	}

	private void addToOtherInvestor(){
		String queryaccountBgd1 = "select a from AccountBgd a " +
				"where a.accOwner = :accOwner1 and a.walletType.code = :code1";
		AccountBgd accountBgd1 = this.dataManager.load(AccountBgd.class)
				.query(queryaccountBgd1)
				.parameter("accOwner1", investorID.getValue().getAccOwner())
				.parameter("code1", "E02")
				.one();

		TransactionAcct transactionAcct1 =this.dataManager.create(TransactionAcct.class);
		String querytransStatus1 = "select t from TransStatus t " +
				"where t.code = :code1";
		TransStatus transStatus1 = this.dataManager.load(TransStatus.class)
				.query(querytransStatus1).parameter("code1", "G05/02")
				.one();

		transactionAcct1.setAccount(accountBgd1);
		accountBgdReceiver = accountBgd1;
		transactionAcct1.setAmount(amountField1.getValue());
		transactionAcct1.setTransStatus(transStatus1);
		transactionAcct1.setTransType(type("F03/E02-02"));
//		String reference1 = managementController.generateSeqNo("BGDF03","6");
		transactionAcct1.setReference(reference);
		transactionAcct1.setCreatedBy(accountBgd1.getAccOwner());
		transactionAcct1.setCreatedDate(new Date());

		this.dataManager.save(transactionAcct1);
	}

	@Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
	private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
		if (activityLogFile.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(activityLogFile.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(activityLogFile.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Install(to = "uploadFileTable1.fileUpload", subject = "columnGenerator")
	private Component uploadFileTable1FileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
		if (activityLogFile.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(activityLogFile.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(activityLogFile.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Subscribe("investorID")
	public void onInvestorIDValueChange(final HasValue.ValueChangeEvent<AccountBgd> event) {
		investorName.setValue(event.getValue().getAccOwner());
	}
	@Subscribe("transferCash")
	public void onTransferCashSelectedTabChange(final TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("investorTransfer")){
			uom1.setValue(rtType("J02").getName());
			supplierName.setValue(null);
			accountNo.setValue(null);
			amountField.setValue(null);
			uom.setValue(null);
			reasonTf.setValue(null);
			remarksFieldId.setValue(null);

			List<ActivityLogFile> activityLogFiles = activityLogFilesDc.getItems();
			activityLogFiles.forEach(activityLogFile -> {
				temporaryStorage.deleteFile(activityLogFile.getId());
			});

			activityLogFilesDc.getMutableItems().removeAll(activityLogFiles);

		}else{
			uom.setValue(rtType("J02").getName());
			investorNameID.setValue(null);
			investorID.setValue(null);
			investorName.setValue(null);
			amountField1.setValue(null);
			uom1.setValue(null);
			reasonTf1.setValue(null);
			remarksFieldId1.setValue(null);
			List<ActivityLogFile> activityLogFiles = activityLogFilesDc.getItems();
			activityLogFiles.forEach(activityLogFile -> {
				temporaryStorage.deleteFile(activityLogFile.getId());
			});

			activityLogFilesDc.getMutableItems().removeAll(activityLogFiles);
			accountNo.setValue(accountBgdsDl.getContainer().getItems().get(0));
			supplierName.setValue(accountBgdsDl.getContainer().getItems().get(0).getAccOwner());
		}

	}
	@Subscribe("closeBtn")
	public void onCloseBtnClick(final Button.ClickEvent event) {
		close(StandardOutcome.CLOSE);
	}

	private void saveTransferInfo(){
		TransferInfo transferInfo = this.dataManager.create(TransferInfo.class);
		transferInfo.setTransAcct(transactionAcctSender);
		transferInfo.setToAcct(accountBgdReceiver);
		if(transferCash.getSelectedTab().getName().equals("supplierCash")) {
			transferInfo.setReason(reasonTf.getValue().getName());
		}else{
			transferInfo.setReason(reasonTf1.getValue().getName());
		}
		this.dataManager.save(transferInfo);
	}

	@Subscribe("investorNameID")
	public void onInvestorNameIDValueChange(final HasValue.ValueChangeEvent<AccountBgd> event) {
		investorID.setValue(event.getValue());
	}
}