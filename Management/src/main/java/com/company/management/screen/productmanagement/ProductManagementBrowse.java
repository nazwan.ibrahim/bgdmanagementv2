package com.company.management.screen.productmanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.ProductManagement;

@UiController("ProductManagement.browse")
@UiDescriptor("product-management-browse.xml")
@LookupComponent("table")
public class ProductManagementBrowse extends MasterDetailScreen<ProductManagement> {
}