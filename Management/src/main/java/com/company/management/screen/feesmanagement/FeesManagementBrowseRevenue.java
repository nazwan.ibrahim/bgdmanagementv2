package com.company.management.screen.feesmanagement;

import io.jmix.core.DataManager;
import io.jmix.core.Metadata;
import io.jmix.ui.Notifications;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.FeesManagement;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("FeesManagementRevenue.browse")
@UiDescriptor("fees-management-browse-revenue.xml")
@LookupComponent("feesManagementsTable")
public class FeesManagementBrowseRevenue extends StandardLookup<FeesManagement> {
	@Autowired
	private GroupTable<FeesManagement> feesManagementsTable;
	@Autowired
	private CollectionContainer<FeesManagement> feesManagementsDc;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private DataContext dataContext;
	@Autowired
	private CollectionLoader<FeesManagement> feesManagementsDl;
	@Autowired
	private Notifications notifications;

	@Subscribe("feesManagementsTable.edit")
	public void onFeesManagementsTableEdit(Action.ActionPerformedEvent event) {
		feesManagementsDc.getItems().forEach(feesManagement -> {
			dataManager.save(feesManagement);
		});
		notifications.create()
						.withType(Notifications.NotificationType.HUMANIZED)
								.withPosition(Notifications.Position.MIDDLE_CENTER)
									.withCaption("Revenue Successfully Updated")
											.show();
		feesManagementsDl.load();
	}

}