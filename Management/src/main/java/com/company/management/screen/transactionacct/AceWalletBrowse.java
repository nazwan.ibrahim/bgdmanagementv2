package com.company.management.screen.transactionacct;

import com.company.management.entity.ActivityLog;
import com.company.management.entity.TransactionHistoriesView;
import com.company.management.entity.wallet.AccountWalletView;
import com.company.management.screen.transactionhistoriesview.TransactionApproveViewEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Label;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Objects;

@UiController("AceWallet.browse")
@UiDescriptor("ace-wallet-browse.xml")
@LookupComponent("transactionAcctsTable")
public class AceWalletBrowse extends StandardLookup<TransactionAcct> {
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private Label<BigDecimal> totalCash;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsGoldDl;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsDl1;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsGoldDl;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsDc1;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsGoldDc;
	@Autowired
	private Label<String> goldBar;

	DecimalFormat df = new DecimalFormat("#,###,##0.000000");
	@Autowired
	private Table<TransactionHistoriesView> transactionAcctsTable;
	@Autowired
	private Table<TransactionHistoriesView> transactionHistoriesViewsTable;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsGoldDl;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsCashDl;

	private void setOnBeforeShow(){
		accountWalletViewsDl1.setParameter("accountOwner1","AceAdmin");
		accountWalletViewsDl1.setParameter("code1","E01");
		accountWalletViewsDl1.load();

		accountWalletViewsGoldDl.setParameter("accountOwner1","AceAdmin");
		accountWalletViewsGoldDl.setParameter("code1","E02");
		accountWalletViewsGoldDl.load();

		transactionHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
		transactionHistoriesViewsDl.setParameter("walletCode1","E01");
		transactionHistoriesViewsDl.load();

		transactionHistoriesViewsGoldDl.setParameter("accOwner1","AceAdmin");
		transactionHistoriesViewsGoldDl.setParameter("walletCode1","E02");
		transactionHistoriesViewsGoldDl.load();

		activityLogsGoldDl.setParameter("moduleCode1","MDL03-D04-E02");
		activityLogsGoldDl.load();

		activityLogsCashDl.setParameter("moduleCode1","MDL03-D04-E01");
		activityLogsCashDl.load();

	}


	/** To change column status color **/
	@Install(to = "transactionAcctsTable", subject = "styleProvider")
	protected String transactionTableStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}



	@Install(to = "transactionHistoriesViewsTable", subject = "styleProvider")
	protected String transactionTableGoldStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		if(!accountWalletViewsDc1.getItems().isEmpty()) {
			totalCash.setValue(accountWalletViewsDc1.getItems().get(0).getAvailableAmount());
		}else {
			totalCash.setValue(BigDecimal.ZERO);
		}
		if(!accountWalletViewsGoldDc.getItems().isEmpty()) {
			goldBar.setValue(df.format(accountWalletViewsGoldDc.getItems().get(0).getAvailableAmount().setScale(6, RoundingMode.HALF_EVEN)));
		}else {
			goldBar.setValue(df.format(BigDecimal.ZERO));
		}
	}


	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		setOnBeforeShow();
	}
	@Subscribe("topup")
	public void onTopupClick(Button.ClickEvent event) {
		TopUpCashEdit topUpCashEdit = screenBuilders.screen(this)
				.withScreenClass(TopUpCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		topUpCashEdit.setAccountOwner("AceAdmin");
		topUpCashEdit.show();
	}
	@Subscribe("withdraw")
	public void onWithdrawClick(Button.ClickEvent event) {
		WithdrawCashEdit withdrawCashEdit = screenBuilders.screen(this)
				.withScreenClass(WithdrawCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		withdrawCashEdit.setAccountOwner("AceAdmin");
		withdrawCashEdit.show();
	}

	@Install(to = "transactionHistoriesViewsTable.amount", subject = "formatter")
	private String transactionHistoriesViewsTableAmountFormatter(Object object) {
		return df.format(object);
	}

	@Subscribe("transferCash")
	public void onTransferCashClick(final Button.ClickEvent event) {
		TransferCashEdit transferCashEdit = screenBuilders.screen(this)
				.withScreenClass(TransferCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferCashEdit.setAccountOwner("AceAdmin");
		transferCashEdit.show();
	}

	@Subscribe("adjustmentCash")
	public void onAdjustmentCashClick(final Button.ClickEvent event) {
		AdjustmentCashEdit adjustmentCashEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentCashEdit.setAccountOwner("AceAdmin");
		adjustmentCashEdit.show();
	}

	@Subscribe("transactionAcctsTable.view")
	public void onTransactionAcctsTableView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionAcctsTable.getSingleSelected()));
		screen.show();
	}

	@Subscribe("transferCashGold")
	public void onTransferCashGoldClick(final Button.ClickEvent event) {
		TransferGoldEdit transferGoldEdit = screenBuilders.screen(this)
				.withScreenClass(TransferGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferGoldEdit.setAccountOwner("AceAdmin");
		transferGoldEdit.show();
	}

	@Subscribe("adjustmentCashGold")
	public void onAdjustmentCashGoldClick(final Button.ClickEvent event) {
		AdjustmentGoldEdit adjustmentCashEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentCashEdit.setAccountOwner("AceAdmin");
		adjustmentCashEdit.show();
	}

	@Subscribe("transactionHistoriesViewsTable.view")
	public void onTransactionHistoriesViewsTableView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionHistoriesViewsTable.getSingleSelected()));
		screen.show();
	}

	@Install(to = "activityLogSupplierCashTable", subject = "styleProvider")
	private String activityLogSupplierCashTableStyleProvider(final ActivityLog entity, final String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (entity.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "activityLogSupplierGoldTable", subject = "styleProvider")
	private String activityLogSupplierGoldTableStyleProvider(final ActivityLog entity, final String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (entity.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}
	
	


}