package com.company.management.screen.b2bgoldselling;

import com.company.management.entity.PurchaseRecord;
import com.company.management.screen.purchaserecord.BGDDemocratisationEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldSelling;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("B2BGoldSellingSubsequent.browse")
@UiDescriptor("b2b-gold-selling-subsequent-browse.xml")
@LookupComponent("b2BGoldSellingsTable")
public class B2BGoldSellingSubsequentBrowse extends StandardLookup<B2BGoldSelling> {
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<B2BGoldSelling> b2BGoldSellingsTable;

	private void goToEditPage(B2BGoldSelling b2BGoldSelling){
		B2BGoldSellingSubsequentEdit b2BGoldSellingSubsequentEdit = screenBuilders.screen(this)
				.withScreenClass(B2BGoldSellingSubsequentEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		assert b2BGoldSelling != null;
		b2BGoldSellingSubsequentEdit.setEntityToEdit(b2BGoldSelling);
		b2BGoldSellingSubsequentEdit.show();
	}

	@Subscribe("b2BGoldSellingsTable.create")
	public void onB2BGoldSellingsTableCreate(Action.ActionPerformedEvent event) {
		B2BGoldSelling b2BGoldSelling = this.dataManager.create(B2BGoldSelling.class);
		goToEditPage(b2BGoldSelling);
	}

	@Subscribe("b2BGoldSellingsTable.edit")
	public void onB2BGoldSellingsTableEdit(Action.ActionPerformedEvent event) {
		B2BGoldSelling b2BGoldSelling = this.b2BGoldSellingsTable.getSingleSelected();
		goToEditPage(b2BGoldSelling);
	}
}