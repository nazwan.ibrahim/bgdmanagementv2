package com.company.management.screen.shariahrecordsubsequent;

import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecordSubsequent;

@UiController("ShariahRecordSubsequent.browse")
@UiDescriptor("shariah-record-subsequent-browse.xml")
@LookupComponent("shariahRecordSubsequentsTable")
public class ShariahRecordSubsequentBrowse extends StandardLookup<ShariahRecordSubsequent> {
}