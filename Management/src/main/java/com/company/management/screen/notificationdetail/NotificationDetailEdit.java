package com.company.management.screen.notificationdetail;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationDetail;

@UiController("NotificationDetail.edit")
@UiDescriptor("notification-detail-edit.xml")
@EditedEntityContainer("notificationDetailDc")
public class NotificationDetailEdit extends StandardEditor<NotificationDetail> {
}