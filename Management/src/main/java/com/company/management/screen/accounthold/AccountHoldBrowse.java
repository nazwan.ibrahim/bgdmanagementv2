package com.company.management.screen.accounthold;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.AccountHold;

@UiController("AccountHold.browse")
@UiDescriptor("account-hold-browse.xml")
@LookupComponent("accountHoldsTable")
public class AccountHoldBrowse extends StandardLookup<AccountHold> {
}