package com.company.management.screen.redemptionreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.RedemptionReport;

@UiController("RedemptionReport.edit")
@UiDescriptor("redemption-report-edit.xml")
@EditedEntityContainer("redemptionReportDc")
public class RedemptionReportEdit extends StandardEditor<RedemptionReport> {
}