package com.company.management.screen.bursagoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.BursaGoldWalletStatement;

@UiController("BursaGoldWalletStatement.edit")
@UiDescriptor("bursa-gold-wallet-statement-edit.xml")
@EditedEntityContainer("bursaGoldWalletStatementDc")
public class BursaGoldWalletStatementEdit extends StandardEditor<BursaGoldWalletStatement> {
}