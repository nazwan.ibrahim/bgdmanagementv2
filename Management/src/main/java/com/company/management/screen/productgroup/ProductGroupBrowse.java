package com.company.management.screen.productgroup;

import io.jmix.ui.screen.*;
import com.company.management.entity.ProductGroup;

@UiController("ProductGroup.browse")
@UiDescriptor("product-group-browse.xml")
@LookupComponent("table")
public class ProductGroupBrowse extends MasterDetailScreen<ProductGroup> {
}