package com.company.management.screen.sapfee2;

import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.ui.component.Button;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.SapFee2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@UiController("SapFee2.browse")
@UiDescriptor("sap-fee2-browse.xml")
@LookupComponent("sapFee2sTable")
public class SapFee2Browse extends StandardLookup<SapFee2> {
	private static final Logger log = LoggerFactory.getLogger(SapFee2Browse.class);
	@Autowired
	private TimeSource timeSource;
	BigDecimal sumAmountnSst = BigDecimal.ZERO;
	BigDecimal sumAllTrans = BigDecimal.ZERO;
	BigDecimal sumAllTrans1 = BigDecimal.ZERO;
	int incrementNum;

	DecimalFormat dfmillion = new DecimalFormat("000000000000.00");
	AtomicInteger x=new AtomicInteger(1);
	@Autowired
	private CollectionContainer<SapFee2> sapFee2sDc;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<SapFee2> sapFee2sDl;

	@Subscribe
	public void onBeforeShow(final BeforeShowEvent event) {
		sapFee2sDl.setParameter("typeFee1","-");
		sapFee2sDl.setFetchPlan("sapFee2-fetch-plan");
		sapFee2sDl.load();
	}




	@Subscribe("downloadBtn")
	public void onDownloadBtnClick(final Button.ClickEvent event) throws FileNotFoundException {
		String dateFormat = "yyyyMM";
		String dateFormatTxt = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat dateSimpleformatterTxt = new SimpleDateFormat(dateFormatTxt);
		String path = String.format("C:\\Users\\amir\\Downloads\\FEE_%s.txt", dateSimpleformatter.format(timeSource.currentTimestamp()));
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalSst = BigDecimal.ZERO;

//		String path="C:\\Users\\amir\\Downloads\\test.txt";

		List<SapFee2> sapFee2List = this.dataManager.load(SapFee2.class)
				.query("select e from SapFee2 e")
				.fetchPlan("sapFee2-fetch-plan")
				.list();
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
			sapFee2sDc.getItems().forEach(a -> {
//				if(!a.getTypeFee().equals("-")) {
					String getDate = dateSimpleformatterTxt.format(a.getTransactionDate());
					String getCreatedDate = getDate != null ? getDate : "";
					String getTypeCode = a.getTypeFee() != null ? a.getTypeFee() : "";
					BigDecimal getAmount = a.getAmount() != null ? a.getAmount() : BigDecimal.ZERO;
					BigDecimal getSst = a.getSst() != null ? a.getSst().abs() : BigDecimal.ZERO;
					BigDecimal amountAddSst = getAmount.add(getSst);
					sumAmountnSst = sumAmountnSst.add(amountAddSst);
					try {
						incrementNum = x.getAndIncrement();
						csvPrinter.printRecord(String.format("%06d", incrementNum), getCreatedDate, getTypeCode, dfmillion.format(getAmount.setScale(2, RoundingMode.UNNECESSARY)), dfmillion.format(getSst.setScale(2, RoundingMode.UNNECESSARY)), dfmillion.format(amountAddSst.setScale(2, RoundingMode.UNNECESSARY)));
					} catch (IOException e) {
//					log.error("IOException Error :"+e);
						log.info("Error While writing CSV ", e);
					}
//				}
			});
			for(SapFee2 sapFee2 : sapFee2sDc.getItems()){
				if(!sapFee2.getTypeFee().equals("-")) {
					totalAmount = totalAmount.add(sapFee2.getAmount() != null ? sapFee2.getAmount() : BigDecimal.ZERO);
					totalSst = totalSst.add(sapFee2.getSst() != null ? sapFee2.getSst().abs() : BigDecimal.ZERO);
				}
			}

			sumAllTrans = totalAmount.add(totalSst);
//			sumAllTrans1 = sumAllTrans1.add(sumAmountnSst);
			csvPrinter.printRecord(String.format("%06d", incrementNum),null,null,dfmillion.format(totalAmount.setScale(2,RoundingMode.UNNECESSARY)),dfmillion.format(totalSst.setScale(2,RoundingMode.UNNECESSARY)),dfmillion.format(sumAllTrans.setScale(2,RoundingMode.UNNECESSARY)));
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
		}
	}
}