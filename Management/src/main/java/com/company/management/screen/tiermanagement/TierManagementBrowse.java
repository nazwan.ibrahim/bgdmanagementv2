package com.company.management.screen.tiermanagement;

import com.company.management.txtFileConverter.Block;
import com.company.management.txtFileConverter.Board;
import com.company.management.txtFileConverter.Table;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.TierManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@UiController("TierManagement.browse")
@UiDescriptor("tier-management-browse.xml")
@LookupComponent("tierManagementsTable")
public class TierManagementBrowse extends StandardLookup<TierManagement> {
	private static final Logger log = LoggerFactory.getLogger(TierManagementBrowse.class);
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<TierManagement> tierManagementsTable;
	@Autowired
	private CollectionLoader<TierManagement> tierManagementsDl;
	@Autowired
	private Notifications notifications;
	@Autowired
	private CollectionContainer<TierManagement> tierManagementsDc;
	@Autowired
	private DataManager dataManager;




	@Subscribe("tierManagementsTable.edit")
	public void onTierManagementsTableEdit(Action.ActionPerformedEvent event) {
		TierManagement tierManagement = tierManagementsTable.getSingleSelected();
		TierManagementEdit tierManagementEdit = screenBuilders.screen(this)
				.withScreenClass(TierManagementEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					tierManagementsDl.load();
				})
				.build();
		assert tierManagement != null;
		tierManagementEdit.setTierManagement(tierManagement);
		tierManagementEdit.setEntityToEdit(tierManagement);
		tierManagementEdit.show();
	}

//	@Subscribe("downloadFile")
//	public void onDownloadFileClick(Button.ClickEvent event) throws IOException {
//		try{
//			String path="C:\\Users\\amir\\Downloads\\test.txt";
//			List<String> headersList = new ArrayList<>();
//			tierManagementsTable.getColumns().forEach(tierManagementColumn -> {
//				headersList.add(tierManagementColumn.getCaption());
//			});
//			List<List<String>> rowsList = new ArrayList<>();
//			tierManagementsDc.getMutableItems().forEach(tierManagement -> {
//				String code = tierManagement.getCode() != null ? tierManagement.getCode(): "-";
//				String name = tierManagement.getName() != null ? tierManagement.getName(): "-";
//				String description = tierManagement.getDescription() != null ? tierManagement.getDescription(): "-";
//				rowsList.add(Arrays.asList(code,name,description));
//			});
//
//			List<Integer> colAlignList = Arrays.asList(
//					Block.DATA_CENTER,
//					Block.DATA_CENTER,
//					Block.DATA_CENTER);
//			//bookmark 1
//			Board board = new Board(75);
//			String tableString = board.setInitialBlock(new Table(board, 75, headersList, rowsList).setColAlignsList(colAlignList).tableToBlocks()).build().getPreview();
//
//			// Create new file
////			String content = "This is the content to write into create file part 3";
////			String path="C:\\Users\\amir\\Downloads\\test.txt";
//			File file = new File(path);
//
//			// If file doesn't exists, then create it
//			if (!file.exists()) {
//				file.createNewFile();
//			}
//
//			FileWriter fw = new FileWriter(file.getAbsoluteFile());
//			BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
//
//			// Write in file
//			bw.write(tableString);
//
//			// Close connection
//			bw.close();
//			notifications.create()
//					.withContentMode(ContentMode.HTML)
//					.withType(Notifications.NotificationType.HUMANIZED)
//					.withPosition(Notifications.Position.MIDDLE_CENTER)
//					.withCaption("Report Finance Text file successfully download")
//					.show();
//
//		}
//		catch(Exception e){
//			log.info(String.valueOf(e));
//			notifications.create()
//					.withContentMode(ContentMode.HTML)
//					.withType(Notifications.NotificationType.ERROR)
//					.withPosition(Notifications.Position.MIDDLE_CENTER)
//					.withCaption("Report Finance Text file unsuccessfully download")
//					.show();
//		}
//
//	}
}