package com.company.management.screen.transactionhistoriesviewadmin;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionHistoriesViewAdmin;

@UiController("TransactionHistoriesViewAdmin.browse")
@UiDescriptor("transaction-histories-view-admin-browse.xml")
@LookupComponent("transactionHistoriesViewAdminsTable")
public class TransactionHistoriesViewAdminBrowse extends StandardLookup<TransactionHistoriesViewAdmin> {
}