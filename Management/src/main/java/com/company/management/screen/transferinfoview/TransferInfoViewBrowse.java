package com.company.management.screen.transferinfoview;

import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransferInfoView;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DecimalFormat;

@UiController("TransferInfoView.browse")
@UiDescriptor("transfer-info-view-browse.xml")
@LookupComponent("transferInfoViewsTable")
public class TransferInfoViewBrowse extends StandardLookup<TransferInfoView> {
	@Autowired
	private CollectionLoader<TransferInfoView> transferInfoViewsDl;

	DecimalFormat df = new DecimalFormat("#,###,##0.000000");

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		getScreenData().loadAll();
	}

	@Install(to = "transferInfoViewsTable.amount", subject = "formatter")
	private String transferInfoViewsTableAmountFormatter(Object object) {
		return df.format(object);
	}

	@Install(to = "transferInfoViewsTable", subject = "styleProvider")
	private String transferInfoViewsTableStyleProvider(TransferInfoView transferInfoView, String property) {
		if (property == null){

		}else if (property.equals("statusName")){
			switch (transferInfoView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}



}