package com.company.management.screen.cashwithdrawalreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.CashWithdrawalReport;

@UiController("CashWithdrawalReport.browse")
@UiDescriptor("cash-withdrawal-report-browse.xml")
@LookupComponent("cashWithdrawalReportsTable")
public class CashWithdrawalReportBrowse extends StandardLookup<CashWithdrawalReport> {
}