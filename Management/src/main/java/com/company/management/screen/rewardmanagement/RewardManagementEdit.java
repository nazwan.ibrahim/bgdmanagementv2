package com.company.management.screen.rewardmanagement;

import com.amazonaws.util.IOUtils;
import com.company.management.app.ActivityLogBean;
import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.TimeSource;
import io.jmix.email.EmailAttachment;
import io.jmix.email.EmailInfo;
import io.jmix.email.EmailInfoBuilder;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import io.jmix.ui.upload.TemporaryStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.*;

@UiController("RewardManagement.edit")
@UiDescriptor("reward-management-edit.xml")
@EditedEntityContainer("rewardManagementDc")
public class RewardManagementEdit extends StandardEditor<RewardManagement> {
    static Logger logger = LogManager.getLogger(RewardManagementEdit.class.getName());
    @Autowired TextField<String> campaignCodeField;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private InstanceContainer<RewardManagement> rewardManagementDc;
    @Autowired
    private CollectionContainer<RewardCriteria> rewardCriteriasDc;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomAmountDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomBuyDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomRedeemDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomSellDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomTopupDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomTransferDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomWithdrawDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomCretiriaDl;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private CollectionContainer<RtType> rtTypesUomBuyDc;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private TemporaryStorage temporaryStorage;
    @Autowired
    private InstanceContainer<ActivityLog> activityLogDc;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc;
    @Autowired
    private Notifications notifications;
    @Autowired
    private InstanceLoader<ActivityLog> activityLogDl;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Named("uploadFileTable.remove")
    private RemoveAction<ActivityLogFile> uploadFileTableRemove;
    @Autowired
    private Downloader downloader;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private TextArea<String> remarksFieldId;
    @Autowired
    private ActivityLogBean activityLogBean;
    @Autowired
    private ScreenValidation screenValidation;
    @Autowired
    private Form validateRemarksForm;
    @Autowired
    private DateField<Date> startDateField;
    @Autowired
    private DateField<Date> endDateField;
    @Autowired
    private TimeSource timeSource;
    @Autowired
    private Form form;

    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        rewardManagementDc.getItem().setBeneficiaries("Investor");
        rewardManagementDc.getItem().setMethod("Automated");

        rtTypesUomAmountDl.setParameter("code1","J01");
        rtTypesUomAmountDl.setParameter("code2","J02");
        rtTypesUomAmountDl.load();

        rtTypesUomBuyDl.setParameter("code1","J01");
        rtTypesUomBuyDl.setParameter("code2","J02");
        rtTypesUomBuyDl.setParameter("code3","J05");
        rtTypesUomBuyDl.load();

        rtTypesUomRedeemDl.setParameter("code1","J03");
        rtTypesUomRedeemDl.setParameter("code2","J05");
        rtTypesUomRedeemDl.load();

        rtTypesUomSellDl.setParameter("code1","J01");
        rtTypesUomSellDl.setParameter("code2","J02");
        rtTypesUomSellDl.setParameter("code3","J05");
        rtTypesUomSellDl.load();

        rtTypesUomTopupDl.setParameter("code1","J01");
        rtTypesUomTopupDl.setParameter("code2","J05");
        rtTypesUomTopupDl.load();

        rtTypesUomTransferDl.setParameter("code1","J01");
        rtTypesUomTransferDl.setParameter("code2","J05");
        rtTypesUomTransferDl.load();

        rtTypesUomWithdrawDl.setParameter("code1","J01");
        rtTypesUomWithdrawDl.setParameter("code2","J05");
        rtTypesUomWithdrawDl.load();

        rtTypesUomCretiriaDl.setParameter("code1", "J01");
        rtTypesUomCretiriaDl.setParameter("code2", "J02");
        rtTypesUomCretiriaDl.setParameter("code3", "J03");
        rtTypesUomCretiriaDl.setParameter("code5", "J05");
        rtTypesUomCretiriaDl.load();

        List<RtType> rtTypeList = this.dataManager.load(RtType.class).all().list();

        RewardCriteria rewardCriteriaBuy = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaBuy.setReward(rewardManagementDc.getItem());
        rewardCriteriaBuy.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F05")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaBuy);

        RewardCriteria rewardCriteriaSell = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaSell.setReward(rewardManagementDc.getItem());
        rewardCriteriaSell.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F06")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaSell);

        RewardCriteria rewardCriteriaTransfer = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaTransfer.setReward(rewardManagementDc.getItem());
        rewardCriteriaTransfer.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F03")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaTransfer);

        RewardCriteria rewardCriteriaRedeem = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaRedeem.setReward(rewardManagementDc.getItem());
        rewardCriteriaRedeem.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F04")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaRedeem);

        RewardCriteria rewardCriteriaTopup = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaTopup.setReward(rewardManagementDc.getItem());
        rewardCriteriaTopup.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F01")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaTopup);

        RewardCriteria rewardCriteriaWithdraw = this.dataManager.create(RewardCriteria.class);
        rewardCriteriaWithdraw.setReward(rewardManagementDc.getItem());
        rewardCriteriaWithdraw.setCriteria(rtTypeList.stream().filter(f -> f.getCode().equals("F02")).toList().get(0));
        rewardCriteriasDc.getMutableItems().add(rewardCriteriaWithdraw);

        activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
        activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());

        uploadMultipleFile();
    }

    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
                activityLogFile.setActivityLog(activityLogDc.getItem());
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
                activityLogFile.setAction(rtType("ACT01"));

                activityLogFilesDc.getMutableItems().add(activityLogFile);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }
    private RtType rtType(String code){
        String query = "select e from RtType e where e.code =:code";
        return this.dataManager.load(RtType.class)
                .query(query)
                .parameter("code", code)
                .one();
    }
    @Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        Button btn = uiComponents.create(Button.class);
        btn.setCaption("Delete");
        btn.addClickListener(clickEvent -> {
            uploadFileTableRemove.execute();
        });
        return btn;
    }
    @Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
    private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }
    @Subscribe("commitBtn")
    public void onCommitBtnClick(final Button.ClickEvent event) {
        int countMinValue = 0; int countActive = 0; int countUom = 0;
        for(RewardCriteria rewardCriteria : rewardCriteriasDc.getMutableItems()){
            if(rewardCriteria.getMinValue()!=null && !rewardCriteria.getMinValue().toString().equals("")){
                countMinValue++;
            }
            if(rewardCriteria.getActive()!=null && rewardCriteria.getActive()){
                countActive++;
            }
            if(rewardCriteria.getUom()!=null && !rewardCriteria.getUom().toString().equals("")){
                countUom++;
            }
        }

        ValidationErrors errors = screenValidation.validateUiComponents(validateRemarksForm);
        ValidationErrors errorsForm = screenValidation.validateUiComponents(form);
        if (!errors.isEmpty() || !errorsForm.isEmpty() || activityLogFilesDc.getItems().isEmpty()) {
//            screenValidation.showValidationErrors(this, errors);
//            screenValidation.showValidationErrors(this, errorsForm);
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.BOTTOM_RIGHT)
                    .withCaption("Please fill in all the Mandatory fields.")
                    .show();
        }
//        else if(activityLogFilesDc.getItems().isEmpty()){
//            notifications.create()
//                    .withContentMode(ContentMode.HTML)
//                    .withType(Notifications.NotificationType.ERROR)
//                    .withPosition(Notifications.Position.MIDDLE_CENTER)
//                    .withCaption("Upload document is required")
//                    .show();
//        }
        else if(countMinValue>2 || countActive>2 || countUom>2){
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.ERROR)
                    .withPosition(Notifications.Position.MIDDLE_CENTER)
                    .withCaption("Reward criteria can be selected up to maximum 2 only")
                    .show();
        }else{
            dialogs.createOptionDialog()
                    .withCaption("Sign Up Reward Confirmation")
                    .withContentMode(ContentMode.HTML)
                    .withMessage("Click Confirm or Cancel button to proceed.")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withPrimary(true)
                                    .withCaption("Confirm")
                                    .withHandler(e -> saveData()),
                            new DialogAction(DialogAction.Type.NO)
                                    .withCaption("Cancel")
                    )
                    .show();
        }
    }
    private void saveData(){
        if(!remarksFieldId.isEmpty()) {
            rewardManagementDc.getItem().setStatus(rtType("G05"));
            rewardManagementDc.getItem().setRewardType(rtType("F12/01"));
            this.dataManager.save(rewardManagementDc.getItem());
            rewardCriteriasDc.getMutableItems().forEach(rewardCriteria -> {
                this.dataManager.save(rewardCriteria);
            });
            activityLogBean.makerActivity("MENU09-01", "LOG08-01",
                    rewardManagementDc.getItem().getCampaignName()+"("+rewardManagementDc.getItem().getCampaignCode()+")",
                        "RewardManagement", rewardManagementDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());

            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.MIDDLE_CENTER)
                    .withCaption("Sign Up Reward Successfully Created")
                    .show();
            closeWithCommit();
        }else{
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.MIDDLE_CENTER)
                    .withCaption("Sign Up Reward Not Successfully Created")
                    .show();
            disableCommitActions();
        }
    }
    private void validatePromoCode(String value){
        List<RewardManagement> list = this.dataManager.load(RewardManagement.class)
                .query("select e from RewardManagement e where e.campaignCode = :promoCode")
                .parameter("promoCode", value).list();
        if(list.size()>0){
            for(RewardManagement rewardManagement : list){
                if(rewardManagement.getCampaignCode().equals(value)){
                    notifications.create()
                            .withContentMode(ContentMode.HTML)
                            .withType(Notifications.NotificationType.ERROR)
                            .withPosition(Notifications.Position.MIDDLE_CENTER)
                            .withCaption("The Prome Code Already Exists : "+value)
                            .show();
                    campaignCodeField.setValue(null);
                }
            }
        }
    }
    @Subscribe("campaignCodeField")
    public void onCampaignCodeFieldValueChange(final HasValue.ValueChangeEvent<String> event) {
        if(event.getValue()!=null){
            validatePromoCode(event.getValue());
        }
    }
    @Subscribe
    public void onInit(final InitEvent event) {
        startDateField.setRangeStart(timeSource.currentTimestamp());
    }
    @Subscribe("startDateField")
    public void onStartDateFieldValueChange(final HasValue.ValueChangeEvent<Date> event) {
        endDateField.setRangeStart(event.getValue());
    }

//    @Install(to = "rewardCriteriaTable.uom", subject = "columnGenerator")
//    private Component rewardCriteriaTableUomColumnGenerator(final RewardCriteria rewardCriteria) {
//        ComboBox comboBox = uiComponents.create(ComboBox.class);
//        comboBox.setEditable(true);
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F05")){
//            comboBox.setOptionsList(rtTypesUomBuyDc.getItems());
//        }
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F06")){
//            comboBox.setOptionsList(rtTypesUomSellDc.getItems());
//        }
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F03")){
//            comboBox.setOptionsList(rtTypesUomTransferDc.getItems());
//        }
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F04")){
//            comboBox.setOptionsList(rtTypesUomRedeemDc.getItems());
//        }
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F01")){
//            comboBox.setOptionsList(rtTypesUomTopupDc.getItems());
//        }
//        if(rewardCriteria.getCriteria().getCode().equalsIgnoreCase("F02")){
//            comboBox.setOptionsList(rtTypesUomWithdrawDc.getItems());
//        }
//        return comboBox;
//    }
}