package com.company.management.screen.rttype;

import io.jmix.ui.screen.*;
import com.company.management.entity.RtType;

@UiController("RtType.edit")
@UiDescriptor("rt-type-edit.xml")
@EditedEntityContainer("rtTypeDc")
public class RtTypeEdit extends StandardEditor<RtType> {
}