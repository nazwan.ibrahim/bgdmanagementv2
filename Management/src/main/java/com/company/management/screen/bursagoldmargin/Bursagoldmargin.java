package com.company.management.screen.bursagoldmargin;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Bursagoldmargin")
@UiDescriptor("bursaGoldMargin.xml")
@DashboardWidget(name = "Bursagoldmargin")
public class Bursagoldmargin extends ScreenFragment {

}