package com.company.management.screen.shariahrecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecord;

@UiController("ShariahRecord.browse")
@UiDescriptor("shariah-record-browse.xml")
@LookupComponent("shariahRecordsTable")
public class ShariahRecordBrowse extends StandardLookup<ShariahRecord> {
}