package com.company.management.screen.accountbgd;

import com.company.management.entity.wallet.AccountBgd;
import io.jmix.ui.screen.*;

@UiController("AccountBgd.browse")
@UiDescriptor("account-bgd-browse.xml")
@LookupComponent("accountBgdsTable")
public class AccountBgdBrowse extends StandardLookup<AccountBgd> {
    @Subscribe
    public void onInit(InitEvent event) {
        
    }
}