package com.company.management.screen.regtype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.RegType;

@UiController("RegType.browse")
@UiDescriptor("reg-type-browse.xml")
@LookupComponent("regTypesTable")
public class RegTypeBrowse extends StandardLookup<RegType> {
}