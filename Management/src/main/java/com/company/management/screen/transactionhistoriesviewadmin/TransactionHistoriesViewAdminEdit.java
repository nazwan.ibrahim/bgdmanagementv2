package com.company.management.screen.transactionhistoriesviewadmin;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionHistoriesViewAdmin;

@UiController("TransactionHistoriesViewAdmin.edit")
@UiDescriptor("transaction-histories-view-admin-edit.xml")
@EditedEntityContainer("transactionHistoriesViewAdminDc")
public class TransactionHistoriesViewAdminEdit extends StandardEditor<TransactionHistoriesViewAdmin> {
}