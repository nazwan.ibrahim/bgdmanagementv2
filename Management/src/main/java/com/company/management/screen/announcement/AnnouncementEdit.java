package com.company.management.screen.announcement;

import io.jmix.ui.screen.*;
import com.company.management.entity.Announcement;

@UiController("Announcement.edit")
@UiDescriptor("announcement-edit.xml")
@EditedEntityContainer("announcementDc")
public class AnnouncementEdit extends StandardEditor<Announcement> {
}