package com.company.management.screen.equilibriumshariah;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.EquilibriumShariah;

@UiController("EquilibriumShariah.edit")
@UiDescriptor("equilibrium-shariah-edit.xml")
@EditedEntityContainer("equilibriumShariahDc")
public class EquilibriumShariahEdit extends StandardEditor<EquilibriumShariah> {
}