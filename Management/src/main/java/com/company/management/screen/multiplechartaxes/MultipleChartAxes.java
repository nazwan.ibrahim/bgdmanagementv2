package com.company.management.screen.multiplechartaxes;

import com.company.management.entity.GoldPriceHistory;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("MultipleChartAxes")
@UiDescriptor("Multiple_Chart_Axes.xml")
public class MultipleChartAxes extends Screen {
	@Autowired
	private CollectionLoader<GoldPriceHistory> goldPriceHistoryDl;

	@Subscribe
	public void onInit(InitEvent event) {
		goldPriceHistoryDl.load();
	}
}