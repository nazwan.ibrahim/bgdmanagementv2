package com.company.management.screen.back2backreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.Back2BackReport;

@UiController("Back2BackReport.edit")
@UiDescriptor("back2-back-report-edit.xml")
@EditedEntityContainer("back2BackReportDc")
public class Back2BackReportEdit extends StandardEditor<Back2BackReport> {
}