package com.company.management.screen.shariahrecordsubsequent;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecordSubsequent;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ShariahRecordSubsequentGold.browse")
@UiDescriptor("shariah-record-subsequent-gold-browse.xml")
@LookupComponent("shariahRecordSubsequentsTable")
public class ShariahRecordSubsequentGoldBrowse extends StandardLookup<ShariahRecordSubsequent> {
	@Autowired
	private TabSheet goldTab;
	@Autowired
	private CollectionLoader<ShariahRecordSubsequent> shariahRecordSubsequentsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<ShariahRecordSubsequent> shariahRecordSubsequentsTable;
	@Autowired
	private GroupTable<ShariahRecordSubsequent> shariahRecordSubsequentsTableDinar;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(goldTab.getTab("goldBar").getName().equals("goldBar")){
			shariahRecordSubsequentsDl.setParameter("deliveryRecordReserveRecordTypeCode1","GT01");
			shariahRecordSubsequentsDl.load();
		}else {
			shariahRecordSubsequentsDl.setParameter("deliveryRecordReserveRecordTypeCode1","GT02");
			shariahRecordSubsequentsDl.load();
		}
	}


	private void goToEditPage(ShariahRecordSubsequent shariahRecordSubsequent){
		ShariahRecordSubsequentGoldEdit shariahRecordSubsequentGoldEdit = screenBuilders.screen(this)
				.withScreenClass(ShariahRecordSubsequentGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
					shariahRecordSubsequentsDl.load();
				})
				.build();
		assert shariahRecordSubsequent != null;
		shariahRecordSubsequentGoldEdit.setEntityToEdit(shariahRecordSubsequent);
		shariahRecordSubsequentGoldEdit.show();
	}

	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			shariahRecordSubsequentsDl.setParameter("deliveryRecordReserveRecordTypeCode1","GT01");
			shariahRecordSubsequentsDl.load();
		}else {
			shariahRecordSubsequentsDl.setParameter("deliveryRecordReserveRecordTypeCode1","GT02");
			shariahRecordSubsequentsDl.load();
		}
	}

	@Subscribe("shariahRecordSubsequentsTable.edit")
	public void onShariahRecordSubsequentsTableEdit(Action.ActionPerformedEvent event) {
		ShariahRecordSubsequent shariahRecordSubsequent = shariahRecordSubsequentsTable.getSingleSelected();
		goToEditPage(shariahRecordSubsequent);
	}

	@Subscribe("shariahRecordSubsequentsTableDinar.edit")
	public void onShariahRecordSubsequentsTableDinarEdit(Action.ActionPerformedEvent event) {
		ShariahRecordSubsequent shariahRecordSubsequent = shariahRecordSubsequentsTableDinar.getSingleSelected();
		goToEditPage(shariahRecordSubsequent);
	}

}