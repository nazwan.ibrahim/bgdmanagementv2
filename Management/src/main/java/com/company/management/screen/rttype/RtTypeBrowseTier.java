package com.company.management.screen.rttype;

import com.company.management.entity.SupportDoc;
import com.company.management.entity.TierManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.RtType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@UiController("RtType-tier.browse")
@UiDescriptor("rt-type-browse-tier.xml")
@LookupComponent("rtTypesTable")
public class RtTypeBrowseTier extends StandardLookup<RtType> {

	private TierManagement tierManagement;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<RtType> rtTypesTable;

	private String typeCode;
	@Autowired
	private CollectionLoader<RtType> rtTypesDlTier;

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public TierManagement getTierManagement() {
		return tierManagement;
	}

	public void setTierManagement(TierManagement tierManagement) {
		this.tierManagement = tierManagement;
	}

	@Autowired
	private DataContext dataContext;

	public void setParentDataContext(DataContext parentDataContext) {
		dataContext.setParent(parentDataContext);
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		rtTypesDlTier.setParameter("typeGroupCode1",getTypeCode());
		rtTypesDlTier.load();
	}


//	@Subscribe
//	public void onBeforeShow(BeforeShowEvent event) {
//		if(getTypeGroupCode() != null) {
//			List<RtType> typeList = this.dataManager.load(RtType.class)
//					.query("select e from RtType e where e.typeGroup.code = :typeGroupCode")
//					.parameter("typeGroupCode", getTypeGroupCode())
//					.list();
//
//			rtTypesDc.setItems(typeList);
//			rtTypesDl.getContainer().setItems(typeList);
//			rtTypesDl.load();
//		}
//	}



	@Subscribe("selectRtType")
	public void onSelectRtTypeClick(Button.ClickEvent event) {
		SupportDoc supportDoc = this.dataManager.create(SupportDoc.class);
		supportDoc.setTierManagement(getTierManagement());
		supportDoc.setType(rtTypesTable.getSingleSelected());
		dataManager.save(supportDoc);

//		dataManager.save(supportDoc);
		close(StandardOutcome.CLOSE);
	}


}