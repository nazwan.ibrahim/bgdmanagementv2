package com.company.management.screen.supportdoctype;

import io.jmix.ui.screen.*;
import com.company.management.entity.DocumentType;

@UiController("SupportDocType.edit")
@UiDescriptor("support-doc-type-edit.xml")
@EditedEntityContainer("supportDocTypeDc")
public class SupportDocTypeEdit extends StandardEditor<DocumentType> {
}