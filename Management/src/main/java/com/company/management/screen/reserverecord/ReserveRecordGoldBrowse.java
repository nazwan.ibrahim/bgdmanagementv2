package com.company.management.screen.reserverecord;

import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ReserveRecordGold.browse")
@UiDescriptor("reserve-record-gold-browse.xml")
@LookupComponent("reserveRecordsTable")
public class ReserveRecordGoldBrowse extends StandardLookup<ReserveRecord> {
	@Autowired
	private TabSheet goldTab;
	@Autowired
	private CollectionLoader<ReserveRecord> reserveRecordsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<ReserveRecord> reserveRecordsTable;
	@Autowired
	private GroupTable<ReserveRecord> reserveRecordsTableDinar;
	@Autowired
	private DataManager dataManager;

	public String goldType;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(goldTab.getTab("goldBar").getName().equals("goldBar")){
			reserveRecordsDl.setParameter("typeCode1","GT01");
			reserveRecordsDl.load();
			goldType = "goldBar";
		}else {
			reserveRecordsDl.setParameter("typeCode1","GT02");
			reserveRecordsDl.load();
			goldType = "goldDinar";
		}
	}
	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			reserveRecordsDl.setParameter("typeCode1","GT01");
			reserveRecordsDl.load();
			goldType = "goldBar";
		}else {
			reserveRecordsDl.setParameter("typeCode1","GT02");
			reserveRecordsDl.load();
			goldType = "goldDinar";
		}
	}
	private void goToEditPage(ReserveRecord reserveRecord){
		ReserveRecordGoldEdit reserveRecordGoldEdit = screenBuilders.screen(this)
				.withScreenClass(ReserveRecordGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					if(goldType.equals("goldBar")) {
						goldType = "goldBar";
					}else{
						goldType = "goldDinar";
					}
					getScreenData().loadAll();
					reserveRecordsDl.load();
				})
				.build();
		assert reserveRecord != null;
		reserveRecordGoldEdit.setEntityToEdit(reserveRecord);
		reserveRecordGoldEdit.setGoldType(goldType);
		reserveRecordGoldEdit.show();
	}
	@Subscribe("reserveRecordsTable.create")
	public void onReserveRecordsTableCreate(Action.ActionPerformedEvent event) {
		ReserveRecord reserveRecord = dataManager.create(ReserveRecord.class);
		goToEditPage(reserveRecord);
	}
	@Subscribe("reserveRecordsTable.edit")
	public void onReserveRecordsTableEdit(Action.ActionPerformedEvent event) {
		ReserveRecord reserveRecord = reserveRecordsTable.getSingleSelected();
		goToEditPage(reserveRecord);
	}
	@Subscribe("reserveRecordsTableDinar.create")
	public void onReserveRecordsTableDinarCreate(Action.ActionPerformedEvent event) {
		ReserveRecord reserveRecord = dataManager.create(ReserveRecord.class);
		goToEditPage(reserveRecord);
	}
	@Subscribe("reserveRecordsTableDinar.edit")
	public void onReserveRecordsTableDinarEdit(Action.ActionPerformedEvent event) {
		ReserveRecord reserveRecord = reserveRecordsTableDinar.getSingleSelected();
		goToEditPage(reserveRecord);
	}
}