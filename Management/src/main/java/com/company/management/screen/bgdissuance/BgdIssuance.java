package com.company.management.screen.bgdissuance;

import com.company.management.entity.BgdIssuanceLoadRecord;

import com.company.management.service.BgdIssuanceService;
import io.jmix.core.Metadata;
import io.jmix.ui.component.*;
import io.jmix.ui.component.data.table.ContainerGroupTableItems;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@UiController("Bgdissuance")
@UiDescriptor("BgdIssuance.xml")
public class BgdIssuance extends Screen {
	@Autowired
	private BgdIssuanceService bgdIssuanceService;
	@Autowired
	private Metadata metadata;
	@Autowired
	private CollectionContainer<BgdIssuanceLoadRecord> bgdIssuanceLoadRecordsDc;
	@Autowired
	private GroupTable<BgdIssuanceLoadRecord> bgdIssuanceLoadRecordsTable;
	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		List<BgdIssuanceLoadRecord> entities = bgdIssuanceService.getCombinedData();
		List<BgdIssuanceLoadRecord> dtoEntities = entities.stream()
				.map(dto -> {
					BgdIssuanceLoadRecord entity = metadata.create(BgdIssuanceLoadRecord.class);
					if(dto.getTimeStamp() != null) {
						entity.setTimeStamp(dto.getTimeStamp());
					}else {
						entity.setTimeStamp(null);
					}
					if(dto.getDescription() != null) {
						entity.setDescription(dto.getDescription());
					}else {
						entity.setDescription("-");
					}
					if(dto.getCode() != null) {
						entity.setCode(dto.getCode());
					}else {
						entity.setCode("-");
					}
					if(dto.getGoldType() != null) {
						entity.setGoldType(dto.getGoldType());
					}else {
						entity.setGoldType("-");
					}
					if(dto.getIssuanceNo() != null) {
						entity.setIssuanceNo(dto.getIssuanceNo());
					}else {
						entity.setIssuanceNo("-");
					}
					if(dto.getTradable() != null) {
						entity.setTradable(dto.getTradable());
					}else {
						entity.setTradable(BigDecimal.ZERO);
					}
					if(dto.getTradableUnit() != null) {
						entity.setTradableUnit(dto.getTradableUnit());
					}else {
						entity.setTradableUnit(BigDecimal.ZERO);
					}
					return entity;
				})
				.toList();
		Collection<BgdIssuanceLoadRecord> collection = new ArrayList<>(dtoEntities);
		bgdIssuanceLoadRecordsDc.setItems(collection);
		bgdIssuanceLoadRecordsTable.setItems(new ContainerGroupTableItems<>(bgdIssuanceLoadRecordsDc));
	}
	@Subscribe("load")
	public void onLoadClick(Button.ClickEvent event) {
		List<BgdIssuanceLoadRecord> entities = bgdIssuanceService.getCombinedData();
		List<BgdIssuanceLoadRecord> dtoEntities = entities.stream()
				.map(dto -> {
					BgdIssuanceLoadRecord entity = metadata.create(BgdIssuanceLoadRecord.class);
					entity.setTimeStamp(dto.getTimeStamp());
					entity.setDescription(dto.getDescription());
					entity.setCode(dto.getCode());
					entity.setGoldType(dto.getGoldType());
					entity.setIssuanceNo(dto.getIssuanceNo());
					entity.setTradable(dto.getTradable());
					entity.setTradableUnit(dto.getTradableUnit());
					return entity;
				})
				.toList();
		Collection<BgdIssuanceLoadRecord> collection = new ArrayList<>(dtoEntities);
		bgdIssuanceLoadRecordsDc.setItems(collection);
		bgdIssuanceLoadRecordsTable.setItems(new ContainerGroupTableItems<>(bgdIssuanceLoadRecordsDc));
	}

	@Install(to = "bgdIssuanceLoadRecordsTable.tradableUnit", subject = "valueProvider")
	private Object bgdIssuanceLoadRecordsTableTradableUnitValueProvider(BgdIssuanceLoadRecord bgdIssuanceLoadRecord) {
		if(bgdIssuanceLoadRecordsDc.getItem().getTradable() != null) {
			BigDecimal unit = bgdIssuanceLoadRecordsDc.getItem().getTradable();
			BigDecimal divide = unit.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
			BigDecimal totalTradable = divide.setScale(0);
			return totalTradable;
		}else{
			return null;
		}
	}
}