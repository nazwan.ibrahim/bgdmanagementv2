package com.company.management.screen.goldpricehistory;

import io.jmix.ui.screen.*;
import com.company.management.entity.GoldPriceHistory;

@UiController("GoldPriceHistory.browse")
@UiDescriptor("gold-price-history-browse.xml")
@LookupComponent("goldPriceHistoriesTable")
public class GoldPriceHistoryBrowse extends StandardLookup<GoldPriceHistory> {
}