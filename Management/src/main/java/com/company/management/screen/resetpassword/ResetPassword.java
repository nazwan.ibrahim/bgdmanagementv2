package com.company.management.screen.resetpassword;

import com.company.management.app.EmailBean;
import com.company.management.app.PasswordRandomGenerator;
import com.company.management.entity.User;
import com.company.management.entity.registrationdb.UserProfile;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.SystemAuthenticator;
import io.jmix.securityui.action.ChangePasswordAction;
import io.jmix.securityui.action.ResetPasswordAction;
import io.jmix.securityui.screen.changepassword.ChangePasswordDialog;
import io.jmix.ui.Notifications;
import io.jmix.ui.Screens;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@UiController("ResetPassword")
@UiDescriptor("reset-password.xml")
public class ResetPassword extends Screen {
    @Autowired
    private TextField<String> email;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private Notifications notifications;
    @Autowired
    private PasswordRandomGenerator passwordRandomGenerator;
    @Autowired
    private EmailBean emailBean;
    private static final Logger logger = LoggerFactory.getLogger(ResetPassword.class);
    @Autowired
    private SystemAuthenticator systemAuthenticator;
    @Autowired
    private CollectionContainer<User> usersDc;
    @Autowired
    private CollectionLoader<User> usersDl;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Authenticated
    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        usersDl.setParameter("system1",true);
        usersDl.load();
    }
    @Authenticated
    @Subscribe("resetPassword")
    public void onResetPasswordClick(final Button.ClickEvent event) {
        try{
            //generate random password
            String newPassword = passwordRandomGenerator.generateSecurePassword();
            String[] text = newPassword.split(",");
            if(text[0].equals("success")){
                //update user password
                User user = this.dataManager.load(User.class)
                        .query("select e from User e where e.username = :email")
                        .parameter("email", email.getValue())
                        .one();
                user.setPassword(passwordEncoder.encode(text[1]));
                this.dataManager.save(user);
                //generate email
                String status = emailBean.generateEmailResetPassword(user.getUsername(), text[1]);
                if(status.equals("success")){
                    notifications.create()
                            .withContentMode(ContentMode.HTML)
                            .withType(Notifications.NotificationType.HUMANIZED)
                            .withCaption("Email has been sent")
                            .show();
                    close(StandardOutcome.COMMIT);
                }
            }
        }catch (Exception e){
            logger.error(e.getMessage());
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.ERROR)
                    .withCaption("Email not exists")
                    .show();
            close(StandardOutcome.CLOSE);
        }
    }
}