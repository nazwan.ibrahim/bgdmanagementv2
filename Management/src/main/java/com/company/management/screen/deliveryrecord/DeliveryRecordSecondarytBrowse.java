package com.company.management.screen.deliveryrecord;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecord;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("DeliveryRecordSecondary.browse")
@UiDescriptor("delivery-record-secondary-browse.xml")
@LookupComponent("deliveryRecordsTable")
public class DeliveryRecordSecondarytBrowse extends StandardLookup<DeliveryRecord> {
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private CollectionLoader<DeliveryRecord> deliveryRecordsDl;
	@Autowired
	private TabSheet goldTab;
	@Autowired
	private Table<DeliveryRecord> deliveryRecordsTableGoldDinar;
	@Autowired
	private Table<DeliveryRecord> deliveryRecordsTable;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(goldTab.getTab("goldBar").getName().equals("goldBar")){
			deliveryRecordsDl.setParameter("purchaseRecordTypeCode1","GT01");
			deliveryRecordsDl.setParameter("purchaseRecordIssuanceTypeCode1","ITF/02");
			deliveryRecordsDl.load();
		}else {
			deliveryRecordsDl.setParameter("purchaseRecordTypeCode1","GT02");
			deliveryRecordsDl.setParameter("purchaseRecordIssuanceTypeCode1","ITF/02");
			deliveryRecordsDl.load();
		}
	}
	private void goToEditPage(DeliveryRecord deliveryRecord1){
		DeliveryRecordGoldEdit deliveryRecordGoldEdit = screenBuilders.screen(this)
				.withScreenClass(DeliveryRecordGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
					deliveryRecordsDl.load();
				})
				.build();
		assert deliveryRecord1 != null;
		deliveryRecordGoldEdit.setEntityToEdit(deliveryRecord1);
		deliveryRecordGoldEdit.show();
	}

	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			deliveryRecordsDl.setParameter("purchaseRecordTypeCode1","GT01");
			deliveryRecordsDl.setParameter("purchaseRecordIssuanceTypeCode1","ITF/02");
			deliveryRecordsDl.load();
		}else {
			deliveryRecordsDl.setParameter("purchaseRecordTypeCode1","GT02");
			deliveryRecordsDl.setParameter("purchaseRecordIssuanceTypeCode1","ITF/02");
			deliveryRecordsDl.load();
		}
	}

	@Subscribe("deliveryRecordsTable.edit")
	public void onDeliveryRecordsTableEdit(Action.ActionPerformedEvent event) {
		DeliveryRecord deliveryRecord = deliveryRecordsTable.getSingleSelected();
		goToEditPage(deliveryRecord);
	}

	@Subscribe("deliveryRecordsTableGoldDinar.edit")
	public void onDeliveryRecordsTableGoldDinarEdit(Action.ActionPerformedEvent event) {
		DeliveryRecord deliveryRecord = deliveryRecordsTableGoldDinar.getSingleSelected();
		goToEditPage(deliveryRecord);
	}

	@Install(to = "deliveryRecordsTable", subject = "styleProvider")
	private String deliveryRecordsTableStyleProvider(DeliveryRecord deliveryRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (deliveryRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "deliveryRecordsTableGoldDinar", subject = "styleProvider")
	private String deliveryRecordsTableGoldDinarStyleProvider(DeliveryRecord deliveryRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (deliveryRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

}