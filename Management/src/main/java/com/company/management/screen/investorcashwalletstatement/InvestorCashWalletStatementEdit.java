package com.company.management.screen.investorcashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.InvestorCashWalletStatement;

@UiController("InvestorCashWalletStatement.edit")
@UiDescriptor("investor-cash-wallet-statement-edit.xml")
@EditedEntityContainer("investorCashWalletStatementDc")
public class InvestorCashWalletStatementEdit extends StandardEditor<InvestorCashWalletStatement> {
}