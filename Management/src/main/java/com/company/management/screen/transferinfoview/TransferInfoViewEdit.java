package com.company.management.screen.transferinfoview;

import com.company.management.app.AdminManagementProcess;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.screen.tiermanagement.TierManagementBrowse;
import com.company.management.screen.tiermanagement.TierManagementEdit;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.ComboBox;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransferInfoView;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@UiController("TransferInfoView.edit")
@UiDescriptor("transfer-info-view-edit.xml")
@EditedEntityContainer("transferInfoViewDc")
public class TransferInfoViewEdit extends StandardEditor<TransferInfoView> {
	@Autowired
	private CollectionLoader<TransStatus> transStatusesDl;
	@Autowired
	private ComboBox<String> approvalTransfer;
	@Autowired
	private CollectionContainer<TransStatus> transStatusesDc;
	@Autowired
	private DataManager dataManager;

	@Autowired
	private AdminManagementProcess adminManagementProcess;
	@Autowired
	private ScreenBuilders screenBuilders;



	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		transStatusesDl.load();

		List<String> optionListData = new ArrayList<>();

		TransStatus pendingStatus = this.dataManager.load(TransStatus.class)
				.query("select e from TransStatus e where e.code = :code")
				.parameter("code", "G05")
				.one();
		optionListData.add(pendingStatus.getName());

		TransStatus approvedStatus = this.dataManager.load(TransStatus.class)
				.query("select e from TransStatus e where e.code = :code")
				.parameter("code", "G06")
				.one();
		optionListData.add(approvedStatus.getName());

		TransStatus rejectStatus = this.dataManager.load(TransStatus.class)
				.query("select e from TransStatus e where e.code = :code")
				.parameter("code", "G07")
				.one();
		optionListData.add(rejectStatus.getName());
//
		approvalTransfer.setOptionsList(optionListData);
	}

//	@Subscribe("submitBtn")
//	public void onSubmitBtnClick(Button.ClickEvent event) {
//
//		if(approvalTransfer.getValue().equals("Approved")){
//			getEditedEntity().setStatusCode("G06");
//			adminManagementProcess.approvedWalletTransfer(getEditedEntity());
//		}else{
//			getEditedEntity().setStatusCode("G07");
//			adminManagementProcess.approvedWalletTransfer(getEditedEntity());
//		}
//		close(StandardOutcome.CLOSE);
//	}

}