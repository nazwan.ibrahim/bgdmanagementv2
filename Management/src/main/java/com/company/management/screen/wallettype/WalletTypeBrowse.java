package com.company.management.screen.wallettype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.WalletType;

@UiController("WalletType.browse")
@UiDescriptor("wallet-type-browse.xml")
@LookupComponent("walletTypesTable")
public class WalletTypeBrowse extends StandardLookup<WalletType> {
}