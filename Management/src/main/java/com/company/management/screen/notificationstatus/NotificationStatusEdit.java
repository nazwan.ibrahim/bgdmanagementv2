package com.company.management.screen.notificationstatus;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationStatus;

@UiController("NotificationStatus.edit")
@UiDescriptor("notification-status-edit.xml")
@EditedEntityContainer("notificationStatusDc")
public class NotificationStatusEdit extends StandardEditor<NotificationStatus> {
}