package com.company.management.screen.wallettype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.WalletType;

@UiController("WalletType.edit")
@UiDescriptor("wallet-type-edit.xml")
@EditedEntityContainer("walletTypeDc")
public class WalletTypeEdit extends StandardEditor<WalletType> {
}