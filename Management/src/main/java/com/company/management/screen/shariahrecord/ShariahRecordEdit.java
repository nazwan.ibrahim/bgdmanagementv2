package com.company.management.screen.shariahrecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecord;

@UiController("ShariahRecord.edit")
@UiDescriptor("shariah-record-edit.xml")
@EditedEntityContainer("shariahRecordDc")
public class ShariahRecordEdit extends StandardEditor<ShariahRecord> {
}