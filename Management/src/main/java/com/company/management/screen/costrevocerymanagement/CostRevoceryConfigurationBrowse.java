package com.company.management.screen.costrevocerymanagement;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.CostRevoceryManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.Objects;

@UiController("CostRevoceryConfiguration.browse")
@UiDescriptor("cost-revocery-configuration-browse.xml")
@LookupComponent("costActiveTable")
public class CostRevoceryConfigurationBrowse extends StandardLookup<CostRevoceryManagement> {
    @Autowired
    private ScreenBuilders screenBuilders;

    @Autowired
    private Table<CostRevoceryManagement> costActiveTable;

    @Autowired
    private Table<CostRevoceryManagement> costPendingTable;

    @Autowired
    private Environment environment;
    @Autowired
    private CollectionLoader<CostRevoceryManagement> costActiveDl;
    @Autowired
    private CollectionLoader<CostRevoceryManagement> costPendingDl;
    @Autowired
    private CollectionLoader<CostRevoceryManagement> costLogsDl;

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        costActiveDl.setParameter("activeCode", "G04");
        costActiveDl.setParameter("marginBuyCode", "C05");
        costActiveDl.setParameter("marginSellCode", "C06");
        costActiveDl.load();

        costPendingDl.setParameter("pendingCode", "G05");
        costPendingDl.setParameter("approvedCode", "G06");
        costPendingDl.setParameter("marginBuyCode2", "C05");
        costPendingDl.setParameter("marginSellCode2", "C05");
        costPendingDl.load();

        costLogsDl.setParameter("marginBuyCode3","C05");
        costLogsDl.setParameter("marginSellCode3","C06");
        costLogsDl.load();
    }

    @Subscribe("costActiveTable.edit")
    public void onCostActiveTableEdit(Action.ActionPerformedEvent event) {
        CostRevoceryManagementEdit costRevoceryManagementEdit = screenBuilders.screen(this)
                .withScreenClass(CostRevoceryManagementEdit.class)
                .withAfterCloseListener(costRevoceryManagementEditAfterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        costRevoceryManagementEdit.setEntityToEdit(Objects.requireNonNull(costActiveTable.getSingleSelected()));
        costRevoceryManagementEdit.show();
    }

    @Subscribe("costPendingTable.view")
    public void onCostPendingTableView(Action.ActionPerformedEvent event) {
        CostRevoceryManagementApprovalEdit costRevoceryManagementApprovalEdit = screenBuilders.screen(this)
                .withScreenClass(CostRevoceryManagementApprovalEdit.class)
                .withAfterCloseListener(costRevoceryManagementEditAfterScreenCloseEvent -> {
                    getScreenData().loadAll();
                })
                .build();
        costRevoceryManagementApprovalEdit.setEntityToEdit(Objects.requireNonNull(costPendingTable.getSingleSelected()));
        costRevoceryManagementApprovalEdit.show();
    }

    @Install(to = "costActiveTable", subject = "styleProvider")
    private String costActiveTableStyleProvider(CostRevoceryManagement costRevoceryManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (costRevoceryManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "costPendingTable", subject = "styleProvider")
    private String costPendingTableStyleProvider(CostRevoceryManagement costRevoceryManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (costRevoceryManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "costLogsTable", subject = "styleProvider")
    private String costLogsTableStyleProvider(CostRevoceryManagement costRevoceryManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (costRevoceryManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

}