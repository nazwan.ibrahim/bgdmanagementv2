package com.company.management.screen.accountverificationreport;

import io.jmix.core.LoadContext;
import io.jmix.core.SaveContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.AccountVerificationReport;

import java.util.Collections;
import java.util.Set;

@UiController("InvestorAccountVerificationReportEdit.edit")
@UiDescriptor("InvestorAccountVerificationReportEdit.xml")
@EditedEntityContainer("accountVerificationReportDc")
public class InvestorAccountVerificationReportEdit extends StandardEditor<AccountVerificationReport> {

    @Install(to = "accountVerificationReportDl", target = Target.DATA_LOADER)
    private AccountVerificationReport accountVerificationReportDlLoadDelegate(LoadContext<AccountVerificationReport> loadContext) {
        // Here you can load entity from an external store by ID passed in LoadContext
        return getEditedEntity();
    }

    @Install(target = Target.DATA_CONTEXT)
    private Set<Object> commitDelegate(SaveContext saveContext) {
        // Here you can save the edited entity or the whole SaveContext in an external store.
        // Return the set of saved instances.
        return Collections.singleton(getEditedEntity());
    }
}