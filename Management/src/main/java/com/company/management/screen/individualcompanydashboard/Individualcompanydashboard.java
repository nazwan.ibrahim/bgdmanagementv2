package com.company.management.screen.individualcompanydashboard;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Individualcompanydashboard")
@UiDescriptor("individualCompanyDashboard.xml")
@DashboardWidget(name = "Individualcompanydashboard")
public class Individualcompanydashboard extends ScreenFragment {
}