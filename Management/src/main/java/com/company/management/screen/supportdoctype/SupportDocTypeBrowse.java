package com.company.management.screen.supportdoctype;

import io.jmix.ui.screen.*;
import com.company.management.entity.DocumentType;

@UiController("SupportDocType.browse")
@UiDescriptor("support-doc-type-browse.xml")
@LookupComponent("supportDocTypesTable")
public class SupportDocTypeBrowse extends StandardLookup<DocumentType> {
}