package com.company.management.screen.user;

import com.company.management.app.EmailBean;
import com.company.management.entity.RewardCriteria;
import com.company.management.entity.RtType;
import com.company.management.entity.User;
import com.company.management.entity.registrationdb.PasswordDictionary;
import com.company.management.form.RegisterIndividualForm;
import com.company.management.screen.rewardmanagement.RewardManagementEdit;
import com.company.management.service.KeyClockTokenService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jmix.core.DataManager;
import io.jmix.core.EntityStates;
import io.jmix.core.security.event.SingleUserPasswordChangeEvent;
import io.jmix.security.role.assignment.RoleAssignmentRoleType;
import io.jmix.securitydata.entity.RoleAssignmentEntity;
import io.jmix.securityui.password.PasswordValidationException;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.screen.*;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@UiController("User.edit")
@UiDescriptor("user-edit.xml")
@EditedEntityContainer("userDc")
@Route(value = "users/edit", parentPrefix = "users")
public class UserEdit extends StandardEditor<User> {

    private static final Logger log = LoggerFactory.getLogger(UserEdit.class);
    @Autowired
    private EntityStates entityStates;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PasswordField passwordField;

    @Autowired
    private PasswordField confirmPasswordField;

    @Autowired
    private Notifications notifications;

    @Autowired
    private MessageBundle messageBundle;

    private boolean isNewEntity;
    @Autowired
    private ComboBox<String> timeZoneIdField;
    @Autowired
    private CollectionLoader<RtType> rtTypesDl;
    @Autowired
    private Environment environment;
    @Autowired
    private TextField<String> emailField;
    @Autowired
    private TextField<String> phoneNoField;
    @Autowired
    private TextField<String> icNoField;
    @Autowired
    private KeyClockTokenService keyClockTokenService;
    @Autowired
    private Button buttonConfirmPassword;
    @Autowired
    private Button buttonPassword;
    @Autowired
    private HBoxLayout hboxPass;
    @Autowired
    private HBoxLayout hboxConfirm;

    public String getDisablePassword() {
        return disablePassword;
    }
    public void setDisablePassword(String disablePassword) {
        this.disablePassword = disablePassword;
    }
    private String disablePassword;

    @Autowired
    private DataManager dataManager;
    @Autowired
    private CollectionLoader<RtType> rtTypesDcParticipantDl;
    @Autowired
    private CollectionContainer<RtType> rtTypesDcParticipantDc;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private InstanceContainer<User> userDc;
    @Autowired
    private EmailBean emailBean;
    @Autowired
    private ScreenValidation screenValidation;
    @Autowired
    private Form form;

    static org.apache.logging.log4j.Logger logger = LogManager.getLogger(UserEdit.class.getName());

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        rtTypesDl.setParameter("code1", "P01");
        rtTypesDl.setParameter("code2", "P03");
        rtTypesDl.setParameter("code3", "P09");
        rtTypesDl.load();

        rtTypesDcParticipantDl.setParameter("code4", "D01");
        rtTypesDcParticipantDl.setParameter("code5", "D04");
        rtTypesDcParticipantDl.setParameter("code6", "D06");
        rtTypesDcParticipantDl.load();

        if (disablePassword.equals("disablePassword")) {
            passwordField.setVisible(false);
            passwordField.setEnabled(false);
            confirmPasswordField.setVisible(false);
            confirmPasswordField.setEnabled(false);
            buttonPassword.setVisible(false);
            buttonPassword.setEnabled(false);
            buttonConfirmPassword.setVisible(false);
            buttonConfirmPassword.setEnabled(false);
            hboxPass.setVisible(false);
            hboxPass.setEnabled(false);
            hboxConfirm.setEnabled(false);
            hboxConfirm.setVisible(false);
        }
    }


    @Subscribe
    public void onInit(InitEvent event) {
        timeZoneIdField.setOptionsList(Arrays.asList(TimeZone.getAvailableIDs()));
    }

    @Subscribe
    public void onInitEntity(InitEntityEvent<User> event) {
        emailField.setEditable(true);
        passwordField.setVisible(true);
        confirmPasswordField.setVisible(true);
        isNewEntity = true;
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        if (entityStates.isNew(getEditedEntity())) {
            emailField.focus();
        }
    }

//    @Subscribe
//    protected void onBeforeCommit(BeforeCommitChangesEvent event) {
//        if (entityStates.isNew(getEditedEntity())) {
//            if (!Objects.equals(passwordField.getValue(), confirmPasswordField.getValue())) {
//                notifications.create(Notifications.NotificationType.WARNING)
//                        .withCaption(messageBundle.getMessage("passwordsDoNotMatch"))
//                        .show();
//                event.preventCommit();
//            }
//            for(PasswordDictionary passwordDictionary : passwordDictionary()){
//                if(passwordField.getValue().contains(passwordDictionary.getWord())){
//                    notifications.create(Notifications.NotificationType.WARNING)
//                            .withCaption("Please use stronger password.")
//                            .show();
//                    event.preventCommit();
//                }
//            }
//            saveDefaulFiltertRole();
//            saveDefaulMinimaltRole();
//            getApplicationContext().publishEvent(new SingleUserPasswordChangeEvent(getEditedEntity().getUsername(), passwordField.getValue()));
//            getEditedEntity().setPassword(passwordEncoder.encode(passwordField.getValue()));
//        }
//    }

//    @Subscribe(target = Target.DATA_CONTEXT)
//    public void onPostCommit(DataContext.PostCommitEvent event) {
//        if (isNewEntity) {
//            getApplicationContext().publishEvent(new SingleUserPasswordChangeEvent(getEditedEntity().getUsername(), passwordField.getValue()));
//            notifications.create(Notifications.NotificationType.HUMANIZED)
//                        .withCaption("User successful created")
//                        .show();
////            keyClockRegisterAccount();
////            createKeyClockAccount();
//        }
//        notifications.create(Notifications.NotificationType.HUMANIZED)
//                .withCaption("User successful updated")
//                .show();
//    }

//    private void keyClockRegisterAccount() {
//        try {
//            String url = environment.getProperty("onboarding.register");
//            RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
//
//            HttpHeaders headers = new HttpHeaders();
//
////            headers.setBearerAuth(token);
//            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.add("Accept", "application/json");
//
//            RegisterIndividualForm registerIndividualForm = new RegisterIndividualForm();
//            registerIndividualForm.setNationalityCode("MY");
//            registerIndividualForm.setFullName(usernameField.getValue());
//            registerIndividualForm.setEmail(emailField.getValue());
////            registerIndividualForm.setIdentificationTypeCode(Objects.requireNonNull(icTypeField.getValue()).getCode());
//            registerIndividualForm.setIdentificationNumber(icNoField.getValue());
//            registerIndividualForm.setPhoneNumber(phoneNoField.getValue());
//            registerIndividualForm.setPassword(confirmPasswordField.getValue());
//            registerIndividualForm.setConfirmPassword(confirmPasswordField.getValue());
//
//            ObjectMapper objectMapper = new ObjectMapper();
//            String requestBody = objectMapper.writeValueAsString(registerIndividualForm);
//
//            HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
////            log.info("request -> " +requestBody);
//            log.info("request -> " + requestBody);
//
//            assert url != null;
//            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
//
//            int statusCode = responseEntity.getStatusCodeValue();
//            String responseBody = responseEntity.getBody();
//
//            log.info("Status code: " + statusCode);
//            log.info("Response body: " + responseBody);
//        } catch (Exception ex) {
//            log.info(ex.getMessage());
//        }
//    }
//
//    private String createKeyClockAccount() {
//        try {
//            String token = keyClockTokenService.getKeyCloakToken();
//            Map<String, Object> keycloakReg = keyClockTokenService.createKeyCloakUser(getEditedEntity().getUsername(), getEditedEntity().getEmail(), confirmPasswordField.getValue(), token);
//            if (!keycloakReg.get("code").equals(201)) {
//                //rollback if fails
//                //            actStatus = actStatusTypes.stream().filter(f -> f.getCode().equals("G05")).collect(Collectors.toList()).get(0);
//                //            user = this.dataManager.load(User.class).id(form.getUserID()).one();
//                //            user.setPassword(oldPassword);
//                //            user.setActStatus(actStatus);
//                //            user.setActive(false);
//                //            this.dataManager.save(user);
////                ResponseEntity.status(HttpStatus.valueOf((Integer) keycloakReg.get("code"))).body("{\"message\":\"" + keycloakReg.get("message") + "\"}");
//                log.error("{\"message\":\"" + keycloakReg.get("message") + "\"}");
//                return "400";
//                //                return ResponseEntity.status(HttpStatus.valueOf((Integer) keycloakReg.get("code"))).body(keycloakReg.get("message"));
//            }
////            ResponseEntity.status(HttpStatus.CREATED).body(keycloakReg.get("message").toString());
//            log.info("{\"message\":\"" + keycloakReg.get("message") + "\"}");
//            return "201";
//        } catch (Exception ex) {
////            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"message\":\"" + ex.getMessage() + "\"}");
//            log.info("{\"message\":\"" + ex.getMessage() + "\"}");
//            return "400";
//        }
//
//    }

    private List<PasswordDictionary> passwordDictionary(){
        String query = "select e from PasswordDictionary e";
        return this. dataManager.load(PasswordDictionary.class)
                .query(query)
                .list();
    }

    private void saveDefaulFiltertRole(){
        RoleAssignmentEntity roleAssignmentEntity = this.dataManager.create(RoleAssignmentEntity.class);
        roleAssignmentEntity.setRoleCode("ui-filter");
        roleAssignmentEntity.setRoleType(RoleAssignmentRoleType.RESOURCE);
        roleAssignmentEntity.setUsername(getEditedEntity().getUsername());
        this.dataManager.save(roleAssignmentEntity);
    }

    private void saveDefaulMinimaltRole(){
        RoleAssignmentEntity roleAssignmentEntity = this.dataManager.create(RoleAssignmentEntity.class);
        roleAssignmentEntity.setRoleCode("ui-minimal");
        roleAssignmentEntity.setRoleType(RoleAssignmentRoleType.RESOURCE);
        roleAssignmentEntity.setUsername(getEditedEntity().getUsername());
        this.dataManager.save(roleAssignmentEntity);
    }

    @Subscribe("buttonPassword")
    public void onButtonPassworkClick(Button.ClickEvent event) {
        notifications.create()
                .withCaption(passwordField.getValue())
                .show();
    }

    @Subscribe("buttonConfirmPassword")
    public void onButtonConfirmPasswordClick(Button.ClickEvent event) {
        notifications.create()
                .withCaption(confirmPasswordField.getValue())
                .show();
    }

    @Subscribe("saveBtn")
    public void onSaveBtnClick(final Button.ClickEvent event) {
        if(isNewEntity){
            ValidationErrors errors = screenValidation.validateUiComponents(form);
            if (!errors.isEmpty()) {
                screenValidation.showValidationErrors(this, errors);
                notifications.create()
                        .withContentMode(ContentMode.HTML)
                        .withType(Notifications.NotificationType.HUMANIZED)
                        .withPosition(Notifications.Position.BOTTOM_RIGHT)
                        .withCaption("Please fill in all the Mandatory fields.")
                        .show();

            }else if (!Objects.equals(passwordField.getValue(), confirmPasswordField.getValue())) {
                notifications.create(Notifications.NotificationType.WARNING)
                        .withCaption(messageBundle.getMessage("passwordsDoNotMatch"))
                        .show();
            }else{
                dialogs.createOptionDialog()
                        .withCaption("Create Admin User")
                        .withContentMode(ContentMode.HTML)
                        .withMessage("Click Confirm or Cancel button to proceed.")
                        .withActions(
                                new DialogAction(DialogAction.Type.YES)
                                        .withPrimary(true)
                                        .withCaption("Confirm")
                                        .withHandler(e -> saveData()),
                                new DialogAction(DialogAction.Type.NO)
                                        .withCaption("Cancel")
                        )
                        .show();
            }
            for(PasswordDictionary passwordDictionary : passwordDictionary()){
                if(passwordField.getValue().contains(passwordDictionary.getWord())){
                    notifications.create(Notifications.NotificationType.WARNING)
                            .withCaption("Please use stronger password.")
                            .show();
                }
            }
        }
    }
    private void saveData(){
        if(isNewEntity){
            String passwordVal = passwordField.getValue();
            userDc.getItem().setPassword(passwordEncoder.encode(passwordField.getValue()));
            userDc.getItem().setUsername(userDc.getItem().getEmail());
            userDc.getItem().setEmail(userDc.getItem().getEmail());
            this.dataManager.save(userDc.getItem());
            saveDefaulFiltertRole();
            saveDefaulMinimaltRole();
            getApplicationContext().publishEvent(new SingleUserPasswordChangeEvent(getEditedEntity().getUsername(), passwordField.getValue()));
            try{
                //send email
                emailBean.generateEmailCreateAdminuser(userDc.getItem().getFullname(), userDc.getItem().getEmail(), passwordVal);
            }catch (Exception e){
                logger.error(e.getMessage());
            }
            notifications.create(Notifications.NotificationType.HUMANIZED)
                    .withCaption("User successful created")
                    .show();
            closeWithCommit();
        }else{
            this.dataManager.save(userDc.getItem());
            notifications.create(Notifications.NotificationType.HUMANIZED)
                    .withCaption("User successful updated")
                    .show();
            closeWithCommit();
        }
    }
}