package com.company.management.screen.transferinfoview;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransferInfoView;

@UiController("TransferInfoView.Detail")
@UiDescriptor("transfer-info-view-detail.xml")
@LookupComponent("transferInfoViewsTable")
public class TransferInfoViewDetail extends StandardLookup<TransferInfoView> {
}