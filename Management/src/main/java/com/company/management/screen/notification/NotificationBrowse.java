package com.company.management.screen.notification;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.Notification;

@UiController("Notification.browse")
@UiDescriptor("notification-browse.xml")
@LookupComponent("notificationsTable")
public class NotificationBrowse extends StandardLookup<Notification> {
}