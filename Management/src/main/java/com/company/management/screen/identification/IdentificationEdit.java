package com.company.management.screen.identification;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.Identification;

@UiController("Identification.edit")
@UiDescriptor("identification-edit.xml")
@EditedEntityContainer("identificationDc")
public class IdentificationEdit extends StandardEditor<Identification> {
}