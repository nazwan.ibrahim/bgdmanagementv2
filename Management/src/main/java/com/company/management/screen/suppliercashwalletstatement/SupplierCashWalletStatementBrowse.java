package com.company.management.screen.suppliercashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.SupplierCashWalletStatement;

@UiController("SupplierCashWalletStatement.browse")
@UiDescriptor("supplier-cash-wallet-statement-browse.xml")
@LookupComponent("supplierCashWalletStatementsTable")
public class SupplierCashWalletStatementBrowse extends StandardLookup<SupplierCashWalletStatement> {
}