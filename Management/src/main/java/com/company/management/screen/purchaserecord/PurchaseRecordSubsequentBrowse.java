package com.company.management.screen.purchaserecord;

import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseRecord;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@UiController("PurchaseSubsequentRecord.browse")
@UiDescriptor("purchase-record-subsequent-browse.xml")
@LookupComponent("purchaseRecordsTable")
public class PurchaseRecordSubsequentBrowse extends StandardLookup<PurchaseRecord> {
	@Autowired
	private CollectionLoader<PurchaseRecord> purchaseRecordsDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Table<PurchaseRecord> purchaseRecordsTable;
	@Autowired
	private Table<PurchaseRecord> purchaseRecordsTablegolddinar;
	@Autowired
	private ScreenBuilders screenBuilders;

	private String goldType;
	@Autowired
	private TabSheet goldTab;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		String a = "a";
		if(Objects.requireNonNull(goldTab.getTab("goldBar")).getName().equals("goldBar")){
			purchaseRecordsDl.setParameter("typeCode1","GT01");
			purchaseRecordsDl.setParameter("issuanceTypeCode1","ITF/02");
			purchaseRecordsDl.load();
			goldType = "goldBar";
		}else {
			purchaseRecordsDl.setParameter("typeCode1","GT02");
			purchaseRecordsDl.load();
			goldType = "goldDinar";
		}
	}



	@Subscribe("purchaseRecordsTable.create")
	public void onPurchaseRecordsTableCreate(Action.ActionPerformedEvent event) {
		PurchaseRecord purchaseRecord1 = dataManager.create(PurchaseRecord.class);
		goToEditPage(purchaseRecord1);
	}

	@Subscribe("purchaseRecordsTable.edit")
	public void onPurchaseRecordsTableEdit(Action.ActionPerformedEvent event) {
		PurchaseRecord purchaseRecord1 = purchaseRecordsTable.getSingleSelected();
		goToEditPage(purchaseRecord1);
	}

	@Subscribe("purchaseRecordsTablegolddinar.create")
	public void onPurchaseRecordsTablegolddinarCreate(Action.ActionPerformedEvent event) {
		PurchaseRecord purchaseRecord1 = dataManager.create(PurchaseRecord.class);
		goToEditPage(purchaseRecord1);
	}

	@Subscribe("purchaseRecordsTablegolddinar.edit")
	public void onPurchaseRecordsTablegolddinarEdit(Action.ActionPerformedEvent event) {
		PurchaseRecord purchaseRecord1 = purchaseRecordsTablegolddinar.getSingleSelected();
		goToEditPage(purchaseRecord1);
	}
	private void goToEditPage(PurchaseRecord purchaseRecord1){
		BGDDemocratisationEdit bgdDemocratisationEdit = screenBuilders.screen(this)
				.withScreenClass(BGDDemocratisationEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					if(goldType.equals("goldBar")) {
						goldType = "goldBar";
					}else{
						goldType = "goldDinar";
					}
					getScreenData().loadAll();
					purchaseRecordsDl.load();
				})
				.build();
		assert purchaseRecord1 != null;
		bgdDemocratisationEdit.setEntityToEdit(purchaseRecord1);
		bgdDemocratisationEdit.setGoldType(goldType);
		bgdDemocratisationEdit.setFormCode("ITF/02");
		bgdDemocratisationEdit.show();
	}


	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			purchaseRecordsDl.setParameter("typeCode1","GT01");
			purchaseRecordsDl.load();
			goldType = "goldBar";
		}else {
			purchaseRecordsDl.setParameter("typeCode1","GT02");
			purchaseRecordsDl.load();
			goldType = "goldDinar";

		}
	}

	@Install(to = "purchaseRecordsTable", subject = "styleProvider")
	private String purchaseRecordsTableStyleProvider(PurchaseRecord purchaseRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (purchaseRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "purchaseRecordsTablegolddinar", subject = "styleProvider")
	private String purchaseRecordsTablegolddinarStyleProvider(PurchaseRecord purchaseRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (purchaseRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

}