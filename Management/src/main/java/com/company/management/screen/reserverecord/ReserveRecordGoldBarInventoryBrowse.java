package com.company.management.screen.reserverecord;

import com.company.management.entity.DeliveryRecord;
import com.company.management.entity.PurchaseRecord;
import com.company.management.screen.deliveryrecord.DeliveryRecordGoldBrowse;
import com.company.management.screen.deliveryrecord.DeliveryRecordGoldEdit;
import com.company.management.screen.purchaserecord.BGDDemocratisationEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ReserveRecordGoldBarInventory.browse")
@UiDescriptor("reserve-record-goldBar-inventory-browse.xml")
@LookupComponent("reserveRecordsTable")
public class ReserveRecordGoldBarInventoryBrowse extends StandardLookup<ReserveRecord> {
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<PurchaseRecord> reserveRecordsTable;
	@Autowired
	private CollectionLoader<ReserveRecord> reserveRecordsDl;
	@Autowired
	private GroupTable<PurchaseRecord> purchaseRecordsTable;
	@Autowired
	private CollectionLoader<PurchaseRecord> purchaseRecordsDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<PurchaseRecord> purchaseRecordsDl1;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		purchaseRecordsDl.setParameter("typeCode1","GT01");
		purchaseRecordsDl.setParameter("issuanceTypeCode1","ITF/01");
		purchaseRecordsDl.load();

		purchaseRecordsDl1.setParameter("typeCode1","GT01");
		purchaseRecordsDl1.setParameter("issuanceTypeCode1","ITF/02");
		purchaseRecordsDl1.load();
	}



	private void goToEditPage(ReserveRecord reserveRecord){
		ReserveRecordGoldBarInventoryEdit reserveRecordGoldBarInventoryEdit = this.screenBuilders.screen(this)
				.withScreenClass(ReserveRecordGoldBarInventoryEdit.class)
				.build();
		reserveRecordGoldBarInventoryEdit.setEntityToEdit(reserveRecord);
		reserveRecordGoldBarInventoryEdit.show();
	}

	private void goToEditPagePurchase(DeliveryRecord deliveryRecord){
		DeliveryRecordGoldEdit deliveryRecordGoldEdit = screenBuilders.screen(this)
				.withScreenClass(DeliveryRecordGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		assert deliveryRecord != null;
		deliveryRecordGoldEdit.setEntityToEdit(deliveryRecord);
		deliveryRecordGoldEdit.setDisableElement("disableAll");
		deliveryRecordGoldEdit.show();
	}

	@Subscribe("reserveRecordsTable.edit")
	public void onReserveRecordsTableEdit(Action.ActionPerformedEvent event) {
		String query = "select d from DeliveryRecord d " +
				"where d.purchaseRecord = :purchaseRecord1";
		DeliveryRecord deliveryRecord = this.dataManager.load(DeliveryRecord.class)
				.query(query)
				.parameter("purchaseRecord1", purchaseRecordsTable.getSingleSelected())
				.one();
		goToEditPagePurchase(deliveryRecord);
	}

	@Subscribe("purchaseRecordsTable.edit")
	public void onPurchaseRecordsTableEdit(Action.ActionPerformedEvent event) {
		String query = "select d from DeliveryRecord d " +
				"where d.purchaseRecord = :purchaseRecord1";
		DeliveryRecord deliveryRecord = this.dataManager.load(DeliveryRecord.class)
						.query(query)
				.parameter("purchaseRecord1", purchaseRecordsTable.getSingleSelected())
				.one();
		goToEditPagePurchase(deliveryRecord);
	}
}