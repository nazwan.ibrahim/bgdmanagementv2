package com.company.management.screen.bgdmanagementuser;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Bgdmanagementuser")
@UiDescriptor("BgdManagementUser.xml")
@DashboardWidget(name = "Bgdmanagementuser")
public class Bgdmanagementuser extends ScreenFragment {
}