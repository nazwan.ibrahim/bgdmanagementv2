package com.company.management.screen.usertier;

import com.company.management.entity.TransactionHistoriesView;
import com.company.management.entity.registrationdb.*;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.AccountWalletView;
import com.company.management.entity.wallet.TransferInfoView;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.Dialogs;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.HBoxLayout;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import liquibase.pro.packaged.ex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

@UiController("InvestorReport.edit")
@UiDescriptor("investor-report-edit.xml")
@EditedEntityContainer("userTierDc")
public class InvestorReportEdit extends StandardEditor<UserTier> {
	private static final Logger log = LoggerFactory.getLogger(InvestorReportEdit.class);
	@Autowired
	private CollectionLoader<TransferInfoView> transferInfoViewsDl;
	@Autowired
	private InstanceLoader<UserTier> userTierDl;
	@Autowired
	private InstanceContainer<UserTier> userTierDc;
	@Autowired
	private Label<BigDecimal> totalCash;
	@Autowired
	private Label<String> totalGoldAmount;
	@Autowired
	private Label<String> totalTransferGold;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;
	@Autowired
	private Environment environment;
	@Autowired
	private CollectionLoader<UserAddress> userAddressesDl;
	@Autowired
	private CollectionContainer<UserAddress> userAddressesDc;
//	@Autowired
//	private TextField<String> addressLineOne;
//	@Autowired
//	private TextField<String> addressLineTwo;
//	@Autowired
//	private TextField<String> postcode;
//	@Autowired
//	private TextField<String> state;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsCashDl;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsGoldDl;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsDl;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsGoldDl;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsDc;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsGoldDc;
	DecimalFormat df = new DecimalFormat("#,###,##0.000000");
	@Autowired
	private TextField<String> bgdID;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl;
	@Autowired
	private CollectionContainer<AccountBgd> accountBgdsDc;
	@Autowired
	private CollectionLoader<UserProfile> userProfileDl;
	@Autowired
	private CollectionContainer<UserProfile> userProfileDc;
	@Autowired
	private CollectionLoader<UserBank> userBankDl;
	@Autowired
	private CollectionContainer<UserBank> userBankDc;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private TextField<String> fullNameField;
	@Autowired
	private Label kycStatus;
	@Autowired
	private TextField tier;
	@Autowired
	private TextField bank;
	@Autowired
	private TextField bankAccountNo;
	@Autowired
	private TextField homeNumber;
	@Autowired
	private TextField officeNumber;
	@Autowired
	private TextField residentialAddress1ID;
	@Autowired
	private TextField residentialAddress2ID;
	@Autowired
	private TextField residentialCityID;
	@Autowired
	private TextField residentialCountryID;
	@Autowired
	private TextField residentialStateID;
	@Autowired
	private TextField residentialPostcodeID;
	@Autowired
	private TextField mailingAddress1ID;
	@Autowired
	private TextField mailingAddress2ID;
	@Autowired
	private TextField mailingCityID;
	@Autowired
	private TextField mailingCountryID;
	@Autowired
	private TextField mailingPostcodeID;
	@Autowired
	private TextField mailingStateID;
	@Autowired
	private VBoxLayout residentialAddressContainer;
	@Autowired
	private VBoxLayout innerResidentialContainer;
	@Autowired
	private TextField accountStatus;

	public String getDisable() {
		return disable;
	}

	public void setDisable(String disable) {
		this.disable = disable;
	}

	private String disable;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		userTierDl.load();

		transactionHistoriesViewsCashDl.setParameter("accOwner1", userTierDc.getItem().getUser().getUsername());
		transactionHistoriesViewsCashDl.setParameter("walletCode1", "E01");
		transactionHistoriesViewsCashDl.load();

		transactionHistoriesViewsGoldDl.setParameter("accOwner1", userTierDc.getItem().getUser().getUsername());
		transactionHistoriesViewsGoldDl.setParameter("walletCode1", "E02");
		transactionHistoriesViewsGoldDl.load();

		transferInfoViewsDl.setParameter("sender1", userTierDc.getItem().getUser().getEmail());
		transferInfoViewsDl.load();

		userAddressesDl.setParameter("user1", userTierDc.getItem().getUser());
		userAddressesDl.load();

		accountWalletViewsDl.setParameter("accountOwner1", userTierDc.getItem().getUser().getUsername());
		accountWalletViewsDl.setParameter("code1", "E01");
		accountWalletViewsDl.load();

		accountWalletViewsGoldDl.setParameter("accountOwner1", userTierDc.getItem().getUser().getUsername());
		accountWalletViewsGoldDl.setParameter("code1", "E02");
		accountWalletViewsGoldDl.load();

		accountBgdsDl.setParameter("accOwner1", userTierDc.getItem().getUser().getEmail());
		accountBgdsDl.setParameter("walletTypeCode1", "E02");
		accountBgdsDl.load();

		userBankDl.setParameter("userUsername1", userTierDc.getItem().getUser().getUsername());
		userBankDl.load();

		userProfileDl.setParameter("userUsername1", userTierDc.getItem().getUser().getUsername());
		userProfileDl.load();

		try{
			String status = userTierDc.getItem().getKycStatus().getName();
			kycStatus.setValue(status);

		}catch (Exception ex){
			log.error(ex.getMessage());
		}
		try{
			String accStatus = userTierDc.getItem().getTier().getName();
			tier.setValue(accStatus);
		}catch (Exception ex){
			log.error(ex.getMessage());
		}


	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		try {
			if (accountWalletViewsDc.getItems().size() != 0) {
				totalCash.setValue(accountWalletViewsDc.getItems().get(0).getAvailableAmount());
			}
			if (accountWalletViewsGoldDc.getItems().size() != 0) {
				totalGoldAmount.setValue(df.format(accountWalletViewsGoldDc.getItems().get(0).getAvailableAmount()));
			}
			if (countTotalTransfer() != null) {
				totalTransferGold.setValue(df.format(countTotalTransfer()));
			} else {
				totalTransferGold.setValue(df.format(0.000000));
			}
			if (userAddressesDc.getMutableItems().size() != 0) {
				List<UserAddress> userAddressResi = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L01")).toList();
				if(userAddressResi.size() > 0) {
					residentialAddress1ID.setValue(userAddressResi.get(0).getAddress1());
					residentialAddress2ID.setValue(userAddressResi.get(0).getAddress2());
					residentialPostcodeID.setValue(userAddressResi.get(0).getPostcode());
					residentialCityID.setValue(userAddressResi.get(0).getTown());
					residentialStateID.setValue(userAddressResi.get(0).getState());
					residentialCountryID.setValue(userAddressResi.get(0).getCountry());
				}

				List<UserAddress> userAddressMail = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L03")).toList();
				if(userAddressMail.size() > 0) {
					mailingAddress1ID.setValue(userAddressMail.get(0).getAddress1());
					mailingAddress2ID.setValue(userAddressMail.get(0).getAddress2());
					mailingPostcodeID.setValue(userAddressMail.get(0).getPostcode());
					mailingCityID.setValue(userAddressMail.get(0).getTown());
					mailingStateID.setValue(userAddressMail.get(0).getState());
					mailingCountryID.setValue(userAddressMail.get(0).getCountry());
				}
			}
			bgdID.setValue(accountBgdsDc.getItems().get(0).getAcctNumber());
			bank.setValue(userBankDc.getItems().get(0).getBank().getName());
			bankAccountNo.setValue(userBankDc.getItems().get(0).getAccountNumber());
			homeNumber.setValue(userProfileDc.getItems().get(0).getHomeNumber());
			officeNumber.setValue(userProfileDc.getItems().get(0).getOfficeNumber());
			accountStatus.setValue(userProfileDc.getItems().get(0).getActStatus().getName());
		} catch (Exception ex) {
			log.info(ex.getMessage());
			totalCash.setValue(BigDecimal.ZERO);
			totalTransferGold.setValue(df.format(BigDecimal.ZERO));
			totalGoldAmount.setValue(df.format(BigDecimal.ZERO));
//			addressLineOne.setValue("-");
//			addressLineTwo.setValue("-");
//			postcode.setValue("-");
//			state.setValue("-");
		}
	}


	@Install(to = "identificationNoField", subject = "formatter")
	private String identificationNoFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
	}

	@Install(to = "phoneNumberField", subject = "formatter")
	private String phoneNumberFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
	}

	private BigDecimal countTotalTransfer() {
		String query = "select SUM(e.amount) from TransferInfoView e where e.sender = :sender1";
		return this.dataManager.loadValue(query, BigDecimal.class)
				.store("wallet")
				.parameter("sender1", userTierDc.getItem().getUser().getUsername())
				.one();
	}

	@Install(to = "transferInfoViewsTable.accountName", subject = "columnGenerator")
	private Component transferInfoViewsTableAccountNameColumnGenerator(TransferInfoView transferInfoView) {
		UserProfile name = userProfile(transferInfoView.getReceiver());
		return new Table.PlainTextCell(name.getUsername());
	}


	private UserProfile userProfile(String username) {
		return this.dataManager.load(UserProfile.class)
				.query("select u from UserProfile u " +
						"where u.username = :username1").parameter("username1", username).one();

	}

	@Install(to = "transactionAcctsTable1.amount", subject = "formatter")
	private String transactionAcctsTable1AmountFormatter(Object object) {
		return df.format(object);
	}

	@Install(to = "transferInfoViewsTable.amount", subject = "formatter")
	private String transferInfoViewsTableAmountFormatter(Object object) {
		return df.format(object);
	}

//	@Subscribe("activeAcctBtn")
//	public void onActiveAcctBtnClick(Button.ClickEvent event) {
//		dialogs.createOptionDialog()
//				.withCaption("Confirm Purchase Record Verification")
//				.withContentMode(ContentMode.HTML)
//				.withMessage("Please make sure all information is correct before proceeding.\n" +
//						"Confirm to proceed?")
//				.withActions(
//						new DialogAction(DialogAction.Type.YES)
//								.withPrimary(true)
//								.withCaption("Yes")
//								.withHandler(e -> saveData()),
//						new DialogAction(DialogAction.Type.NO)
//								.withCaption("No, Back to Edit")
//				)
//				.show();
//	}

	private void saveData() {
		UserProfile userProfile = userProfile(userTierDc.getItem().getUser().getUsername());
		String query = "select e from ActivationStatus e where e.code = :code";
		userProfile.setActStatus(this.dataManager.load(ActivationStatus.class).query(query).parameter("code", "G03").one());
		this.dataManager.save(userProfile);
		closeWithDiscard();
	}



	//Hide Button for each Investor Details
	@Subscribe("togglePersonalDetails")
	public void onTogglePersonalDetailsClick(Button.ClickEvent event) {
		// Get the parent layout, which is a VBoxLayout in your case
		VBoxLayout personalDetailsContainer = (VBoxLayout) getWindow().getComponentNN("personalDetailsContainer");

		// Find the inner container that holds the content components
		VBoxLayout innerPersonalContainer = (VBoxLayout) personalDetailsContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerPersonalContainer"))
				.findFirst()
				.orElse(null);

		if (innerPersonalContainer != null) {
			// Toggle the visibility of the inner container
			innerPersonalContainer.setVisible(!innerPersonalContainer.isVisible());
		}
	}

	@Subscribe("toggleContactDetails")
	public void onToggleContactDetailsClick(Button.ClickEvent event) {
		VBoxLayout contactDetailsContainer = (VBoxLayout) getWindow().getComponentNN("contactDetailsContainer");

		VBoxLayout innerContactContainer = (VBoxLayout) contactDetailsContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerContactContainer"))
				.findFirst()
				.orElse(null);

		if(innerContactContainer != null) {
			innerContactContainer.setVisible(!innerContactContainer.isVisible());
		}

		}

	@Subscribe("toggleResidentialAddress")
	public void onToggleResidentialAddressClick(Button.ClickEvent event) {
		VBoxLayout residentialAddressContainer = (VBoxLayout) getWindow().getComponentNN("residentialAddressContainer");

		VBoxLayout innerResidentialContainer = (VBoxLayout) residentialAddressContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerResidentialContainer"))
				.findFirst()
				.orElse(null);

		if(innerResidentialContainer != null) {
			innerResidentialContainer.setVisible(!innerResidentialContainer.isVisible());
		}

	}

	@Subscribe("toggleMailingAddress")
	public void onToggleMailingAddressClick(Button.ClickEvent event) {
		VBoxLayout mailingAddressContainer = (VBoxLayout) getWindow().getComponentNN("mailingAddressContainer");

		VBoxLayout innerMailingContainer = (VBoxLayout) mailingAddressContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerMailingContainer"))
				.findFirst()
				.orElse(null);

		if(innerMailingContainer != null) {
			innerMailingContainer.setVisible(!innerMailingContainer.isVisible());
		}
	}

	@Subscribe("toggleEmploymentDetails")
	public void onToggleEmploymentDetailsClick(Button.ClickEvent event) {
		VBoxLayout employmentDetailsContainer = (VBoxLayout) getWindow().getComponentNN("employmentDetailsContainer");

		VBoxLayout innerEmploymentContainer = (VBoxLayout) employmentDetailsContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerEmploymentContainer"))
				.findFirst()
				.orElse(null);

		if(innerEmploymentContainer != null) {
			innerEmploymentContainer.setVisible(!innerEmploymentContainer.isVisible());
		}
	}

	@Subscribe("toggleBpfws")
	public void onToggleBpfwsClick(Button.ClickEvent event) {
		VBoxLayout bpfwsContainer = (VBoxLayout) getWindow().getComponentNN("bpfwsContainer");

		VBoxLayout innerbpfwsContainer = (VBoxLayout) bpfwsContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerbpfwsContainer"))
				.findFirst()
				.orElse(null);

		if(innerbpfwsContainer != null) {
			innerbpfwsContainer.setVisible(!innerbpfwsContainer.isVisible());
		}
	}

	@Subscribe("toggleBankInformation")
	public void onToggleBankInformationClick(Button.ClickEvent event) {
		VBoxLayout bankInformationContainer = (VBoxLayout) getWindow().getComponentNN("bankInformationContainer");

		VBoxLayout innerBankContainer = (VBoxLayout) bankInformationContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerBankContainer"))
				.findFirst()
				.orElse(null);

		if(innerBankContainer != null) {
			innerBankContainer.setVisible(!innerBankContainer.isVisible());
		}
	}

	@Subscribe("toggleBgdAccountInformation")
	public void onToggleBgdAccountInformationClick(Button.ClickEvent event) {
		VBoxLayout bgdAccountInformationContainer = (VBoxLayout) getWindow().getComponentNN("bgdAccountInformationContainer");

		VBoxLayout innerBgdContainer = (VBoxLayout) bgdAccountInformationContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerBgdContainer"))
				.findFirst()
				.orElse(null);

		if(innerBgdContainer != null) {
			innerBgdContainer.setVisible(!innerBgdContainer.isVisible());
		}
	}

	@Subscribe("toggleOnboardingApproval")
	public void onToggleOnboardingApprovalClick(Button.ClickEvent event) {
		VBoxLayout onboardingApprovalContainer = (VBoxLayout) getWindow().getComponentNN("onboardingApprovalContainer");

		VBoxLayout innerOnboardingContainer = (VBoxLayout) onboardingApprovalContainer.getComponents()
				.stream()
				.filter(component -> component.getId().equals("innerOnboardingContainer"))
				.findFirst()
				.orElse(null);

		if(innerOnboardingContainer != null) {
			innerOnboardingContainer.setVisible(!innerOnboardingContainer.isVisible());
		}
	}
	
	

}