package com.company.management.screen.b2bgoldpurchasing;

import com.company.management.entity.PurchaseRecord;
import com.company.management.screen.purchaserecord.BGDDemocratisationEdit;
import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldPurchasing;

@UiController("B2BGoldPurchasing.browse")
@UiDescriptor("b2b-gold-purchasing-browse.xml")
@LookupComponent("b2BGoldPurchasingsTable")
public class B2BGoldPurchasingBrowse extends StandardLookup<B2BGoldPurchasing> {
}