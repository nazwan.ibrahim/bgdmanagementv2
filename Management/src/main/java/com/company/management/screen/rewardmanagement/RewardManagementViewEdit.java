package com.company.management.screen.rewardmanagement;

import com.company.management.app.BgdIDBean;
import com.company.management.entity.*;
import com.company.management.entity.registrationdb.UserProfile;
import com.google.common.base.Strings;
import io.jmix.core.DataManager;
import io.jmix.core.LoadContext;
import io.jmix.core.Metadata;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TextField;
import io.jmix.ui.component.data.table.ContainerGroupTableItems;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@UiController("RewardManagementView.edit")
@UiDescriptor("reward-management-view-edit.xml")
@EditedEntityContainer("rewardManagementDc")
public class RewardManagementViewEdit extends StandardEditor<RewardManagement> {
    @Autowired
    private InstanceContainer<RewardManagement> rewardManagementDc;
    @Autowired
    private CollectionLoader<RewardParticipant> rewardParticipantsDl;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private CollectionContainer<RewardParticipant> rewardParticipantsDc;
    @Autowired
    private CollectionContainer<RewardParticipantDTO> rewardParticipantDToesDc;
    @Autowired
    private BgdIDBean bgdIDBean;
    @Autowired
    private Metadata metadata;
    @Autowired
    private TextField<String> campaignName;
    @Autowired
    private TextField<String> investorName;
    @Autowired
    private TextField<String> rewardValue;
    @Autowired
    private CollectionLoader<RewardParticipantDTO> rewardParticipantDToesDl;

    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        rewardParticipantsDl.setParameter("reward1", rewardManagementDc.getItem());
        rewardParticipantsDl.load();

        List<RewardParticipantDTO> rewardParticipantDTOS = new ArrayList<>();
        rewardParticipantsDc.getItems().forEach(e -> {
            RewardParticipantDTO rewardParticipantDTO = metadata.create(RewardParticipantDTO.class);
            rewardParticipantDTO.setRedeemed_amount(e.getRedeemedAmount());
            rewardParticipantDTO.setRedeemed_date(e.getRedeemedDate());
            rewardParticipantDTO.setMaxParticipant(rewardManagementDc.getItem().getMaxParticipant());
            UserProfile userProfile = userProfile(UUID.fromString(e.getUserID()));
            rewardParticipantDTO.setFullname(userProfile.getFullName());
            rewardParticipantDTO.setCampaign_name(rewardManagementDc.getItem().getCampaignName());
//            BgdIDDTO bgdIDDTO = bgdIDBean.getBgdID(userProfile.getUsername());
//            if(bgdIDDTO.getBgdID()!=null){
//                rewardParticipantDTO.setBgdIdStr(bgdIDDTO.getBgdID());
//            }
            rewardParticipantDTOS.add(rewardParticipantDTO);
        });
        rewardParticipantDToesDc.setItems(rewardParticipantDTOS);
    }
    public UserProfile userProfile(UUID userID) {
        try {
            return this.dataManager.load(UserProfile.class).id(userID).one();
        } catch (Exception ex) {
            UserProfile userProfile = this.dataManager.create(UserProfile.class);
            userProfile.setFullName(userID.toString());

            return userProfile;
        }
    }
    @Subscribe("refreshBtn")
    public void onRefreshBtnClick(final Button.ClickEvent event) {
        rewardParticipantDToesDl.load();
    }
    private List<RewardParticipantDTO> getListRewardParticipantDTO(){
        rewardParticipantsDl.setParameter("reward1", rewardManagementDc.getItem());
        rewardParticipantsDl.load();

        List<RewardParticipantDTO> rewardParticipantDTOS = new ArrayList<>();
        rewardParticipantsDc.getItems().forEach(e -> {
            RewardParticipantDTO rewardParticipantDTO = metadata.create(RewardParticipantDTO.class);
            rewardParticipantDTO.setRedeemed_amount(e.getRedeemedAmount());
            rewardParticipantDTO.setRedeemed_date(e.getRedeemedDate());
            rewardParticipantDTO.setMaxParticipant(rewardManagementDc.getItem().getMaxParticipant());
            UserProfile userProfile = userProfile(UUID.fromString(e.getUserID()));
            rewardParticipantDTO.setFullname(userProfile.getFullName());
            rewardParticipantDTO.setCampaign_name(rewardManagementDc.getItem().getCampaignName());
//            BgdIDDTO bgdIDDTO = bgdIDBean.getBgdID(userProfile.getUsername());
//            if(bgdIDDTO.getBgdID()!=null){
//                rewardParticipantDTO.setBgdIdStr(bgdIDDTO.getBgdID());
//            }
            rewardParticipantDTOS.add(rewardParticipantDTO);
        });
        rewardParticipantDToesDc.setItems(rewardParticipantDTOS);

        return rewardParticipantDTOS;
    }
    @Install(to = "rewardParticipantDToesDl", target = Target.DATA_LOADER)
    private List<RewardParticipantDTO> rewardParticipantDToesDlLoadDelegate(final LoadContext<RewardParticipantDTO> loadContext) {
        String fullname = investorName.getValue();
        String campaign = campaignName.getValue();
        String redeemedAmt = rewardValue.getValue();
        return loadRewardParticipant(fullname, campaign, redeemedAmt);
    }
    public List<RewardParticipantDTO> loadRewardParticipant(String fullname, String campaign, String redeemedAmt) {
        List<RewardParticipantDTO> list = getListRewardParticipantDTO();
        return list.stream().filter(rewardParticipantDTO -> {
            return (Strings.isNullOrEmpty(fullname) || fullname.contains(rewardParticipantDTO.getFullname())) &&
                    (Strings.isNullOrEmpty(campaign) || campaign.contains(rewardParticipantDTO.getCampaign_name())) &&
                    (Strings.isNullOrEmpty(redeemedAmt) || redeemedAmt.contains(rewardParticipantDTO.getRedeemed_amount().toString()));
        }).collect(Collectors.toList());
    }
}