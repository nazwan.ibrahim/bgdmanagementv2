package com.company.management.screen.transactionacct;

import com.company.management.api.ManagementController;
import com.company.management.app.ActivityLogBean;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.ActivityLogFile;
import com.company.management.entity.AdjustmentCash;
import com.company.management.entity.RtType;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionType;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@UiController("AdjustmentGoldEdit.edit")
@UiDescriptor("AdjustmentGoldEdit.xml")
public class AdjustmentGoldEdit extends Screen {
	private static final Logger log = LoggerFactory.getLogger(AdjustmentGoldEdit.class);
	@Autowired
	private UiComponents uiComponents;
	@Named("uploadFileTable.remove")
	private RemoveAction<ActivityLogFile> uploadFileTableRemove;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private TemporaryStorage temporaryStorage;
	@Autowired
	private CollectionContainer<ActivityLogFile> activityLogFilesDc;
	@Autowired
	private Notifications notifications;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private ActivityLogBean activityLogBean;
	@Autowired
	private TextField<BigDecimal> amountField;
	@Autowired
	private EntityComboBox<AccountBgd> accountNo;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private TextArea<String> remarksFieldId;
	@Autowired
	private TextField<String> uom;
	@Autowired
	private RadioButtonGroup<AdjustmentCash> addOrDeductCash;
	@Autowired
	private EntityComboBox<AccountBgd> investorID;
	@Autowired
	private TextField<String> investorName;
	@Autowired
	private HBoxLayout investorAccountList;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl1;

	public String getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}

	private String accountOwner;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl;
	@Autowired
	private InstanceContainer<ActivityLog> activityLogDc;
	@Autowired
	private CollectionLoader<ActivityLogFile> activityLogFilesDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Downloader downloader;

	@Subscribe
	public void onBeforeShow(final BeforeShowEvent event) {
		if(getAccountOwner().equals("AceAdmin")) {
			accountBgdsDl.setParameter("accOwner1", "AceAdmin");
		}else if(getAccountOwner().equals("BgdAdmin")) {
			accountBgdsDl.setParameter("accOwner1","BgdAdmin");
		}else {
			String query = "select e from AccountBgd e where e.accOwner <> :accOwner1 and e.accOwner <> :accOwner2 and e.walletType.code = :walletTypeCode1";
			accountBgdsDl.setQuery(query);
			accountBgdsDl.setParameter("accOwner1","AceAdmin");
			accountBgdsDl.setParameter("accOwner2","BgdAdmin");
		}
		accountBgdsDl.setParameter("walletTypeCode1","E02");
		accountBgdsDl.load();

		try{
			accountNo.setValue(accountBgdsDl.getContainer().getItems().get(0));
		}catch (Exception ex){
			log.info(ex.getMessage());
		}
		accountBgdsDl1.setParameter("walletTypeCode1","E02");
		accountBgdsDl1.setParameter("accOwner1","AceAdmin");
		accountBgdsDl1.setParameter("accOwner2","BgdAdmin");
		accountBgdsDl1.load();



	}

	@Subscribe
	public void onInit(final InitEvent event) {
		uom.setValue(rtType("J02").getName());
	}
	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
		activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
		uploadMultipleFile();
		investorAccountList.setVisible(getAccountOwner().equals("Investor"));
	}
	private RtType rtType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}
	private TransactionType type (String code){
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
		return this.dataManager.load(TransactionType.class)
				.query(query).parameter("code1", code)
				.one();
	}
	@Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
	private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
		Button btn = uiComponents.create(Button.class);
		btn.setCaption("Delete");
		btn.addClickListener(clickEvent -> {
			uploadFileTableRemove.execute();
		});
		return btn;
	}
	@Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
	private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
		if (activityLogFile.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(activityLogFile.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(activityLogFile.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
				activityLogFile.setActivityLog(activityLogDc.getItem());
				activityLogFile.setName(fileName);
				activityLogFile.setFileUpload(fileRef);
				activityLogFile.setAction(rtType("ACT01"));
				activityLogFilesDc.getMutableItems().add(activityLogFile);
			}
			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Adjustment Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> checkBeforeSave()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void checkBeforeSave(){
		if(!activityLogFilesDc.getItems().isEmpty() & !remarksFieldId.isEmpty() & !addOrDeductCash.isEmpty()){
			adjustmentGold();
		}else{
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Please fill in all the blanks")
					.show();
		}
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(final Button.ClickEvent event) {
		close(StandardOutcome.CLOSE);
	}

	private void adjustmentGold(){
		if(accountOwner.equals("Investor")){
			accountNo.setValue(investorID.getValue());
		}else{
			accountNo.setValue(accountBgdsDl.getContainer().getItems().get(0));
		}
		if(amountField.getValue() != null & Objects.nonNull(accountNo.getValue())) {
			String queryaccountBgd = "select a from AccountBgd a " +
					"where a.acctNumber = :acctNumber1";
			AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
					.query(queryaccountBgd).parameter("acctNumber1", accountNo.getValue().getAcctNumber()).one();

			TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);
			String querytransStatus = "select t from TransStatus t " +
					"where t.code = :code1";
			TransStatus transStatus = this.dataManager.load(TransStatus.class)
					.query(querytransStatus).parameter("code1", "G05/02")
					.one();

			transactionAcct.setAccount(accountBgd);
			if(addOrDeductCash.getValue().equals(AdjustmentCash.ADDITION)){
				transactionAcct.setAmount(amountField.getValue());
			}else{
				transactionAcct.setAmount(amountField.getValue().negate());
			}
			transactionAcct.setTransStatus(transStatus);
			transactionAcct.setTransType(type("F13/E02"));
			String reference = managementController.generateSeqNo("BGDF13","6");
			transactionAcct.setReference(reference);
			transactionAcct.setCreatedBy(accountBgd.getAccOwner());
			transactionAcct.setCreatedDate(new Date());

			this.dataManager.save(transactionAcct);

			if(accountBgd.getAccOwner().equals("BgdAdmin")) {
				activityLogBean.makerActivity("MDL03-D01-E02", "LOG03-F11", "Reference No = " +reference,
						"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
			} else if(accountBgd.getAccOwner().equals("AceAdmin")) {
				activityLogBean.makerActivity("MDL03-D04-E02", "LOG03-F11", "Reference No = " +reference,
						"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
			}else{
				activityLogBean.makerActivity("MDL03-D02-E02", "LOG03-F11", "Reference No = " +reference,
						"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
			}

			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.HUMANIZED)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Successfully")
					.show();
		}else {
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Amount Not Successfully")
					.show();
		}
		close(StandardOutcome.CLOSE);
	}

	@Subscribe("investorID")
	public void onInvestorIDValueChange(final HasValue.ValueChangeEvent<AccountBgd> event) {
		investorName.setValue(event.getValue().getAccOwner());
	}

	@Subscribe("investorNameID")
	public void onInvestorNameIDValueChange(final HasValue.ValueChangeEvent<AccountBgd> event) {
		investorID.setValue(event.getValue());
	}
}