package com.company.management.screen.investorreportbrowsedto;

import com.company.management.app.BgdIDBean;
import com.company.management.entity.*;
import com.company.management.entity.registrationdb.*;
import com.company.management.entity.wallet.AccountWalletView;
import com.company.management.entity.wallet.RedeemInfo;
import com.company.management.entity.wallet.RedeemTracker;
import com.company.management.entity.wallet.TransferInfoView;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.core.LoadContext;
import io.jmix.core.Metadata;
import io.jmix.core.SaveContext;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Label;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@UiController("InvestorReportDTO.edit")
@UiDescriptor("investor-report-dto-edit.xml")
@EditedEntityContainer("investorReportBrowseDTODc")
public class InvestorReportDTOEdit extends StandardEditor<InvestorReportBrowseDTO> {

    private static final Logger log = LoggerFactory.getLogger(InvestorReportDTOEdit.class);
    @Autowired
    private InstanceLoader<UserProfile> userProfileDl;
    @Autowired
    private InstanceLoader<UserAddress> userAddressesDl;
    @Autowired
    private InstanceContainer<UserProfile> userProfileDc;
    @Autowired
    private InstanceLoader<UserBank> userBanksDl;
    @Autowired
    private InstanceLoader<UserTier> userTiersDl;
//    @Autowired
//    private InstanceLoader<KycStatus> kycStatusesDl;
//    @Autowired
//    private InstanceLoader<KycStatus> kycStatusesDl1;
    @Autowired
    private InstanceLoader<UserEmployment> userEmploymentDl;
    @Autowired
    private InstanceContainer<UserEmployment> userEmploymentDc;
    @Autowired
    private InstanceLoader<StatutoryDetail> statutoryDetailsDl;
    @Autowired
    private InstanceContainer<StatutoryDetail> statutoryDetailsDc;
    @Autowired
    private InstanceLoader<FinancialSource> financialSourcesDl;
    @Autowired
    private InstanceContainer<FinancialSource> financialSourcesDc;
    @Autowired
    private InstanceContainer<ActivityLog> activityLogDc;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Autowired
    private InstanceLoader<UserVerification> userVerificationDl;
    @Autowired
    private InstanceContainer<UserVerification> userVerificationDc;
    @Autowired
    private BgdIDBean bgdIDBean;
    @Autowired
    private InstanceContainer<InvestorReportBrowseDTO> investorReportBrowseDTODc;
    @Autowired
    private InstanceLoader<InvestorReportBrowseDTO> investorReportBrowseDTODl;
    @Autowired
    private InstanceContainer<UserAddress> userAddressesDc;
    @Autowired
    private InstanceLoader<UserAddress> userAddressesMailDl;
    @Autowired
    private InstanceContainer<UserAddress> userAddressesMailDc;
    @Autowired
    private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsCashDl;
    @Autowired
    private CipherEncryptionManagement cipherEncryptionManagement;
    @Autowired
    private Environment environment;
    @Autowired
    private TextField bgdID;
    @Autowired
    private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsGoldDl;
    @Autowired
    private InstanceContainer<UserTier> userTiersDc;
    @Autowired
    private CollectionLoader<TransferInfoView> transferInfoViewsDl;
    @Autowired
    private CollectionLoader<AccountWalletView> accountWalletViewsDl;
    @Autowired
    private CollectionLoader<AccountWalletView> accountWalletViewsGoldDl;
    @Autowired
    private CollectionContainer<AccountWalletView> accountWalletViewsDc;
    @Autowired
    private Label<BigDecimal> totalCash;
    @Autowired
    private Label totalGoldAmount;
    @Autowired
    private Label totalTransferGold;

    DecimalFormat df = new DecimalFormat("#,###,##0.000000");
    @Autowired
    private CollectionContainer<TransferInfoView> transferInfoViewsDc;
    @Autowired
    private Label totalGoldReceived;
    @Autowired
    private CollectionLoader<TransferInfoView> transferInfoViewsReceiverDl;
    @Autowired
    private CollectionContainer<TransferInfoView> transferInfoViewsReceiverDc;
    @Autowired
    private Label totalReceivedGold;
    @Autowired
    private CollectionLoader<TransferInfoView> transferInfoViewsSenderDl;
    @Autowired
    private CollectionContainer<TransferInfoView> transferInfoViewsSenderDc;
    @Autowired
    private CollectionLoader<RedeemInfo> redeemInfoDl;
    @Autowired
    private CollectionContainer<RedeemInfo> redeemInfoDc;
    @Autowired
    private Label totalRedeemedGoldG;
    @Autowired
    private Label totalRedeemedGoldPcs;
    @Autowired
    private Metadata metadata;
    @Autowired
    private CollectionContainer<GoldRedemptionDTO> goldRedemptionDTODc;
    @Autowired
    private TextField<String> otherSourceFunds;
    @Autowired
    private TextField<String> otherSourceWealth;
    @Autowired
    private CollectionLoader<FatcaCrsDeclaration> fatcaCRSDl;
    @Autowired
    private CollectionContainer<FatcaCrsDeclaration> fatcaCRSDc;
    @Autowired
    private CollectionContainer<FatcaCRSDTO> fatcaCRSDTODc;
    @Autowired
    private TextField<String> employerName;
    @Autowired
    private TextField<JobPosition> position;
    @Autowired
    private TextField<NatureOfBusiness> natureBusiness;
    @Autowired
    private TextField<String> countryTaxResidence;
    @Autowired
    private TextField<String> businessName;
    @Autowired
    private TextField<String> previousEmployerName;
//    @Autowired
//    private CollectionLoader<RedeemInfo> goldRedemptionDl;
//    @Autowired
//    private CollectionContainer<RedeemInfo> goldRedemptionDc;
//    @Autowired
//    private CollectionLoader<RedeemTracker> redeemTrackerDl;
//    @Autowired
//    private CollectionLoader<RedeemInfo> redeemInfoDl;
//    @Autowired
//    private CollectionContainer<RedeemInfo> redeemInfoDc;

    @Install(to = "investorReportBrowseDTODl", target = Target.DATA_LOADER)
    private InvestorReportBrowseDTO investorReportBrowseDTODlLoadDelegate(LoadContext<InvestorReportBrowseDTO> loadContext) {
        // Here you can load entity from an external store by ID passed in LoadContext

        return getEditedEntity();
    }

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {
        try{
            investorReportBrowseDTODl.load();
//            String selectedEmail = investorReportBrowseDTODc.getItem().getEmail();
//            String emailSelected = selectedEmail;

        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try {
            userProfileDl.setParameter("email1", investorReportBrowseDTODc.getItem().getUsername());
            userProfileDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try {
            userAddressesDl.setParameter("userEmail1",investorReportBrowseDTODc.getItem().getUsername());
            userAddressesDl.setParameter("addTypeCode1","L01");
            userAddressesDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try {
            userAddressesMailDl.setParameter("userEmail1",investorReportBrowseDTODc.getItem().getUsername());
            userAddressesMailDl.setParameter("addTypeCode1","L03");
            userAddressesMailDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            userBanksDl.setParameter("userEmail1", investorReportBrowseDTODc.getItem().getUsername());
            userBanksDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            userTiersDl.setParameter("userEmail1", investorReportBrowseDTODc.getItem().getUsername());
            userTiersDl.setParameter("currentTier1", true);
            userTiersDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
//        try{
//            kycStatusesDl.setParameter("code1", "G12");
//            kycStatusesDl1.setParameter("code1", "G10");
//            kycStatusesDl.load();
//            kycStatusesDl1.load();
//        }catch (Exception ex){}
        try{
            userEmploymentDl.setParameter("userEmail1",investorReportBrowseDTODc.getItem().getUsername());
            userEmploymentDl.load();
            userEmploymentDc.getItem().getPosition().getName();
            userEmploymentDc.getItem().getNatureOfBusiness().getCode();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try {
            if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("EMPL")){ //Employed
                //set visible true
                employerName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("SLEMPL")) { //Self - Employed
                businessName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("UNEMPL")) { //Unemployed
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("RETIRED")) { // Retired
                previousEmployerName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try{
            statutoryDetailsDl.setParameter("userEmail1", investorReportBrowseDTODc.getItem().getUsername());
            statutoryDetailsDl.load();
            statutoryDetailsDc.getItem().getTransactionPurposeType().getName();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            financialSourcesDl.setParameter("userEmail1",investorReportBrowseDTODc.getItem().getUsername());
            financialSourcesDl.load();
            try{
                financialSourcesDc.getItem().getSourceFundType().getName();
                }catch (Exception ex){
                    log.info(ex.getMessage());
                }
            try {
                financialSourcesDc.getItem().getSourceWealthType().getName();
                }catch (Exception ex){
                    log.info(ex.getMessage());
                }
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        //SET VISIBLE SOURCE OF FUNDS/WEALTH
            try{
                if (financialSourcesDc.getItem().getSourceFundType().getName().equals("Others"))
                    otherSourceFunds.setVisible(true);
            }catch (Exception ex){
            log.info(ex.getMessage());
            }
            try{
                if (financialSourcesDc.getItem().getSourceWealthType().getName().equals("Others"))
                    otherSourceWealth.setVisible(true);
            }catch (Exception ex){
            log.info(ex.getMessage());
            }
        try{
            activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            userVerificationDl.setParameter("userEmail1", investorReportBrowseDTODc.getItem().getUsername());
            userVerificationDl.load();
            try {
                userVerificationDc.getItem().getKycResult().getName();
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
            try {
                userVerificationDc.getItem().getNsResult().getName();
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
            try {
                userVerificationDc.getItem().getCrpResult().getName();
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
            BgdIDDTO bgdIDBean1 = bgdIDBean.getBgdID(userProfileDc.getItem().getUsername());
            bgdID.setValue(bgdIDBean1.getBgdID());
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //CASH TRANSACTION
            transactionHistoriesViewsCashDl.setParameter("accOwner1", investorReportBrowseDTODc.getItem().getUsername());
            transactionHistoriesViewsCashDl.setParameter("walletCode1", "E01");
            transactionHistoriesViewsCashDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //GOLD TRANSACTION
            transactionHistoriesViewsGoldDl.setParameter("accOwner1", investorReportBrowseDTODc.getItem().getUsername());
            transactionHistoriesViewsGoldDl.setParameter("walletCode1", "E02");
            transactionHistoriesViewsGoldDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //TABLE TRANSFERRED GOLD
            transferInfoViewsDl.setParameter("sender1", investorReportBrowseDTODc.getItem().getUsername());
            transferInfoViewsDl.setParameter("receiver1", investorReportBrowseDTODc.getItem().getUsername());
            transferInfoViewsDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //TRANSFERRED GOLD
            transferInfoViewsSenderDl.setParameter("sender1", investorReportBrowseDTODc.getItem().getUsername());
            transferInfoViewsSenderDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //RECEIVED GOLD
            transferInfoViewsReceiverDl.setParameter("receiver1", investorReportBrowseDTODc.getItem().getUsername());
            transferInfoViewsReceiverDl.load();
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //CASH WALLET
            accountWalletViewsDl.setParameter("accountOwner1", investorReportBrowseDTODc.getItem().getUsername());
            accountWalletViewsDl.setParameter("code1", "E01");
            accountWalletViewsDl.load();
            totalCash.setValue(accountWalletViewsDl.getContainer().getMutableItems().get(0).getCurrentAmmount());
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{ //GOLD WALLET
            accountWalletViewsGoldDl.setParameter("accountOwner1", investorReportBrowseDTODc.getItem().getUsername());
            accountWalletViewsGoldDl.setParameter("code1", "E02");
            accountWalletViewsGoldDl.load();
            totalGoldAmount.setValue(accountWalletViewsGoldDl.getContainer().getMutableItems().get(0).getCurrentAmmount());
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try{
            totalTransferGold.setValue(df.format(countTotalTransfer()));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try{
            totalReceivedGold.setValue(df.format(countTotalReceived()));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try {
            redeemInfoDl.setParameter("createdBy1", investorReportBrowseDTODc.getItem().getUsername());
            redeemInfoDl.load();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try {
            totalRedeemedGoldPcs.setValue(df.format(countTotalRedeemedPcs()));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
        try {
            totalRedeemedGoldG.setValue(df.format(countTotalRedeemedG()));
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }


//        try{
//            goldRedemptionDl.setParameter("accountOwner1", userTiersDc.getItem().getUser().getUsername());
//            goldRedemptionDl.load();
//            goldRedemptionDc.getItems().get(0).getReference();
//
//        }catch (Exception ex){
//            System.out.println(ex.getMessage());
//        }
//        List<GoldRedemptionDTO> goldRedemptionDTOS = new ArrayList<>();
//        redeemInfoDc.getItems().forEach(e - > {
//            GoldRedemptionDTO goldRedemptionDTO
//        });
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
            List<GoldRedemptionDTO> goldRedemptionDTOS = new ArrayList<>();
            try {
                redeemInfoDc.getItems().forEach(e -> {
                    GoldRedemptionDTO goldRedemptionDTO = metadata.create(GoldRedemptionDTO.class);
                    if (e.getReference() != null) {
                        goldRedemptionDTO.setReference(e.getReference());
                    }
                    if (e.getUnit() != null) {
                        goldRedemptionDTO.setUnit(e.getUnit());
                    }
                    if (e.getAddress() != null) {
                        goldRedemptionDTO.setAddress(e.getAddress());
                    }
                    if (e.getCreatedDate() != null) {
                        goldRedemptionDTO.setCreated_date(e.getCreatedDate());
                    }
                    List<RedeemTracker> redeemTracker = this.dataManager.load(RedeemTracker.class)
                            .query("select e from RedeemTracker e where e.createdBy = :createdBy1")
                            .parameter("createdBy1", e.getCreatedBy())
                            .list();
                    if (redeemTracker.size() > 0 && redeemTracker.get(0).getTrackerStatus() != null) {
                        goldRedemptionDTO.setStatus(redeemTracker.get(0).getTrackerStatus().getName());
                    }else{
                        goldRedemptionDTO.setStatus("");
                    }
                    goldRedemptionDTOS.add(goldRedemptionDTO);

                });
            }catch (Exception ex){
                log.error(ex.getMessage());
            }
            goldRedemptionDTODc.setItems(goldRedemptionDTOS);

        try {
            fatcaCRSDl.setParameter("user1", userProfileDc.getItem());
            fatcaCRSDl.load();

            List<String[]> list1 = new ArrayList<>();
            List<String[]> list2 = new ArrayList<>();
            List<String[]> list3 = new ArrayList<>();
            List<String[]> list4 = new ArrayList<>();
            List<String[]> data = new ArrayList<>();
            List<FatcaCRSDTO> fatcaCRSDTOS = new ArrayList<>();
            try {
                fatcaCRSDc.getItems().forEach(e -> {
                    if(e.getDeclaration().getCode().equals("DCLRN01")){
                        String[] str1 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list1.add(str1);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN02")){
                        String[] str2 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list2.add(str2);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN03")){
                        String[] str3 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list3.add(str3);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN04")){
                        String[] str4 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list4.add(str4);
                    }
                });
                data.add(list1.get(0));
                data.add(list2.get(0));
                data.add(list3.get(0));
                data.add(list4.get(0));
                for(String[] str : data){
                    FatcaCRSDTO fatcaCRSDTO = metadata.create(FatcaCRSDTO.class);
                    fatcaCRSDTO.setAgreed(Boolean.parseBoolean(str[0]));
                    fatcaCRSDTO.setDeclaration(str[1]);
                    fatcaCRSDTOS.add(fatcaCRSDTO);
                }
                fatcaCRSDTODc.setItems(fatcaCRSDTOS);
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        }

    @Install(target = Target.DATA_CONTEXT)
    private Set<Object> commitDelegate(SaveContext saveContext) {
        // Here you can save the edited entity or the whole SaveContext in an external store.
        // Return the set of saved instances.
        return Collections.singleton(getEditedEntity());
    }

    //Transferred Gold Function
    private BigDecimal countTotalTransfer() {
        BigDecimal bd = new BigDecimal(0);
        for(TransferInfoView transferInfoView : transferInfoViewsSenderDc.getItems()){
            if(transferInfoView.getStatusCode().equals("G01")){
                bd = bd.add(transferInfoView.getAmount());
            }
        }
        return bd;
    }

    //Gold Received Function
    private BigDecimal countTotalReceived() {
        BigDecimal bd = new BigDecimal(0);
        for(TransferInfoView transferInfoView : transferInfoViewsReceiverDc.getItems()){
            if(transferInfoView.getStatusCode().equals("G01")){
                bd = bd.add(transferInfoView.getAmount());
            }
        }
        return bd;
    }

    //Gold Redeemed Pieces Function
    private BigDecimal countTotalRedeemedPcs() {
        BigDecimal pcs = new BigDecimal(0);
        for(RedeemInfo redeemInfo : redeemInfoDc.getItems()){
                pcs = pcs.add(redeemInfo.getUnit());
        }
        return pcs;
    }

    //Gold Redeemed Gram Function
    private BigDecimal countTotalRedeemedG() {
        BigDecimal g = new BigDecimal(0);
        for(RedeemInfo redeemInfo : redeemInfoDc.getItems()){
                g = g.add(redeemInfo.getUnit());
        }
        BigDecimal gbd = new BigDecimal(4.25);
        g = g.multiply(gbd);
        return g;
    }
    @Install(to = "identificationNoField", subject = "formatter")
    private String identificationNoFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }

    @Install(to = "phoneNumberField", subject = "formatter")
    private String phoneNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }

    @Install(to = "homeNumberField", subject = "formatter")
    private String homeNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }

    @Install(to = "officeNumberField", subject = "formatter")
    private String officeNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }
    @Install(to = "bankAccount", subject = "formatter")
    private String bankAccountFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }
    @Install(to = "dobField", subject = "formatter")
    private String dobFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value, environment.getProperty("secret_key"));
    }

    @Subscribe("closeBtn")
    public void onCloseBtnClick(Button.ClickEvent event) {
        close(StandardOutcome.CLOSE);
    }

//    @Install(to = "goldRedemptionDTODl", target = Target.DATA_LOADER)
//    private List<GoldRedemptionDTO> goldRedemptionDTOLoadDelegate(final LoadContext<GoldRedemptionDTO> loadContext) {
//        String fullname = investorName.getValue();
//        String campaign = campaignName.getValue();
//        String redeemedAmt = rewardValue.getValue();
//        return loadRewardParticipant(fullname, campaign, redeemedAmt);
//    }
//    public List<RewardParticipantDTO> loadRewardParticipant(String fullname, String campaign, String redeemedAmt) {
//        List<RewardParticipantDTO> list = getListRewardParticipantDTO();
//        return list.stream().filter(rewardParticipantDTO -> {
//            return (Strings.isNullOrEmpty(fullname) || fullname.contains(rewardParticipantDTO.getFullname())) &&
//                    (Strings.isNullOrEmpty(campaign) || campaign.contains(rewardParticipantDTO.getCampaign_name())) &&
//                    (Strings.isNullOrEmpty(redeemedAmt) || redeemedAmt.contains(rewardParticipantDTO.getRedeemed_amount().toString()));
//        }).collect(Collectors.toList());
//    }
}