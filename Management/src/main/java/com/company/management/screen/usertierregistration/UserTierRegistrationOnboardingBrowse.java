package com.company.management.screen.usertierregistration;

import com.company.management.entity.registrationdb.UserTier;
import io.jmix.core.DataManager;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@UiController("UserTierRegistrationOnboarding.browse")
@UiDescriptor("user-tier-registration-onboarding-browse.xml")
@LookupComponent("userTierRegistrationsTable")
public class UserTierRegistrationOnboardingBrowse extends StandardLookup<UserTier> {

	@Autowired
	private CollectionLoader<UserTier> userTierRegistrationsDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<UserTier> userTierRegistrationsDc;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		userTierRegistrationsDl.setParameter("tierCode1","T01");
		userTierRegistrationsDl.load();
	}

	@Subscribe("userFreeTab")
	public void onUserFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("userFreeAccount")){
			userTierRegistrationsDl.setParameter("tierCode1","T01");
			userTierRegistrationsDl.load();
		}else {
			List<UserTier> userTierRegistration = this.dataManager.load(UserTier.class)
							.query("select e from UserTierRegistration e where e.tier.code <> :code")
									.parameter("code","T01")
											.list();
			userTierRegistrationsDc.setItems(userTierRegistration);
		}
	}
}