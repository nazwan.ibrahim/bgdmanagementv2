package com.company.management.screen.transstatus;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransStatus;

@UiController("TransStatus.browse")
@UiDescriptor("trans-status-browse.xml")
@LookupComponent("transStatusesTable")
public class TransStatusBrowse extends StandardLookup<TransStatus> {
}