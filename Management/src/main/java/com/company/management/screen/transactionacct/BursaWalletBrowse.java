package com.company.management.screen.transactionacct;

import com.company.management.bgdreporting.RevenueFeesReport;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.PurchaseRecordProduct;
import com.company.management.entity.TransactionHistoriesView;
import com.company.management.entity.wallet.AccountWalletView;
import com.company.management.screen.transactionhistoriesview.TransactionApproveViewEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.screen.LookupComponent;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

@UiController("BursaWallet.browse")
@UiDescriptor("bursa-wallet-browse.xml")
@LookupComponent("transactionAcctsTable")
public class BursaWalletBrowse extends StandardLookup<TransactionAcct> {
	private static final Logger log = LoggerFactory.getLogger(BursaWalletBrowse.class);
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Label<BigDecimal> totalCash;
	@Autowired
	private Label<String> totalgoldBar;
//	@Autowired
//	private Label<BigDecimal> totalgoldDinar;
	private String accountNum;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsDl;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsDc;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsDl1;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsDc1;
	@Autowired
	private CollectionLoader<PurchaseRecordProduct> purchaseRecordProductsDl;
	@Autowired
	private CollectionContainer<PurchaseRecordProduct> purchaseRecordProductsDc;

	BigDecimal totalTradable = BigDecimal.ZERO;
	BigDecimal totalHolding = BigDecimal.ZERO;
	@Autowired
	private Label<String> totalgoldPhysical;
	@Autowired
	private Label<String> totalgoldDinar;
	DecimalFormat df = new DecimalFormat("#,###,##0.000000");
	@Autowired
	private CollectionContainer<TransactionHistoriesView> transactionHistoriesViewsDc;
	DecimalFormat dfmillion = new DecimalFormat("000,000,000,000.00");
	@Autowired
	private RevenueFeesReport revenueFeesReport;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsBursaCashDl;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsBursaGoldDl;
	@Autowired
	private Table<TransactionHistoriesView> transactionAcctsTable;
	@Autowired
	private Table<TransactionHistoriesView> transactionAcctsTableGold;
	@Autowired
	private Table<ActivityLog> activityLogBursaCashTable;
	@Autowired
	private Table<ActivityLog> activityLogBursaGoldTable;

	private void setOnBeforeShow(){
		accountWalletViewsDl1.setParameter("accountOwner1","BgdAdmin");
		accountWalletViewsDl1.setParameter("code1","E01");
		accountWalletViewsDl1.load();


		accountWalletViewsDl.setParameter("accountOwner1","BgdAdmin");
		accountWalletViewsDl.setParameter("code1","E02");
		accountWalletViewsDl.load();

		transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
		transactionHistoriesViewsDl.setParameter("walletCode1","E01");
		transactionHistoriesViewsDl.load();

		activityLogsBursaCashDl.setParameter("moduleCode", "MDL03-D01-E01");
		activityLogsBursaCashDl.load();

		activityLogsBursaGoldDl.setParameter("moduleCode", "MDL03-D01-E02");
		activityLogsBursaGoldDl.load();

		if(!accountWalletViewsDc1.getItems().isEmpty()) {
			totalCash.setValue(accountWalletViewsDc1.getItems().get(0).getAvailableAmount());
		}else {
			totalCash.setValue(BigDecimal.ZERO);
		}
		if(!accountWalletViewsDc.getItems().isEmpty()){
			totalgoldBar.setValue(df.format(accountWalletViewsDc.getItems().get(0).getAvailableAmount().setScale(6, RoundingMode.HALF_EVEN)));
			BigDecimal divide = accountWalletViewsDc.getItems().get(0).getAvailableAmount().divide(BigDecimal.valueOf(4.25), 6, RoundingMode.DOWN);
			totalgoldDinar.setValue(df.format(divide));
		}else{
			totalgoldBar.setValue(df.format(BigDecimal.ZERO));
			totalgoldDinar.setValue(df.format(BigDecimal.ZERO));
		}

	}

	/** To change column status color **/
	@Install(to = "transactionAcctsTable", subject = "styleProvider")
	protected String transactionTableStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "transactionAcctsTableGold", subject = "styleProvider")
	protected String transactionTableGoldStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}


	@Install(to = "activityLogBursaCashTable", subject = "styleProvider")
	protected String activityLogBursaCashTableStyleProvider(ActivityLog activityLogCashBursa, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (activityLogCashBursa.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "activityLogBursaGoldTable", subject = "styleProvider")
	protected String activityLogBursaGoldTableStyleProvider(ActivityLog activityLogGoldBursa, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (activityLogGoldBursa.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}
	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		setOnBeforeShow();
	}
	@Subscribe
	public void onAfterShow(AfterShowEvent event) {

		purchaseRecordProductsDc.getItems().forEach(purchaseRecordProduct -> {
			totalTradable = totalTradable.add(purchaseRecordProduct.getTradable_gold());
			totalHolding = totalHolding.add(purchaseRecordProduct.getHolding_acct());
		});

		BigDecimal totalAdd = totalHolding.add(totalTradable);
		totalgoldPhysical.setValue(df.format(totalAdd));
	}
	@Subscribe("topup")
	public void onTopupClick(Button.ClickEvent event) {
		TopUpCashEdit topUpCashEdit = screenBuilders.screen(this)
				.withScreenClass(TopUpCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();

				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		topUpCashEdit.setAccountOwner("BgdAdmin");
		topUpCashEdit.show();
	}
	@Subscribe("withdraw")
	public void onWithdrawClick(Button.ClickEvent event) {
		WithdrawCashEdit withdrawCashEdit = screenBuilders.screen(this)
				.withScreenClass(WithdrawCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();

				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		withdrawCashEdit.setAccountOwner("BgdAdmin");
		withdrawCashEdit.show();
	}
	@Subscribe("freeTab")
	public void onFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("cashAccount")) {
			transactionHistoriesViewsDl.setParameter("accOwner1", "BgdAdmin");
			transactionHistoriesViewsDl.setParameter("walletCode1", "E01");
			transactionHistoriesViewsDl.load();
		}else{
			transactionHistoriesViewsDl.setParameter("accOwner1", "BgdAdmin");
			transactionHistoriesViewsDl.setParameter("walletCode1", "E02");
			transactionHistoriesViewsDl.load();
		}
	}

	@Install(to = "transactionAcctsTableGold.amount", subject = "formatter")
	private String transactionAcctsTableGoldAmountFormatter(Object object) {
		return df.format(object);
	}

	@Subscribe("CSVDownload")
	public void onCSVDownloadClick(Button.ClickEvent event) throws FileNotFoundException {
		String dateFormat = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		BigDecimal totalAmount = BigDecimal.ZERO;
		String path="C:\\Users\\amir\\Downloads\\test.txt";
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
//			tierManagementsTable.getColumns().forEach(tierManagementColumn -> {
//				headersList.add(tierManagementColumn.getCaption());
//			});
//			csvPrinter.printRecord(headersList);
			transactionHistoriesViewsDc.getMutableItems().forEach(a -> {
				String getTransactionCode = a.getTransactionCode() != null ? a.getTransactionCode(): "";
				String getReference = a.getReference() != null ? a.getReference(): "";
				String getAmount = a.getAmount() != null ? dfmillion.format(a.getAmount()) : "";
				String getCreatedDate = a.getCreatedDate() != null ? String.valueOf(a.getCreatedDate()) : "";
				String getStatus = a.getStatus() != null ? a.getStatus() : "";
				try {
					csvPrinter.printRecord(getTransactionCode,getReference,getAmount,getCreatedDate,getStatus);
				} catch (IOException e) {
					log.error("IOException Error :"+e);
				}
			});

			for(TransactionHistoriesView transactionHistoriesView : transactionHistoriesViewsDc.getItems()){
				totalAmount = totalAmount.add(transactionHistoriesView.getAmount());
			}
			csvPrinter.printRecord(null,null,dfmillion.format(totalAmount),null,null);
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
		}
	}

	@Subscribe("transferCash")
	public void onTransferCashClick(final Button.ClickEvent event) {
		TransferCashEdit transferCashEdit = screenBuilders.screen(this)
				.withScreenClass(TransferCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferCashEdit.setAccountOwner("BgdAdmin");
		transferCashEdit.show();
	}

	@Subscribe("testCas")
	public void onTestCasClick(final Button.ClickEvent event) throws ParseException {
		revenueFeesReport.saveRevenueAndFees();
	}

	@Subscribe("transactionAcctsTable.view")
	public void onTransactionAcctsTableView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionAcctsTable.getSingleSelected()));
		screen.show();
	}

	@Subscribe("adjustmentCash")
	public void onAdjustmentCashClick(final Button.ClickEvent event) {
		AdjustmentCashEdit adjustmentCashEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentCashEdit.setAccountOwner("BgdAdmin");
		adjustmentCashEdit.show();
	}

	@Subscribe("adjustmentGold")
	public void onAdjustmentGoldClick(final Button.ClickEvent event) {
		AdjustmentGoldEdit adjustmentCashEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentCashEdit.setAccountOwner("BgdAdmin");
		adjustmentCashEdit.show();
	}

	@Subscribe("transferGold")
	public void onTransferGoldClick(final Button.ClickEvent event) {
		TransferGoldEdit transferGoldEdit = screenBuilders.screen(this)
				.withScreenClass(TransferGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferGoldEdit.setAccountOwner("BgdAdmin");
		transferGoldEdit.show();
	}

	@Subscribe("transactionAcctsTableGold.view")
	public void onTransactionAcctsTableGoldView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionAcctsTableGold.getSingleSelected()));
		screen.show();
	}














//	@Override
//	public String getFileName(DataGrid<Object> dataGrid) {
//		return "MyExcelFile.xlsx";
//	}

}