package com.company.management.screen.holdingacct;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.Table;
import io.jmix.ui.screen.*;
import com.company.management.entity.HoldingAcct;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@UiController("HoldingAcct.browse")
@UiDescriptor("holding-acct-browse.xml")
@LookupComponent("holdingAcctsTable")
public class HoldingAcctBrowse extends StandardLookup<HoldingAcct> {

	@Autowired
	private ScreenBuilders screenBuilders;

	@Install(to = "holdingAcctsTable.physical", subject = "columnGenerator")
	private Component holdingAcctsTablePhysicalColumnGenerator(HoldingAcct holdingAcct) {
		BigDecimal holdAcct = holdingAcct.getHolding();
		BigDecimal tradable = holdingAcct.getTradable();
		BigDecimal total = holdAcct.add(tradable);
		return new Table.PlainTextCell(total.toString());
	}

	@Subscribe("convertToDinar")
	public void onConvertToDinarClick(Button.ClickEvent event) {
		HoldingAcctEdit holdingAcctEdit = screenBuilders.screen(this)
				.withScreenClass(HoldingAcctEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.withOpenMode(OpenMode.DIALOG)
				.build();
		holdingAcctEdit.show();
	}
}