package com.company.management.screen.tierthreshold;

import io.jmix.ui.screen.*;
import com.company.management.entity.TierThreshold;

@UiController("TierThreshold.edit")
@UiDescriptor("tier-threshold-edit.xml")
@EditedEntityContainer("tierThresholdDc")
public class TierThresholdEdit extends StandardEditor<TierThreshold> {
}