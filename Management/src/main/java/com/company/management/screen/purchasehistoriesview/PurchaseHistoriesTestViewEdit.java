package com.company.management.screen.purchasehistoriesview;

import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseHistoriesView;

@UiController("PurchaseHistoriesTestView.edit")
@UiDescriptor("purchase-histories-test-view-edit.xml")
@EditedEntityContainer("purchaseHistoriesViewDc")
public class PurchaseHistoriesTestViewEdit extends StandardEditor<PurchaseHistoriesView> {
}