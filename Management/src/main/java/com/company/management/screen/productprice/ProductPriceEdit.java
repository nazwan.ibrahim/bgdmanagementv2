package com.company.management.screen.productprice;

import io.jmix.ui.screen.*;
import com.company.management.entity.ProductPrice;

@UiController("ProductPrice.edit")
@UiDescriptor("product-price-edit.xml")
@EditedEntityContainer("productPriceDc")
public class ProductPriceEdit extends StandardEditor<ProductPrice> {
}