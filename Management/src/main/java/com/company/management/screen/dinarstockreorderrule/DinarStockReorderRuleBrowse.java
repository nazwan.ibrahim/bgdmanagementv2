package com.company.management.screen.dinarstockreorderrule;

import io.jmix.ui.screen.*;
import com.company.management.entity.DinarStockReorderRule;

@UiController("DinarStockReorderRule.browse")
@UiDescriptor("dinar-stock-reorder-rule-browse.xml")
@LookupComponent("dinarStockReorderRulesTable")
public class DinarStockReorderRuleBrowse extends StandardLookup<DinarStockReorderRule> {
}