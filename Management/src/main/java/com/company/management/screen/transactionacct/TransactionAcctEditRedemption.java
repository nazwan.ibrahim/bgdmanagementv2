package com.company.management.screen.transactionacct;

import com.company.management.entity.TransactionHistoriesView;
import com.company.management.entity.registrationdb.UserAddress;
import com.company.management.entity.registrationdb.UserTier;
import com.company.management.entity.wallet.RedeemInfo;
import com.company.management.entity.wallet.RedeemTracker;
import com.company.management.entity.wallet.TransferInfoView;
import io.jmix.ui.Dialogs;
import io.jmix.ui.UiComponents;
import io.jmix.ui.component.*;
import io.jmix.ui.component.data.table.ContainerGroupTableItems;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

@UiController("TransactionAcctRedemption.edit")
@UiDescriptor("transaction-acct-redemption-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class TransactionAcctEditRedemption extends StandardEditor<TransactionAcct> {
	@Autowired
	private CollectionLoader<UserTier> userTierRegistrationsDl;
//	@Autowired
//	private TextField<String> idTypeField;
	@Autowired
	private CollectionContainer<UserTier> userTierRegistrationsDc;
//	@Autowired
//	private TextField<String> idNoField;
	@Autowired
	private CollectionLoader<UserAddress> userAddressesDl;
//	@Autowired
//	private TextField<String> addressLineOne;
//	@Autowired
//	private TextField<String> addressLineTwo;
//	@Autowired
//	private TextField<String> postcodeAddress;
//	@Autowired
//	private TextField<String> stateAddress;
	@Autowired
	private CollectionContainer<UserAddress> userAddressesUserDc;
	@Autowired
	private CollectionLoader<UserAddress> userAddressesUserDl;
	@Autowired
	private CollectionLoader<TransferInfoView> transferInfoViewsDl;
	@Autowired
	private CollectionContainer<RedeemInfo> redeemInfoesDc;
	@Autowired
	private CollectionLoader<RedeemInfo> redeemInfoesDl;
	@Autowired
	private InstanceContainer<TransactionAcct> transactionAcctDc;
	@Autowired
	private TextField<String> quantity;
	@Autowired
	private TextField<String> name;
	@Autowired
	private TextField<String> createdDate;
	@Autowired
	private TextField<String> reason;
	@Autowired
	private TextField<String> phoneNo;
	@Autowired
	private TextArea<String> address;
	@Autowired
	private TextField<String> reference;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
	@Autowired
	private CollectionContainer<TransactionHistoriesView> transactionHistoriesViewsDc;
	@Autowired
	private TextField<String> courierFee;
	@Autowired
	private TextField<String> mintingFee;
	@Autowired
	private TextField<String> takafulFee;
	@Autowired
	private CollectionLoader<RedeemTracker> redeemTrackersDl;
	@Autowired
	private GroupTable<RedeemInfo> serialNoTable;
	static Logger logger = LogManager.getLogger(TransactionAcctEditRedemption.class);

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		try{
			userAddressesDl.setParameter("addTypeCode1", "L04");
			userAddressesDl.setParameter("userUsername1", getEditedEntity().getAccount().getAccOwner());
			userAddressesDl.load();

			userTierRegistrationsDl.setParameter("userUsername1", getEditedEntity().getAccount().getAccOwner());
			userTierRegistrationsDl.load();

			userAddressesUserDl.setParameter("username", getEditedEntity().getAccount().getAccOwner());
			userAddressesUserDl.load();

			transferInfoViewsDl.setParameter("sender1",getEditedEntity().getAccount().getAccOwner());
			transferInfoViewsDl.load();
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
//		if (userTierRegistrationsDc.getMutableItems().size() != 0) {
//			idTypeField.setValue(userTierRegistrationsDc.getMutableItems().get(0).getUser().getIdentificationType().getName());
//			idNoField.setValue(userTierRegistrationsDc.getMutableItems().get(0).getUser().getIdentificationNo());
//		} else {
//			idTypeField.setValue("-");
//			idNoField.setValue("-");
//		}
//
//		if(userAddressesUserDc.getMutableItems().size() != 0) {
//			addressLineOne.setValue(userAddressesUserDc.getMutableItems().get(0).getAddress1());
//			addressLineTwo.setValue(userAddressesUserDc.getMutableItems().get(0).getAddress2());
//			postcodeAddress.setValue(userAddressesUserDc.getMutableItems().get(0).getPostcode());
//			stateAddress.setValue(userAddressesUserDc.getMutableItems().get(0).getState());
//		}else {
//			addressLineOne.setValue("-");
//			addressLineTwo.setValue("-");
//			postcodeAddress.setValue("-");
//			stateAddress.setValue("-");
//		}

		redeemInfoesDl.setParameter("transaction1", transactionAcctDc.getItem());
		redeemInfoesDl.load();
		quantity.setValue(String.valueOf(redeemInfoesDc.getItems().size()));
		name.setValue(redeemInfoesDc.getItems().get(0).getName());
		createdDate.setValue(redeemInfoesDc.getItems().get(0).getCreatedDate().toString());
		phoneNo.setValue(redeemInfoesDc.getItems().get(0).getPhone());
		reason.setValue(redeemInfoesDc.getItems().get(0).getReason());
		address.setValue(redeemInfoesDc.getItems().get(0).getAddress());
		reference.setValue(redeemInfoesDc.getItems().get(0).getReference());

		transactionHistoriesViewsDl.setParameter("reference1", transactionAcctDc.getItem().getReference());
		transactionHistoriesViewsDl.setParameter("accOwner1", "BgdAdmin");
		transactionHistoriesViewsDl.load();
		transactionHistoriesViewsDc.getItems().forEach(transactionHistoriesView -> {
			if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B05")){
				mintingFee.setValue(transactionHistoriesView.getAmount().abs().toString());
			}
			if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B03")){
				courierFee.setValue(transactionHistoriesView.getAmount().abs().toString());
			}
			if(transactionHistoriesView.getTransactionCode().equalsIgnoreCase("F09/B04")){
				takafulFee.setValue(transactionHistoriesView.getAmount().abs().toString());
			}
		});

		redeemTrackersDl.setParameter("transactionId1", transactionAcctDc.getItem());
		redeemTrackersDl.load();
	}
}

