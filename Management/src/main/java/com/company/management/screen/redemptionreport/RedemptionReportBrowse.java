package com.company.management.screen.redemptionreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.RedemptionReport;

@UiController("RedemptionReport.browse")
@UiDescriptor("redemption-report-browse.xml")
@LookupComponent("redemptionReportsTable")
public class RedemptionReportBrowse extends StandardLookup<RedemptionReport> {
}