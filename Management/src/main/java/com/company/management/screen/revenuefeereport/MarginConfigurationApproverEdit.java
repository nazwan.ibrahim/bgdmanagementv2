package com.company.management.screen.revenuefeereport;

import com.company.management.entity.RevenueManagement;
import com.company.management.entity.RevenueManagementDoc;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.usersubstitution.CurrentUserSubstitution;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.RevenueFeeReport;
import io.jmix.ui.upload.TemporaryStorage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@UiController("MarginConfigurationApprover.edit")
@UiDescriptor("margin-configuration-approver-edit.xml")
@EditedEntityContainer("revenueManagementDc")
public class MarginConfigurationApproverEdit extends StandardEditor<RevenueManagement> {
    @Autowired
    private DataManager dataManager;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private InstanceContainer<RevenueManagement> revenueManagementDc;
    @Autowired
    private CollectionLoader<RtType> rtTypesStatusDl;
    @Autowired
    private CollectionContainer<RtType> rtTypesStatusDc;
    @Autowired
    private RadioButtonGroup<RtType> statusFieldId;
    @Autowired
    private TextArea<String> remarks2FieldId;

    @Autowired
    private CollectionLoader<RevenueManagementDoc> revenueManagementDocsDl;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private Downloader downloader;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private TemporaryStorage temporaryStorage;
    RevenueManagementDoc revenueManagementDoc;
    @Autowired
    private CollectionContainer<RevenueManagementDoc> revenueManagementDocsDc;
    @Autowired
    private Notifications notifications;
    @Named("uploadFileTableApproval.remove")
    private RemoveAction<RevenueManagementDoc> uploadFileTableApprovalRemove;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private CollectionContainer<RevenueManagementDoc> revenueManagementDocsApprovalDc;
    @Autowired
    private CollectionLoader<RevenueManagementDoc> revenueManagementDocsApprovalDl;
    @Autowired
    private CurrentUserSubstitution currentUserSubstitution;
    @Autowired
    private Button saveBtn;
    @Autowired
    private TextField<String> approveByFieldId;
    @Autowired
    private TextField<Date> approveDateFieldId;
    @Autowired
    private Label uploadDocuments;

    @Subscribe
    public void onBeforeShow(final BeforeShowEvent event) {
        loadCollection();
    }

    private void loadCollection(){
        rtTypesStatusDl.setParameter("code1", "G06");
        rtTypesStatusDl.setParameter("code2", "G07");
        rtTypesStatusDl.load();
        revenueManagementDocsDl.setParameter("revenueManagement1",getEditedEntity());
        revenueManagementDocsDl.setParameter("actionCode1","ACT01");
        revenueManagementDocsDl.load();
        revenueManagementDocsApprovalDl.setParameter("actionCode1", "ACT02");
        revenueManagementDocsApprovalDl.setParameter("createdBy1", currentUserSubstitution.getAuthenticatedUser().getUsername());
        revenueManagementDocsApprovalDl.load();
    }

    @Subscribe
    public void onInit(final InitEvent event) {
        uploadMultipleFile();
    }



    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        if(revenueManagementDc.getItem().getStatus().getCode().equals("G05")) {
            statusFieldId.setEditable(true);
            remarks2FieldId.setEditable(true);
            saveBtn.setVisible(true);
            approveByFieldId.setVisible(false);
            approveDateFieldId.setVisible(false);
            uploadDocuments.setVisible(true);
            fileUpload.setVisible(true);
        }
    }

    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                revenueManagementDoc = this.dataManager.create(RevenueManagementDoc.class);
                revenueManagementDoc.setRevenueManagement(revenueManagementDc.getItem());
                revenueManagementDoc.setName(fileName);
                revenueManagementDoc.setFile_upload(fileRef);
                revenueManagementDoc.setAction(loadStatus("ACT02"));

                revenueManagementDocsApprovalDc.getMutableItems().add(revenueManagementDoc);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        loadCollection();
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }

    @Install(to = "uploadFileTableApproval.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableApprovalDeleteFileUploadColumnGenerator(final RevenueManagementDoc revenueManagementDoc) {
        Button btn = uiComponents.create(Button.class);
        btn.setCaption("Delete");
        btn.addClickListener(clickEvent -> {
            uploadFileTableApprovalRemove.execute();
        });
        return btn;
    }

    private void saveData(){
        revenueManagementDc.getItem().setApprovedBy(currentAuthentication.getUser().getUsername());
        revenueManagementDc.getItem().setApprovedDate(new Timestamp(System.currentTimeMillis()));
        revenueManagementDc.getItem().setStatus(statusFieldId.getValue());
        this.dataManager.save(revenueManagementDc.getItem());
        revenueManagementDocsDc.getMutableItems().forEach(revenueManagementDoc1 -> {
            this.dataManager.save(revenueManagementDoc1);
        });
        closeWithDiscard();
    }

    @Install(to = "uploadFileTableApproval.file_upload", subject = "columnGenerator")
    private Component uploadFileTableFile_uploadColumnGenerator(final RevenueManagementDoc revenueManagementDoc) {
        if (revenueManagementDoc.getFile_upload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(revenueManagementDoc.getFile_upload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(revenueManagementDoc.getFile_upload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "uploadFileTable.file_upload", subject = "columnGenerator")
    private Component uploadFileTableFile_uploadColumnGenerator1(final RevenueManagementDoc revenueManagementDoc) {
        if (revenueManagementDoc.getFile_upload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(revenueManagementDoc.getFile_upload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(revenueManagementDoc.getFile_upload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    private RtType loadStatus(String code){
        return this.dataManager.load(RtType.class)
                .query("select r from RtType r " +
                        "where r.code = :code1")
                .parameter("code1", code)
                .one();
    }

    @Subscribe("saveBtn")
    public void onSaveBtnClick(final Button.ClickEvent event) {
        dialogs.createOptionDialog()
                .withCaption("Revenue Update Confirmation")
                .withContentMode(ContentMode.HTML)
                .withMessage("Click Confirm or Cancel button to proceed.")
                .withActions(
                        new DialogAction(DialogAction.Type.YES)
                                .withPrimary(true)
                                .withCaption("Confirm")
                                .withHandler(e -> saveData()),
                        new DialogAction(DialogAction.Type.NO)
                                .withCaption("Cancel")
                )
                .show();
    }

}