package com.company.management.screen.revenuemanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;

@UiController("RevenueManagement.browse")
@UiDescriptor("revenue-management-browse.xml")
@LookupComponent("revenueManagementsTable")
public class RevenueManagementBrowse extends StandardLookup<RevenueManagement> {
}