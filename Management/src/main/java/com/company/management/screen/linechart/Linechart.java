package com.company.management.screen.linechart;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Linechart")
@UiDescriptor("lineChart.xml")
public class Linechart extends Screen {
}