package com.company.management.screen.revenuemanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;

@UiController("BursaRevenueManagement.edit")
@UiDescriptor("bursa-revenue-management-edit.xml")
@EditedEntityContainer("revenueManagementDc")
public class BursaRevenueManagementEdit extends StandardEditor<RevenueManagement> {
}