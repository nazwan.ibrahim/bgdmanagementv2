package com.company.management.screen.transactionacct;

import com.company.management.api.ManagementController;
import com.company.management.app.ActivityLogBean;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.ActivityLogFile;
import com.company.management.entity.RevenueManagementDoc;
import com.company.management.entity.RtType;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionType;
import com.company.management.service.KeyClockTokenService;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.session.SessionData;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@UiController("TopUpCash.edit")
@UiDescriptor("topup-case-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class TopUpCashEdit extends Screen {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(TopUpCashEdit.class);
	@Autowired
	private TextField<String> uom;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private TextField<BigDecimal> amountField;
	@Autowired
	private Notifications notifications;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private CollectionLoader<AccountBgd> accountBgdsDl;
	@Autowired
	private EntityComboBox<AccountBgd> accountNo;
	@Autowired
	private KeyClockTokenService keyClockTokenService;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private InstanceContainer<TransactionAcct> transactionAcctDc;
	@Autowired
	private ActivityLogBean activityLogBean;
	@Autowired
	private TextArea<String> remarksFieldId;
	@Autowired
	private CollectionContainer<ActivityLogFile> activityLogFilesDc;
	@Autowired
	private InstanceContainer<ActivityLog> activityLogDc;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private TemporaryStorage temporaryStorage;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Named("uploadFileTable.remove")
	private RemoveAction<ActivityLogFile> uploadFileTableRemove;
	@Autowired
	private CollectionLoader<ActivityLogFile> activityLogFilesDl;

	public String getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}

	private String accountOwner;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		accountBgdsDl.setParameter("accOwner1",accountOwner);
		accountBgdsDl.setParameter("walletTypeCode1","E01");
		accountBgdsDl.load();
		accountNo.setValue(accountBgdsDl.getContainer().getItems().get(0));
//		uploadMultipleFile();
	}

	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
		activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
		uploadMultipleFile();
	}


	@Subscribe
	public void onInit(InitEvent event) {
		uom.setValue(rtType("J01").getName());
//		uploadMultipleFile();
	}

	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
				activityLogFile.setActivityLog(activityLogDc.getItem());
				activityLogFile.setName(fileName);
				activityLogFile.setFileUpload(fileRef);
				activityLogFile.setAction(rtType("ACT01"));

				activityLogFilesDc.getMutableItems().add(activityLogFile);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}

	private RtType rtType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		close(StandardOutcome.CLOSE);
	}


	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Top Up Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> checkBeforeSave()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void checkBeforeSave(){
		if(!activityLogFilesDc.getItems().isEmpty() & !remarksFieldId.isEmpty()){
			topUpCash();
		}else{
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Please fill in all the blanks")
					.show();
		}
	}
	private void topUpCash(){
		try{
			if(amountField.getValue() != null & Objects.nonNull(accountNo.getValue())) {

				String queryaccountBgd = "select a from AccountBgd a " +
						"where a.acctNumber = :acctNumber1";
				AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
						.query(queryaccountBgd).parameter("acctNumber1", accountNo.getValue().getAcctNumber()).one();

				TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);
				String querytransStatus = "select t from TransStatus t " +
						"where t.code = :code1";
				TransStatus transStatus = this.dataManager.load(TransStatus.class)
						.query(querytransStatus).parameter("code1", "G05/02") //Pending
						.one();

				transactionAcct.setAccount(accountBgd);

				transactionAcct.setAmount(amountField.getValue());
				transactionAcct.setTransType(type());

				String reference = managementController.generateSeqNo("BGDF01","6");
				transactionAcct.setReference(reference);

				transactionAcct.setTransStatus(transStatus);
				transactionAcct.setCreatedBy(accountBgd.getAccOwner());
				transactionAcct.setCreatedDate(new Date());

				this.dataManager.save(transactionAcct);

				if(accountBgd.getAccOwner().equals("BgdAdmin")) {
					activityLogBean.makerActivity("MDL03-D01-E01", "LOG03-F01", "Reference No = " +reference,
							"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
				} else if(accountBgd.getAccOwner().equals("AceAdmin")) {
					activityLogBean.makerActivity("MDL03-D04-E01", "LOG03-F01", "Reference No = " +reference,
							"TransactionAcct", transactionAcct.getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
				}

				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.HUMANIZED)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Amount Successfully Top Up")
						.show();
			}else {
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.ERROR)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Amount Not Successfully Top Up")
						.show();
			}
		}catch (Exception ex){
			log.info(ex.getMessage());
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.ERROR)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("User not found for the account number :"+accountNo.toString())
					.show();
		}
			close(StandardOutcome.CLOSE);
		}

		private TransactionType type (){
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
			return this.dataManager.load(TransactionType.class)
					.query(query).parameter("code1", "F01")
					.one();
		}

		@Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
		private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
			if (activityLogFile.getFileUpload() != null) {
				LinkButton linkButton = uiComponents.create(LinkButton.class);
				linkButton.setAction(new BaseAction("download")
						.withCaption(activityLogFile.getFileUpload().getFileName())
						.withHandler(actionPerformedEvent ->
								downloader.download(activityLogFile.getFileUpload())
						)
				);
				return linkButton;
			} else {
				return new Table.PlainTextCell("<empty>");
			}
		}

		@Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
		private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {

			Button btn = uiComponents.create(Button.class);
			btn.setCaption("Delete");
			btn.addClickListener(clickEvent -> {
				uploadFileTableRemove.execute();
			});
			return btn;
		}
	}


