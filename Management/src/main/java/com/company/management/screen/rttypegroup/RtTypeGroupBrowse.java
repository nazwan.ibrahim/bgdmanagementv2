package com.company.management.screen.rttypegroup;

import com.company.management.entity.SupportDoc;
import com.company.management.entity.TierManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.RtTypeGroup;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("RtTypeGroup.browse")
@UiDescriptor("rt-type-group-browse.xml")
@LookupComponent("rtTypeGroupsTable")
public class RtTypeGroupBrowse extends StandardLookup<RtTypeGroup> {

	private TierManagement tierManagement;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<RtTypeGroup> rtTypeGroupsTable;

	public TierManagement getTierManagement() {
		return tierManagement;
	}

	public void setTierManagement(TierManagement tierManagement) {
		this.tierManagement = tierManagement;
	}

	@Autowired
	private DataContext dataContext;

	public void setParentDataContext(DataContext parentDataContext) {
		dataContext.setParent(parentDataContext);
	}

	@Subscribe("selectRtTypeGroup")
	public void onSelectRtTypeGroupClick(Button.ClickEvent event) {
		SupportDoc supportDoc = this.dataManager.create(SupportDoc.class);
		supportDoc.setTierManagement(getTierManagement());
		supportDoc.getType().setCode(rtTypeGroupsTable.getSingleSelected().getCode());
		dataManager.save(supportDoc);

//		dataManager.save(supportDoc);
		close(StandardOutcome.CLOSE);
	}
}