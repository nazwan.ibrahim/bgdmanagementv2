package com.company.management.screen.shariahrecordsubsequent;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;

import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionAcct;
import com.company.management.entity.wallet.TransactionType;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;

@UiController("ShariahRecordSubsequentGold.edit")
@UiDescriptor("shariah-record-subsequent-gold-edit.xml")
@EditedEntityContainer("shariahRecordSubsequentDc")
public class ShariahRecordSubsequentGoldEdit extends StandardEditor<ShariahRecordSubsequent> {
	@Autowired
	private InstanceLoader<ShariahRecordSubsequent> shariahRecordSubsequentDl;
	@Autowired
	private CollectionLoader<ReserveRecordProduct> reserveRecordProductsDl;
	@Autowired
	private CollectionLoader<ReserveRecordDoc> reserveRecordDocsDl;
	@Autowired
	private CollectionLoader<DeliveryRecordSubsequentDoc> deliveryRecordSubsequentDocsDl;
	@Autowired
	private CollectionLoader<ShariahSubsequentDoc> shariahSubsequentDocsDl;
	@Autowired
	private InstanceContainer<ShariahRecordSubsequent> shariahRecordSubsequentDc;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private Button commit;
	@Autowired
	private GroupTable<ReserveRecordProduct> reserveRecordProductsTable;
	@Autowired
	private CollectionContainer<ReserveRecordProduct> reserveRecordProductsDc;
	private BigDecimal total = BigDecimal.ZERO;
	@Autowired
	private ManagementController managementController;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		shariahRecordSubsequentDl.load();
		reserveRecordProductsDl.setParameter("reverse_record1", shariahRecordSubsequentDc.getItem().getDeliveryRecord().getReserveRecord());
		reserveRecordProductsDl.load();
		reserveRecordDocsDl.setParameter("reserve_record1", shariahRecordSubsequentDc.getItem().getDeliveryRecord().getReserveRecord());
		reserveRecordDocsDl.load();
		deliveryRecordSubsequentDocsDl.setParameter("deliveryRecordReserveRecord1", shariahRecordSubsequentDc.getItem().getDeliveryRecord().getReserveRecord());
		deliveryRecordSubsequentDocsDl.load();
		shariahSubsequentDocsDl.setParameter("shariahRecordDeliveryRecordReserveRecord1", shariahRecordSubsequentDc.getItem().getDeliveryRecord().getReserveRecord());
		shariahSubsequentDocsDl.load();
	}

	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(ReserveRecordDoc reserveRecordDoc) {
		if (reserveRecordDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(reserveRecordDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(reserveRecordDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Install(to = "fileUploadShariah.fileUpload", subject = "columnGenerator")
	private Component fileUploadShariahFileUploadColumnGenerator(ShariahSubsequentDoc shariahSubsequentDoc) {
		if (shariahSubsequentDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(shariahSubsequentDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(shariahSubsequentDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Install(to = "fileUploadDelivery.uploadFile", subject = "columnGenerator")
	private Component fileUploadDeliveryUploadFileColumnGenerator(DeliveryRecordSubsequentDoc deliveryRecordSubsequentDoc) {
		if (deliveryRecordSubsequentDoc.getUploadFile() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(deliveryRecordSubsequentDoc.getUploadFile().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(deliveryRecordSubsequentDoc.getUploadFile())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	private RtType loadType(String code){
		return this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", code)
				.one();
	}

	private void saveUploadDoc(){
		ShariahSubsequentDoc shariahSubsequentDoc = this.dataManager.create(ShariahSubsequentDoc.class);
		shariahSubsequentDoc.setShariahRecord(shariahRecordSubsequentDc.getItem());
		shariahSubsequentDoc.setFileUpload(uploadPurchaseForm.getValue());
		shariahSubsequentDoc.setName(uploadPurchaseForm.getFileName());
		shariahSubsequentDoc.setCreatedBy(currentAuthentication.getAuthentication().getName());
		shariahSubsequentDoc.setCreatedDate(timeSource.currentTimestamp());
		this.dataManager.save(shariahSubsequentDoc);
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Shariah Record")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}


	private void saveData(){
//		getEditedEntity().setStatus(loadType("G01"));
		dataManager.save(getEditedEntity());

		if(uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		if(getEditedEntity().getStatus().equals(loadType("G01"))){
			addtogoldWallet();
		}
		closeWithDiscard();
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Shariah Record")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void addtogoldWallet(){
		AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
				.query("select a from AccountBgd a " +
						"where a.accOwner = :accOwner1 " +
						"and a.walletType.code = :walletTypeCode1").parameter("accOwner1", "AceAdmin").parameter("walletTypeCode1", "E02").one();

		TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);
		TransStatus transStatus = this.dataManager.load(TransStatus.class)
				.query("select t from TransStatus t " +
						"where t.code = :code1").parameter("code1", "G01")
				.one();

		transactionAcct.setAccount(accountBgd);


		for (ReserveRecordProduct value : reserveRecordProductsDc.getItems()) {
			if(value.getType().getCode().equals("GT01")) {
				total = total.add(BigDecimal.valueOf(1000));
			}else{
				total = total.add(BigDecimal.valueOf(4.25));
			}
		}
		transactionAcct.setTransType(type("F01"));
		String reference = managementController.generateSeqNo("BGDF01","6");
		transactionAcct.setReference(reference);
		transactionAcct.setAmount(total);
		transactionAcct.setTransStatus(transStatus);
		transactionAcct.setCreatedBy(accountBgd.getAccOwner());
		transactionAcct.setCreatedDate(new Date());

		this.dataManager.save(transactionAcct);
	}

	private TransactionType type(String code){
		return this.dataManager.load(TransactionType.class)
				.query("select t from TransactionType t " +
						"where t.code = :code1").parameter("code1", code)
				.one();
	}


}