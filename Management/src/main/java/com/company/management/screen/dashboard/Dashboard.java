package com.company.management.screen.dashboard;

import com.company.management.entity.DinarStockReorderRule;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("Dashboard")
@UiDescriptor("dashboard.xml")
public class Dashboard extends ScreenFragment {
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Notifications notifications;
}