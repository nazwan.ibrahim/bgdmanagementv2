package com.company.management.screen.investorgoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.InvestorGoldWalletStatement;

@UiController("InvestorGoldWalletStatement.edit")
@UiDescriptor("investor-gold-wallet-statement-edit.xml")
@EditedEntityContainer("investorGoldWalletStatementDc")
public class InvestorGoldWalletStatementEdit extends StandardEditor<InvestorGoldWalletStatement> {
}