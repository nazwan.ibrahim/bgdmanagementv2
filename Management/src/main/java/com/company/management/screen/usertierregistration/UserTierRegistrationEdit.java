package com.company.management.screen.usertierregistration;

import com.company.management.entity.registrationdb.UserTier;
import io.jmix.ui.screen.*;

@UiController("UserTierRegistration.edit")
@UiDescriptor("user-tier-registration-edit.xml")
@EditedEntityContainer("userTierRegistrationDc")
public class UserTierRegistrationEdit extends StandardEditor<UserTier> {
}