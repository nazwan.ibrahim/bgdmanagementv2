package com.company.management.screen.cashtopupreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.CashTopupReport;

@UiController("CashTopupReport.edit")
@UiDescriptor("cash-topup-report-edit.xml")
@EditedEntityContainer("cashTopupReportDc")
public class CashTopupReportEdit extends StandardEditor<CashTopupReport> {
}