package com.company.management.screen.costrevocerymanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.CostRevoceryManagement;

@UiController("BursaCostRevoceryManagement.edit")
@UiDescriptor("bursa-cost-revocery-management-edit.xml")
@EditedEntityContainer("costRevoceryManagementDc")
public class BursaCostRevoceryManagementEdit extends StandardEditor<CostRevoceryManagement> {
}