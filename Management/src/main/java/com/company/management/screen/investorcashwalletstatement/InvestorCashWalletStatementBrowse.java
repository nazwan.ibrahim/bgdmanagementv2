package com.company.management.screen.investorcashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.InvestorCashWalletStatement;

@UiController("InvestorCashWalletStatement.browse")
@UiDescriptor("investor-cash-wallet-statement-browse.xml")
@LookupComponent("investorCashWalletStatementsTable")
public class InvestorCashWalletStatementBrowse extends StandardLookup<InvestorCashWalletStatement> {
}