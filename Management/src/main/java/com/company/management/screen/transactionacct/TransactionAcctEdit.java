package com.company.management.screen.transactionacct;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;

@UiController("TransactionAcct.edit")
@UiDescriptor("transaction-acct-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class TransactionAcctEdit extends StandardEditor<TransactionAcct> {
}