package com.company.management.screen.revenuefeereport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.RevenueFeeReport;

@UiController("RevenueFeeReport.browse")
@UiDescriptor("revenue-fee-report-browse.xml")
@LookupComponent("revenueFeeReportsTable")
public class RevenueFeeReportBrowse extends StandardLookup<RevenueFeeReport> {
}