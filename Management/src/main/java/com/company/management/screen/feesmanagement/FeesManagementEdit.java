package com.company.management.screen.feesmanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.FeesManagement;

@UiController("FeesManagement.edit")
@UiDescriptor("fees-management-edit.xml")
@EditedEntityContainer("feesManagementDc")
public class FeesManagementEdit extends StandardEditor<FeesManagement> {
}