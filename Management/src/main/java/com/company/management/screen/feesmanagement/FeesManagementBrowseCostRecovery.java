package com.company.management.screen.feesmanagement;

import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.action.Action;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.FeesManagement;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("FeesManagementCostRecovery.browse")
@UiDescriptor("fees-management-browse-cost-recovery.xml")
@LookupComponent("feesManagementsTableCostCovery")
public class FeesManagementBrowseCostRecovery extends StandardLookup<FeesManagement> {
	@Autowired
	private CollectionContainer<FeesManagement> feesManagementsDc;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<FeesManagement> feesManagementsDl;
	@Autowired
	private Notifications notifications;

	@Subscribe("feesManagementsTableCostCovery.edit")
	public void onFeesManagementsTableCostCoveryEdit(Action.ActionPerformedEvent event) {
		feesManagementsDc.getItems().forEach(feesManagementCostRecovery -> {
			dataManager.save(feesManagementCostRecovery);
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Cost Recovery Successfully Updated")
							.show();
		feesManagementsDl.load();
	}


}