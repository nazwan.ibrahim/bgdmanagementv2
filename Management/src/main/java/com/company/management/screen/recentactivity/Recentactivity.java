package com.company.management.screen.recentactivity;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.component.RadioButtonGroup;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedHashMap;
import java.util.Map;

@UiController("Recentactivity")
@UiDescriptor("RecentActivity.xml")
@DashboardWidget(name = "Recentactivity")
public class Recentactivity extends ScreenFragment {
	@Autowired
	private RadioButtonGroup<Integer> radioButtonGroupWithMap;

	@Subscribe
	public void onInit(InitEvent event) {
		Map<String, Integer> map = new LinkedHashMap<>();
		map.put("two", 2);
		map.put("four", 4);
		map.put("five", 5);
		map.put("seven", 7);
		radioButtonGroupWithMap.setOptionsMap(map);
	}
}