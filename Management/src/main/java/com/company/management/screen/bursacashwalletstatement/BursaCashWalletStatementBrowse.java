package com.company.management.screen.bursacashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.BursaCashWalletStatement;

@UiController("BursaCashWalletStatement.browse")
@UiDescriptor("bursa-cash-wallet-statement-browse.xml")
@LookupComponent("bursaCashWalletStatementsTable")
public class BursaCashWalletStatementBrowse extends StandardLookup<BursaCashWalletStatement> {
}