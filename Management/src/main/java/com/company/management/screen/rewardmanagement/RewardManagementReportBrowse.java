package com.company.management.screen.rewardmanagement;

import io.jmix.ui.component.Button;
import io.jmix.ui.screen.*;
import com.company.management.entity.RewardManagement;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("RewardManagementReport.browse")
@UiDescriptor("reward-management-report-browse.xml")
@LookupComponent("rewardManagementsTable")
public class RewardManagementReportBrowse extends StandardLookup<RewardManagement> {
    @Install(to = "rewardManagementsTable", subject = "styleProvider")
    private String rewardManagementsTableStyleProvider(RewardManagement rewardManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (rewardManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }


}

