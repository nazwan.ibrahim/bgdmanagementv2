package com.company.management.screen.login;

import com.company.management.screen.resetpassword.ResetPassword;
import com.company.management.screen.transactionacct.TransactionAcctEditRedemption;
import com.company.management.screen.transactionacct.TransactionAcctReplacementEdit;
import io.jmix.audit.UserSessions;
import io.jmix.audit.entity.UserSession;
import io.jmix.core.MessageTools;
import io.jmix.core.Messages;
import io.jmix.core.security.Authenticated;
import io.jmix.core.security.SystemAuthenticator;
import io.jmix.securityui.authentication.AuthDetails;
import io.jmix.securityui.authentication.LoginScreenSupport;
import io.jmix.securityui.screen.changepassword.ChangePasswordDialog;
import io.jmix.ui.*;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.navigation.ScreenNavigator;
import io.jmix.ui.screen.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;

import javax.cache.annotation.CacheResult;
import java.util.List;
import java.util.Locale;


@UiController("LoginScreen")
@UiDescriptor("login-screen.xml")
@Route(path = "login", root = true)
public class LoginScreen extends Screen {
    @Autowired
    private TextField<String> usernameField;
    @Autowired
    private PasswordField passwordField;
    @Autowired
    private CheckBox rememberMeCheckBox;
    @Autowired
    private ComboBox<Locale> localesField;
    @Autowired
    private Notifications notifications;
    @Autowired
    private Messages messages;
    @Autowired
    private MessageTools messageTools;
    @Autowired
    private LoginScreenSupport loginScreenSupport;
    @Autowired
    private JmixApp app;
    @Value("${ui.login.defaultUsername:}")
    private String defaultUsername;
    @Value("${ui.login.defaultPassword:}")
    private String defaultPassword;
    private final Logger log = LoggerFactory.getLogger(LoginScreen.class);
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private UserSessions userSessions;
    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private Screens screens;

    @Autowired SystemAuthenticator systemAuthenticator;

    @Subscribe
    private void onInit(InitEvent event) {
        usernameField.focus();
        initLocalesField();
        initDefaultCredentials();
    }

    private void initLocalesField() {
        localesField.setOptionsMap(messageTools.getAvailableLocalesMap());
        localesField.setValue(app.getLocale());
        localesField.addValueChangeListener(this::onLocalesFieldValueChangeEvent);
    }

    private void onLocalesFieldValueChangeEvent(HasValue.ValueChangeEvent<Locale> event) {
        //noinspection ConstantConditions
        app.setLocale(event.getValue());
        UiControllerUtils.getScreenContext(this).getScreens()
                .create(this.getClass(), OpenMode.ROOT)
                .show();
    }

    private void initDefaultCredentials() {
        if (StringUtils.isNotBlank(defaultUsername)) {
            usernameField.setValue(defaultUsername);
        } else {
            usernameField.setValue("");
        }

        if (StringUtils.isNotBlank(defaultPassword)) {
            passwordField.setValue(defaultPassword);
        } else {
            passwordField.setValue("");
        }
    }

    @CacheResult(cacheName = "submit")
    @Subscribe("submit")
    private void onSubmitActionPerformed(Action.ActionPerformedEvent event) {
        login();
    }

    private void login() {
        String username = usernameField.getValue();
        String password = passwordField.getValue();

//        Added our code to verify that such a user is already working and optional kill old session
        if (checkOtherUserSessionsPresent(username)) {
            String finalUsername = username;
            dialogs.createOptionDialog()
                    .withCaption(messages.getMessage("OptionDialog.caption"))
                    .withMessage(messages.getMessage("OptionDialog.message"))
                    .withActions(
                            new DialogAction(DialogAction.Type.YES, Action.Status.PRIMARY)
                                    .withHandler(e -> {
                                        killSession(finalUsername);
                                        tryAuthenticate(finalUsername, password);
                                    }),
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .show();
        } else {
            //           Standard Authentication
            tryAuthenticate(username, password);
        }
    }

    private void tryAuthenticate(String username, String password) {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            notifications.create(Notifications.NotificationType.WARNING)
                    .withCaption(messages.getMessage(getClass(), "emptyUsernameOrPassword"))
                    .show();
            return;
        }

        try {
            loginScreenSupport.authenticate(
                    AuthDetails.of(username, password)
                            .withLocale(localesField.getValue())
                            .withRememberMe(rememberMeCheckBox.isChecked()),

                    this);
//            getKeyClockLoginToken();
//            openOtherScreen();

        } catch (BadCredentialsException | DisabledException | LockedException e) {
            log.warn("Login failed for user '{}': {}", username, e.toString());
            notifications.create(Notifications.NotificationType.ERROR)
                    .withCaption(messages.getMessage(getClass(), "loginFailed"))
                    .withDescription(messages.getMessage(getClass(), "badCredentials"))
                    .show();
        }
    }

    //    Our method to check the user's session
    private Boolean checkOtherUserSessionsPresent(String username) {
        long userSessionCount = 0L;
        List<UserSession> sessionList = userSessions.sessions().toList();
        if (!sessionList.isEmpty()) {
            userSessionCount = sessionList.stream().filter(s -> s.getPrincipalName().equals(username)).count();
        }
        return userSessionCount > 0;
    }

    private void killSession(String username){
        userSessions.sessions()
                .filter(s -> s.getPrincipalName().equals(username))
                .forEach((os) -> {
                    os.getSessionInformation().expireNow();
                    log.warn("Session id = {} user [ {} ] has been aborted due to login from another device or browser.",
                            os.getSessionId(), os.getPrincipalName());
                });
    }
    @Authenticated
    @Subscribe("btnForgotPassword")
    public void onBtnForgotPasswordClick(final Button.ClickEvent event) {
        systemAuthenticator.withSystem(() ->
                screenBuilders.screen(this)
                        .withScreenClass(ResetPassword.class)
                        .withOpenMode(OpenMode.DIALOG)
                        .build()
                        .show()
        );
    }

    @Subscribe("showPasswordBtn")
    public void onShowPasswordBtnClick(final Button.ClickEvent event) {
        notifications.create()
                .withCaption(passwordField.getValue())
                .show();
    }

}
