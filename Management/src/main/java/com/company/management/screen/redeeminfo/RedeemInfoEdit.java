package com.company.management.screen.redeeminfo;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.RedeemInfo;

@UiController("RedeemInfo.edit")
@UiDescriptor("redeem-info-edit.xml")
@EditedEntityContainer("redeemInfoDc")
public class RedeemInfoEdit extends StandardEditor<RedeemInfo> {
}