package com.company.management.screen.userprofile;

import com.company.management.entity.*;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.AccountWalletView;
import com.company.management.service.KeyClockTokenService;
import io.jmix.core.Metadata;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.app.ActivityLogBean;
import com.company.management.app.BgdIDBean;
import com.company.management.entity.registrationdb.*;
import com.company.management.entity.registrationdb.UserEmployment;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import org.springframework.core.env.Environment;

import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.inject.Named;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;

@UiController("InvestorAccountManagement.edit")
@UiDescriptor("investor-account-management-edit.xml")
@EditedEntityContainer("userProfileDc")
public class InvestorAccountManagementEdit extends StandardEditor<UserProfile> {
    private static final Logger log = LoggerFactory.getLogger(InvestorAccountManagementEdit.class);
    @Autowired
    private InstanceLoader<UserProfile> userProfileDl;
    @Autowired
    private InstanceLoader<UserAddress> userAddressesDl;
    @Autowired
    private InstanceContainer<UserProfile> userProfileDc;
    @Autowired
    private InstanceLoader<UserBank> userBanksDl;
    @Autowired
    private InstanceLoader<UserTier> userTiersDl;
    @Autowired
    private CollectionLoader<KycStatus> kycStatusesDl;
    @Autowired
    private CollectionLoader<KycStatus> kycStatusesDl1;
    @Autowired
    private InstanceLoader<UserEmployment> userEmploymentDl;
    @Autowired
    private InstanceContainer<UserEmployment> userEmploymentDc;
    @Autowired
    private InstanceLoader<StatutoryDetail> statutoryDetailsDl;
    @Autowired
    private InstanceContainer<StatutoryDetail> statutoryDetailsDc;
    @Autowired
    private InstanceLoader<FinancialSource> financialSourcesDl;
    @Autowired
    private InstanceContainer<FinancialSource> financialSourcesDc;
    @Autowired
    private CollectionContainer<ActivityLog> activityLogDc;

    @Autowired
    private Environment environment;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private InstanceLoader<UserVerification> userVerificationDl;
    @Autowired
    private InstanceContainer<UserVerification> userVerificationDc;
    @Autowired
    private BgdIDBean bgdIDBean;
    @Autowired
    private TextField<String> emailVerified;
    @Autowired
    private EntityComboBox<KycStatus> approvalStatusEntity;
    @Autowired
    private CollectionContainer<KycStatus> kycStatusesDc;
    @Autowired
    private CollectionContainer<KycStatus> kycStatusesDc2;
    @Autowired
    private InstanceContainer<UserAddress> userAddressesDc;
    @Autowired
    private InstanceContainer<UserTier> userTiersDc;
    @Autowired
    private TextField bgdID;
    @Autowired
    private InstanceContainer<UserBank> userBanksDc;

    @Autowired
    private CipherEncryptionManagement cipherEncryptionManagement;
    @Autowired
    private RadioButtonGroup<KycStatus> approveCheckBox;
    @Autowired
    private ActivityLogBean activityLogBean;
    @Autowired
    private TextArea<String> remarksFieldId;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc;
    @Autowired
    private InstanceLoader<UserAddress> userAddressesMailDl;
//    @Autowired
//    private EntityComboBox<ActivationStatus> accountStatus;
    @Autowired
    private CollectionLoader<ActivationStatus> activationStatusesDl;
    @Autowired
    private Notifications notifications;
    @Autowired
    private Label uploadDocuments;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private Downloader downloader;
    @Autowired
    private CollectionLoader<RtType> rtTypesIDTypeDl;
    @Autowired
    private Form form2;
    @Autowired
    private VBoxLayout uploadFileZone;
    @Autowired
    private FileMultiUploadField fileUploadApprover;
    @Autowired
    private GroupTable<ActivityLogFile> uploadFileTableApproval;
    @Autowired
    private GroupTable<ActivityLogFile> uploadFileTable;
    @Autowired
    private VBoxLayout fileTypesApprover;
    @Autowired
    private TemporaryStorage temporaryStorage;

    ActivityLogFile activityLogFile;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesApproverDl;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesApproverDc;
    @Named("uploadFileTableApproval.remove")
    private RemoveAction<ActivityLogFile> uploadFileTableApprovalRemove;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private RadioButtonGroup<ApproveRejectEnum> statusFieldId;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Named("uploadFileTable.remove")
    private RemoveAction<ActivityLogFile> uploadFileTableRemove;
    @Autowired
    private InstanceLoader<UserDetailHistory> userDetailHistoryDl;
    @Autowired
    private InstanceContainer<UserDetailHistory> userDetailHistoryDc;
    @Autowired
    private TextField<SourceOfWealthType> sourceWealth;
    @Autowired
    private TextField<String> otherSourceWealth;
    @Autowired
    private TextField<SourceOfFundType> sourceFunds;
    @Autowired
    private TextField<String> otherSourceFund;
    @Autowired
    private CollectionLoader<FatcaCrsDeclaration> fatcaCRSDl;
    @Autowired
    private CollectionContainer<FatcaCrsDeclaration> fatcaCRSDc;
    @Autowired
    private Metadata metadata;
    @Autowired
    private CollectionContainer<FatcaCRSDTO> fatcaCRSDTODc;
    @Autowired
    private CollectionContainer<ActivationStatus> activationStatusesDc;
    @Autowired
    private TextField<String> employerName;
    @Autowired
    private TextField<String> previousEmployerName;
    @Autowired
    private TextField<JobPosition> position;
    @Autowired
    private TextField<NatureOfBusiness> natureBusiness;
    @Autowired
    private TextField<String> countryTaxResidence;
    @Autowired
    private TextField<String> businessName;

    @Autowired
    private KeyClockTokenService keycloakService;
    @Autowired
    private EntityComboBox<ActivationStatus> accountStatusField;
    @Autowired
    private CollectionLoader<ActivityLog> activityLogDl;
    @Autowired
    private TextField<String> requestBy;
    @Autowired
    private TextField<java.util.Date> requestDate;


    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) throws IOException {
        try {
            userProfileDl.load();

        try {
//            activationStatusesDl.setParameter("code1", "G04"); //Active
//            activationStatusesDl.setParameter("code2", "G19"); //Dormant
//            activationStatusesDl.setParameter("code3", "G20"); //Suspend (Disciplinary Action)
//            activationStatusesDl.setParameter("code4", "G21"); //Suspend (Under Investigation)
//            activationStatusesDl.setParameter("code5", "G22"); //Suspend (Redo e-KYC)
//            activationStatusesDl.setParameter("code6", "G23"); //Close
//            activationStatusesDl.setParameter("code7", "G18"); //Deleted
//            activationStatusesDl.load();
            setAccountStatusDCList();

        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userAddressesDl.setParameter("user1", userProfileDc.getItem());
            userAddressesDl.setParameter("addTypeCode1", "L01");
            userAddressesDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userAddressesMailDl.setParameter("user1", userProfileDc.getItem());
            userAddressesMailDl.setParameter("addTypeCode1", "L03");
            userAddressesMailDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userBanksDl.setParameter("user1", userProfileDc.getItem());
            userBanksDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userTiersDl.setParameter("user1", userProfileDc.getItem());
            userTiersDl.setParameter("currentTier1", true);
            userTiersDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            kycStatusesDl.setParameter("code1", "G12");
            kycStatusesDl1.setParameter("code1", "G10");
            kycStatusesDl.load();
            kycStatusesDl1.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userEmploymentDl.setParameter("user1", userProfileDc.getItem());
            userEmploymentDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("EMPL")){ //Employed
                //set visible true
                employerName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("SLEMPL")) { //Self - Employed
                businessName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("UNEMPL")) { //Unemployed
                countryTaxResidence.setVisible(true);
            }
            else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("RETIRED")) { // Retired
                previousEmployerName.setVisible(true);
                position.setVisible(true);
                natureBusiness.setVisible(true);
                countryTaxResidence.setVisible(true);
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            statutoryDetailsDl.setParameter("user1", userProfileDc.getItem());
            statutoryDetailsDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            financialSourcesDl.setParameter("user1", userProfileDc.getItem());
            financialSourcesDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            if (financialSourcesDc.getItem().getSourceFundType().getName().equals("Others")){
                otherSourceFund.setVisible(true);
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        } try {
            if (financialSourcesDc.getItem().getSourceWealthType().getName().equals("Others")){
                otherSourceWealth.setVisible(true);
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userVerificationDl.setParameter("user1", userProfileDc.getItem());
            userVerificationDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
//        try {
//            rtTypesIDTypeDl.setParameter("code1", "G06");
//            rtTypesIDTypeDl.setParameter("code2", "G07");
//            rtTypesIDTypeDl.load();
//        }catch (Exception ex) {
//            log.error(ex.getMessage());
//        }
        try {
            try {
                activityLogDl.setParameter("refEntity1", "UserProfile");
                activityLogDl.setParameter("refData1", userProfileDc.getItem().getId().toString());
                activityLogDl.load();

            }catch (Exception ex){
            log.error(ex.getMessage());
            }
            try {
                activityLogFilesDl.setParameter("actionCode1", "ACT01");
                activityLogFilesDl.setParameter("activityLogId1", activityLogDc.getItems().get(0).getId());
                activityLogFilesDl.load();
            }catch (Exception ex){
                log.error(ex.getMessage());
            }
            try {
                activityLogFilesApproverDl.setParameter("actionCode1", "ACT02");
                activityLogFilesApproverDl.setParameter("activityLogId1", activityLogDc.getItems().get(0).getId());
                activityLogFilesApproverDl.load();
            }catch (Exception ex){
                log.error(ex.getMessage());
            }
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }
        try {
            userDetailHistoryDl.setParameter("user1", userProfileDc.getItem());
            userDetailHistoryDl.load();
        }catch (Exception ex) {
            log.error(ex.getMessage());
        }

            if(accountStatusField.getValue().getCode().equals("G04/02")){//Pending Active
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G19/02")) {//Pending Dormant
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G20/02")){//Pending Suspend (Disciplinary Action)
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G21/02")){//Pending Suspend (Under Investigation)
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G22/02")){//Pending Suspend (Redo e-KYC)
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G23/02")){//Pending Close
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }
            if(accountStatusField.getValue().getCode().equals("G18/02")){//Pending Deleted
                accountStatusField.setEditable(false);
                remarksFieldId.setEditable(false);
                remarksFieldId.setRequired(false);
                uploadDocuments.setVisible(false);
                fileUpload.setVisible(false);
                form2.setVisible(true);
                uploadFileZone.setVisible(true);
                fileUploadApprover.setVisible(true);
                uploadFileTableApproval.setVisible(true);
                fileTypesApprover.setVisible(true);
                requestBy.setVisible(true);
                requestDate.setVisible(true);
                try {
                    requestBy.setValue(activityLogDc.getItems().get(0).getRequestBy());
                    requestDate.setValue(activityLogDc.getItems().get(0).getRequestDate());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
                try {
                    remarksFieldId.setValue(activityLogDc.getItems().get(0).getRequestRemarks());
                }catch (Exception ex){
                    log.error(ex.getMessage());
                }
            }

            if (!userTiersDc.getItem().getTier().getCode().equals("T00")) {
                BgdIDDTO bgdIDBean1 = bgdIDBean.getBgdID(userProfileDc.getItem().getUsername());
                bgdIDBean1.getBgdID();
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    public void setAccountStatusDCList() {
        try{
            List<ActivationStatus> activationStatuses = this.dataManager.load(ActivationStatus.class).all().list();

            if(userProfileDc.getItem().getActStatus().getCode().equals("G04")){ //Active Choices
                activationStatuses = activationStatuses.stream().filter(f -> f.getCode().equals("G19") || f.getCode().equals("G20") || f.getCode().equals("G21") || f.getCode().equals("G22")).toList();
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G19")) { //Dormant Choices
                activationStatuses = activationStatuses.stream().filter(f -> f.getCode().equals("G04") || f.getCode().equals("G22") || f.getCode().equals("G23")).toList();
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G20")) { //Suspend (Disciplinary Action) Choices
                activationStatuses = activationStatuses.stream().filter(f -> f.getCode().equals("G04") || f.getCode().equals("G22") || f.getCode().equals("G23")).toList();
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G21")) { //Suspend (Under Investigation) Choices
                activationStatuses = activationStatuses.stream().filter(f -> f.getCode().equals("G04") || f.getCode().equals("G22") || f.getCode().equals("G23")).toList();
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G22")) { //Suspend (Redo e-KYC) Choices
                accountStatusField.setEditable(false);
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G23")) { //Close Choices
                activationStatuses = activationStatuses.stream().filter(f -> f.getCode().equals("G04") || f.getCode().equals("G22") || f.getCode().equals("G18")).toList();
            } else if (userProfileDc.getItem().getActStatus().getCode().equals("G18")) { //Deleted Choices
                accountStatusField.setEditable(false);
            }

            activationStatusesDc.setItems(activationStatuses);
            accountStatusField.setOptionsContainer(activationStatusesDc);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    @Subscribe
    public void onAfterShow(AfterShowEvent event) {
        try {
            setAccountStatusDCList();
            emailVerified.setValue(getuserTier().getUser().getEmailVerified().toString());
        }catch (Exception ex){
            log.error(ex.getMessage());
        }
        try {
            fatcaCRSDl.setParameter("user1", userProfileDc.getItem());
            fatcaCRSDl.load();

            List<String[]> list1 = new ArrayList<>();
            List<String[]> list2 = new ArrayList<>();
            List<String[]> list3 = new ArrayList<>();
            List<String[]> list4 = new ArrayList<>();
            List<String[]> data = new ArrayList<>();
            List<FatcaCRSDTO> fatcaCRSDTOS = new ArrayList<>();
            try {
                fatcaCRSDc.getItems().forEach(e -> {
                    if(e.getDeclaration().getCode().equals("DCLRN01")){
                        String[] str1 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list1.add(str1);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN02")){
                        String[] str2 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list2.add(str2);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN03")){
                        String[] str3 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list3.add(str3);
                    }
                    if(e.getDeclaration().getCode().equals("DCLRN04")){
                        String[] str4 = {e.getAgreed().toString(),e.getDeclaration().getName()};
                        list4.add(str4);
                    }
                });
                data.add(list1.get(0));
                data.add(list2.get(0));
                data.add(list3.get(0));
                data.add(list4.get(0));
                for(String[] str : data){
                    FatcaCRSDTO fatcaCRSDTO = metadata.create(FatcaCRSDTO.class);
                    fatcaCRSDTO.setAgreed(Boolean.parseBoolean(str[0]));
                    fatcaCRSDTO.setDeclaration(str[1]);
                    fatcaCRSDTOS.add(fatcaCRSDTO);
                }
                fatcaCRSDTODc.setItems(fatcaCRSDTOS);
            }catch (Exception ex){
                log.info(ex.getMessage());
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
//        uploadMultipleFile();
        try{
            if(getuserTier().getKycStatus().getCode().equals("G12")){
                approvalStatusEntity.setOptionsContainer(kycStatusesDc);
                String querykycStatuses = "select e from KycStatus e where e.code = :code1 or e.code = :code2";
                List<KycStatus> kycStatuses = this.dataManager.load(KycStatus.class)
                        .query(querykycStatuses)
                        .parameter("code1", "G09")
                        .parameter("code2", "G10")
                        .list();
                kycStatusesDc2.setItems(kycStatuses);
            }else{
                approvalStatusEntity.setOptionsContainer(kycStatusesDc);
                String querykycStatuses = "select e from KycStatus e where e.code = :code1";
                List<KycStatus> kycStatuses = this.dataManager.load(KycStatus.class)
                        .query(querykycStatuses)
                        .parameter("code1", "G10")
                        .list();
                kycStatusesDc2.setItems(kycStatuses);
            }
        }catch (Exception ex){
            log.info("Incomplete User Data : "+ex.getMessage());
        }

//        try{
//            if(userAddressesDc.getMutableItems().size() != 0) {
//                List<UserAddress> userAddressResi = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L01")).toList();
//                if (userAddressResi.size() > 0) {
//                    addressLineOneResi.setValue(userAddressesDc.getMutableItems().get(0).getAddress1());
//                    addressLineTwoResi.setValue(userAddressesDc.getMutableItems().get(0).getAddress2());
//                    postcodeResi.setValue(userAddressesDc.getMutableItems().get(0).getPostcode());
//                    stateResi.setValue(userAddressesDc.getMutableItems().get(0).getState());
//                    townResi.setValue(userAddressesDc.getMutableItems().get(0).getTown());
//                    countryDataResi.setValue(userAddressesDc.getMutableItems().get(0).getCountry());
//                }
//
//                List<UserAddress> userAddressMail = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L03")).toList();
//                if (userAddressMail.size() > 0) {
//                    addressLineOneMail.setValue(userAddressesDc.getMutableItems().get(0).getAddress1());
//                    addressLineTwoMail.setValue(userAddressesDc.getMutableItems().get(0).getAddress2());
//                    postcodeMail.setValue(userAddressesDc.getMutableItems().get(0).getPostcode());
//                    stateMail.setValue(userAddressesDc.getMutableItems().get(0).getState());
//                    townMail.setValue(userAddressesDc.getMutableItems().get(0).getTown());
//                    countryDataMail.setValue(userAddressesDc.getMutableItems().get(0).getCountry());
//                }
//            }
//            if (userTiersDc.getMutableItems().size() !=0){
//                tier.setValue(userTiersDc.getMutableItems().get(0).getTier().getName());
//                accountStatus.setValue(userProfileDc.getItem().getActStatus().getName());
//                identificationNoField.setValue(userProfileDc.getItem().getIdentificationNo());
//
//                if(userTiersDc.getMutableItems().get(0).getVerification() != null) {
//                    try {
//                        kycStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getKycStatus().getName());
//                    } catch (Exception ex) {
//                        log.error(ex.getMessage());
//                    }
//                    try {
//                        nameScreeningStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getNsResult().getName());
//                    } catch (Exception ex) {
//                        log.error(ex.getMessage());
//                    }
//                    try {
//                        riskProfilingStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getCrpResult().getName());
//                    } catch (Exception ex) {
//                        log.error(ex.getMessage());
//                    }
//                }
//
                try {
                    bgdID.setValue(bgdIDBean.getBgdID(userProfileDc.getItem().getUsername()).getBgdID());
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }
//
//            }
//
//            if(userBanksDc.getMutableItems().size() != 0) {
//                bankName.setValue(userBanksDc.getMutableItems().get(0).getBank().getName());
//                bankAccount.setValue(userBanksDc.getMutableItems().get(0).getAccountNumber());
//            }
//            if(userEmploymentDc.getMutableItems().size() != 0){
//                employmentDetails.setValue(userEmploymentDc.getMutableItems().get(0).getEmploymentStatus().getName());
//                employerName.setValue(userEmploymentDc.getMutableItems().get(0).getEmployerName());
//                position.setValue(userEmploymentDc.getMutableItems().get(0).getPosition().getName());
//                natureBusiness.setValue(userEmploymentDc.getMutableItems().get(0).getNatureOfBusiness().getName());
//                countryTaxResidence.setValue(userEmploymentDc.getMutableItems().get(0).getCountryOftaxResidence());
//                phoneNumberField.setValue(userProfileDc.getItem().getPhoneNumber());
//            }
//            if (statutoryDetailsDc.getMutableItems().size() !=0){
//                purposeAccountTransaction.setValue(statutoryDetailsDc.getMutableItems().get(0).getTransactionPurposeType().getName());
//                expectedVolume.setValue(statutoryDetailsDc.getMutableItems().get(0).getFrequencyMonthlyTransaction().getName());
//                expectedNumber.setValue(statutoryDetailsDc.getMutableItems().get(0).getTotalMonthlyTransaction().getName());
//            }
//            if(financialSourcesDc.getMutableItems().size() !=0) {
//                sourceFunds.setValue(financialSourcesDc.getMutableItems().get(0).getSourceFundType().getName());
//                sourceWealth.setValue(financialSourcesDc.getMutableItems().get(0).getSourceWealthType().getName());
//            }
//
//        }catch (Exception ex){
//            log.info("Incomplete User Data : "+ex.getMessage());
//        }
     }

    @Subscribe
    public void onInit(final InitEvent event) {
        uploadMultipleFile();
        uploadMultipleFileApprover();
    }

//    Decryption
    @Install(to = "identificationNoField", subject = "formatter")
    private String identificationNoFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
    }

    @Install(to = "phoneNumberField", subject = "formatter")
    private String phoneNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
    }

    @Install(to = "homeNumberField", subject = "formatter")
    private String homeNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
    }

    @Install(to = "officeNumberField", subject = "formatter")
    private String officeNumberFieldFormatter(String value) {
        return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
    }

    @Install(to = "bankAccount", subject = "formatter")
    private String bankAccountFormatter(final Object value) {
        return cipherEncryptionManagement.decrypt(value.toString(),environment.getProperty("secret_key"));
    }

    private UserTier getuserTier(){
        String query = "select e from UserTier e " +
                "where e.user = :user " +
                "and e.currentTier = :currentTier1";
        return this.dataManager.load(UserTier.class)
                .query(query)
                .parameter("currentTier1", "")
                .parameter("user", userProfileDc.getItem())
                .parameter("currentTier1", true)
                .one();
    }

    private ActivationStatus activationStatus(String code){
        String query = "select e from ActivationStatus e where e.code = :code";
        return this.dataManager.load(ActivationStatus.class)
                .query(query)
                .parameter("code", code)
                .one();
    }

    private KycStatus kycStatus(String code){
        String query = "select e from KycStatus e where e.code = :code";
        return this.dataManager.load(KycStatus.class)
                .query(query)
                .parameter("code",code)
                .one();
    }

    private RtType rtType(String code){
        String query = "select e from RtType e where e.code =:code";
        return this.dataManager.load(RtType.class)
                .query(query)
                .parameter("code", code)
                .one();
    }

    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                activityLogFile = this.dataManager.create(ActivityLogFile.class);
//                activityLogFile.setActivityLog(activityLogDc.getItem());
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
                if(userProfileDc.getItem().getActStatus().getCode().contains("/02")) {
                    activityLogFile.setAction(rtType("ACT02"));

                    activityLogFilesApproverDc.getMutableItems().add(activityLogFile);

                } else {
                    activityLogFile.setAction(rtType("ACT01"));

                    activityLogFilesDc.getMutableItems().add(activityLogFile);
                }
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
//        loadCollection();
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }

    private void uploadMultipleFileApprover() {
        fileUploadApprover.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUploadApprover.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                activityLogFile = this.dataManager.create(ActivityLogFile.class);
//                activityLogFile.setActivityLog(activityLogDc.getItem());
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
                if(userProfileDc.getItem().getActStatus().getCode().contains("/02")) {
                    activityLogFile.setAction(rtType("ACT02"));

                    activityLogFilesApproverDc.getMutableItems().add(activityLogFile);

                } else {
                    activityLogFile.setAction(rtType("ACT01"));

                    activityLogFilesDc.getMutableItems().add(activityLogFile);
                }
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUploadApprover.getUploadsMap().values())
                    .show();
        });
//        loadCollection();
        fileUploadApprover.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }

    @Install(to = "uploadFileTable.file_upload", subject = "columnGenerator")
    private Component uploadFileTableFile_uploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }



    @Install(to = "uploadFileTableApproval.file_upload_approver", subject = "columnGenerator")
    private Component uploadFileTableApprovalFile_uploadColumnGenerator(final ActivityLogFile activityLogFile1) {
        if (activityLogFile1.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile1.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile1.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (!userProfileDc.getItem().getActStatus().getCode().contains("/02")) {
            Button btn = uiComponents.create(Button.class);
            btn.setCaption("Delete");
            btn.addClickListener(clickEvent -> {
                uploadFileTableRemove.execute();
            });
            return btn;
        } else {
            return null;
        }
    }

    @Install(to = "uploadFileTableApproval.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableApprovalDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile1) {
            Button btn = uiComponents.create(Button.class);
            btn.setCaption("Delete");
            btn.addClickListener(clickEvent -> {
                uploadFileTableApprovalRemove.execute();
            });
            return btn;
    }

    private void loadCollection(){
//        rtTypesIDTypeDl.setParameter("code1", "G06");
//        rtTypesIDTypeDl.setParameter("code2", "G07");
//        rtTypesIDTypeDl.load();

        userProfileDl.load();
    try {
        activityLogFilesDl.setParameter("actionCode1", "ACT01");
        activityLogFilesDl.setParameter("activityLogModuleCode1", "MENU04");
        activityLogFilesDl.setParameter("activityLogRefEntity1", "UserProfile");
        activityLogFilesDl.setParameter("activityLogRefData1", userProfileDc.getItem().getId().toString());
        activityLogFilesDl.load();
    }catch (Exception ex){
        log.error(ex.getMessage());
    }
    try {
        activityLogFilesApproverDl.setParameter("actionCode1", "ACT02");
        activityLogFilesApproverDl.setParameter("activityLogModuleCode1", "MENU04");
        activityLogFilesApproverDl.setParameter("activityLogRefEntity1", "UserProfile");
        activityLogFilesApproverDl.setParameter("activityLogRefData1", userProfileDc.getItem().getId().toString());
        activityLogFilesApproverDl.load();
    }catch (Exception ex){
        log.error(ex.getMessage());
    }
    }

//    public enum InvestorAccountManagementEditApprover implements EnumClass<String>{;
//
//        private String id;
//
//        InvestorAccountManagementEditApprover(String id){
//            this.id = id;
//        }
//
//        @Override
//        public String getId() {
//            return id;
//        }
//        public static InvestorAccountManagementEditApprover fromId(String id){
//            for (InvestorAccountManagementEditApprover approver : InvestorAccountManagementEditApprover.values()) {
//                if (approver.getId().equals(id))
//                    return approver;
//            }
//            return null;
//        }
//    }

    public void saveDetailsHistory(UserProfile userProfile) {
        try {
            UserDetailHistory userDetailHistory = this.dataManager.create(UserDetailHistory.class);
            userDetailHistory.setUser(userProfile);
            userDetailHistory.setEmail(userProfileDc.getItem().getEmail());
            userDetailHistory.setFullName(userProfileDc.getItem().getFullName());
            userDetailHistory.setIdentificationNumber(userProfileDc.getItem().getIdentificationNo());
            userDetailHistory.setIdentificationType(userProfileDc.getItem().getIdentificationType());
            userDetailHistory.setNationality(userProfileDc.getItem().getNationality());
            userDetailHistory.setPhoneNumber(userProfileDc.getItem().getPhoneNumber());
            userDetailHistory.setUserType(userProfileDc.getItem().getUserType().getId());
            userDetailHistory.setActStatus(userProfile.getActStatus());

            this.dataManager.save(userDetailHistory);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    private void saveStatus(){
        try {
//            UserProfile userProfile = userProfileDc.getItem();
            UserProfile userProfile = this.dataManager.load(UserProfile.class).id(userProfileDc.getItem().getId()).one();

            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G04")) {//Active
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G04/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G19")) {//Dormant
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G19/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G20")) {//Suspend (Disciplinary Action)
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G20/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G21")) {//Suspend (Under Investigation)
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G21/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G22")) {//Suspend (Redo e-KYC)
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G22/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G23")) {//Close
                saveDetailsHistory(userProfile);
                if(checkAccountZeroise(userProfile)) {
                    userProfile.setActStatus(activationStatus("G23/02"));
                    this.dataManager.save(userProfile);
                    activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                            "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
                } else {
                    //kuar notification can't close that account. please zerorize that account before close
                    dialogs.createOptionDialog()
                            .withCaption("Investor Account Management")
                            .withContentMode(ContentMode.HTML)
                            .withMessage("Cash Wallet Balance and Gold Wallet Balance must be zero. Please zeroise the account before closing")
                            .withActions(
                                    new DialogAction(DialogAction.Type.OK)
                                            .withPrimary(true)
                                            .withCaption("Okay")
                            )
                            .show();
                }
//                userProfile.setActStatus(activationStatus("G23/02"));
//                this.dataManager.save(userProfile);
            }
            if (Objects.requireNonNull(accountStatusField.getValue()).getCode().equals("G18")) {//Deleted
                saveDetailsHistory(userProfile);
                userProfile.setActStatus(activationStatus("G18/02"));
                this.dataManager.save(userProfile);
                activityLogBean.makerActivity("MENU04", "LOG04-01", userProfile.getActStatus().getName(),
                        "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
            }
            //checker
            try {
                if (statusFieldId.getValue().name().equals("APPROVE")) {
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G04/02"))) {//Active
                        userProfile.setActStatus(activationStatus("G04"));
                        this.dataManager.save(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G19/02"))) {//Dormant
                        userProfile.setActStatus(activationStatus("G19"));
                        this.dataManager.save(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G20/02"))) {//Suspend (Disciplinary Action)
                        userProfile.setActStatus(activationStatus("G20"));
                        this.dataManager.save(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G21/02"))) {//Suspend (Under Investigation)
                        userProfile.setActStatus(activationStatus("G21"));
                        this.dataManager.save(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G22/02"))) {//Suspend (Redo e-KYC)
                        userProfile.setActStatus(activationStatus("G22"));
                        this.dataManager.save(userProfile);
                        downgradeTier(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G23/02"))) {//Close
                        if(checkAccountZeroise(userProfile)) {
                            userProfile.setActStatus(activationStatus("G23"));
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        } else {
                            dialogs.createOptionDialog()
                                    .withCaption("Investor Account Management")
                                    .withContentMode(ContentMode.HTML)
                                    .withMessage("Cash Wallet Balance and Gold Wallet Balance must be zero. Please zeroise the account before closing")
                                    .withActions(
                                            new DialogAction(DialogAction.Type.OK)
                                                    .withPrimary(true)
                                                    .withCaption("Okay")
                                    )
                                    .show();
                        }

                    }
                    if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G18/02"))) {//Deleted

                        Map<String, Object> response = keycloakService.deleteUser(userProfile.getUsername());
                        if(!response.get("code").equals(HttpStatus.OK)) {
                            //send email to ridwan
                        }
                        List<AccountBgd> accountBgds = this.dataManager.load(AccountBgd.class)
                                .query("select e from AccountBgd e " +
                                        "where e.accOwner = :accOwner")
                                .parameter("accOwner", userProfile.getUsername())
                                .list();

                        accountBgds.forEach(e -> {
                            e.setAccOwner(userProfile.getId().toString());
                            this.dataManager.save(e);
                        });

                        userProfile.setActStatus(activationStatus("G18"));
                        userProfile.setUsername(userProfile.getId().toString());
                        userProfile.setEmail(userProfile.getId().toString()+ "@deleted.com");
                        userProfile.setIdentificationNo(null);
                        if(accountBgds.size() == 0) {
                            userProfile.setDeletedBy(currentAuthentication.getUser().getUsername());
                            userProfile.setDeletedDate(new Date(System.currentTimeMillis()));
                        }

                        this.dataManager.save(userProfile);
                        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                    }
                }else {
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G04/02"))) {
                            //Set Previous Status from User Detail History
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G19/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G20/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G21/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G22/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G23/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                        if (Objects.requireNonNull(accountStatusField.getValue().getCode().equals("G18/02"))) {
                            ActivationStatus activationStatus = userDetailHistoryDc.getItem().getActStatus();
                            userProfile.setActStatus(activationStatus);
                            this.dataManager.save(userProfile);
                            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), userProfile.getActStatus().getCode(), activityLogFilesDc.getItems());
                        }
                    }
            }catch (Exception ex){
                log.error(ex.getMessage());
            }

//            activityLogBean.makerActivity("MENU04", "LOG04-01", "",
//                    "UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems()
//            );

        }catch (Exception ex){
            log.error("Data Unable to save due to :" + ex.getMessage());
        }
        close(StandardOutcome.DISCARD);
    }

    public void downgradeTier(UserProfile userProfile){
        //Downgrade Tier
        UserTier userTier = userTiersDc.getItem();
        userTier.setCurrentTier(false);

        UserTier userTier1 = this.dataManager.create(UserTier.class);
        userTier1.setUser(userProfile);
        userTier1.setTier(userTier.getTier());
        userTier1.setCurrentTier(true);

        this.dataManager.save(userTier);
        this.dataManager.save(userTier1);
//
//        TierType tier0 = this.dataManager.load(TierType.class)
//                .query("select e from TierType e " +
//                        "where e.code = :code1")
//                .parameter("code1", "T00")
//                .one();
//
//        UserTier userTier0 = this.dataManager.create(UserTier.class);
//        userTier0.setUser(userProfile);
//        userTier0.setCurrentTier(true);
//        userTier0.setTier(tier0);
//
//        this.dataManager.save(userTier);
//        this.dataManager.save(userTier0);



        List<UserVerification> userVerification = this.dataManager.load(UserVerification.class)
                .query("select e from UserVerification e " +
                        "where e.user = :user1")
                .parameter("user1", userProfile)
                .list();

        userVerification.forEach(e -> {
            this.dataManager.remove(e);
        });
    }

    public boolean checkAccountZeroise(UserProfile userProfile){
        List<AccountWalletView> accountWalletViews = this.dataManager.load(AccountWalletView.class)
                .query("select e from AccountWalletView e " +
                        "where e.accountOwner = :accountOwner1")
                .parameter("accountOwner1", userProfile.getUsername())
                .list();
        if (accountWalletViews.size() == 0){
            return true;
        } else {
            boolean flag = true;

//            accountWalletViews.forEach(e -> {
//                if(!e.getCurrentAmmount().equals(BigDecimal.ZERO)) {
//                    flage = false;
//
//                }
//            });
            for (int i = 0; i<accountWalletViews.size(); i++) {
                if(!accountWalletViews.get(i).getCurrentAmmount().equals(BigDecimal.ZERO)) {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
    }


    private void saveData(){
//        activityLogDc.getItem().setApproveBy(currentAuthentication.getUser().getUsername());
//        activityLogDc.getItem().setApproveDate(new Timestamp(System.currentTimeMillis()));
//        activityLogDc.getItem().setStatus(statusFieldId.getValue());
//        this.dataManager.save(activityLogDc.getItem());
//        activityLogFilesDc.getMutableItems().forEach(activityLogFile1 -> {
//            this.dataManager.save(activityLogFile1);
//        });


//        closeWithDiscard();
    }

//    @Subscribe("saveBtn")
//    public void onSaveBtnClick(final Button.ClickEvent event) {
//        dialogs.createOptionDialog()
//                .withCaption("Revenue Update Confirmation")
//                .withContentMode(ContentMode.HTML)
//                .withMessage("Click Confirm or Cancel button to proceed.")
//                .withActions(
//                        new DialogAction(DialogAction.Type.YES)
//                                .withPrimary(true)
//                                .withCaption("Confirm")
//                                .withHandler(e -> saveData()),
//                        new DialogAction(DialogAction.Type.NO)
//                                .withCaption("Cancel")
//                )
//                .show();
//    }

    @Subscribe("commitAndCloseBtn")
    public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
        if (remarksFieldId.isEmpty()) {
            notifications.create()
                    .withCaption("Please Fill All Mandatory Fields")
                    .withType(Notifications.NotificationType.ERROR)
                    .show();
            disableCommitActions();
        } else {
            dialogs.createOptionDialog()
                    .withCaption("Investor Account Management")
                    .withContentMode(ContentMode.HTML)
                    .withMessage("Click Confirm or Cancel button to proceed.")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withPrimary(true)
                                    .withCaption("Confirm")
                                    .withHandler(e -> saveData())
                                    .withHandler(e -> saveStatus()),
                            new DialogAction(DialogAction.Type.NO)
                                    .withCaption("Cancel")
                    )
                    .show();
            }
        }

        @Subscribe("closeBtn")
        public void onCloseBtnClick(Button.ClickEvent event) {
            closeWithDiscard();
        }
    }



