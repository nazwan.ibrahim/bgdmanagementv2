package com.company.management.screen.financecashtransactioninterface;

import com.company.management.entity.SapCash2;
import com.company.management.entity.TransactionHistoriesView;
import com.company.management.screen.sapfee2.SapFee2Browse;
import io.jmix.core.TimeSource;
import io.jmix.ui.component.Button;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.FinanceCashTransactionInterface;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicInteger;

@UiController("FinanceCashTransactionInterface.browse")
@UiDescriptor("finance-cash-transaction-interface-browse.xml")
@LookupComponent("financeCashTransactionInterfacesTable")
public class FinanceCashTransactionInterfaceBrowse extends StandardLookup<FinanceCashTransactionInterface> {
	private static final Logger log = LoggerFactory.getLogger(FinanceCashTransactionInterfaceBrowse.class);
	@Autowired
	private CollectionContainer<FinanceCashTransactionInterface> financeCashTransactionInterfacesDc;

	DecimalFormat dfmillionAmount = new DecimalFormat("000000000000.00");
	DecimalFormat dfmillionFee = new DecimalFormat("00000000000.00");
	@Autowired
	private TimeSource timeSource;

	int incrementNum;

	AtomicInteger x=new AtomicInteger(1);

	@Subscribe("importCSV")
	public void onImportCSVClick(final Button.ClickEvent event) throws FileNotFoundException {
		BigDecimal totalAmount = BigDecimal.ZERO;
		String dateFormat = "yyyyMM";
		String dateFormatTxt = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat dateSimpleformatterTxt = new SimpleDateFormat(dateFormatTxt);
		String path = String.format("C:\\Users\\amir\\Downloads\\CASH_%s.txt", dateSimpleformatter.format(timeSource.currentTimestamp()));
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
			financeCashTransactionInterfacesDc.getMutableItems().forEach(a -> {
				String getTransaction_date = a.getTransaction_date() != null ? a.getTransaction_date() : "";
				String getTransaction_code = a.getTransaction_code() != null ? a.getTransaction_code() : "";
				BigDecimal getAmount = a.getAmount().setScale(2,RoundingMode.UNNECESSARY);
				try {
					incrementNum = x.getAndIncrement();
					csvPrinter.printRecord(String.format("%06d", incrementNum),getTransaction_date,getTransaction_code,dfmillionAmount.format(getAmount));
				} catch (IOException e) {
					log.error("IOException Error :"+e);
				}
			});

			for(FinanceCashTransactionInterface financeCashTransactionInterface : financeCashTransactionInterfacesDc.getItems()){
				totalAmount = totalAmount.add(financeCashTransactionInterface.getAmount());
			}
			csvPrinter.printRecord(String.format("%06d", incrementNum),null,null,dfmillionAmount.format((totalAmount.setScale(2,RoundingMode.UNNECESSARY))));
			x.set(1);
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
//			log.error();
		}
	}




}