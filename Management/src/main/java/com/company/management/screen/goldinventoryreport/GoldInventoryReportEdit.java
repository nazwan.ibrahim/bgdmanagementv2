package com.company.management.screen.goldinventoryreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.GoldInventoryReport;

@UiController("GoldInventoryReport.edit")
@UiDescriptor("gold-inventory-report-edit.xml")
@EditedEntityContainer("goldInventoryReportDc")
public class GoldInventoryReportEdit extends StandardEditor<GoldInventoryReport> {
}