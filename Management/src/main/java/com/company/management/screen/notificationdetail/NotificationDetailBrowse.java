package com.company.management.screen.notificationdetail;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationDetail;

@UiController("NotificationDetail.browse")
@UiDescriptor("notification-detail-browse.xml")
@LookupComponent("notificationDetailsTable")
public class NotificationDetailBrowse extends StandardLookup<NotificationDetail> {
}