package com.company.management.screen.revenuemanagement;

import com.company.management.entity.RevenueManagementDoc;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.usersubstitution.CurrentUserSubstitution;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;
import io.jmix.ui.screen.LookupComponent;
import io.jmix.ui.upload.TemporaryStorage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@UiController("MarginConfigurationMaker.edit")
@UiDescriptor("margin-configuration-maker-edit.xml")
@EditedEntityContainer("revenueManagementDc")
public class MarginConfigurationMakerEdit extends StandardEditor<RevenueManagement> {

    @Autowired
    private DataManager dataManager;
    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private CollectionContainer<RtType> rtTypesTaxDc;
    @Autowired
    private CollectionContainer<RtType> rtTypesUomDc;
    @Autowired
    private CollectionLoader<RtType> rtTypesTaxDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomDl;
    @Autowired
    private InstanceContainer<RevenueManagement> revenueManagementDc;
//    @Autowired
//    private TextField<BigDecimal> minFieldId;
//    @Autowired
//    private TextField<BigDecimal> maxFieldId;
    @Autowired
    private TextField<BigDecimal> valueTaxField;
    @Autowired
    private EntityComboBox<RtType> uomTaxFieldId;
    @Autowired
    private DateField<Date> efectiveDateField;
    @Autowired
    private TimeSource timeSource;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private Notifications notifications;
    @Autowired
    private TemporaryStorage temporaryStorage;

    RevenueManagementDoc revenueManagementDoc;
    @Autowired
    private CollectionContainer<RevenueManagementDoc> revenueManagementDocsDc;
    @Autowired
    private CollectionLoader<RevenueManagementDoc> revenueManagementDocsDl;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private Downloader downloader;
    @Autowired
    private GroupTable<RevenueManagementDoc> uploadFileTable;
    @Named("uploadFileTable.remove")
    private RemoveAction<RevenueManagementDoc> uploadFileTableRemove;
    @Autowired
    private CollectionLoader<RtType> rtTypesDl;
    @Autowired
    private CollectionContainer<RtType> rtTypesDc;
    @Autowired
    private TextArea<String> remarksFieldId;

    @Subscribe
    public void onBeforeShow(final BeforeShowEvent event) {
        rtTypesUomDl.setParameter("codeJ01", "J01");
        rtTypesUomDl.setParameter("codeJ04", "J04");
        rtTypesUomDl.load();

        rtTypesTaxDl.setParameter("codeTax", "TAX");
        rtTypesTaxDl.load();

//        revenueManagementDocsDl.setParameter("actionCode1", "ACT01");
//        revenueManagementDocsDl.setParameter("createdBy1", currentAuthentication.getUser().getUsername());
        revenueManagementDocsDl.setParameter("revenueManagement1", getEditedEntity());
        revenueManagementDocsDl.load();

        efectiveDateField.setRangeStart(timeSource.currentTimestamp());


    }
    @Subscribe
    public void onInit(final InitEvent event) {
        uploadMultipleFile();
    }

    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        RtType statusPending = this.dataManager.load(RtType.class)
                .query("select e from RtType e " +
                        "where e.code = :code")
                .parameter("code", "G05").one();

        RevenueManagement newRevenueManagement = this.dataManager.create(RevenueManagement.class);
        newRevenueManagement.setProduct(revenueManagementDc.getItem().getProduct());
        newRevenueManagement.setType(revenueManagementDc.getItem().getType());
        newRevenueManagement.setCharge(revenueManagementDc.getItem().getCharge());
        newRevenueManagement.setUom(revenueManagementDc.getItem().getUom());
        newRevenueManagement.setMinRM(revenueManagementDc.getItem().getMinRM());
        newRevenueManagement.setMaxRM(revenueManagementDc.getItem().getMaxRM());
        newRevenueManagement.setTypeTax(revenueManagementDc.getItem().getTypeTax());
        newRevenueManagement.setValueTax(revenueManagementDc.getItem().getValueTax());
        newRevenueManagement.setUomTax(revenueManagementDc.getItem().getUomTax());
        newRevenueManagement.setEfectiveDate(revenueManagementDc.getItem().getEfectiveDate());
        newRevenueManagement.setRequestBy(currentAuthentication.getUser().getUsername());
        newRevenueManagement.setRequestDate(new Timestamp(System.currentTimeMillis()));
        newRevenueManagement.setStatus(statusPending);

        revenueManagementDc.setItem(newRevenueManagement);

//        if(newRevenueManagement.getUom().getCode().equals("J01")) {
//            minFieldId.setValue(null);
//            minFieldId.setEditable(false);
//
//            maxFieldId.setValue(null);
//            maxFieldId.setEditable(false);
//        } else {
//            minFieldId.setEditable(true);
//            maxFieldId.setEditable(true);
//        }

        if(newRevenueManagement.getTypeTax().getCode().equals("TAX00")) {
            valueTaxField.setValue(BigDecimal.ZERO);
            valueTaxField.setEditable(false);

//            uomTaxFieldId.setValue(null);
            uomTaxFieldId.setEditable(false);
        } else {
            valueTaxField.setEditable(true);
            uomTaxFieldId.setEditable(true);
        }
    }

    @Subscribe("taxFieldId")
    public void onTaxFieldIdValueChange(final HasValue.ValueChangeEvent<RtType> event) {
        if(event.getValue().getCode().equals("TAX00")){
            valueTaxField.setEditable(false);
            valueTaxField.setValue(BigDecimal.ZERO);
            uomTaxFieldId.setEditable(false);
        }else{
            valueTaxField.setEditable(true);
        }
    }



    private void saveData(){
        if(!efectiveDateField.isEmpty() && !remarksFieldId.isEmpty() && !revenueManagementDocsDc.getMutableItems().isEmpty()) {
            revenueManagementDc.getItem().setRequestBy(currentAuthentication.getUser().getUsername());
            revenueManagementDc.getItem().setRequestDate(new Timestamp(System.currentTimeMillis()));
            this.dataManager.save(revenueManagementDc.getItem());
            revenueManagementDocsDc.getMutableItems().forEach(revenueManagementDoc1 -> {
                this.dataManager.save(revenueManagementDoc1);
            });
            closeWithDiscard();
        }else{
            notifications.create()
                    .withCaption("Please Fill All Mandatory Fields")
                    .withType(Notifications.NotificationType.ERROR)
                    .show();
            disableCommitActions();
        }
    }


    private void uploadMultipleFile() {
        rtTypesDl.setParameter("code1", "ACT01");
        rtTypesDl.load();
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                revenueManagementDoc = this.dataManager.create(RevenueManagementDoc.class);
                revenueManagementDoc.setRevenueManagement(revenueManagementDc.getItem());
                revenueManagementDoc.setName(fileName);
                revenueManagementDoc.setFile_upload(fileRef);
                revenueManagementDoc.setAction(loadStatus("ACT01"));

                revenueManagementDocsDc.getMutableItems().add(revenueManagementDoc);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }


    @Install(to = "uploadFileTable.file_upload", subject = "columnGenerator")
    private Component uploadFileTableFile_uploadColumnGenerator(final RevenueManagementDoc revenueManagementDoc) {
        if (revenueManagementDoc.getFile_upload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(revenueManagementDoc.getFile_upload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(revenueManagementDoc.getFile_upload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableDeleteFileUploadColumnGenerator(final RevenueManagementDoc revenueManagementDoc) {
        Button btn = uiComponents.create(Button.class);
        btn.setCaption("Delete");
        btn.addClickListener(clickEvent -> {
            uploadFileTableRemove.execute();
        });
        return btn;
    }

    private RtType loadStatus(String code){
        return this.dataManager.load(RtType.class)
                .query("select r from RtType r " +
                        "where r.code = :code1")
                .parameter("code1", code)
                .one();
    }

    @Subscribe("commitBtn")
    public void onCommitBtnClick(final Button.ClickEvent event) {
        dialogs.createOptionDialog()
                .withCaption("Revenue Update Confirmation")
                .withContentMode(ContentMode.HTML)
                .withMessage("Click Confirm or Cancel button to proceed.")
                .withActions(
                        new DialogAction(DialogAction.Type.YES)
                                .withPrimary(true)
                                .withCaption("Confirm")
                                .withHandler(e -> saveData()),
                        new DialogAction(DialogAction.Type.NO)
                                .withCaption("Cancel")
                )
                .show();
    }
}