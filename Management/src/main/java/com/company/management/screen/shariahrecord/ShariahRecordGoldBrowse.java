package com.company.management.screen.shariahrecord;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecord;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ShariahRecordGold.browse")
@UiDescriptor("shariah-record-gold-browse.xml")
@LookupComponent("shariahRecordsTable")
public class ShariahRecordGoldBrowse extends StandardLookup<ShariahRecord> {
	@Autowired
	private TabSheet goldTab;
	@Autowired
	private CollectionLoader<ShariahRecord> shariahRecordsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private Table<ShariahRecord> shariahRecordsTable;
	@Autowired
	private Table<ShariahRecord> shariahRecordsTableDinar;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(goldTab.getTab("goldBar").getName().equals("goldBar")){
			shariahRecordsDl.setParameter("deliveryRecordPurchaseRecordTypeCode1","GT01");
			shariahRecordsDl.setParameter("code","ITF/01");
			shariahRecordsDl.load();
		}else {
			shariahRecordsDl.setParameter("deliveryRecordPurchaseRecordTypeCode1","GT02");
			shariahRecordsDl.setParameter("code","ITF/01");
			shariahRecordsDl.load();
		}
	}
	private void goToEditPage(ShariahRecord shariahRecord){
		ShariahRecordGoldEdit shariahRecordGoldEdit = screenBuilders.screen(this)
				.withScreenClass(ShariahRecordGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
					shariahRecordsDl.load();
				})
				.build();
		assert shariahRecord != null;
		shariahRecordGoldEdit.setEntityToEdit(shariahRecord);
		shariahRecordGoldEdit.show();
	}
	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			shariahRecordsDl.setParameter("deliveryRecordPurchaseRecordTypeCode1","GT01");
			shariahRecordsDl.setParameter("code","ITF/01");
			shariahRecordsDl.load();
		}else {
			shariahRecordsDl.setParameter("deliveryRecordPurchaseRecordTypeCode1","GT02");
			shariahRecordsDl.setParameter("code","ITF/01");
			shariahRecordsDl.load();
		}
	}
	@Subscribe("shariahRecordsTable.edit")
	public void onShariahRecordsTableEdit(Action.ActionPerformedEvent event) {
		ShariahRecord shariahRecord = shariahRecordsTable.getSingleSelected();
		goToEditPage(shariahRecord);
	}
	@Subscribe("shariahRecordsTableDinar.edit")
	public void onShariahRecordsTableDinarEdit(Action.ActionPerformedEvent event) {
		ShariahRecord shariahRecord = shariahRecordsTableDinar.getSingleSelected();
		goToEditPage(shariahRecord);
	}

	@Install(to = "shariahRecordsTable", subject = "styleProvider")
	private String shariahRecordsTableStyleProvider(ShariahRecord shariahRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (shariahRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "shariahRecordsTableDinar", subject = "styleProvider")
	private String shariahRecordsTableDinarStyleProvider(ShariahRecord shariahRecord, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (shariahRecord.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

}