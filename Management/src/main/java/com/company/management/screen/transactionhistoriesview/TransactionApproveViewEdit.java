package com.company.management.screen.transactionhistoriesview;

import com.company.management.app.ActivityLogBean;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.ActivityLogFile;
import com.company.management.entity.RtType;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.wallet.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.TimeSource;
import io.jmix.email.EmailException;
import io.jmix.email.EmailInfo;
import io.jmix.email.Emailer;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;
import io.jmix.emailtemplates.exception.ReportParameterTypeChangedException;
import io.jmix.emailtemplates.exception.TemplateNotFoundException;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.TransactionHistoriesView;
import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import javax.inject.Named;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@UiController("TransactionApproveView.edit")
@UiDescriptor("transaction-appove-view-edit.xml")
@EditedEntityContainer("transactionHistoriesViewDc")
public class TransactionApproveViewEdit extends StandardEditor<TransactionHistoriesView> {

    private static final Logger log = LoggerFactory.getLogger(TransactionApproveViewEdit.class);
    @Autowired
    private DataManager dataManager;
    @Autowired
    private ActivityLogBean activityLogBean;
    @Autowired
    private InstanceLoader<ActivityLog> activityLogDl;
    @Autowired
    private InstanceContainer<TransactionHistoriesView> transactionHistoriesViewDc;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesMakerDl;
    @Autowired
    private InstanceContainer<ActivityLog> activityLogDc;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesCheckerDl;
    @Autowired
    private CollectionLoader<RtType> rtTypesStatusDl;
    @Autowired
    private TextArea<String> approveRemarksField;
    @Autowired
    private RadioButtonGroup<RtType> statusFieldRadioBtn;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesCheckerDc;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private TemporaryStorage temporaryStorage;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc;
    @Autowired
    private Notifications notifications;
    @Autowired
    private InstanceContainer<ActivityLog> activityLogInsDc;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private Downloader downloader;
    @Named("activityLogChecker.remove")
    private RemoveAction<ActivityLogFile> activityLogCheckerRemove;
    @Autowired
    private Label uploadDocuments;
    @Autowired
    private EmailTemplates emailTemplates;
    @Autowired
    private Emailer emailer;

    @Autowired
    private Environment env;
    @Autowired
    private TimeSource timeSource;
    private String emailReceiver;
    private BigDecimal amount;
    private String reason;


    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        try{
            activityLogDl.setParameter("refEntity1", "TransactionAcct");
            activityLogDl.setParameter("refData1", transactionHistoriesViewDc.getItem().getId().toString());
            activityLogDl.load();
        }catch (Exception ex){
            log.info(ex.toString());
        }

        try{
            activityLogFilesMakerDl.setParameter("activityLog1", activityLogDc.getItem());
            activityLogFilesMakerDl.setParameter("actionCode1", "ACT01");
            activityLogFilesMakerDl.load();

        }catch (Exception ex){
            log.info(ex.toString());
        }

        try{
            activityLogFilesCheckerDl.setParameter("activityLog1", activityLogDc.getItem());
            activityLogFilesCheckerDl.setParameter("actionCode1", "ACT02");
            activityLogFilesCheckerDl.load();
        }catch (Exception ex){
            log.info(ex.toString());
        }

        rtTypesStatusDl.setParameter("code1", "G06");
        rtTypesStatusDl.setParameter("code2", "G07");
        rtTypesStatusDl.load();

        try{
            if(activityLogDl.getContainer().getItem().getStatus().getCode().equals("G05")) {
                approveRemarksField.setEditable(true);
                statusFieldRadioBtn.setEditable(true);
            }else{
                fileUpload.setVisible(false);
                uploadDocuments.setVisible(false);
            }
        }catch (Exception ex){
            log.error(ex.getMessage());
        }

        activityLogInsDc.setItem(this.dataManager.create(ActivityLog.class));
        activityLogFilesDl.setParameter("activityLog1", activityLogInsDc.getItem());
        uploadMultipleFile();

    }

    @Subscribe("saveBtn")
    public void onSaveBtnClick(final Button.ClickEvent event) {
        dialogs.createOptionDialog()
                .withCaption("Approval Confirmation")
                .withContentMode(ContentMode.HTML)
                .withMessage("Click Confirm or Cancel button to proceed.")
                .withActions(
                        new DialogAction(DialogAction.Type.YES)
                                .withPrimary(true)
                                .withCaption("Confirm")
                                .withHandler(e -> saveData()),
                        new DialogAction(DialogAction.Type.NO)
                                .withCaption("Cancel")
                )
                .show();
    }

    public void saveData() {
        List<TransStatus> transStatus = this.dataManager.load(TransStatus.class).all().list();

        List<TransactionAcct> transactionAcctslist = this.dataManager.load(TransactionAcct.class)
                .query("select t from TransactionAcct t " +
                        "where t.reference = :reference1")
                .parameter("reference1", transactionHistoriesViewDc.getItem().getReference())
                .list();

        if(statusFieldRadioBtn.getValue().getCode().equals("G06")) {
            transactionAcctslist.forEach(transactionAcct1 -> {
                transactionAcct1.setTransStatus(transStatus.stream().filter(f -> f.getCode().equals("G01")).toList().get(0));
                this.dataManager.save(transactionAcct1);
                if(transactionAcct1.getAmount().compareTo(BigDecimal.ZERO)>0){
                    emailReceiver = transactionAcct1.getAccount().getAccOwner();
                    amount = transactionAcct1.getAmount();
                }
            });
//            transactionAcct.setTransStatus(transStatus.stream().filter(f -> f.getCode().equals("G01")).toList().get(0));
        } else if (statusFieldRadioBtn.getValue().getCode().equals("G07")) {
            transactionAcctslist.forEach(transactionAcct1 -> {
                transactionAcct1.setTransStatus(transStatus.stream().filter(f -> f.getCode().equals("G07")).toList().get(0));
                this.dataManager.save(transactionAcct1);
                if(transactionAcct1.getAmount().compareTo(BigDecimal.ZERO)>0){
                    emailReceiver = transactionAcct1.getAccount().getAccOwner();
                    amount = transactionAcct1.getAmount();
                }
            });
        }

        List<TransactionAcct> transactionAcct1 = this.dataManager.load(TransactionAcct.class)
                .query("select t from TransactionAcct t " +
                        "where t.reference = :reference1")
                .parameter("reference1", transactionHistoriesViewDc.getItem().getReference())
                .list();
        if(statusFieldRadioBtn.getValue().getCode().equals("G06")) {
            transactionAcct1.forEach(transactionAcct2 -> {
                if(transactionAcct2.getTransType().getCode().equals("F03") && transactionAcct2.getTransStatus().getCode().equals("G05")){
                    transactionAcct2.setTransStatus(transStatus.stream().filter(f -> f.getCode().equals("G01")).toList().get(0));
                    this.dataManager.save(transactionAcct2);
                }
            });
        } else if (statusFieldRadioBtn.getValue().getCode().equals("G07")) {
            transactionAcct1.forEach(transactionAcct2 -> {
                if(transactionAcct2.getTransType().getCode().equals("F03") && transactionAcct2.getTransStatus().getCode().equals("G05")){
                    transactionAcct2.setTransStatus(transStatus.stream().filter(f -> f.getCode().equals("G07")).toList().get(0));
                    this.dataManager.save(transactionAcct2);
                }
            });
        }

        if(transactionHistoriesViewDc.getItem().getTransactionCode().equals("F03/E01-01")){
            if(!transactionHistoriesViewDc.getItem().getAccOwner().equals("BgdAdmin")) {
                if(!transactionHistoriesViewDc.getItem().getAccOwner().equals("AceAdmin")){
                    sendEmailCashReceive();
                }

            }
        }else if(transactionHistoriesViewDc.getItem().getTransactionCode().equals("F03")){
            if(!transactionHistoriesViewDc.getItem().getAccOwner().equals("BgdAdmin")){
                if(!transactionHistoriesViewDc.getItem().getAccOwner().equals("AceAdmin")){
                    sendEmailGoldReceive();
                }
            }
        }
        activityLogBean.checkerActivity(activityLogDc.getItem().getId(), approveRemarksField.getValue(), statusFieldRadioBtn.getValue().getCode(), activityLogFilesCheckerDc.getItems());
        closeWithDiscard();
    }

    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
                activityLogFile.setActivityLog(activityLogDc.getItem());
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
                activityLogFile.setAction(rtType("ACT01"));

                activityLogFilesCheckerDc.getMutableItems().add(activityLogFile);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }

    private RtType rtType(String code){
        String query = "select e from RtType e where e.code =:code";
        return this.dataManager.load(RtType.class)
                .query(query)
                .parameter("code", code)
                .one();
    }

    @Install(to = "activityLogChecker.fileUpload", subject = "columnGenerator")
    private Component activityLogCheckerFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "activityLogMaker.fileUpload", subject = "columnGenerator")
    private Component activityLogMakerFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "activityLogChecker.deleteFileUpload", subject = "columnGenerator")
    private Component activityLogCheckerDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        Button btn = uiComponents.create(Button.class);
        btn.setCaption("Delete");
        btn.addClickListener(clickEvent -> {
            activityLogCheckerRemove.execute();
        });
        return btn;
    }

    private void sendEmailGoldReceive(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "receive_gold";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailReceiver)
                    .setSubject("Receive Gold")
                    .setFrom(env.getProperty("supportHelpEmail"))
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", userProfile(emailReceiver).getFullName());
            params.put("date", dateFormat.format(timeSource.currentTimeMillis()));
            params.put("bgd", env.getProperty("bursa_label_name"));
            params.put("transfer_reason", transferInfo().getReason());
            params.put("amount", amount.setScale(0, RoundingMode.UNNECESSARY)+"g");
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailReceiver);
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
             } catch (EmailException e) {
	            throw new RuntimeException(e.getMessage());
            }
        } catch (TemplateNotFoundException | ReportParameterTypeChangedException e) {
	        throw new RuntimeException(e);
        }
    }

    private void sendEmailCashReceive(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "receive_cash";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailReceiver)
                    .setSubject("Receive Cash")
                    .setFrom(env.getProperty("supportHelpEmail"))
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", userProfile(emailReceiver).getFullName());
            params.put("date", dateFormat.format(timeSource.currentTimeMillis()));
            params.put("bgd", env.getProperty("bursa_label_name"));
            params.put("transfer_reason", transferInfo().getReason());
            params.put("amount", "RM"+amount.setScale(2, RoundingMode.UNNECESSARY));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailReceiver);
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
            } catch (EmailException e) {
                throw new RuntimeException(e.getMessage());
            }
        } catch (TemplateNotFoundException | ReportParameterTypeChangedException e) {
            throw new RuntimeException(e);
        }
    }

    private TransferInfo transferInfo(){
        return this.dataManager.load(TransferInfo.class)
                .query("select t from TransferInfo t " +
                        "where t.toAcct.accOwner = :toAcctAccOwner1")
                .parameter("toAcctAccOwner1", emailReceiver)
                .one();
    }

    private UserProfile userProfile(String email1){
            return this.dataManager.load(UserProfile.class)
                    .query("select u from UserProfile u " +
                            "where u.email = :email1")
                    .parameter("email1", email1)
                    .one();
    }

}
