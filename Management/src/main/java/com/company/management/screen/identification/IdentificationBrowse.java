package com.company.management.screen.identification;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.Identification;

@UiController("Identification.browse")
@UiDescriptor("identification-browse.xml")
@LookupComponent("identificationsTable")
public class IdentificationBrowse extends StandardLookup<Identification> {
}