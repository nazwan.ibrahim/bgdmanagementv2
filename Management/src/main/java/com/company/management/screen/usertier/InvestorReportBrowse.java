package com.company.management.screen.usertier;

import com.company.management.entity.InvestorReportBrowseDTO;
import com.company.management.entity.registrationdb.UserAddress;
import com.company.management.entity.registrationdb.UserEmployment;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.screen.b2bgoldpurchasing.B2BGoldPurchasingSubsequentEdit;
import com.company.management.screen.investorreportbrowsedto.InvestorReportDTOEdit;
import com.company.management.service.CipherEncryptionManagement;
import com.sun.star.rdf.XMetadatable;
import io.jmix.core.DataManager;
import io.jmix.core.Metadata;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.UserTier;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@UiController("InvestorReport.browse")
@UiDescriptor("investor-report-browse.xml")
@LookupComponent("userTiersTable")
public class InvestorReportBrowse extends StandardLookup<UserTier> {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(InvestorReportBrowse.class);
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private Table<InvestorReportBrowseDTO> userTiersTable;
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;

	@Autowired
	private Environment environment;
	@Autowired
	private CollectionLoader<UserTier> userTiersDl;
	@Autowired
	private CollectionContainer<UserTier> userTiersDc;
	@Autowired
	private Metadata metadata;
	@Autowired
	private CollectionContainer<InvestorReportBrowseDTO> investorReportBrowseDTODc;
	@Autowired
	private DataManager dataManager;

	@Subscribe
	public void onInit(InitEvent event) {
		userTiersDl.setParameter("currentTier",true);
		userTiersDl.setParameter("tierCode1","T00");
		userTiersDl.load();
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		List<InvestorReportBrowseDTO> investorReportBrowseDTOS = new ArrayList<>();
		try {
			userTiersDc.getItems().forEach(e -> {
				InvestorReportBrowseDTO investorReportBrowseDTO = metadata.create(InvestorReportBrowseDTO.class);
				if (e.getUser().getUsername() != null) {
					investorReportBrowseDTO.setUsername(e.getUser().getUsername());
				}
				if (e.getKycStatus() != null) {
					investorReportBrowseDTO.setStatus(e.getKycStatus().getName());
					investorReportBrowseDTO.setStatusCode(e.getKycStatus().getCode());
				}
				if (e.getUser().getFullName() != null) {
					investorReportBrowseDTO.setFullName(e.getUser().getFullName());
				}
				if (e.getTier() != null) {
					investorReportBrowseDTO.setTier(e.getTier().getName());
				}
				if (e.getUser().getIdentificationType() != null) {
					investorReportBrowseDTO.setIdentificationType(e.getUser().getIdentificationType().getName());
				}
				if (e.getUser().getIdentificationNo() != null) {
					investorReportBrowseDTO.setIdentificationNo(e.getUser().getIdentificationNo());
				}
				if (e.getUser().getPhoneNumber() != null) {
					investorReportBrowseDTO.setPhoneNumber(e.getUser().getPhoneNumber());
				}
				if (e.getUser().getEmail() != null) {
					investorReportBrowseDTO.setEmail(e.getUser().getEmail());
				}
				if (e.getUser().getNationality() != null) {
					investorReportBrowseDTO.setNationality(e.getUser().getNationality());
				}
				List<UserEmployment> userEmployment = this.dataManager.load(UserEmployment.class)
						.query("select e from UserEmployment e where e.user.id = :user")
						.parameter("user", e.getUser().getId())
						.list();
				if (userEmployment.size() > 0 && userEmployment.get(0).getCountryOftaxResidence() != null) {
					investorReportBrowseDTO.setCountryTax(userEmployment.get(0).getCountryOftaxResidence());
				}
				List<UserAddress> userAddress1 = this.dataManager.load(UserAddress.class)
						.query("select e from UserAddress e where e.user.id = :user and e.addType.code = :addressTypeCode")
						.parameter("user", e.getUser().getId())
						.parameter("addressTypeCode", "L01")
						.list();
				if (userAddress1.size() > 0 && userAddress1.get(0).getCountry() != null) {
					investorReportBrowseDTO.setPostcode(userAddress1.get(0).getPostcode());
					investorReportBrowseDTO.setCountry(userAddress1.get(0).getCountry());
				}
				try {
					List<AccountBgd> accountBgd = this.dataManager.load(AccountBgd.class)
							.query("select e from AccountBgd e " + "where e.accOwner = :accOwner1 " + "and e.walletType.code = :walletTypeCode1")
							.parameter("accOwner1", e.getUser().getUsername())
							.parameter("walletTypeCode1", "E02")
							.list();
					if (accountBgd.size() > 0 && accountBgd.get(0).getAcctNumber() != null) {
						investorReportBrowseDTO.setAccountNumber(accountBgd.get(0).getAcctNumber());
					}
					if (e.getUser().getApproveDate() != null) {
						investorReportBrowseDTO.setCreatedDate(e.getUser().getApproveDate());
					}
					if (e.getUser().getCloseDate() != null) {
						investorReportBrowseDTO.setClosingDate(e.getUser().getCloseDate());
					}
				} catch (Exception ex) {
					log.info(ex.getMessage());
				}
				investorReportBrowseDTOS.add(investorReportBrowseDTO);
			});
		investorReportBrowseDTODc.setItems(investorReportBrowseDTOS);
		}catch (Exception ex){
			log.info(ex.getMessage());
		}
	}


	@Subscribe("userTiersTable.view")
	public void onUserTiersTableView(Action.ActionPerformedEvent event) {
		InvestorReportDTOEdit investorReportDTOEdit = screenBuilders.screen(this)
				.withScreenClass(InvestorReportDTOEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					//getScreenData().loadAll();
				})
				.build();
		investorReportDTOEdit.setEntityToEdit(Objects.requireNonNull(userTiersTable.getSingleSelected()));
		investorReportDTOEdit.show();
	}

	@Install(to = "userTiersTable.[identificationNo]", subject = "columnGenerator")
	private Component userTiersTableUserIdentificationNoColumnGenerator(InvestorReportBrowseDTO investorReportBrowseDTO) {
		String decrypt = cipherEncryptionManagement.decrypt(investorReportBrowseDTO.getIdentificationNo(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}
	@Install(to = "userTiersTable.[phoneNumber]", subject = "columnGenerator")
	private Component userTiersTableUserPhoneNumberColumnGenerator(InvestorReportBrowseDTO investorReportBrowseDTO) {
		String decrypt = cipherEncryptionManagement.decrypt(investorReportBrowseDTO.getPhoneNumber(),environment.getProperty("secret_key"));
		if(decrypt != null) {
			return new Table.PlainTextCell(decrypt);
		}else{
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userTiersTable", subject = "styleProvider")
	private String userTiersTableStyleProvider(InvestorReportBrowseDTO investorReportBrowseDTO, String property) {
		if (property == null) {

		} else if (property.equals("status")) {
			switch (investorReportBrowseDTO.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

}