package com.company.management.screen.b2bgoldpurchasing;

import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldPurchasing;

@UiController("B2BGoldPurchasing.edit")
@UiDescriptor("b2b-gold-purchasing-edit.xml")
@EditedEntityContainer("b2BGoldPurchasingDc")
public class B2BGoldPurchasingEdit extends StandardEditor<B2BGoldPurchasing> {
}