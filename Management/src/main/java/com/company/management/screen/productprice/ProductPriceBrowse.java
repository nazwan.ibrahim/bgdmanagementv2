package com.company.management.screen.productprice;

import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ProductPrice.browse")
@UiDescriptor("product-price-browse.xml")
@LookupComponent("productPricesTable")
public class ProductPriceBrowse extends StandardLookup<ProductPrice> {
	@Autowired
	private CollectionLoader<ProductPrice> productPricesDl;

	@Subscribe
	public void onInit(InitEvent event) {
//		productPricesDl.setParameter("typeTypeGroupName1","Transaction Type");
//		productPricesDl.load();
	}
}