package com.company.management.screen.regtype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.RegType;

@UiController("RegType.edit")
@UiDescriptor("reg-type-edit.xml")
@EditedEntityContainer("regTypeDc")
public class RegTypeEdit extends StandardEditor<RegType> {
}