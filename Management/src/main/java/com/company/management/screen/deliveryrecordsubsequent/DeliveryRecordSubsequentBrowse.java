package com.company.management.screen.deliveryrecordsubsequent;

import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecordSubsequent;

@UiController("DeliveryRecordSubsequent.browse")
@UiDescriptor("delivery-record-subsequent-browse.xml")
@LookupComponent("deliveryRecordSubsequentsTable")
public class DeliveryRecordSubsequentBrowse extends StandardLookup<DeliveryRecordSubsequent> {
}