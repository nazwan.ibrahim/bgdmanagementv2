package com.company.management.screen.transactionacct;

import com.company.management.entity.ActivityLog;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.ui.screen.LookupComponent;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;

@UiController("TransactionAcctRedemption.browse")
@UiDescriptor("transaction-acct-redemption-browse.xml")
@LookupComponent("transactionAcctsTable")
public class TransactionAcctRedemptionBrowse extends StandardLookup<TransactionAcct> {
	@Autowired
	private CollectionLoader<TransactionAcct> transactionAcctsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private Table<TransactionAcct> transactionAcctsTable;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		transactionAcctsDl.setParameter("transTypeCode1","F04");
		transactionAcctsDl.load();
	}

	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		activityLogsDl.setParameter("moduleCode1", "MENU07");
		activityLogsDl.load();
	}

	@Install(to = "transactionAcctsTable", subject = "styleProvider")
	private String transactionAcctsTableStyleProvider(TransactionAcct transactionAcct, String property) {
		if (property == null) {

		} else if (property.equals("transStatus.name")) {
			switch (transactionAcct.getTransStatus().getCode()) {
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "activityLogTable", subject = "styleProvider")
	private String activityLogTableStyleProvider(ActivityLog activityLog, String property) {
		if (property == null) {

		} else if (property.equals("status")) {
			switch (activityLog.getStatus().getCode()) {
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}


	@Subscribe("transactionAcctsTable.edit")
	public void onTransactionAcctsTableEdit(Action.ActionPerformedEvent event) {
		TransactionAcct transactionAcct = transactionAcctsTable.getSingleSelected();

		TransactionAcctEditRedemption transactionAcctEditRedemption = this. screenBuilders.screen(this)
				.withScreenClass(TransactionAcctEditRedemption.class)
				.build();
		assert transactionAcct != null;
		transactionAcctEditRedemption.setEntityToEdit(transactionAcct);
		transactionAcctEditRedemption.show();
	}
	@Install(to = "transactionAcctsTable.amount", subject = "columnGenerator")
	private Component transactionAcctsTableAmountColumnGenerator(final TransactionAcct transactionAcct) {
		Label<String> label = uiComponents.create(Label.TYPE_STRING);
		BigDecimal amtDivideTo = new BigDecimal("4.25");
		BigDecimal amount = transactionAcct.getAmount().abs().divide(amtDivideTo, 2, RoundingMode.HALF_UP);
		label.setValue(amount.abs().toString());
		return label;
	}

	@Subscribe("transactionAcctsTable.replacementEdit")
	public void onTransactionAcctsTableReplacementEdit(final Action.ActionPerformedEvent event) {
		TransactionAcct transactionAcct = transactionAcctsTable.getSingleSelected();

		TransactionAcctReplacementEdit transactionAcctReplacementEdit = this. screenBuilders.screen(this)
				.withScreenClass(TransactionAcctReplacementEdit.class)
				.build();
		assert transactionAcct != null;
		transactionAcctReplacementEdit.setEntityToEdit(transactionAcct);
		transactionAcctReplacementEdit.show();
	}
}