package com.company.management.screen.inventorymanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.InventoryTransaction;

@UiController("InventoryManagement.browse")
@UiDescriptor("inventory-management-browse.xml")
@LookupComponent("table")
public class InventoryManagementBrowse extends MasterDetailScreen<InventoryTransaction> {
}