package com.company.management.screen.reserverecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;

@UiController("ReserveRecord.browse")
@UiDescriptor("reserve-record-browse.xml")
@LookupComponent("reserveRecordsTable")
public class ReserveRecordBrowse extends StandardLookup<ReserveRecord> {
}