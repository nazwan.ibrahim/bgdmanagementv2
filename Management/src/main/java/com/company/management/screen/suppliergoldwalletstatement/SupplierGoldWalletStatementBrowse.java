package com.company.management.screen.suppliergoldwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.SupplierGoldWalletStatement;

@UiController("SupplierGoldWalletStatement.browse")
@UiDescriptor("supplier-gold-wallet-statement-browse.xml")
@LookupComponent("supplierGoldWalletStatementsTable")
public class SupplierGoldWalletStatementBrowse extends StandardLookup<SupplierGoldWalletStatement> {
}