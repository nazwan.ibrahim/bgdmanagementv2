package com.company.management.screen.transactionhistoriesviewadmin;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionHistoriesViewAdmin;

@UiController("TransactionHistoriesViewAdminB2BSupplier.edit")
@UiDescriptor("transaction-histories-view-admin-b2bsupplier-edit.xml")
@EditedEntityContainer("transactionHistoriesViewAdminDc")
public class TransactionHistoriesViewAdminB2BSupplierEdit extends StandardEditor<TransactionHistoriesViewAdmin> {
}