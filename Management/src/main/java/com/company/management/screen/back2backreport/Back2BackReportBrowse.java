package com.company.management.screen.back2backreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.Back2BackReport;

@UiController("Back2BackReport.browse")
@UiDescriptor("back2-back-report-browse.xml")
@LookupComponent("back2BackReportsTable")
public class Back2BackReportBrowse extends StandardLookup<Back2BackReport> {
}