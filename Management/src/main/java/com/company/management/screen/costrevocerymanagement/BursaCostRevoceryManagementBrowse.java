package com.company.management.screen.costrevocerymanagement;

import com.company.management.entity.RevenueManagement;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.CostRevoceryManagement;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@UiController("BursaCostRevoceryManagement.browse")
@UiDescriptor("bursa-cost-revocery-management-browse.xml")
@LookupComponent("costRevoceryManagementsTable")

public class BursaCostRevoceryManagementBrowse extends StandardLookup<CostRevoceryManagement> {
	@Autowired
	private Notifications notifications;

	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<CostRevoceryManagement> costRevoceryManagementsDc;
	@Autowired
	private CollectionLoader<CostRevoceryManagement> costRevoceryManagementsDl;
	@Autowired
	private CollectionLoader<RtType> rtTypesTaxDl;
	@Autowired
	private CollectionLoader<RtType> rtTypesUOMDl;
	@Autowired
	private Dialogs dialogs;

	Set<CostRevoceryManagement> costRevoceryManagements;

	private RtType type;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private CollectionLoader<CostRevoceryManagement> costRevoceryManagementsDl1;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		loadTable();
	}



	@Subscribe
	public void onAfterShow(AfterShowEvent event) throws IOException {

//		String pathInputStream = "src/main/resources/firebase/bursa-gold-dinar-60900-firebase-adminsdk-3w5p3-6a51d4fbf1.json";
//		InputStream inputStream = new FileInputStream(new File(pathInputStream));
//
//		FirebaseOptions options = FirebaseOptions.builder()
//				.setCredentials(GoogleCredentials.fromStream(inputStream))
//				.setProjectId("project-965379884782")
//				.build();
//		FirebaseApp.initializeApp(options);
//
//		FirebaseDatabase database = FirebaseDatabase.getInstance();
//		DatabaseReference ref = database.getReference("path/to/data");
//
//		ref.addValueEventListener(new ValueEventListener() {
//			@Override
//			public void onDataChange(DataSnapshot dataSnapshot) {
//				// Handle data change
//			}
//
//			@Override
//			public void onCancelled(DatabaseError databaseError) {
//				// Handle database error
//			}
//		});

	}



	@Subscribe("costRevoceryManagementsTable.edit")
	public void onCostRevoceryManagementsTableEdit(Action.ActionPerformedEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Cost Recovery Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
		}

		private void saveData(){
			costRevoceryManagementsDc.getItems().forEach(costRevoceryManagement -> {
				dataManager.save(costRevoceryManagement);
			});
			notifications.create()
					.withType(Notifications.NotificationType.HUMANIZED)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Bursa Cost Revenue Successfully Updated")
					.show();
			costRevoceryManagementsDl.load();
		}

	private void makerRequest(){
		costRevoceryManagements.forEach(revenueManagement -> {
			RevenueManagement revenueManagement1 = this.dataManager.create(RevenueManagement.class);
			revenueManagement1.setStatus(rtType("G05"));
			revenueManagement1.setCharge(revenueManagement.getCharge());
			revenueManagement1.setProduct(revenueManagement.getProduct());
			revenueManagement1.setType(revenueManagement.getType());
			revenueManagement1.setTypeTax(revenueManagement.getTypeTax());
			revenueManagement1.setUom(revenueManagement.getUom());
			revenueManagement1.setUomTax(revenueManagement.getUomTax());
			revenueManagement1.setActive(false);
			revenueManagement1.setValueTax(revenueManagement.getValueTax());
			dataManager.save(revenueManagement1);
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated\nPending for Approval")
				.show();

		loadTable();
	}

	private void checkerApprovedRequest(){
		List<RevenueManagement> revenueManagements = this.dataManager.load(RevenueManagement.class)
				.query("select e from RevenueManagement e where e.status.code = :code")
				.parameter("code","G05")
				.fetchPlan("bursaRevenueManagement")
				.list();
		revenueManagements.forEach(revenueManagement -> {
			if (revenueManagement.getStatus().getCode().equals("G05")) {
				type = revenueManagement.getType();
				deArchivedPrevious();
				revenueManagement.setStatus(rtType("G01"));
				revenueManagement.setApprovedBy(currentAuthentication.getUser().getUsername());
				revenueManagement.setApprovedDate(timeSource.currentTimestamp());
				revenueManagement.setActive(true);
				this.dataManager.save(revenueManagement);
			}
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated")
				.show();

		loadTable();
	}

	private void checkerRejectRequest(){
		List<RevenueManagement> revenueManagements = this.dataManager.load(RevenueManagement.class)
				.query("select e from RevenueManagement e where e.status.code = :code")
				.parameter("code","G05")
				.fetchPlan("bursaRevenueManagement")
				.list();
		revenueManagements.forEach(revenueManagement -> {
			if (revenueManagement.getStatus().getCode().equals("G05")) {
				type = revenueManagement.getType();
				deArchivedPrevious();
				revenueManagement.setActive(true);
				revenueManagement.setStatus(rtType("G07"));
				revenueManagement.setApprovedBy(currentAuthentication.getUser().getUsername());
				revenueManagement.setApprovedDate(timeSource.currentTimestamp());
				this.dataManager.save(revenueManagement);
			}
		});
		notifications.create()
				.withType(Notifications.NotificationType.HUMANIZED)
				.withPosition(Notifications.Position.MIDDLE_CENTER)
				.withCaption("Bursa Revenue Successfully Updated")
				.show();

		loadTable();
	}
	private void deArchivedPrevious(){
		String query = "select e from RevenueManagement e where e.type = :type order by e.createdDate desc";
		List<RevenueManagement> revenueManagementList = this.dataManager.load(RevenueManagement.class)
				.query(query)
				.parameter("type", type)
				.fetchPlan("bursaRevenueManagement")
				.list();

		revenueManagementList.forEach(revenueManagement1 -> {
			if(revenueManagement1.getStatus().getCode().equals("G01")){
				revenueManagement1.setStatus(rtType("G15"));
				this.dataManager.save(revenueManagement1);
			}
		});
	}

	private RtType rtType(String code){
		String query = "select e from RtType e where e.code = :code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code",code)
				.one();
	}

	private void loadTable(){
		costRevoceryManagementsDl.setParameter("statusCode1","G01");
		costRevoceryManagementsDl.load();

		costRevoceryManagementsDl1.setParameter("statusCode1","G05");
		costRevoceryManagementsDl1.load();
	}
}