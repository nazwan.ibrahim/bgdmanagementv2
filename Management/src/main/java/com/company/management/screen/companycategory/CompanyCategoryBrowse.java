package com.company.management.screen.companycategory;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.CompanyCategory;

@UiController("CompanyCategory.browse")
@UiDescriptor("company-category-browse.xml")
@LookupComponent("companyCategoriesTable")
public class CompanyCategoryBrowse extends StandardLookup<CompanyCategory> {
}