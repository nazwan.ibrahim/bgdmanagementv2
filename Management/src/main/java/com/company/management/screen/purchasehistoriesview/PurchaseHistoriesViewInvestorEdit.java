package com.company.management.screen.purchasehistoriesview;

import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseHistoriesView;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("PurchaseHistoriesInvestorView.edit")
@UiDescriptor("purchase-histories-investor-view-edit.xml")
@EditedEntityContainer("purchaseHistoriesViewDc")
public class PurchaseHistoriesViewInvestorEdit extends StandardEditor<PurchaseHistoriesView> {
	@Autowired
	private InstanceLoader<PurchaseHistoriesView> purchaseHistoriesViewDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		purchaseHistoriesViewDl.setParameter("reference1",getEditedEntity().getReference());
		purchaseHistoriesViewDl.load();
	}




}