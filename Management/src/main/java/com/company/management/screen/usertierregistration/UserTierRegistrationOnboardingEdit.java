package com.company.management.screen.usertierregistration;

import com.company.management.entity.registrationdb.UserTier;
import io.jmix.ui.screen.*;

@UiController("UserTierRegistrationOnboarding.edit")
@UiDescriptor("user-tier-registration-onboarding-edit.xml")
@EditedEntityContainer("userTierRegistrationDc")
public class UserTierRegistrationOnboardingEdit extends StandardEditor<UserTier> {
}