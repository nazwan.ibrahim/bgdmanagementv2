package com.company.management.screen.purchaserecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseRecord;

@UiController("PurchaseRecord.edit")
@UiDescriptor("purchase-record-edit.xml")
@EditedEntityContainer("purchaseRecordDc")
public class PurchaseRecordEdit extends StandardEditor<PurchaseRecord> {
}