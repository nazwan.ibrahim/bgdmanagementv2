package com.company.management.screen.costrevocerymanagement;

import com.company.management.entity.CostRecoveryManagementDoc;
import com.company.management.entity.RevenueManagementDoc;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.usersubstitution.CurrentUserSubstitution;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.CostRevoceryManagement;
import io.jmix.ui.upload.TemporaryStorage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@UiController("CostRevoceryManagementApproval.edit")
@UiDescriptor("cost-revocery-management-approval-edit.xml")
@EditedEntityContainer("costRevoceryManagementDc")
public class CostRevoceryManagementApprovalEdit extends StandardEditor<CostRevoceryManagement> {
	@Autowired
	private UiComponents uiComponents;
	@Named("uploadFileTableApproval.remove")
	private RemoveAction<CostRecoveryManagementDoc> uploadFileTableApprovalRemove;
	@Autowired
	private Downloader downloader;
	@Autowired
	private CollectionLoader<RtType> rtTypesStatusDl;
	@Autowired
	private CollectionContainer<CostRecoveryManagementDoc> costRecoveryManagementDocsDc;
	@Autowired
	private CollectionLoader<CostRecoveryManagementDoc> costRecoveryManagementDocsDl;
	@Autowired
	private CollectionLoader<CostRecoveryManagementDoc> costRecoveryManagementDocsApprovalDl;
	@Autowired
	private CurrentUserSubstitution currentUserSubstitution;
	@Autowired
	private InstanceContainer<CostRevoceryManagement> costRevoceryManagementDc;
	@Autowired
	private RadioButtonGroup<RtType> statusFieldId;
	@Autowired
	private TextArea remarks2FieldId;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private TemporaryStorage temporaryStorage;
	@Autowired
	private DataManager dataManager;

	CostRecoveryManagementDoc costRecoveryManagementDoc;
	@Autowired
	private CollectionContainer<CostRecoveryManagementDoc> costRecoveryManagementDocsApprovalDc;
	@Autowired
	private Notifications notifications;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private Button saveBtn;
	@Autowired
	private TextField<String> approveByFieldId;
	@Autowired
	private TextField<Date> approveDateFieldId;
	@Autowired
	private Label uploadDocuments;

	@Subscribe
	public void onBeforeShow(final BeforeShowEvent event) {
		loadCollection();
	}

	private void loadCollection(){
		rtTypesStatusDl.setParameter("code1", "G06");
		rtTypesStatusDl.setParameter("code2", "G07");
		rtTypesStatusDl.load();
		costRecoveryManagementDocsDl.setParameter("costRecoveryManagement1",getEditedEntity());
		costRecoveryManagementDocsDl.setParameter("actionCode1","ACT01");
		costRecoveryManagementDocsDl.load();
		costRecoveryManagementDocsApprovalDl.setParameter("actionCode1", "ACT02");
		costRecoveryManagementDocsApprovalDl.setParameter("createdBy1", currentUserSubstitution.getAuthenticatedUser().getUsername());
		costRecoveryManagementDocsApprovalDl.load();
	}

	@Subscribe
	public void onInit(final InitEvent event) {
		uploadMultipleFile();
	}

	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		if(costRevoceryManagementDc.getItem().getStatus().getCode().equals("G05")) {
			statusFieldId.setEditable(true);
			remarks2FieldId.setEditable(true);
			saveBtn.setVisible(true);
			approveByFieldId.setVisible(false);
			approveDateFieldId.setVisible(false);
			uploadDocuments.setVisible(true);
			fileUpload.setVisible(true);
		}
	}

	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				costRecoveryManagementDoc = this.dataManager.create(CostRecoveryManagementDoc.class);
				costRecoveryManagementDoc.setCostRecoveryManagement(costRevoceryManagementDc.getItem());
				costRecoveryManagementDoc.setName(fileName);
				costRecoveryManagementDoc.setFile_upload(fileRef);
				costRecoveryManagementDoc.setAction(loadStatus("ACT02"));

				costRecoveryManagementDocsApprovalDc.getMutableItems().add(costRecoveryManagementDoc);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		loadCollection();
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}
	@Install(to = "uploadFileTableApproval.deleteFileUpload", subject = "columnGenerator")
	private Component uploadFileTableApprovalDeleteFileUploadColumnGenerator(final CostRecoveryManagementDoc costRecoveryManagementDoc) {
		Button btn = uiComponents.create(Button.class);
		btn.setCaption("Delete");
		btn.addClickListener(clickEvent -> {
			uploadFileTableApprovalRemove.execute();
		});
		return btn;
	}

	@Install(to = "uploadFileTable.file_upload", subject = "columnGenerator")
	private Component uploadFileTableFile_uploadColumnGenerator(final CostRecoveryManagementDoc costRecoveryManagementDoc) {
		if (costRecoveryManagementDoc.getFile_upload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(costRecoveryManagementDoc.getFile_upload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(costRecoveryManagementDoc.getFile_upload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Install(to = "uploadFileTableApproval.file_upload", subject = "columnGenerator")
	private Component uploadFileTableApprovalFile_uploadColumnGenerator(final CostRecoveryManagementDoc costRecoveryManagementDoc) {
		if (costRecoveryManagementDoc.getFile_upload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(costRecoveryManagementDoc.getFile_upload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(costRecoveryManagementDoc.getFile_upload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}




	private void saveData(){
		costRevoceryManagementDc.getItem().setApprovedBy(currentAuthentication.getUser().getUsername());
		costRevoceryManagementDc.getItem().setApprovedDate(new Timestamp(System.currentTimeMillis()));
		costRevoceryManagementDc.getItem().setStatus(statusFieldId.getValue());
		this.dataManager.save(costRevoceryManagementDc.getItem());
		costRecoveryManagementDocsDc.getMutableItems().forEach(costRecoveryManagementDoc -> {
			this.dataManager.save(costRecoveryManagementDoc);
		});
		closeWithDiscard();
	}

	private RtType loadStatus(String code){
		return this.dataManager.load(RtType.class)
				.query("select r from RtType r " +
						"where r.code = :code1")
				.parameter("code1", code)
				.one();
	}

	@Subscribe("saveBtn")
	public void onSaveBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Revenue Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

}