package com.company.management.screen.notificationtype;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationType;

@UiController("NotificationType.browse")
@UiDescriptor("notification-type-browse.xml")
@LookupComponent("notificationTypesTable")
public class NotificationTypeBrowse extends StandardLookup<NotificationType> {
}