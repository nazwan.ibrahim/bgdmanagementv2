package com.company.management.screen.transactionhistoriesview;

import io.jmix.ui.screen.*;
import com.company.management.entity.TransactionHistoriesView;

@UiController("InvestorWallet.edit")
@UiDescriptor("investor-wallet-edit.xml")
@EditedEntityContainer("transactionHistoriesViewDc")
public class InvestorWalletEdit extends StandardEditor<TransactionHistoriesView> {
}