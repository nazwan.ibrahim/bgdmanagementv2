package com.company.management.screen.b2bgoldselling;

import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldSelling;

@UiController("B2BGoldSelling.browse")
@UiDescriptor("b2b-gold-selling-browse.xml")
@LookupComponent("b2BGoldSellingsTable")
public class B2BGoldSellingBrowse extends StandardLookup<B2BGoldSelling> {
}