package com.company.management.screen.accountverificationreport;

import com.company.management.service.AccountVerificationReportService;
import com.company.management.service.CipherEncryptionManagement;
import com.google.common.base.Strings;
import io.jmix.core.LoadContext;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.AccountVerificationReport;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UiController("InvestorAccountVerificationReportBrowse.browse")
@UiDescriptor("InvestorAccountVerificationReportBrowse.xml")
@LookupComponent("accountVerificationReportsTable")
public class InvestorAccountVerificationReportBrowse extends StandardLookup<AccountVerificationReport> {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(InvestorAccountVerificationReportBrowse.class);
    @Autowired
    private AccountVerificationReportService accountVerificationReportService;
    @Autowired
    private CipherEncryptionManagement cipherEncryptionManagement;

    @Autowired
    private Environment environment;
    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private GroupTable<AccountVerificationReport> accountVerificationReportsTable;
    @Autowired
    private CollectionLoader<AccountVerificationReport> accountVerificationReportsDl;
    @Autowired
    private TextField<String> investorName;
    @Autowired
    private TextField<String> BgdID;
    @Autowired
    private TextField<String> contactNum;

    @Install(to = "accountVerificationReportsDl", target = Target.DATA_LOADER)
    private List<AccountVerificationReport> accountVerificationReportsDlLoadDelegate(LoadContext<AccountVerificationReport> loadContext) {
        // Here you can load entities from an external store
//        String investorName1 = investorName.getValue();
//        String bgdid = BgdID.getValue();
//        String contactnum = contactNum.getValue();

        try{
            if (!Objects.requireNonNull(investorName.getValue()).isEmpty()) {
                return loadFilterAccountVerification(investorName.getValue(), BgdID.getValue(), contactNum.getValue());
            }
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
           if(!Objects.requireNonNull(BgdID.getValue()).isEmpty()){
                return loadFilterAccountVerification(investorName.getValue(), BgdID.getValue(), contactNum.getValue());
            }
        }catch (Exception ex){
            log.info(ex.getMessage());
        }
        try{
             if(!Objects.requireNonNull(contactNum.getValue()).isEmpty()){
                return loadFilterAccountVerification(investorName.getValue(), BgdID.getValue(), contactNum.getValue());
            }
        }catch (Exception ex){
            log.info(ex.getMessage());
        }

        return accountVerificationReportService.getCombinedData();

    }

    @Install(to = "accountVerificationReportsTable.dateOfBirth", subject = "formatter")
    private String accountVerificationReportsTableDateOfBirthFormatter(final Object object) {
        String decrypt = cipherEncryptionManagement.decrypt(object.toString(), environment.getProperty("secret_key"));
        return Objects.requireNonNullElse(decrypt, "-");
    }

    @Install(to = "accountVerificationReportsTable.contactNum", subject = "formatter")
    private String accountVerificationReportsTableContactNumFormatter(final Object object) {
        String decrypt = cipherEncryptionManagement.decrypt(object.toString(), environment.getProperty("secret_key"));
        return Objects.requireNonNullElse(decrypt, "-");
    }

    @Subscribe("refreshBtn")
    public void onRefreshBtnClick(final Button.ClickEvent event) {
        accountVerificationReportsDl.load();
    }

    private List<AccountVerificationReport> loadFilterAccountVerification(String investorName1, String bgdid, String contactnum) {
        return accountVerificationReportService.getCombinedData().stream().filter(accountVerificationReport -> (Strings.isNullOrEmpty(investorName1) || investorName1.contains(accountVerificationReport.getInvestorName())) &&
               (Strings.isNullOrEmpty(bgdid) || bgdid.startsWith(accountVerificationReport.getBgdID())) &&
               (Strings.isNullOrEmpty(contactnum) || contactnum.startsWith(accountVerificationReport.getContactNum()))).collect(Collectors.toList());
    }

//    @Subscribe("accountVerificationReportsTable.edit")
//    public void onAccountVerificationReportsTableEdit(final Action.ActionPerformedEvent event) {
//        InvestorAccountVerificationReportEdit investorAccountVerificationReportEdit = screenBuilders.screen(this)
//                .withScreenClass(InvestorAccountVerificationReportEdit.class)
//                .withAfterCloseListener(afterScreenCloseEvent -> {
//
//                })
//                .build();
//        investorAccountVerificationReportEdit.setEntityToEdit(Objects.requireNonNull(accountVerificationReportsTable.getSingleSelected()));
//        investorAccountVerificationReportEdit.show();
//    }

}