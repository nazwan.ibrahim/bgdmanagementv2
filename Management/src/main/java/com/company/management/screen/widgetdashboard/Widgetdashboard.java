package com.company.management.screen.widgetdashboard;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Widgetdashboard")
@UiDescriptor("widgetDashboard.xml")
public class Widgetdashboard extends Screen {
}