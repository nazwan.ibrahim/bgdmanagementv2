package com.company.management.screen.b2bgoldpurchasing;

import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldPurchasing;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("B2BGoldPurchasingSubsequent.browse")
@UiDescriptor("b2b-gold-purchasing-subsequent-browse.xml")
@LookupComponent("b2BGoldPurchasingsTable")
public class B2BGoldPurchasingSubsequentBrowse extends StandardLookup<B2BGoldPurchasing> {
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Table<B2BGoldPurchasing> b2BGoldPurchasingsTable;
	@Autowired
	private CollectionLoader<B2BGoldPurchasing> b2BGoldPurchasingsDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		b2BGoldPurchasingsDl.setParameter("typeCode1","ITF/03");
		b2BGoldPurchasingsDl.load();
	}



	private void goToEditPage(B2BGoldPurchasing b2BGoldPurchasing){
		B2BGoldPurchasingSubsequentEdit b2BGoldPurchasingSubsequentEdit = screenBuilders.screen(this)
				.withScreenClass(B2BGoldPurchasingSubsequentEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		assert b2BGoldPurchasing != null;
		b2BGoldPurchasingSubsequentEdit.setEntityToEdit(b2BGoldPurchasing);
		b2BGoldPurchasingSubsequentEdit.setTypeTransaction("purchase");
		b2BGoldPurchasingSubsequentEdit.show();
	}

	@Subscribe("b2BGoldPurchasingsTable.edit")
	public void onB2BGoldPurchasingsTableEdit(Action.ActionPerformedEvent event) {
		B2BGoldPurchasing b2BGoldPurchasing = this.b2BGoldPurchasingsTable.getSingleSelected();
		goToEditPage(b2BGoldPurchasing);
	}

	@Subscribe("b2BGoldPurchasingsTable.create")
	public void onB2BGoldPurchasingsTableCreate(Action.ActionPerformedEvent event) {
		B2BGoldPurchasing b2BGoldPurchasing = this.dataManager.create(B2BGoldPurchasing.class);
		goToEditPage(b2BGoldPurchasing);
	}

	@Install(to = "b2BGoldPurchasingsTable", subject = "styleProvider")
	private String b2BGoldPurchasingsTableStyleProvider(B2BGoldPurchasing b2BGoldPurchasing, String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (b2BGoldPurchasing.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

}