package com.company.management.screen.productprice;

import com.company.management.entity.ProductPriceHistories;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Timer;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ProductPrice;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("ProductLivePrice.browse")
@UiDescriptor("product-live-price-browse.xml")
@LookupComponent("stockChart")
public class ProductLivePriceBrowse extends StandardLookup<ProductPriceHistories> {
	@Autowired
	private Timer timer;
	@Autowired
	private CollectionLoader<ProductPriceHistories> productPriceHistoriesesBursaDl;
	@Autowired
	private CollectionLoader<ProductPriceHistories> productPriceHistoriesesSupplierDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		productPriceHistoriesesBursaDl.setParameter("productPriceTypeCode1","A02");
		productPriceHistoriesesBursaDl.load();

		productPriceHistoriesesSupplierDl.setParameter("productPriceTypeCode1","A01");
		productPriceHistoriesesSupplierDl.load();
	}

	@Subscribe("startTimer")
	public void onStartTimerClick(Button.ClickEvent event) {
		timer.start();
	}

	@Subscribe("stopTimer")
	public void onStopTimerClick(Button.ClickEvent event) {
		timer.stop();
	}

}