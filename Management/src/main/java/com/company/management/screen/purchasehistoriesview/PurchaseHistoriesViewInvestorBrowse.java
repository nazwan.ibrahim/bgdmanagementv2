package com.company.management.screen.purchasehistoriesview;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseHistoriesView;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DecimalFormat;

@UiController("PurchaseHistoriesViewInvestor.browse")
@UiDescriptor("purchase-histories-investor-view-browse.xml")
@LookupComponent("purchaseHistoriesViewsTable")
public class PurchaseHistoriesViewInvestorBrowse extends StandardLookup<PurchaseHistoriesView> {
	@Autowired
	private CollectionLoader<PurchaseHistoriesView> purchaseHistoriesViewsDl;
	DecimalFormat df = new DecimalFormat("#,###,##0.000000");
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<PurchaseHistoriesView> purchaseHistoriesViewsTable;
	@Autowired
	private GroupTable<PurchaseHistoriesView> purchaseHistoriesViewsTableSell;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		purchaseHistoriesViewsDl.setParameter("transactionCode1","F06");
		purchaseHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
		purchaseHistoriesViewsDl.load();
	}

	@Subscribe("b2bInvestor")
	public void onB2bInvestorSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("bursaSellfromInvestor")){
			purchaseHistoriesViewsDl.setParameter("transactionCode1","F05");
			purchaseHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
		}else{
			purchaseHistoriesViewsDl.setParameter("transactionCode1","F06");
			purchaseHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
		}
		purchaseHistoriesViewsDl.load();
	}

	@Install(to = "purchaseHistoriesViewsTableSell.goldAmount", subject = "formatter")
	private String purchaseHistoriesViewsTableSellGoldAmountFormatter(Object object) {
		return df.format(object);
	}

	@Install(to = "purchaseHistoriesViewsTable.goldAmount", subject = "formatter")
	private String purchaseHistoriesViewsTableGoldAmountFormatter(Object object) {
		return df.format(object);
	}

	private void goToEdit(PurchaseHistoriesView purchaseHistoriesView){
		PurchaseHistoriesB2BViewEdit openScreen = screenBuilders.screen(this)
				.withScreenClass(PurchaseHistoriesB2BViewEdit.class)
				.build();
		openScreen.setOwnerType("Investor");
		openScreen.setPurchaseHistoriesView(purchaseHistoriesView);
		openScreen.show();
	}

	@Subscribe("purchaseHistoriesViewsTable.view")
	public void onPurchaseHistoriesViewsTableView(Action.ActionPerformedEvent event) {
		goToEdit(purchaseHistoriesViewsTable.getSingleSelected());
	}
	@Subscribe("purchaseHistoriesViewsTableSell.view")
	public void onPurchaseHistoriesViewsTableSellView(Action.ActionPerformedEvent event) {
		goToEdit(purchaseHistoriesViewsTableSell.getSingleSelected());
	}
}