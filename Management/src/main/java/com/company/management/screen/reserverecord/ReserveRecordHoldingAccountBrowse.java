package com.company.management.screen.reserverecord;

import com.company.management.entity.PurchaseRecord;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.Table;
import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;

import java.math.BigDecimal;

@UiController("ReserveRecordHoldingAccount.browse")
@UiDescriptor("reserve-record-holding-account-browse.xml")
@LookupComponent("reserveRecordsTable")
public class ReserveRecordHoldingAccountBrowse extends StandardLookup<ReserveRecord> {
	@Install(to = "reserveRecordsTable.non_tradable", subject = "columnGenerator")
	private Component reserveRecordsTableNon_tradableColumnGenerator(ReserveRecord reserveRecord) {
		if(reserveRecord.getPhysical_stock() != null & reserveRecord.getAcc_tradable() != null) {
			BigDecimal physical = reserveRecord.getPhysical_stock();
			BigDecimal tradable = reserveRecord.getAcc_tradable();
			BigDecimal nonTradable = physical.subtract(tradable);
			return new Table.PlainTextCell(nonTradable.toPlainString()) ;
		}else {
			return null;
		}
	}

	@Install(to = "purchaseRecordsTable.non_tradable", subject = "columnGenerator")
	private Component purchaseRecordsTableNon_tradableColumnGenerator(PurchaseRecord purchaseRecord) {
		if(purchaseRecord.getPhysical_stock() != null & purchaseRecord.getAcc_tradable() != null) {
			BigDecimal physical = purchaseRecord.getPhysical_stock();
			BigDecimal tradable = purchaseRecord.getAcc_tradable();
			BigDecimal nonTradable = physical.subtract(tradable);
			return new Table.PlainTextCell(nonTradable.toPlainString()) ;
		}else {
			return null;
		}
	}
}