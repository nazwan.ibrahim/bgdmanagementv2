package com.company.management.screen.accountwalletview;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.AccountWalletView;

@UiController("AccountWalletView.browse")
@UiDescriptor("account-wallet-view-browse.xml")
@LookupComponent("accountWalletViewsTable")
public class AccountWalletViewBrowse extends StandardLookup<AccountWalletView> {
}