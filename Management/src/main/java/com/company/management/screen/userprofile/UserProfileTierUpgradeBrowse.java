package com.company.management.screen.userprofile;

import com.company.management.entity.registrationdb.UserTier;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.UserProfile;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

@UiController("UserProfileTierUpgrade.browse")
@UiDescriptor("user-profile-tier-upragde-browse.xml")
@LookupComponent("userProfilesTable")
public class UserProfileTierUpgradeBrowse extends StandardLookup<UserProfile> {

	private static final Logger log = org.slf4j.LoggerFactory.getLogger(UserProfileTierUpgradeBrowse.class);
	@Autowired
	private CollectionLoader<UserProfile> userProfilesDl;
	@Autowired
	private CollectionLoader<UserProfile> userProfilesTier3Dl1;

	String typeUser = "tierTwoAccount";
	@Autowired
	private GroupTable<UserProfile> userProfilesTable;
	@Autowired
	private GroupTable<UserProfile> userProfilesTableTier3;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;

	@Autowired
	private Environment environment;
	@Autowired
	private DataManager dataManager;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		userProfilesDl.setParameter("userTierTierCode1","T02");
		userProfilesDl.load();
		userProfilesTier3Dl1.setParameter("userTierTierCode1","T03");
		userProfilesTier3Dl1.load();
	}

	@Subscribe("tierTab")
	public void onTierTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("tierTwoAccount")){
			userProfilesDl.setParameter("userTierTierCode1","T02");
			userProfilesDl.load();
			typeUser = "tierTwoAccount";
		}else {
			userProfilesTier3Dl1.setParameter("userTierTierCode1","T03");
			userProfilesTier3Dl1.load();
			typeUser = "tierThreeAccount";
		}
	}

	public void tierTwoAccount(){
		UserProfile userProfitierTwoAccount = userProfilesTable.getSingleSelected();
		pageNavigation(userProfitierTwoAccount);
	}

	public void tierThreeAccount(){
		UserProfile userProfitierThreeAccount = userProfilesTableTier3.getSingleSelected();
		pageNavigation(userProfitierThreeAccount);
	}

	public void pageNavigation(UserProfile userProfile){

		UserProfileTierUpgradeEdit userProfileEdit = screenBuilders.screen(this)
				.withScreenClass(UserProfileTierUpgradeEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					if(typeUser.equals("freeAccount")) {
						typeUser = "freeAccount";
					}else{
						typeUser = "tradableAccount";
					}
					userProfilesDl.setParameter("userTierTierCode1","T02");
					userProfilesDl.load();
					userProfilesTier3Dl1.setParameter("userTierTierCode1","T03");
					userProfilesTier3Dl1.load();
				})
				.build();
		userProfileEdit.setTypeUser(typeUser);
		userProfileEdit.setEntityToEdit(userProfile);
		userProfileEdit.show();
	}
	@Subscribe("userProfilesTable.edit")
	public void onUserProfilesTableEdit(Action.ActionPerformedEvent event) {
		tierTwoAccount();
	}

	@Subscribe("userProfilesTable.view")
	public void onUserProfilesTableView(Action.ActionPerformedEvent event) {
		tierTwoAccount();
	}

	@Subscribe("userProfilesTableTier3.view")
	public void onUserProfilesTableTier3View(Action.ActionPerformedEvent event) {
		tierThreeAccount();
	}

	@Subscribe("userProfilesTableTier3.edit")
	public void onUserProfilesTableTier3Edit(Action.ActionPerformedEvent event) {
		tierThreeAccount();
	}

	@Install(to = "userProfilesTable.identificationNo", subject = "columnGenerator")
	private Component userProfilesTableIdentificationNoColumnGenerator(UserProfile userProfile) {
		String decrypt = cipherEncryptionManagement.decrypt(userProfile.getPhoneNumber(),environment.getProperty("secret_key"));
		return new Table.PlainTextCell(decrypt);
	}

	@Install(to = "userProfilesTable.phoneNumber", subject = "columnGenerator")
	private Component userProfilesTablePhoneNumberColumnGenerator(UserProfile userProfile) {
		String decrypt = cipherEncryptionManagement.decrypt(userProfile.getPhoneNumber(),environment.getProperty("secret_key"));
		return new Table.PlainTextCell(decrypt);
	}

	@Install(to = "userProfilesTableTier3.identificationNo", subject = "columnGenerator")
	private Component userProfilesTableTier3IdentificationNoColumnGenerator(UserProfile userProfile) {
		String decrypt = cipherEncryptionManagement.decrypt(userProfile.getPhoneNumber(),environment.getProperty("secret_key"));
		return new Table.PlainTextCell(decrypt);
	}

	@Install(to = "userProfilesTableTier3.phoneNumber", subject = "columnGenerator")
	private Component userProfilesTableTier3PhoneNumberColumnGenerator(UserProfile userProfile) {
		String decrypt = cipherEncryptionManagement.decrypt(userProfile.getPhoneNumber(),environment.getProperty("secret_key"));
		return new Table.PlainTextCell(decrypt);
	}

	@Install(to = "userProfilesTable.status", subject = "columnGenerator")
	private Component userProfilesTableStatusColumnGenerator(UserProfile userProfile) {
		try {
			String queryuserTier = "select e from UserTier e where e.user = :user";
			UserTier userTier = this.dataManager.load(UserTier.class)
					.query(queryuserTier)
					.parameter("user", userProfile)
					.one();
			if (userTier.getKycStatus().getName() != null) {
				return new Table.PlainTextCell(userTier.getKycStatus().getName());
			} else {
				return new Table.PlainTextCell("-");
			}
		}catch (Exception ex){
			log.info("Selected data not found");
			return new Table.PlainTextCell("-");
		}
	}

	@Install(to = "userProfilesTableTier3.status", subject = "columnGenerator")
	private Component userProfilesTableTier3StatusColumnGenerator(UserProfile userProfile) {
		try {
			String queryuserTier = "select e from UserTier e where e.user = :user";
			UserTier userTier = this.dataManager.load(UserTier.class)
					.query(queryuserTier)
					.parameter("user", userProfile)
					.one();
			if (userTier.getKycStatus().getName() != null) {
				return new Table.PlainTextCell(userTier.getKycStatus().getName());
			} else {
				return new Table.PlainTextCell("-");
			}
		}catch (Exception ex){
			log.info("Selected data not found");
			return new Table.PlainTextCell("-");
		}
	}


}