package com.company.management.screen.resourceroleentity;

import com.company.management.entity.User;
import io.jmix.core.DataManager;
import io.jmix.core.Metadata;
import io.jmix.security.model.BaseRole;
import io.jmix.security.role.ResourceRoleRepository;
import io.jmix.security.role.assignment.RoleAssignmentRoleType;
import io.jmix.securitydata.entity.RoleAssignmentEntity;
import io.jmix.securityui.model.ResourceRoleModel;
import io.jmix.ui.Notifications;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import io.jmix.securitydata.entity.ResourceRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.stream.Collectors;

@UiController("sec_ResourceRoleEntity.browse")
@UiDescriptor("resource-role-entity-browse.xml")
@LookupComponent("resourceRoleEntitiesTable")
public class ResourceRoleEntityBrowse extends StandardLookup<ResourceRoleEntity> {
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<ResourceRoleEntity> resourceRoleEntitiesTable;
	@Autowired
	private CollectionContainer<RoleAssignmentEntity> roleAssignmentEntitiesDc;
	@Autowired
	private CollectionLoader<RoleAssignmentEntity> roleAssignmentEntitiesDl;
	@Autowired
	private Metadata metadata;
	@Autowired
	private Notifications notifications;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	private User user;
	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
//		BaseRole role = resourceRoleRepository.findRoleByCode(roleAssignmentEntity.getRoleCode());
		roleAssignmentEntitiesDl.setParameter("username1",getUser().getUsername());
		roleAssignmentEntitiesDl.setParameter("roleType1",RoleAssignmentRoleType.RESOURCE);
		roleAssignmentEntitiesDl.load();
	}
	@Subscribe("addRole")
	public void onAddRoleClick(Button.ClickEvent event) {
		resourceRoleEntitiesTable.getSelected().forEach(resourceRoleEntity -> {
//				if(roleAssignmentEntitiesDc.getRoleCode().equals(resourceRoleEntity.getCode())){
					RoleAssignmentEntity roleAssignmentEntity1 = this.dataManager.create(RoleAssignmentEntity.class);
					roleAssignmentEntity1.setRoleCode(resourceRoleEntity.getCode());
					roleAssignmentEntity1.setRoleType(RoleAssignmentRoleType.RESOURCE);
					roleAssignmentEntity1.setUsername(getUser().getUsername());
					this.dataManager.save(roleAssignmentEntity1);
//				}else{
//					notifications.create(Notifications.NotificationType.ERROR)
//							.withCaption("Role Assignment had been selected : "+resourceRoleEntity.getCode())
//							.show();
//				}
		});
		closeWithDefaultAction();
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDefaultAction();
	}

}