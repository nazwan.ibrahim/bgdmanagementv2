package com.company.management.screen.rewardmanagement;

import com.company.management.app.ActivityLogBean;
import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import io.jmix.ui.upload.TemporaryStorage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.util.*;

@UiController("RewardManagementSignupApprove.edit")
@UiDescriptor("reward-management-signup-approve-edit.xml")
@EditedEntityContainer("rewardManagementDc")
public class RewardManagementSignupApproveEdit extends StandardEditor<RewardManagement> {
    @Autowired
    private InstanceContainer<ActivityLog> activityLogDc;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc;
    @Autowired
    private InstanceContainer<RewardManagement> rewardManagementDc;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private CollectionContainer<RewardCriteria> rewardCriteriasDc;
    @Autowired
    private CollectionLoader<RtType> rtTypesUomCretiriaDl;
    @Autowired
    private FileMultiUploadField fileUpload;
    @Autowired
    private UiComponents uiComponents;
    @Autowired
    private TemporaryStorage temporaryStorage;
    @Autowired
    private Notifications notifications;
    @Named("uploadFileTable.remove")
    private RemoveAction<ActivityLogFile> uploadFileTableRemove;
    @Autowired
    private Downloader downloader;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private TextArea<String> remarksFieldId;
    @Autowired
    private ActivityLogBean activityLogBean;
    @Autowired
    private CollectionLoader<RtType> rtTypesRadioDl;
    @Autowired
    private RadioButtonGroup<RtType> radioButtonApprover;
    @Autowired
    private CollectionContainer<ActivityLogFile> activityLogFilesDc1;
    @Autowired
    private CollectionLoader<RewardCriteria> rewardCriteriasDl;
    @Autowired
    private InstanceLoader<ActivityLog> activityLogDl;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl;
    @Autowired
    private CollectionLoader<ActivityLogFile> activityLogFilesDl1;
    @Autowired
    private TextField<String> approveByFieldId;
    @Autowired
    private TextField<Date> approveDateFieldId;
    @Autowired
    private TextArea<String> remarksApproveFieldId;
    @Autowired
    private GroupTable<ActivityLogFile> uploadFileTable;
    @Autowired
    private ScreenValidation screenValidation;
    static Logger logger = LogManager.getLogger(RewardManagementSignupApproveEdit.class.getName());

    @Subscribe
    public void onInit(final InitEvent event) {
    }

    @Subscribe
    public void onAfterShow(final AfterShowEvent event) {
        rewardCriteriasDl.setParameter("reward1", rewardManagementDc.getItem());
        rewardCriteriasDl.load();

        if(rewardManagementDc.getItem().getStatus().getCode().equals("G05")) {
            radioButtonApprover.setEditable(true);
            radioButtonApprover.setVisible(true);
            remarksFieldId.setEditable(true);
            remarksFieldId.setVisible(true);
            fileUpload.setVisible(true);
            remarksApproveFieldId.setVisible(false);
            approveByFieldId.setVisible(false);
            approveDateFieldId.setVisible(false);
            uploadFileTable.setSortable(true);
            uploadFileTable.setStyleName("border");
        }

        rtTypesRadioDl.setParameter("code1", "G06");
        rtTypesRadioDl.setParameter("code2", "G07");
        rtTypesRadioDl.load();

        try{
            activityLogDl.setParameter("refEntity1", "RewardManagement");
            activityLogDl.setParameter("refData1", rewardManagementDc.getItem().getId().toString());
            activityLogDl.load();

            activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
            activityLogFilesDl.setParameter("actionCode1", "ACT01");
            activityLogFilesDl.load();

            activityLogFilesDl1.setParameter("activityLog1", activityLogDc.getItem());
            activityLogFilesDl1.setParameter("actionCode1", "ACT02");
            activityLogFilesDl1.load();
        }catch(Exception e){
            logger.error(e.getMessage());
        }

        uploadMultipleFile();
    }

    private void uploadMultipleFile() {
        fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
            for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
                UUID fileId = entry.getKey();
                String fileName = entry.getValue();
                FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
                ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
                activityLogFile.setActivityLog(activityLogDc.getItem());
                activityLogFile.setName(fileName);
                activityLogFile.setFileUpload(fileRef);
                activityLogFile.setAction(rtType("ACT02"));

                activityLogFilesDc1.getMutableItems().add(activityLogFile);
            }

            notifications.create()
                    .withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
                    .show();
        });
        fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
                notifications.create()
                        .withCaption("File upload error")
                        .show());
    }
    private RtType rtType(String code){
        String query = "select e from RtType e where e.code =:code";
        return this.dataManager.load(RtType.class)
                .query(query)
                .parameter("code", code)
                .one();
    }
    @Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
    private Component uploadFileTableDeleteFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if(rewardManagementDc.getItem().getStatus().getCode().equals("G05")) {
            Button btn = uiComponents.create(Button.class);
                btn.setCaption("Delete");
                btn.setId("deleteBtn");
                btn.addClickListener(clickEvent -> {
                    uploadFileTableRemove.execute();
                });
            return btn;
        }
        return null;
    }
    @Install(to = "uploadFileTable.fileUpload", subject = "columnGenerator")
    private Component uploadFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Install(to = "uploadedFileTable.fileUploaded", subject = "columnGenerator")
    private Component uploadedFileTableFileUploadColumnGenerator(final ActivityLogFile activityLogFile) {
        if (activityLogFile.getFileUpload() != null) {
            LinkButton linkButton = uiComponents.create(LinkButton.class);
            linkButton.setAction(new BaseAction("download")
                    .withCaption(activityLogFile.getFileUpload().getFileName())
                    .withHandler(actionPerformedEvent ->
                            downloader.download(activityLogFile.getFileUpload())
                    )
            );
            return linkButton;
        } else {
            return new Table.PlainTextCell("<empty>");
        }
    }

    @Subscribe("commitBtn")
    public void onCommitBtnClick(final Button.ClickEvent event) {
        if(radioButtonApprover.isEmpty()){
            notifications.create(Notifications.NotificationType.HUMANIZED)
                    .withCaption("Alert")
                    .withDescription("Status is required")
                    .withPosition(Notifications.Position.BOTTOM_RIGHT)
                    .show();
        }else{
            dialogs.createOptionDialog()
                    .withCaption("Sign Up Reward Edit Confirmation")
                    .withContentMode(ContentMode.HTML)
                    .withMessage("Click Confirm or Cancel button to proceed.")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withPrimary(true)
                                    .withCaption("Confirm")
                                    .withHandler(e -> saveData()),
                            new DialogAction(DialogAction.Type.NO)
                                    .withCaption("Cancel")
                    )
                    .show();
        }
    }

    private void saveData(){
        if(!radioButtonApprover.isEmpty()) {
            RtType rtType = this.dataManager.load(RtType.class).query("select e from RtType e where e.code = :code")
                    .parameter("code", radioButtonApprover.getValue().getCode()).one();
            //edit status from radio value
            RewardManagement rewardManagement = this.dataManager.load(RewardManagement.class).id(rewardManagementDc.getItem().getId()).one();
            rewardManagement.setStatus(rtType);
            this.dataManager.save(rewardManagement);
            //checker
            activityLogBean.checkerActivity(activityLogDc.getItem().getId(), remarksFieldId.getValue(), rtType.getCode(), activityLogFilesDc1.getItems());
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.MIDDLE_CENTER)
                    .withCaption("Sign Up Reward Successfully Created")
                    .show();
            closeWithDiscard();
        }else{
            notifications.create()
                    .withContentMode(ContentMode.HTML)
                    .withType(Notifications.NotificationType.HUMANIZED)
                    .withPosition(Notifications.Position.MIDDLE_CENTER)
                    .withCaption("Sign Up Reward Not Successfully Created")
                    .show();
            disableCommitActions();
        }
    }
}