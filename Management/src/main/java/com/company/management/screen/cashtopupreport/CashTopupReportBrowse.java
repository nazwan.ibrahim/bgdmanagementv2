package com.company.management.screen.cashtopupreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.CashTopupReport;

@UiController("CashTopupReport.browse")
@UiDescriptor("cash-topup-report-browse.xml")
@LookupComponent("cashTopupReportsTable")
public class CashTopupReportBrowse extends StandardLookup<CashTopupReport> {
}