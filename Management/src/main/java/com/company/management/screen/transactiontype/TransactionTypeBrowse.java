package com.company.management.screen.transactiontype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionType;

@UiController("TransactionType.browse")
@UiDescriptor("transaction-type-browse.xml")
@LookupComponent("transactionTypesTable")
public class TransactionTypeBrowse extends StandardLookup<TransactionType> {
}