package com.company.management.screen.deliveryrecordsubsequent;

import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("DeliveryRecordSubsequentGold.edit")
@UiDescriptor("delivery-record-subsequent-gold-edit.xml")
@EditedEntityContainer("deliveryRecordSubsequentDc")
public class DeliveryRecordSubsequentGoldEdit extends StandardEditor<DeliveryRecordSubsequent> {
	@Autowired
	private InstanceLoader<DeliveryRecordSubsequent> deliveryRecordSubsequentDl;
	@Autowired
	private InstanceContainer<DeliveryRecordSubsequent> deliveryRecordSubsequentDc;
	@Autowired
	private CollectionLoader<DeliveryRecordSubsequentDoc> deliveryRecordSubsequentDocsDl;
	@Autowired
	private CollectionLoader<ReserveRecordProduct> reserveRecordProductsDl;
	@Autowired
	private CollectionLoader<ReserveRecordDoc> reserveRecordDocsDl;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private Button commit;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		deliveryRecordSubsequentDl.load();
		deliveryRecordSubsequentDocsDl.setParameter("deliveryRecordReserveRecord1", deliveryRecordSubsequentDc.getItem().getReserveRecord());
		deliveryRecordSubsequentDocsDl.load();
		reserveRecordProductsDl.setParameter("reverse_record1", deliveryRecordSubsequentDc.getItem().getReserveRecord());
		reserveRecordProductsDl.load();
		reserveRecordDocsDl.setParameter("reserve_record1", deliveryRecordSubsequentDc.getItem().getReserveRecord());
		reserveRecordDocsDl.load();
	}

	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(ReserveRecordDoc reserveRecordDoc) {
		if (reserveRecordDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(reserveRecordDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(reserveRecordDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Install(to = "fileUploadDelivery.uploadFile", subject = "columnGenerator")
	private Component fileUploadDeliveryUploadFileColumnGenerator(DeliveryRecordSubsequentDoc deliveryRecordSubsequentDoc) {
		if (deliveryRecordSubsequentDoc.getUploadFile() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(deliveryRecordSubsequentDoc.getUploadFile().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(deliveryRecordSubsequentDoc.getUploadFile())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dataManager.save(getEditedEntity());
		if(uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
	}

	private RtType loadType(String code){
		return this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", code)
				.one();
	}

	private void saveUploadDoc(){
		DeliveryRecordSubsequentDoc deliveryRecordSubsequentDoc = this.dataManager.create(DeliveryRecordSubsequentDoc.class);
		deliveryRecordSubsequentDoc.setDeliveryRecord(deliveryRecordSubsequentDc.getItem());
		deliveryRecordSubsequentDoc.setUploadFile(uploadPurchaseForm.getValue());
		deliveryRecordSubsequentDoc.setName(uploadPurchaseForm.getFileName());
		deliveryRecordSubsequentDoc.setCreatedBy(currentAuthentication.getAuthentication().getName());
		deliveryRecordSubsequentDoc.setCreatedDate(timeSource.currentTimestamp());
		this.dataManager.save(deliveryRecordSubsequentDoc);
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Confirm Delivery Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveData(){
		getEditedEntity().setStatus(loadType("G01"));
		dataManager.save(getEditedEntity());
		if(uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		closeWithDiscard();
	}
}