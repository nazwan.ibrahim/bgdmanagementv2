package com.company.management.screen.financecashtransactioninterface;

import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.Button;
import io.jmix.ui.screen.*;
import com.company.management.entity.FinanceCashTransactionInterface;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("FinanceCashTransactionInterfacePart1.browse")
@UiDescriptor("finance-cash-transaction-interface-part1-browse.xml")
@LookupComponent("financeCashTransactionInterfacesTable")
public class FinanceCashTransactionInterfacePart1Browse extends StandardLookup<FinanceCashTransactionInterface> {


	@Autowired
	private DataManager dataManager;
	@Autowired
	private Notifications notifications;
	private void sumAmountF01(){

		TransactionAcct transactionAcct = this.dataManager.load(TransactionAcct.class)
				.query("select SUM(e.amount) from TransactionAcct e " +
						"where e.transType.code = :transTypeCode1 " +
						"and e.account.walletType.code = :accountWalletTypeCode1 " +
						"and e.transStatus.code = :transStatusCode1 " +
						"and e.account.regType.code = :accountRegTypeCode1 " +
						"or e.account.regType.code = :accountRegTypeCode2 " +
						"and e.createdDate in :createdDate1 " +
						"and e.createdDate in :createdDate2")
				.parameter("accountRegTypeCode1", "F01")
				.parameter("accountRegTypeCode2", "E01")
				.parameter("accountWalletTypeCode1", "G01")
				.parameter("createdDate1", "")
				.parameter("createdDate2", "")
				.parameter("transStatusCode1", "")
				.parameter("transTypeCode1", "")
				.one();
	}




	@Subscribe("clickBtn")
	public void onClickBtnClick(final Button.ClickEvent event) {
		notifications.create()
				.withType(Notifications.NotificationType.ERROR)
				.withCaption("Hai Awak Semua. Buat apa tuuuuuuuu????")
				.show();
	}
}