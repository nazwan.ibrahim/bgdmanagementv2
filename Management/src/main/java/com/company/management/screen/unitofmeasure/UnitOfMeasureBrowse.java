package com.company.management.screen.unitofmeasure;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.UnitOfMeasure;

@UiController("UnitOfMeasure.browse")
@UiDescriptor("unit-of-measure-browse.xml")
@LookupComponent("unitOfMeasuresTable")
public class UnitOfMeasureBrowse extends StandardLookup<UnitOfMeasure> {
}