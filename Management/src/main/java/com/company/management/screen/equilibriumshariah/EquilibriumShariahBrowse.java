package com.company.management.screen.equilibriumshariah;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.EquilibriumShariah;

@UiController("EquilibriumShariah.browse")
@UiDescriptor("equilibrium-shariah-browse.xml")
@LookupComponent("equilibriumShariahsTable")
public class EquilibriumShariahBrowse extends StandardLookup<EquilibriumShariah> {
}