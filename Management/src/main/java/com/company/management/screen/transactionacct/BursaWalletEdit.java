package com.company.management.screen.transactionacct;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;

@UiController("BursaWallet.edit")
@UiDescriptor("bursa-wallet-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class BursaWalletEdit extends StandardEditor<TransactionAcct> {
}