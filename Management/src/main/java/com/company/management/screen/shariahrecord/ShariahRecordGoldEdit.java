package com.company.management.screen.shariahrecord;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionAcct;
import com.company.management.entity.wallet.TransactionType;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@UiController("ShariahRecordGold.edit")
@UiDescriptor("shariah-record-gold-edit.xml")
@EditedEntityContainer("shariahRecordDc")
public class ShariahRecordGoldEdit extends StandardEditor<ShariahRecord> {
	private static final Logger log = LoggerFactory.getLogger(ShariahRecordGoldEdit.class);
	@Autowired
	private CollectionLoader<PurchaseRecordProduct> purchaseRecordProductsDl;
	@Autowired
	private CollectionLoader<PurchaseRecordSuppDoc> purchaseRecordSuppDocsDl;
	@Autowired
	private CollectionLoader<DeliveryRecordDoc> deliveryRecordDocsDl;
	@Autowired
	private InstanceLoader<ShariahRecord> shariahRecordDl;
	@Autowired
	private InstanceContainer<ShariahRecord> shariahRecordDc;
	@Autowired
	private CollectionLoader<ShariahRecordDoc> shariahRecordDocsDl;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private Button commit;
	@Autowired
	private CollectionContainer<PurchaseRecordProduct> purchaseRecordProductsDc;
	private BigDecimal total = BigDecimal.ZERO;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private Button commitAndCloseBtn;
	@Autowired
	private VBoxLayout shariahUpload;
	@Autowired
	private Notifications notifications;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		shariahRecordDl.load();
		purchaseRecordProductsDl.setParameter("purchase_record1", shariahRecordDc.getItem().getDeliveryRecord().getPurchaseRecord());
		purchaseRecordProductsDl.load();
		purchaseRecordSuppDocsDl.setParameter("purchase_record1", shariahRecordDc.getItem().getDeliveryRecord().getPurchaseRecord());
		purchaseRecordSuppDocsDl.load();
		deliveryRecordDocsDl.setParameter("deliveryRecordPurchaseRecord1", shariahRecordDc.getItem().getDeliveryRecord().getPurchaseRecord());
		deliveryRecordDocsDl.load();
		shariahRecordDocsDl.setParameter("shariahRecordDeliveryRecordPurchaseRecord1", shariahRecordDc.getItem().getDeliveryRecord().getPurchaseRecord());
		shariahRecordDocsDl.load();
	}
	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		if (getEditedEntity().getStatus().getCode().equals("G01")) {
			commitAndCloseBtn.setVisible(false);
			commit.setVisible(false);
			commitAndCloseBtn.setEnabled(false);
			commit.setEnabled(false);
			shariahUpload.setVisible(false);
		}
	}
	@Install(to = "fileUploadShariah.fileUpload", subject = "columnGenerator")
	private Component fileUploadShariahFileUploadColumnGenerator(ShariahRecordDoc shariahRecordDoc) {
		if (shariahRecordDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(shariahRecordDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(shariahRecordDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(PurchaseRecordSuppDoc purchaseRecordSuppDoc) {
		if (purchaseRecordSuppDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(purchaseRecordSuppDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(purchaseRecordSuppDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Shariah Record")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}
	private void saveData(){
		dataManager.save(getEditedEntity());

		if(uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		if(getEditedEntity().getStatus().equals(loadType("G01"))){
			addtogoldWallet();
		}
		closeWithDiscard();
	}
	private RtType loadType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}
	private void saveUploadDoc(){
		ShariahRecordDoc shariahRecordDoc = this.dataManager.create(ShariahRecordDoc.class);
		shariahRecordDoc.setShariahRecord(shariahRecordDc.getItem());
		shariahRecordDoc.setFileUpload(uploadPurchaseForm.getValue());
		shariahRecordDoc.setName(uploadPurchaseForm.getFileName());
		shariahRecordDoc.setCreatedBy(currentAuthentication.getAuthentication().getName());
		shariahRecordDoc.setCreatedDate(timeSource.currentTimestamp());
		this.dataManager.save(shariahRecordDoc);
	}
	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Shariah Record")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}
	@Install(to = "fileUploadDelivery.uploadFile", subject = "columnGenerator")
	private Component fileUploadDeliveryUploadFileColumnGenerator(DeliveryRecordDoc deliveryRecordDoc) {
		if (deliveryRecordDoc.getUploadFile() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(deliveryRecordDoc.getUploadFile().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(deliveryRecordDoc.getUploadFile())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	private void addtogoldWallet(){
		String queryaccountBgd = "select a from AccountBgd a " +
				"where a.accOwner = :accOwner1 " +
				"and a.walletType.code = :walletTypeCode1";
		AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
				.query(queryaccountBgd).parameter("accOwner1", "BgdAdmin").parameter("walletTypeCode1", "E02").one();

		TransactionAcct transactionAcct =this.dataManager.create(TransactionAcct.class);

		String querytransStatus = "select t from TransStatus t " +
				"where t.code = :code1";
		TransStatus transStatus = this.dataManager.load(TransStatus.class)
				.query(querytransStatus).parameter("code1", "G01")
				.one();

		transactionAcct.setAccount(accountBgd);


		List<HoldingAcct> holdingAcctList = new ArrayList<>();
		for (PurchaseRecordProduct value : purchaseRecordProductsDc.getItems()) {
			if(value.getType().getCode().equals("GT01")) {
				total = total.add(value.getTradable_gold());

//				purchaseRecordProductsDc.getItems().forEach(e -> {
				HoldingAcct holdingAcct = this.dataManager.create(HoldingAcct.class);
				holdingAcct.setReference_Num(value.getSerial_number());
				holdingAcct.setHolding(value.getHolding_acct());
				holdingAcct.setTradable(value.getTradable_gold());
				holdingAcct.setConvert(BigDecimal.ZERO);
				holdingAcctList.add(holdingAcct);
//				});

			}else{
				total = total.add(value.getTradable_gold());

			}
		}
		transactionAcct.setTransType(type("F01"));
		String reference = managementController.generateSeqNo("BGDF01","6");
		transactionAcct.setReference(reference);
		transactionAcct.setAmount(total);
		transactionAcct.setTransStatus(transStatus);
		transactionAcct.setCreatedBy(accountBgd.getAccOwner());
		transactionAcct.setCreatedDate(new Date());

		holdingAcctList.forEach(e -> {
			this.dataManager.save(e);
		});

		this.dataManager.save(transactionAcct);
	}
	private TransactionType type(String code){
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
		return this.dataManager.load(TransactionType.class)
				.query(query).parameter("code1", code)
				.one();
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDiscard();
	}

	@Subscribe("uploadPurchaseForm")
	public void onUploadPurchaseFormValueChange(HasValue.ValueChangeEvent<FileRef> event) {
		try {
			String fileName = Objects.requireNonNull(event.getValue()).getFileName();
			// Check if the file name contains multiple extensions
			if (fileName.contains("..") || fileName.indexOf('.') != fileName.lastIndexOf('.')) {
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.WARNING)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Invalid file upload. Please use the correct file")
						.show();
				uploadPurchaseForm.clear();
			}
		}catch (Exception ex){
			log.info(ex.getMessage());
		}
	}
}