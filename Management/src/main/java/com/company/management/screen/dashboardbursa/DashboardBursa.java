package com.company.management.screen.dashboardbursa;

import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("DashboardBursa")
@UiDescriptor("Dashboard_Bursa.xml")
public class DashboardBursa extends Screen {
}