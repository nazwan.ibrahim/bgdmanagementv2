package com.company.management.screen.physicalgoldbar;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Physicalgoldbar")
@UiDescriptor("physicalGoldBar.xml")
@DashboardWidget(name = "Physicalgoldbar")
public class Physicalgoldbar extends ScreenFragment {
}