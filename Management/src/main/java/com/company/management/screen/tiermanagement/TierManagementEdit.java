package com.company.management.screen.tiermanagement;

import com.company.management.entity.*;
import com.company.management.screen.rttype.RtTypeByGroupBrowse;
import io.jmix.core.DataManager;
import io.jmix.ui.Dialogs;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.model.*;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("TierManagement.edit")
@UiDescriptor("tier-management-edit.xml")
@EditedEntityContainer("tierManagementDc")
public class TierManagementEdit extends StandardEditor<TierManagement> {

	private TierManagement tierManagement;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private CollectionLoader<TierThreshold> tierThresholdsDl;
	@Autowired
	private CollectionLoader<SupportDoc> supportDocsDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private InstanceLoader<TierManagement> tierManagementDl;
	@Autowired
	private CollectionLoader<RtType> rtTypesDl;

	public TierManagement getTierManagement() {
		return tierManagement;
	}

	public void setTierManagement(TierManagement tierManagement) {
		this.tierManagement = tierManagement;
	}

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		rtTypesDl.setParameter("typeGroupCode1","M");
		rtTypesDl.load();
		tierManagementDl.setParameter("id", getTierManagement().getId());
		tierManagementDl.load();
		tierThresholdsDl.setParameter("tierManagement1", getTierManagement());
		tierThresholdsDl.load();
		supportDocsDl.setParameter("tierManagement1", getTierManagement());
		supportDocsDl.load();
	}

	@Subscribe("supportDocsTable.add")
	public void onSupportDocsTableAdd(Action.ActionPerformedEvent event) {

		RtTypeByGroupBrowse rtTypeByGroupBrowse = screenBuilders.screen(this)
				.withOpenMode(OpenMode.DIALOG)
				.withScreenClass(RtTypeByGroupBrowse.class)
				.build();
		rtTypeByGroupBrowse.addAfterCloseListener(afterCloseEvent -> {
			tierManagementDl.setParameter("id", getTierManagement().getId());
			tierManagementDl.load();
			tierThresholdsDl.setParameter("tierManagement1", getTierManagement());
			tierThresholdsDl.load();
			supportDocsDl.setParameter("tierManagement1", getTierManagement());
			supportDocsDl.load();
		});
		rtTypeByGroupBrowse.setTypeCode("P");
		rtTypeByGroupBrowse.setTierManagement(getTierManagement());
		rtTypeByGroupBrowse.setParentDataContext(getScreenData().getDataContext());
		rtTypeByGroupBrowse.show();
	}

	@Subscribe("tierThresholdsTable.add")
	public void onTierThresholdsTableAdd(Action.ActionPerformedEvent event) {
		addDeposit();
		addWithdraw();
		addAssetTransferAuto();
		addAssetTransferApproval();
	}

	private void addDeposit(){
		String queryrtType = "select e from RtType e where e.code =:code";
		RtType rtType = this.dataManager.load(RtType.class)
				.query(queryrtType)
				.parameter("code", "F01")
				.one();

		String queryrtUom = "select e from RtType e where e.code =:code";
		RtType rtUom = this.dataManager.load(RtType.class)
				.query(queryrtUom)
				.parameter("code", "J01")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	private void addWithdraw(){
		String queryrtType = "select e from RtType e where e.code =:code";
		RtType rtType = this.dataManager.load(RtType.class)
				.query(queryrtType)
				.parameter("code", "F02")
				.one();

		String queryrtUom = "select e from RtType e where e.code =:code";
		RtType rtUom = this.dataManager.load(RtType.class)
				.query(queryrtUom)
				.parameter("code", "J01")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	private void addAssetTransferAuto(){
		String queryrtType = "select e from RtType e where e.code =:code";
		RtType rtType = this.dataManager.load(RtType.class)
				.query(queryrtType)
				.parameter("code", "ASSET TRANSFER (AUTO)")
				.one();

		String queryrtUom = "select e from RtType e where e.code =:code";
		RtType rtUom = this.dataManager.load(RtType.class)
				.query(queryrtUom)
				.parameter("code", "J02")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	private void addAssetTransferApproval(){
		String queryrtType = "select e from RtType e where e.code =:code";
		RtType rtType = this.dataManager.load(RtType.class)
				.query(queryrtType)
				.parameter("code", "ASSET TRANSFER (APPROVAL)")
				.one();

		String queryrtUom = "select e from RtUom e where e.code =:code";
		RtType rtUom = this.dataManager.load(RtType.class)
				.query(queryrtUom)
				.parameter("code", "J02")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}
}
