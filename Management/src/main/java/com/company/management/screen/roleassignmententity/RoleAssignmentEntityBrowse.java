package com.company.management.screen.roleassignmententity;

import com.company.management.entity.User;
import com.company.management.screen.resourceroleentity.ResourceRoleEntityBrowse;
import io.jmix.core.DataManager;
import io.jmix.security.role.ResourceRoleRepository;
import io.jmix.securitydata.entity.ResourceRoleEntity;
import io.jmix.securityui.model.ResourceRoleModel;
import io.jmix.securityui.model.RoleModelConverter;
import io.jmix.securityui.screen.rolefilter.RoleFilter;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.Table;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import io.jmix.securitydata.entity.RoleAssignmentEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@UiController("sec_RoleAssignmentEntity.browse")
@UiDescriptor("role-assignment-entity-browse.xml")
@LookupComponent("roleAssignmentEntitiesTable")
public class RoleAssignmentEntityBrowse extends StandardLookup<RoleAssignmentEntity> {
	@Autowired
	private CollectionLoader<RoleAssignmentEntity> roleAssignmentEntitiesDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private DataManager dataManager;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	private User user;


	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		roleAssignmentEntitiesDl.setParameter("username1",getUser().getUsername());
		roleAssignmentEntitiesDl.load();
	}

	@Subscribe("roleAssignmentEntitiesTable.add")
	public void onRoleAssignmentEntitiesTableAdd(Action.ActionPerformedEvent event) {
		ResourceRoleEntityBrowse userEdit = screenBuilders.screen(this)
				.withScreenClass(ResourceRoleEntityBrowse.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		userEdit.setUser(getUser());
		userEdit.show();
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDefaultAction();
	}
}