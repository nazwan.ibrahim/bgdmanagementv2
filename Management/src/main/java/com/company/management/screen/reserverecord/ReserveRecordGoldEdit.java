package com.company.management.screen.reserverecord;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

@UiController("ReserveRecordGold.edit")
@UiDescriptor("reserve-record-gold-edit.xml")
@EditedEntityContainer("reserveRecordDc")
public class ReserveRecordGoldEdit extends StandardEditor<ReserveRecord> {
	@Autowired
	private InstanceLoader<ReserveRecord> reserveRecordDl;
	@Autowired
	private CollectionLoader<ReserveRecordProduct> reserveRecordProductsDl;
	@Autowired
	private InstanceContainer<ReserveRecord> reserveRecordDc;
	@Autowired
	private CollectionLoader<ReserveRecordDoc> reserveRecordDocsDl;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<ReserveRecordProduct> reserveRecordProductsDc;
	private String goldType;
	@Autowired
	private Button commit;
	@Autowired
	private GroupTable<ReserveRecordProduct> reserveRecordProductsTable;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private FileStorageUploadField uploadDoc;
	@Autowired
	private Notifications notifications;
	private BigDecimal physical = BigDecimal.ZERO;
	private BigDecimal tradable = BigDecimal.ZERO;
	@Autowired
	private ManagementController managementController;


	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		reserveRecordDl.load();
		reserveRecordProductsDl.setParameter("reverse_record1", reserveRecordDc.getItem());
		reserveRecordProductsDl.load();
		reserveRecordDocsDl.setParameter("reserve_record1", reserveRecordDc.getItem());
		reserveRecordDocsDl.load();
	}

	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(ReserveRecordDoc reserveRecordDoc) {
		if (reserveRecordDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(reserveRecordDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(reserveRecordDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}


	private void saveUploadDoc(){
		ReserveRecordDoc reserveRecordDoc = this.dataManager.create(ReserveRecordDoc.class);
		reserveRecordDoc.setReserve_record(getEditedEntity());
		reserveRecordDoc.setFileUpload(uploadDoc.getValue());
		reserveRecordDoc.setName(uploadDoc.getFileName());
		this.dataManager.save(reserveRecordDoc);
	}

	private RtType loadType(String code){
		return this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", code)
				.one();
	}

	@Subscribe("reserveRecordProductsTable.add")
	public void onReserveRecordProductsTableAdd(Action.ActionPerformedEvent event) {
		ReserveRecordProduct reserveRecordProduct = this.dataManager.create(ReserveRecordProduct.class);
		reserveRecordProduct.setReverse_record(getEditedEntity());
		if(goldType.equals("goldBar")){
			reserveRecordProduct.setType(loadType("GT01"));
		}else{
			reserveRecordProduct.setType(loadType("GT02"));
		}
		reserveRecordProduct.setPurity(BigDecimal.valueOf(999.9));
		reserveRecordProduct.setQuantity(1);
		reserveRecordProductsDc.getMutableItems().add(reserveRecordProduct);
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Reserve Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveCompleteData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Reserve Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveCompleteData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}



	private void createNewSetData(){
		DeliveryRecordSubsequent deliveryRecordSubsequent = this.dataManager.create(DeliveryRecordSubsequent.class);
		deliveryRecordSubsequent.setReserveRecord(getEditedEntity());
		this.dataManager.save(deliveryRecordSubsequent);

		ShariahRecordSubsequent shariahRecordSubsequent = this.dataManager.create(ShariahRecordSubsequent.class);
		shariahRecordSubsequent.setDeliveryRecord(deliveryRecordSubsequent);
		this.dataManager.save(shariahRecordSubsequent);
	}

	@Install(to = "reserveRecordProductsTable.sub_total", subject = "valueProvider")
	private Object reserveRecordProductsTableSub_totalValueProvider(ReserveRecordProduct reserveRecordProduct) {
		if (reserveRecordProduct.getType().getCode().equals("GT01")) {
			if (reserveRecordProduct.getPrice() != null) {
				BigDecimal price = reserveRecordProduct.getPrice();
				BigDecimal quantity = BigDecimal.valueOf(1000);
				BigDecimal gold = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = gold.multiply(BigDecimal.valueOf(4.25));
				BigDecimal goldHolding = quantity.subtract(goldInGram);
				return price;
			}
			return BigDecimal.ZERO;
		} else {
			BigDecimal price = reserveRecordProduct.getPrice();
			BigDecimal quantity = BigDecimal.valueOf(4.25);
			return price;
		}
	}

	@Install(to = "reserveRecordProductsTable.tradable_gold", subject = "valueProvider")
	private Object reserveRecordProductsTableTradable_goldValueProvider(ReserveRecordProduct reserveRecordProduct) {
		if (reserveRecordProduct.getType().getCode().equals("GT01")) {
			if (reserveRecordProduct.getPrice() != null) {
				BigDecimal quantity = BigDecimal.valueOf(1000);
				BigDecimal gold = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = gold.multiply(BigDecimal.valueOf(4.25));
				return goldInGram;
			}
			return BigDecimal.ZERO;
		} else {
			BigDecimal quantity = BigDecimal.valueOf(4.25);
			return quantity;
		}
	}

	@Install(to = "reserveRecordProductsTable.holding_acct", subject = "valueProvider")
	private Object reserveRecordProductsTableHolding_acctValueProvider(ReserveRecordProduct reserveRecordProduct) {
		if(reserveRecordProduct.getType().getCode().equals("GT01")){
			if(reserveRecordProduct.getPrice() != null){
				BigDecimal quantity = BigDecimal.valueOf(1000);
				BigDecimal gold = quantity.divide(BigDecimal.valueOf(4.25),0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = gold.multiply(BigDecimal.valueOf(4.25));
				BigDecimal goldHolding = quantity.subtract(goldInGram);
				return goldHolding;
			}
			return BigDecimal.ZERO;
		}else{
			BigDecimal quantity = BigDecimal.valueOf(4.25);
			return quantity;
		}
	}

	private void saveCompleteData(){
		if (getEditedEntity().getPo_Num() == null) {
			notifications.create().withCaption("PO Nom Required required").withType(Notifications.NotificationType.ERROR).show();
		}else {
			commitData();
		}
	}

	private void commitData(){
		if (goldType.equals("goldBar")) {
			getEditedEntity().setType(loadType("GT01"));
			String reference = managementController.generateSeqNo("SIB","6");
			getEditedEntity().setReference_Num(reference);
		} else {
			getEditedEntity().setType(loadType("GT02"));
			String reference = managementController.generateSeqNo("SIB","6");
			getEditedEntity().setReference_Num(reference);
		}
		this.dataManager.save(getEditedEntity());
		reserveRecordProductsDc.getItems().forEach(reserveRecordProduct -> {
			if(reserveRecordProduct.getPrice() != null){
				BigDecimal quantity = BigDecimal.valueOf(1000);
				physical = physical.add(quantity);
				BigDecimal goldPcs = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = goldPcs.multiply(BigDecimal.valueOf(4.25));
				BigDecimal goldHolding = quantity.subtract(goldInGram);
				reserveRecordProduct.setHolding_acct(goldHolding);
				reserveRecordProduct.setTradable_gold(goldInGram);
				tradable = tradable.add(goldInGram);
				reserveRecordProduct.setSub_total(reserveRecordProduct.getPrice());
				dataManager.save(reserveRecordProduct);
			}
		});

		getEditedEntity().setAcc_tradable(tradable);
		getEditedEntity().setPhysical_stock(physical);
		this.dataManager.save(getEditedEntity());

		if (uploadDoc.getValue() != null) {
			saveUploadDoc();
		}
		createNewSetData();
		closeWithDiscard();
	}
}