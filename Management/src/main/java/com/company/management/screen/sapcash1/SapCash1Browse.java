package com.company.management.screen.sapcash1;

import io.jmix.ui.screen.*;
import com.company.management.entity.SapCash1;

@UiController("SapCash1.browse")
@UiDescriptor("sap-cash1-browse.xml")
@LookupComponent("sapCash1sTable")
public class SapCash1Browse extends StandardLookup<SapCash1> {
}