package com.company.management.screen.notificationstatus;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationStatus;

@UiController("NotificationStatus.browse")
@UiDescriptor("notification-status-browse.xml")
@LookupComponent("notificationStatusesTable")
public class NotificationStatusBrowse extends StandardLookup<NotificationStatus> {
}