package com.company.management.screen.accounthold;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.AccountHold;

@UiController("AccountHold.edit")
@UiDescriptor("account-hold-edit.xml")
@EditedEntityContainer("accountHoldDc")
public class AccountHoldEdit extends StandardEditor<AccountHold> {
}