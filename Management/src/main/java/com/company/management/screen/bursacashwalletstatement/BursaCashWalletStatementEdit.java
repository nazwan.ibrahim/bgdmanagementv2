package com.company.management.screen.bursacashwalletstatement;

import io.jmix.ui.screen.*;
import com.company.management.entity.BursaCashWalletStatement;

@UiController("BursaCashWalletStatement.edit")
@UiDescriptor("bursa-cash-wallet-statement-edit.xml")
@EditedEntityContainer("bursaCashWalletStatementDc")
public class BursaCashWalletStatementEdit extends StandardEditor<BursaCashWalletStatement> {
}