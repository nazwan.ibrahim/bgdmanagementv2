package com.company.management.screen.totalrevenue;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.dashboardsui.event.DashboardEvent;
import io.jmix.dashboardsui.widget.RefreshableWidget;
import io.jmix.ui.component.Label;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("Totalrevenue")
@UiDescriptor("totalRevenue.xml")
@DashboardWidget(name = "Totalrevenue")
public class Totalrevenue extends ScreenFragment implements RefreshableWidget {
	@Autowired
	private Label revenueAmount;

	@Override
	public void refresh(DashboardEvent dashboardEvent) {

	}

	@Subscribe
	public void onInit(InitEvent event) {

	}


}