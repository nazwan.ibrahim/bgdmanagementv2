package com.company.management.screen.purchaserecord;

import com.company.management.entity.DeliveryRecord;
import com.company.management.entity.DinarStockReorderRule;
import com.company.management.entity.ReserveRecord;
import com.company.management.screen.deliveryrecord.DeliveryRecordGoldEdit;
import com.company.management.screen.dinarstockreorderrule.DinarStockReorderRuleEdit;
import com.company.management.screen.reserverecord.ReserveRecordGoldBarInventoryEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseRecord;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("PurchaseRecordGoldDinarStock.browse")
@UiDescriptor("purchase-record-goldDinar-Stock-browse.xml")
@LookupComponent("purchaseRecordsTable")
public class PurchaseRecordGoldDinarStockBrowse extends StandardLookup<PurchaseRecord> {
	@Autowired
	private CollectionLoader<PurchaseRecord> purchaseRecordsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<PurchaseRecord> reserveRecordsTable;
	@Autowired
	private GroupTable<PurchaseRecord> purchaseRecordsTable;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<PurchaseRecord> purchaseRecordsDl1;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		purchaseRecordsDl.setParameter("typeCode1","GT02");
		purchaseRecordsDl.setParameter("issuanceTypeCode1","ITF/01");
		purchaseRecordsDl.load();
		purchaseRecordsDl1.setParameter("typeCode1","GT02");
		purchaseRecordsDl1.setParameter("issuanceTypeCode1","ITF/02");
		purchaseRecordsDl1.load();
	}

	private void goToEditPage(ReserveRecord reserveRecord){
		ReserveRecordGoldBarInventoryEdit reserveRecordGoldBarInventoryEdit = this.screenBuilders.screen(this)
				.withScreenClass(ReserveRecordGoldBarInventoryEdit.class)
				.build();
		reserveRecordGoldBarInventoryEdit.setEntityToEdit(reserveRecord);
		reserveRecordGoldBarInventoryEdit.show();
	}

	private void goToEditPagePurchase(DeliveryRecord deliveryRecord){
		DeliveryRecordGoldEdit deliveryRecordGoldEdit = screenBuilders.screen(this)
				.withScreenClass(DeliveryRecordGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		assert deliveryRecord != null;
		deliveryRecordGoldEdit.setReadOnly(true);
		deliveryRecordGoldEdit.setEntityToEdit(deliveryRecord);
		deliveryRecordGoldEdit.setDisableElement("disableAll");
		deliveryRecordGoldEdit.show();
	}

	@Subscribe("reserveRecordsTable.edit")
	public void onReserveRecordsTableEdit(Action.ActionPerformedEvent event) {
		String query = "select d from DeliveryRecord d " +
				"where d.purchaseRecord = :purchaseRecord1";
		DeliveryRecord deliveryRecord = this.dataManager.load(DeliveryRecord.class)
				.query(query)
				.parameter("purchaseRecord1", purchaseRecordsTable.getSingleSelected())
				.one();
		goToEditPagePurchase(deliveryRecord);
	}

	@Subscribe("purchaseRecordsTable.edit")
	public void onPurchaseRecordsTableEdit(Action.ActionPerformedEvent event) {
		String query = "select d from DeliveryRecord d " +
				"where d.purchaseRecord = :purchaseRecord1";
		DeliveryRecord deliveryRecord = this.dataManager.load(DeliveryRecord.class)
				.query(query)
				.parameter("purchaseRecord1", purchaseRecordsTable.getSingleSelected())
				.one();
		goToEditPagePurchase(deliveryRecord);
	}

	@Subscribe("goldDinar")
	public void onGoldDinarClick(Button.ClickEvent event) {
		String query = "select d from DinarStockReorderRule d " +
				"where d.code = :code1";
		DinarStockReorderRule dinarStockReorderRule = this.dataManager.load(DinarStockReorderRule.class)
				.query(query)
				.parameter("code1", "GD1")
				.one();

		DinarStockReorderRuleEdit deliveryRecord = screenBuilders.screen(this)
				.withScreenClass(DinarStockReorderRuleEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.withOpenMode(OpenMode.DIALOG)
				.build();

		deliveryRecord.setEntityToEdit(dinarStockReorderRule);
		deliveryRecord.show();
	}
}