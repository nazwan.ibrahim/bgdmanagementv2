package com.company.management.screen.transactionhistoriesviewadmin;

import com.company.management.entity.PurchaseHistoriesView;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionHistoriesViewAdmin;
import io.jmix.ui.screen.LookupComponent;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
@UiController("TransactionHistoriesViewAdminB2B.browse")
@UiDescriptor("transaction-histories-view-admin-B2B-browse.xml")
@LookupComponent("transactionHistoriesViewAdminsTable")
public class TransactionHistoriesViewAdminB2BBrowse extends StandardLookup<TransactionHistoriesViewAdmin> {
	@Autowired
	private TabSheet freeTab;
	@Autowired
	private ScreenBuilders screenBuilders;

	private BigDecimal purchasePrice;
	private BigDecimal stockGram;
	private BigDecimal stockPrice;
	@Autowired
	private CollectionLoader<PurchaseHistoriesView> purchaseHistoriesViewsDl;


	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(freeTab.getSelectedTab().getName().equals("freeAccount")) {
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F06");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
			purchaseHistoriesViewsDl.setFirstResult(20);
		}else{
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F05");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
		}
	}

	@Subscribe("freeTab")
	public void onFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("freeAccount")){
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F06");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
			purchaseHistoriesViewsDl.setFirstResult(20);
		}else {
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F05");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
		}

	}
	private void goToEditPage(TransactionHistoriesViewAdmin transactionHistoriesViewAdmin){
		TransactionHistoriesViewAdminB2BEdit transactionHistoriesViewAdminB2BEdit = screenBuilders.screen(this)
				.withScreenClass(TransactionHistoriesViewAdminB2BEdit.class)
				.build();
		assert transactionHistoriesViewAdmin != null;
		transactionHistoriesViewAdminB2BEdit.setEntityToEdit(transactionHistoriesViewAdmin);
		transactionHistoriesViewAdminB2BEdit.setTransactionHistoriesViewAdmin(transactionHistoriesViewAdmin);
		transactionHistoriesViewAdminB2BEdit.setPurchasePrice(purchasePrice);
		transactionHistoriesViewAdminB2BEdit.setStockGram(stockGram);
		transactionHistoriesViewAdminB2BEdit.setStockPrice(stockPrice);
		transactionHistoriesViewAdminB2BEdit.show();
	}
}