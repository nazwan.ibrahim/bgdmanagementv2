package com.company.management.screen.transactiontype;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionType;

@UiController("TransactionType.edit")
@UiDescriptor("transaction-type-edit.xml")
@EditedEntityContainer("transactionTypeDc")
public class TransactionTypeEdit extends StandardEditor<TransactionType> {
}