package com.company.management.screen.transferinfo;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransferInfo;

@UiController("TransferInfo.edit")
@UiDescriptor("transfer-info-edit.xml")
@EditedEntityContainer("transferInfoDc")
public class TransferInfoEdit extends StandardEditor<TransferInfo> {
}