package com.company.management.screen.productsupplier;

import io.jmix.ui.screen.*;
import com.company.management.entity.ProductSupplier;

@UiController("ProductSupplier.browse")
@UiDescriptor("product-supplier-browse.xml")
@LookupComponent("table")
public class ProductSupplierBrowse extends MasterDetailScreen<ProductSupplier> {
}