package com.company.management.screen.purchasehistoriesview;

import com.company.management.entity.TransactionHistoriesView;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Label;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseHistoriesView;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DecimalFormat;

@UiController("PurchaseHistoriesB2BView.edit")
@UiDescriptor("purchase-histories-B2b-view-edit.xml")
@EditedEntityContainer("purchaseHistoriesViewDc")
public class PurchaseHistoriesB2BViewEdit extends Screen {
	@Autowired
	private InstanceContainer<PurchaseHistoriesView> purchaseHistoriesViewDc;
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
	@Autowired
	private Label labelTable;
	@Autowired
	private GroupTable<TransactionHistoriesView> purchaseRecordProductsTable;

	public PurchaseHistoriesView getPurchaseHistoriesView() {
		return purchaseHistoriesView;
	}

	public void setPurchaseHistoriesView(PurchaseHistoriesView purchaseHistoriesView) {
		this.purchaseHistoriesView = purchaseHistoriesView;
	}
	public PurchaseHistoriesView purchaseHistoriesView;

	public String getOwnerType() {
		return ownerType;
	}

	public void setOwnerType(String ownerType) {
		this.ownerType = ownerType;
	}

	private String ownerType;

	DecimalFormat df = new DecimalFormat("#,###,##0.000000");

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		purchaseHistoriesViewDc.setItem(purchaseHistoriesView);
		transactionHistoriesViewsDl.setParameter("transactionCode1","F09");
		transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
		transactionHistoriesViewsDl.setParameter("reference1",purchaseHistoriesView.getReference());
		transactionHistoriesViewsDl.load();

	}

	@Subscribe
	public void onAfterShow1(AfterShowEvent event) {
		if(getOwnerType().equals("Supplier")){
			labelTable.setEnabled(false);
			labelTable.setVisible(false);
			purchaseRecordProductsTable.setEnabled(false);
			purchaseRecordProductsTable.setVisible(false);
		}
	}



	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDefaultAction();
	}

	@Install(to = "goldAmount", subject = "formatter")
	private String goldAmountFormatter(BigDecimal value) {
		return df.format(value);
	}


}