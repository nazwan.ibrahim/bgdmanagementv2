package com.company.management.screen.tiermanagement;

import com.company.management.entity.RtType;
import com.company.management.entity.TierThreshold;
import io.jmix.core.DataManager;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.TierManagement;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("TierManagementCreate.edit")
@UiDescriptor("tier-management-create.xml")
@EditedEntityContainer("tierManagementCreateDc")
public class TierManagementCreate extends StandardEditor<TierManagement> {

	private TierManagement tierManagement;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionLoader<TierThreshold> tierThresholdsCreateDl;

	public TierManagement getTierManagement() {
		return tierManagement;
	}

	public void setTierManagement(TierManagement tierManagement) {
		this.tierManagement = tierManagement;
	}


	private void addDeposit(){
		RtType rtType = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "F01")
				.one();

		RtType rtUom = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "J01")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	@Subscribe
	public void onInit(InitEvent event) {
		addDeposit();
		addWithdraw();
		addAssetTransferAuto();
		addAssetTransferApproval();
		tierThresholdsCreateDl.setParameter("tierManagement1", getTierManagement().getId());
		tierThresholdsCreateDl.load();
	}

	private void addWithdraw(){
		RtType rtType = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "F02")
				.one();

		RtType rtUom = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "J01")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	private void addAssetTransferAuto(){
		RtType rtType = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "ASSET TRANSFER (AUTO)")
				.one();

		RtType rtUom = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "J02")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}

	private void addAssetTransferApproval(){
		RtType rtType = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "ASSET TRANSFER (APPROVAL)")
				.one();

		RtType rtUom = this.dataManager.load(RtType.class)
				.query("select e from RtType e where e.code =:code")
				.parameter("code", "J02")
				.one();

		TierThreshold tierThreshold = this.dataManager.create(TierThreshold.class);
		tierThreshold.setTierManagement(getTierManagement());
		tierThreshold.setType(rtType);
		tierThreshold.setUom(rtUom);
		dataManager.save(tierThreshold);
	}
}