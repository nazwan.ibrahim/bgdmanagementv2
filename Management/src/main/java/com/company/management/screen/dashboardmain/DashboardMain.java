package com.company.management.screen.dashboardmain;

import com.company.management.entity.DinarStockReorderRule;
import com.company.management.entity.ProductPrice;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.Label;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;


@UiController("DashboardMain")
@UiDescriptor("dashboard-main.xml")
public class DashboardMain extends Screen {


    private static final Logger log = org.slf4j.LoggerFactory.getLogger(DashboardMain.class);
    @Autowired
    private DataManager dataManager;
    private BigDecimal tradableGoldBarGram;
    private BigDecimal tradableGoldDinarGram;
    private BigDecimal tradableGoldDinarUnit = BigDecimal.valueOf(0);

    private BigDecimal tradableGoldBarUnit= BigDecimal.valueOf(0);


    //////////////////// Label for cart1 //////////////////////////

    @Autowired
    private Label<String> tradableGoldDinarUnitLabel;
    @Autowired
    private Label<String> tradableGoldDinarGramLabel;
    @Autowired
    private Label<String> tradableGoldBarUnitLabel;
    @Autowired
    private Label<String> tradableGoldBarGramLabel;
    @Autowired
    private Label<String> totalBGDIssuanceLabel;
    ///////////////////////////////////////////////////////////////
    //////////////////// Label for cart1 //////////////////////////


    @Autowired
    private Label<String> supplierGoldWalletLabel;
    @Autowired
    private Label<String> supplierCashWalletLabel;
    @Autowired
    private Label<String> bursaGoldWalletLabel;
    @Autowired
    private Label<String> bursaCashWalletLabel;
    ///////////////////////////////////////////////////////////////
    //////////////////// Label for cart4 //////////////////////////
    @Autowired
    private Label<String> totalGoldDinarLabel;
    @Autowired
    private Label<String> totalGoldBarLabel;
    @Autowired
    private Label<String> redeemedGoldDinarLabel;
    @Autowired
    private Label<String> holdingAccountLabel;
    @Autowired
    private Label<String> goldDinarLabel;
    @Autowired
    private Label<String> goldBarLabel;
    ///////////////////////////////////////////////////////////////
    ///////////////////// Label for cart5 ////////////////////////
    @Autowired
    private Label<String> bursaSellPriceLabel;
    @Autowired
    private Label<String> bursaBuyPriceLabel;
    @Autowired
    private Label<String> supplierBuyPriceLabel;
    @Autowired
    private Label<String> supplierSellPriceLabel;
    @Autowired
    private Label<String> formattedPriceDateLabel;
/////////////////////////////////////////////////////////
    ///////////////////// Label for cart6 //////////////////

    @Autowired
    private Label<Integer> pendingTier3Label;
    @Autowired
    private Label<Integer> pendingTier2Label;
    @Autowired
    private Label<Integer> pendingTier1Label;
    @Autowired
    private Label<Integer> totalInvestorLabel;
    @Autowired
    private Label<String> newInvestorLabel;
///////////////////////////////////////////////////////////
    /////////////////////// Label for cart 7 //////////////////


    @Autowired
    private Label<String> totalBuySellToInvestorLabel;
    @Autowired
    private Label<String> totalBuySellToAceLabel;
    @Autowired
    private Label<String> totalSellToInvestorLabel;
    @Autowired
    private Label<String> totalSellToAceLabel;
    @Autowired
    private Label<String> totalBuyFromInvestorLabel;
    @Autowired
    private Label<String> totalBuyFromAceLabel;



    ///////////////////////////////////////////////////////////
    ///////////////////////////  Label for cart 8 //////////////
    @Autowired
    private Label<Integer> totalSupplierlabel;
    @Autowired
    private Label<Integer> totalAgentsLabel;
    @Autowired
    private Label<Integer> totalActiveSupplierAgentLabel;
    @Autowired
    private Label<Integer> totalBgdAdminlabel;
    @Autowired
    private Label<Integer> totalActiveBgdAdminLabel;
    @Autowired
    private Notifications notifications;
///////////////////////////////////////////////////////////

    @Subscribe
    public void onBeforeShow(BeforeShowEvent event) {

        log.info("\n//////  INFO CART 1 : ////");
        cart1();
        log.info("\n//////  INFO CART 2 : ////");
        cart2();
        log.info("\n//////  INFO CART 3 : ////");
        cart3();
        log.info("\n//////  INFO CART 4 : ////");
        cart4();
        log.info("\n//////  INFO CART 5 : ////");
        cart5();
        log.info("\n//////  INFO CART 6 : ////");
        cart6();
        log.info("\n//////  INFO CART 7 : ////");
        cart7();
        log.info("\n//////  INFO CART 8 : ////");
        cart8();


    }

    @Subscribe
    public void onInit(InitEvent event) {
        String queryDinarStockReorderRule = "select d from DinarStockReorderRule d where d.code = :code1";
        Optional<DinarStockReorderRule> dinarStockReorderRule = this.dataManager.load(DinarStockReorderRule.class)
                .query(queryDinarStockReorderRule)
                .parameter("code1", "GD1")
                .optional();

        if (dinarStockReorderRule.isPresent()) { // check null cases , exception for null cases
            BigDecimal result = countTotalGoldDinar();
            log.info("DINAR STOCK REORDERING RULE: TOTAL GOLD DINAR: " + result);
            BigDecimal divide = result.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.DOWN);
            BigDecimal totalTradable = divide.setScale(0);

            if (dinarStockReorderRule.get().getMinQuantity() > totalTradable.intValue()) {
                //            notifications.create(Notifications.NotificationType.SYSTEM)
                //                    .withType(Notifications.NotificationType.SYSTEM)
                //                    .withCaption("Gold Dinar have reach maximum quantity")
                //                    .withDescription("Current Gold Wallet : "+ dinarStockReorderRule.getMaxQuantity()+"pcs")
                //                    .show();
                notifications.create(Notifications.NotificationType.SYSTEM)
                        .withType(Notifications.NotificationType.SYSTEM)
                        .withCaption("Gold Dinar have reach minimum quantity")
                        .withDescription("Current Gold Wallet : " + totalTradable + " pcs")
                        .show();
            }
        }
    }
    public void cart1(){
        tradableGoldBarGram= dataManager.loadValue(
                        "select COALESCE(SUM(e.tradable_gold), 0) from PurchaseRecordProduct e " +
                                "where e.type.code = :code and e.purchase_record.deletedDate is null", BigDecimal.class)
                .parameter("code","GT01")
                .one();
        tradableGoldDinarGram= dataManager.loadValue(
                        "select COALESCE(SUM(e.tradable_gold), 0) from PurchaseRecordProduct e " +
                                "where e.type.code = :code and e.purchase_record.deletedDate is null", BigDecimal.class)
                .parameter("code","GT02")
                .one();


        if(!tradableGoldBarGram.equals(BigDecimal.valueOf(0))) {
            tradableGoldBarUnit = tradableGoldBarGram.divide(BigDecimal.valueOf(4.25), RoundingMode.HALF_UP);
        }
        if(!tradableGoldDinarGram.equals(BigDecimal.valueOf(0))) {
            tradableGoldDinarUnit = tradableGoldDinarGram.divide(BigDecimal.valueOf(4.25), RoundingMode.HALF_UP);
        }

        BigDecimal totalBGDIssuance= tradableGoldBarGram.add(tradableGoldDinarGram);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.000000");
        DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");

        tradableGoldBarGramLabel.setValue(decimalFormat.format(tradableGoldBarGram));
        tradableGoldDinarGramLabel.setValue(decimalFormat.format(tradableGoldDinarGram));
        tradableGoldBarUnitLabel.setValue(decimalFormat2.format(tradableGoldBarUnit));
        tradableGoldDinarUnitLabel.setValue(decimalFormat2.format(tradableGoldDinarUnit));
        totalBGDIssuanceLabel.setValue(decimalFormat.format(totalBGDIssuance));


        log.info("TOTAL TRADABLE Bar gram: " + tradableGoldBarGram.toString());
        log.info("TOTAL TRADABLE Dinar gram: " + tradableGoldDinarGram.toString());
        log.info("TOTAL TRADABLE Bar unit: " + tradableGoldBarUnit.toString());
        log.info("TOTAL TRADABLE Dinar unit: " + tradableGoldDinarUnit.toString());
        log.info("TOTAL TBGD ISSUANCE gram: " + totalBGDIssuance);

    }



    public void cart2(){
        String querybursaCashWallet = "select COALESCE(sum(a.availableAmount),0) from AccountWalletView a where a.accountOwner=:username and a.code=:typeCode";
        BigDecimal bursaCashWallet = dataManager.loadValue(querybursaCashWallet,BigDecimal.class)
                .parameter("username","BgdAdmin")
                .parameter("typeCode","E01")
                .store("wallet")
                .one();
        log.info("Bursa available cash amount :" + bursaCashWallet);

        String querybursaGoldWallet = "select COALESCE(sum(a.availableAmount),0) from AccountWalletView a where a.accountOwner=:username and a.code=:typeCode";
        BigDecimal bursaGoldWallet = dataManager.loadValue(querybursaGoldWallet,BigDecimal.class)
                .parameter("username","BgdAdmin")
                .parameter("typeCode","E02")
                .store("wallet")
                .one();
        log.info("Bursa available cash amount :" + bursaGoldWallet);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.000000");
        DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");
        bursaCashWalletLabel.setValue(decimalFormat2.format(bursaCashWallet));
        bursaGoldWalletLabel.setValue(decimalFormat.format(bursaGoldWallet));
    }


    public void cart3(){

        String querysupplierCashWallet = "select COALESCE(sum(a.availableAmount),0) from AccountWalletView a where a.accountOwner=:username and a.code=:typeCode";
        BigDecimal supplierCashWallet = dataManager.loadValue(querysupplierCashWallet,BigDecimal.class)
                .parameter("username","AceAdmin")
                .parameter("typeCode","E01")
                .store("wallet")
                .one();
        log.info("Supplier available cash amount :" + supplierCashWallet);

        String querysupplierGoldWallet = "select COALESCE(sum(a.availableAmount),0) from AccountWalletView a where a.accountOwner=:username and a.code=:typeCode";
        BigDecimal supplierGoldWallet = dataManager.loadValue(querysupplierGoldWallet,BigDecimal.class)
                .parameter("username","AceAdmin")
                .parameter("typeCode","E02")
                .store("wallet")
                .one();
        log.info("Supplier available cash amount :" + supplierGoldWallet);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.000000");
        DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");
        supplierCashWalletLabel.setValue(decimalFormat2.format(supplierCashWallet));
        supplierGoldWalletLabel.setValue(decimalFormat.format(supplierGoldWallet));
    }




    public void cart4() {
        String querytotalGoldBar = "select COALESCE(SUM(e.quantity), 0) from PurchaseRecordProduct e where e.type.code = :code";
        BigDecimal totalGoldBar = dataManager.loadValue(querytotalGoldBar, BigDecimal.class)
                .parameter("code", "GT01")
                .one().multiply(BigDecimal.valueOf(1000));



        BigDecimal goldBar=tradableGoldBarGram;

        String queryholdingAccount = "select COALESCE(SUM(e.holding_acct), 0) from PurchaseRecordProduct e where e.type.code = :code";
        BigDecimal holdingAccount = dataManager.loadValue(queryholdingAccount, BigDecimal.class)
                .parameter("code", "GT01")
                .one();
        log.info("TOTAL Gold bar : " + totalGoldBar);
        log.info("Gold bar : " + goldBar);
        log.info("Holding Account : " + holdingAccount);

        String querytotalGoldDinar = "select COALESCE(SUM(e.quantity), 0) from PurchaseRecordProduct e where e.type.code = :code";
        BigDecimal totalGoldDinar = dataManager.loadValue(querytotalGoldDinar, BigDecimal.class)
                .parameter("code", "GT02")
                .one().multiply(BigDecimal.valueOf(4.25));

        BigDecimal goldDinar= tradableGoldDinarUnit;

//        String queryredeemedGoldDinar = "select COALESCE(SUM(e.holding_acct), 0) from PurchaseRecordProduct e where e.type.code = :code";
//        BigDecimal redeemedGoldDinar = dataManager.loadValue(queryredeemedGoldDinar, BigDecimal.class)
//                .parameter("code", "GT02")
//                .one();

//        String queryRedeem - "select SUM(e."
        String query = "select COALESCE(SUM(e.unit), 0) from RedeemInfo e";
        BigDecimal redeemInfo = dataManager.loadValue(query,BigDecimal.class)
                .store("wallet")
                .one();

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.000000");

        DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.00");

        totalGoldBarLabel.setValue(decimalFormat.format(totalGoldBar));
        goldBarLabel.setValue(decimalFormat.format(goldBar));
        holdingAccountLabel.setValue(decimalFormat.format(holdingAccount));

        totalGoldDinarLabel.setValue(decimalFormat2.format(totalGoldDinar));
        goldDinarLabel.setValue(decimalFormat2.format(goldDinar.subtract(redeemInfo)));
        redeemedGoldDinarLabel.setValue(decimalFormat2.format(redeemInfo));


        log.info("TOTAL Gold dinar : " + totalGoldDinar);
        log.info("Gold Dinar : " + goldDinar);
        log.info("Redeemed Gold Dinar : " + redeemInfo);
    }


    public void cart5(){
        String querybursaPrice = "select p from ProductPrice p where p.type.code = :typeCode";
        ProductPrice bursaPrice = dataManager.load(ProductPrice.class)
                .query(querybursaPrice)
                .parameter("typeCode", "A02")
                .one();

        String querysupplierPrice = "select p from ProductPrice p where p.type.code = :typeCode";
        ProductPrice supplierPrice = dataManager.load(ProductPrice.class)
                .query(querysupplierPrice)
                .parameter("typeCode", "A01")
                .one();

        BigDecimal bursaBuyPrice= bursaPrice.getBuyPrice().setScale(2,RoundingMode.HALF_UP);
        BigDecimal bursaSellPrice= bursaPrice.getSellPrice().setScale(2,RoundingMode.HALF_UP);

        BigDecimal supplierBuyPrice = supplierPrice.getBuyPrice().setScale(2,RoundingMode.HALF_UP);
        BigDecimal supplierSellPrice = supplierPrice.getSellPrice().setScale(2,RoundingMode.HALF_UP);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");

        Date priceDate = bursaPrice.getLastModifiedDate();

        log.info("TIMEE : " + priceDate.toString());



        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy - h:mm a");
        String formattedPriceDate = formatter.format(priceDate);
        log.info("TIMEE formated : " + formattedPriceDate);

        formattedPriceDateLabel.setValue(formattedPriceDate);


        bursaBuyPriceLabel.setValue("RM"+decimalFormat.format(bursaBuyPrice)+"/g");
        bursaSellPriceLabel.setValue("RM"+decimalFormat.format(bursaSellPrice)+"/g");
        supplierBuyPriceLabel.setValue("RM"+decimalFormat.format(supplierBuyPrice)+"/g");
        supplierSellPriceLabel.setValue("RM"+decimalFormat.format(supplierSellPrice)+"/g");

        log.info("Bursa Buy Price : " + bursaBuyPrice);
        log.info("Bursa Sell Price : " + bursaSellPrice);
        log.info("Supplier Buy Price : " + supplierBuyPrice);
        log.info("Supplier Sell Price : " + supplierSellPrice);

    }

    public void cart6(){

        String querytotalInvestor = "select COALESCE(count(e),0) from UserProfile e where e.userType.code =:uCode and e.actStatus.code=:actCode";
        Integer totalInvestor = dataManager.loadValue(querytotalInvestor,Integer.class)
                .parameter("uCode","K01")   //individual
                .parameter("actCode","G04") //active
                .store("registration")
                .one();
        log.info("Total Investor : " + totalInvestor);

        String querynewInvestor = "select COALESCE(count(u),0) from UserProfile u where @between(u.createdDate,now-1,now,day) and u.userType.code =:uCode and u.actStatus.code=:actCode";
        Integer newInvestor = dataManager.loadValue(querynewInvestor,Integer.class)
                .parameter("uCode","K01")   //individual
                .parameter("actCode","G04") //active
                .store("registration")
                .one();

        log.info("Total New Investor : " + newInvestor);

        totalInvestorLabel.setValue(totalInvestor);
        newInvestorLabel.setValue("+" +newInvestor+" New Investors");




        String querypendingTier1 = "select COALESCE(COUNT(e),0) from UserTier e where e.kycStatus.code=:sCode and e.tier.code=:tCode";
        Integer pendingTier1 = dataManager.loadValue(querypendingTier1,Integer.class)
                .parameter("sCode","G12")
                .parameter("tCode","T01")
                .store("registration").one();

        String querypendingTier2 = "select COALESCE(COUNT(e),0) from UserTier e where e.kycStatus.code=:sCode and e.tier.code=:tCode";
        Integer pendingTier2 = dataManager.loadValue(querypendingTier2,Integer.class)
                .parameter("sCode","G12")
                .parameter("tCode","T02")
                .store("registration").one();
        String querypendingTier3 = "select COALESCE(COUNT(e),0) from UserTier e where e.kycStatus.code=:sCode and e.tier.code=:tCode";
        Integer pendingTier3 = dataManager.loadValue(querypendingTier3,Integer.class)
                .parameter("sCode","G12")
                .parameter("tCode","T03")
                .store("registration").one();

        pendingTier1Label.setValue(pendingTier1);
        pendingTier2Label.setValue(pendingTier2);
        pendingTier3Label.setValue(pendingTier3);

        log.info("Pending Tier 1 : " + pendingTier1);
        log.info("Pending Tier 2 : " + pendingTier2);
        log.info("Pending Tier 3 : " + pendingTier3);
    }

    public void cart7(){
        String querytotalBuyFromAce = "select COALESCE(sum(e.amount),0) from TransactionHistoriesView e where e.status=:status and e.walletCode = :wCode and e.transactionCode= :txCode and e.accOwner=:user";
        BigDecimal totalBuyFromAce = dataManager.loadValue(querytotalBuyFromAce, BigDecimal.class)
                .parameter("status","Completed")
                .parameter("wCode","E02")
                .parameter("txCode","F06") //choose opposite
                .parameter("user","AceAdmin")
                .store("wallet")
                .one()
                .abs();
        log.info("Total Bursa Buy From Ace : " + totalBuyFromAce);


        String querytotalSellToAce = "select COALESCE(sum(e.amount),0) from TransactionHistoriesView e where " +
                "e.status=:status and " +
                "e.walletCode = :wCode and " +
                "e.transactionCode= :txCode and " +
                "e.accOwner=:user ";
        BigDecimal totalSellToAce = dataManager.loadValue(querytotalSellToAce, BigDecimal.class)
                .parameter("status","Completed")
                .parameter("wCode","E02")
                .parameter("txCode","F05") //choose opposite
                .parameter("user", "AceAdmin")
                .store("wallet")
                .one()
                .abs();
        log.info("Total Bursa Sell To Ace : " + totalSellToAce);

        String querytotalBuyFromInvestor = "select COALESCE(sum(e.amount),0) from TransactionHistoriesView e where " +
                "e.status=:status and " +
                "e.walletCode = :wCode and " +
                "e.transactionCode= :txCode and " +
                "e.accOwner not in (:user) ";
        BigDecimal totalBuyFromInvestor = dataManager.loadValue(querytotalBuyFromInvestor, BigDecimal.class)
                .parameter("status","Completed")
                .parameter("wCode","E02")
                .parameter("txCode","F06")  //choose opposite
                .parameter("user", Arrays.asList("AceAdmin", "BGDAdmin").toString())
                .store("wallet")
                .one()
                .abs();
        log.info("Total Bursa Buy From Investor : " + totalBuyFromInvestor);

        String querytotalSellToInvestor = "select COALESCE(sum(e.amount),0) from TransactionHistoriesView e where " +
                "e.status=:status and " +
                "e.walletCode = :wCode and " +
                "e.transactionCode= :txCode and " +
                "e.accOwner not in (:user) ";
        BigDecimal totalSellToInvestor = dataManager.loadValue(querytotalSellToInvestor, BigDecimal.class)
                .parameter("status","Completed")
                .parameter("wCode","E02")
                .parameter("txCode","F05")  //choose opposite
                .parameter("user",Arrays.asList("AceAdmin", "BGDAdmin").toString())
                .store("wallet")
                .one()
                .abs();
        log.info("Total Bursa Sell To Investor : " + totalSellToInvestor);

        BigDecimal totalBuySellToInvestor = totalSellToInvestor.add(totalBuyFromInvestor);
        BigDecimal totalBuySellToAce = totalSellToAce.add(totalBuyFromAce);

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.000000");

        totalBuySellToAceLabel.setValue(decimalFormat.format(totalBuySellToAce));
        totalBuySellToInvestorLabel.setValue(decimalFormat.format(totalBuySellToInvestor));

        totalBuyFromInvestorLabel.setValue(decimalFormat.format(totalBuyFromInvestor));
        totalSellToInvestorLabel.setValue(decimalFormat.format(totalSellToInvestor));

        totalBuyFromAceLabel.setValue(decimalFormat.format(totalBuyFromAce));
        totalSellToAceLabel.setValue(decimalFormat.format(totalSellToAce));

        log.info("Total Bursa Buy & Sell To Ace : " + totalBuySellToAce);
        log.info("Total Bursa Buy & Sell To Investor : " + totalBuySellToInvestor);
    }

    public void cart8(){
        String querytotalSupplier = "select COALESCE(count(distinct u.username),0) from User u  " +
                " where u.partner = true  ";
        Integer totalSupplier = dataManager.loadValue(querytotalSupplier, Integer.class)
                .one();
        log.info("Total Supplier: " + totalSupplier);

        Integer totalAgents=0;
        Integer totalActiveAgent=0;
        log.info("Total Agents: " + totalAgents);

        String querytotalActiveSupplier = "select COALESCE(count(distinct u.username),0) from User u  " +
                " where u.partner = true and u.active=true  ";
        Integer totalActiveSupplier = dataManager.loadValue(querytotalActiveSupplier, Integer.class)
                .one();

        Integer totalActiveSupplierAgent = totalActiveSupplier+totalActiveAgent;
        log.info("Total Active Supplier & Agent : " + totalActiveSupplierAgent);


        totalSupplierlabel.setValue(totalSupplier);
        totalAgentsLabel.setValue(totalAgents);
        totalActiveSupplierAgentLabel.setValue(totalActiveSupplierAgent);

        String querytotalBgdAdmin = "select COALESCE(count(distinct u.username),0) from User u  " +
                " where u.partner = false or u.partner is null  ";
        Integer totalBgdAdmin = dataManager.loadValue(querytotalBgdAdmin, Integer.class)
                .one();
        log.info("Total BgdAdmin: " + totalBgdAdmin);

        String querytotalActiveBgdAdmin = "select COALESCE(count(distinct u.username),0) from User u  " +
                "where (u.partner = false or u.partner is null) and u.active=true ";
        Integer totalActiveBgdAdmin = dataManager.loadValue(querytotalActiveBgdAdmin, Integer.class)
                .one();
        log.info("Total Active BGD Admin : " + totalActiveBgdAdmin);

        totalBgdAdminlabel.setValue(totalBgdAdmin);
        totalActiveBgdAdminLabel.setValue(totalActiveBgdAdmin);
    }

    private BigDecimal countTotalGoldDinar(){
        String query = "select distinct SUM(e.amount) from TransactionAcct e where e.account.accOwner = :accountAccOwner1 " +
                "and e.account.walletType.code = :accountWalletTypeCode1";
        return this.dataManager.loadValue(query, BigDecimal.class)
                .store("wallet")
                .parameter("accountAccOwner1","BgdAdmin")
                .parameter("accountWalletTypeCode1","E02")
                .one();
    }





}