package com.company.management.screen.reserverecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;

@UiController("ReserveRecordHoldingAccount.edit")
@UiDescriptor("reserve-record-holding-account-edit.xml")
@EditedEntityContainer("reserveRecordDc")
public class ReserveRecordHoldingAccountEdit extends StandardEditor<ReserveRecord> {
}