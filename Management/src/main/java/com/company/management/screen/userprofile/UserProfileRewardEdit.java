package com.company.management.screen.userprofile;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.UserProfile;

@UiController("UserProfileReward.edit")
@UiDescriptor("user-profile-reward-edit.xml")
@EditedEntityContainer("userProfileDc")
public class UserProfileRewardEdit extends StandardEditor<UserProfile> {
}