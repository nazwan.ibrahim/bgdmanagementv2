package com.company.management.screen.deliveryrecordsubsequent;

import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecordSubsequent;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("DeliveryRecordSubsequentGold.browse")
@UiDescriptor("delivery-record-subsequent-gold-browse.xml")
@LookupComponent("deliveryRecordSubsequentsTable")
public class DeliveryRecordSubsequentGoldBrowse extends StandardLookup<DeliveryRecordSubsequent> {
	@Autowired
	private TabSheet goldTab;
	@Autowired
	private CollectionLoader<DeliveryRecordSubsequent> deliveryRecordSubsequentsDl;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<DeliveryRecordSubsequent> deliveryRecordSubsequentsTable;
	@Autowired
	private GroupTable<DeliveryRecordSubsequent> deliveryRecordSubsequentsTableDinar;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(goldTab.getTab("goldBar").getName().equals("goldBar")){
			deliveryRecordSubsequentsDl.setParameter("reserveRecordTypeCode1","GT01");
			deliveryRecordSubsequentsDl.load();
		}else {
			deliveryRecordSubsequentsDl.setParameter("reserveRecordTypeCode1","GT02");
			deliveryRecordSubsequentsDl.load();
		}
	}

	private void goToEditPage(DeliveryRecordSubsequent deliveryRecordSubsequent){
		DeliveryRecordSubsequentGoldEdit deliveryRecordSubsequentGoldEdit = screenBuilders.screen(this)
				.withScreenClass(DeliveryRecordSubsequentGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
					deliveryRecordSubsequentsDl.load();
				})
				.build();
		assert deliveryRecordSubsequent != null;
		deliveryRecordSubsequentGoldEdit.setEntityToEdit(deliveryRecordSubsequent);
		deliveryRecordSubsequentGoldEdit.show();
	}

	@Subscribe("goldTab")
	public void onGoldTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldBar")){
			deliveryRecordSubsequentsDl.setParameter("reserveRecordTypeCode1","GT01");
			deliveryRecordSubsequentsDl.load();
		}else {
			deliveryRecordSubsequentsDl.setParameter("reserveRecordTypeCode1","GT02");
			deliveryRecordSubsequentsDl.load();
		}
	}

	@Subscribe("deliveryRecordSubsequentsTable.edit")
	public void onDeliveryRecordSubsequentsTableEdit(Action.ActionPerformedEvent event) {
		DeliveryRecordSubsequent deliveryRecordSubsequent = deliveryRecordSubsequentsTable.getSingleSelected();
		goToEditPage(deliveryRecordSubsequent);
	}

	@Subscribe("deliveryRecordSubsequentsTableDinar.edit")
	public void onDeliveryRecordSubsequentsTableDinarEdit(Action.ActionPerformedEvent event) {
		DeliveryRecordSubsequent deliveryRecordSubsequent = deliveryRecordSubsequentsTableDinar.getSingleSelected();
		goToEditPage(deliveryRecordSubsequent);
	}

}