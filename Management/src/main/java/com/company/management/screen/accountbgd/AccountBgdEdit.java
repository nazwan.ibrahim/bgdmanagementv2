package com.company.management.screen.accountbgd;

import com.company.management.entity.wallet.AccountBgd;
import io.jmix.ui.screen.*;

@UiController("AccountBgd.edit")
@UiDescriptor("account-bgd-edit.xml")
@EditedEntityContainer("accountBgdDc")
public class AccountBgdEdit extends StandardEditor<AccountBgd> {
}