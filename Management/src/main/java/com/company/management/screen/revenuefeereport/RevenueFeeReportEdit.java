package com.company.management.screen.revenuefeereport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.RevenueFeeReport;

@UiController("RevenueFeeReport.edit")
@UiDescriptor("revenue-fee-report-edit.xml")
@EditedEntityContainer("revenueFeeReportDc")
public class RevenueFeeReportEdit extends StandardEditor<RevenueFeeReport> {
}