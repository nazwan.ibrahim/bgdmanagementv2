package com.company.management.screen.rewardmanagement;

import com.company.management.entity.ActivityLog;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.ui.component.ComboBox;
import io.jmix.ui.component.DataGrid;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.RewardManagement;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@UiController("RewardManagement.browse")
@UiDescriptor("reward-management-browse.xml")
@LookupComponent("rewardManagementsTable")
public class RewardManagementBrowse extends StandardLookup<RewardManagement> {
    @Autowired
    private CollectionLoader<RewardManagement> rewardManagementsDl;
    @Autowired
    private CollectionLoader<RewardManagement> campaignApprovalDl;
    @Autowired
    private CollectionLoader<ActivityLog> campaignActivityLogDl;
    @Autowired
    private CollectionContainer<RewardManagement> rewardManagementsDc;

    @Subscribe
    public void onBeforeShow(final BeforeShowEvent event) {
    }
    @Subscribe
    public void onAfterShow(final AfterShowEvent event){
        rewardManagementsDl.setParameter("statusActiveCode", "G04");
        rewardManagementsDl.setParameter("statusArchiveCode", "G15");
        rewardManagementsDl.load();

        campaignApprovalDl.setParameter("statusPendingCode", "G05");
        campaignApprovalDl.setParameter("statusApprovedCode", "G06");
        campaignApprovalDl.load();

        campaignActivityLogDl.setParameter("moduleCode1", "MENU09-01");
        campaignActivityLogDl.load();
    }

    @Install(to = "rewardManagementsTable", subject = "styleProvider")
    private String rewardManagementsTableStyleProvider(RewardManagement rewardManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (rewardManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "campaignApprovalTable", subject = "styleProvider")
    private String campaignApprovalTableStyleProvider(RewardManagement rewardManagement, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (rewardManagement.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }

    @Install(to = "campaignActivityLogTable", subject = "styleProvider")
    private String campaignActivityLogTableStyleProvider(ActivityLog activityLog, String property) {
        if (property == null){

        }else if (property.equals("status.name")){
            switch (activityLog.getStatus().getCode()){
                case "G01", "G04","G06", "G09":
                    return "green-status";
                case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
                    return "red-status";
                case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
                    return "yellow-status";
                case "G19", "G03", "G15":
                    return "grey-status";
                case "G06/01":
                    return "orange-status";
            }
        }
        return null;
    }
}