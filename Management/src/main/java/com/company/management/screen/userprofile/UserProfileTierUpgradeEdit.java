package com.company.management.screen.userprofile;

import com.company.management.bean.AmazonS3Bean;
import com.company.management.entity.registrationdb.UserAddress;
import com.company.management.entity.registrationdb.UserBank;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.registrationdb.UserTier;
import com.company.management.entity.wallet.IntegrationService;
import com.company.management.service.CipherEncryptionManagement;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@UiController("UserProfileTierUpgrade.edit")
@UiDescriptor("user-profile-tier-upgrade-edit.xml")
@EditedEntityContainer("userProfileDc")
public class UserProfileTierUpgradeEdit extends StandardEditor<UserProfile> {

	private static final Logger log = LoggerFactory.getLogger(UserProfileTierUpgradeEdit.class);
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Notifications notifications;
	@Autowired
	private AmazonS3Bean AmazonS3Bean;
	private String typeUser;
	@Autowired
	private InstanceLoader<UserProfile> userProfileDl;
	@Autowired
	private Image kycImage;
	@Autowired
	private Image originalImage;
	@Autowired
	private Label<String> similarityKYC;
	@Autowired
	private TextField<String> addressLineOne;
	@Autowired
	private TextField<String> addressLineTwo;
	@Autowired
	private TextField<String> postcode;
	@Autowired
	private TextField<String> state;
	@Autowired
	private TextField<String> bankName;
	@Autowired
	private TextField<String> bankAccount;
	@Autowired
	private CollectionLoader<UserAddress> userAddressesDl;
	@Autowired
	private CollectionLoader<UserBank> userBanksDl;
	@Autowired
	private InstanceContainer<UserProfile> userProfileDc;
	@Autowired
	private CollectionContainer<UserAddress> userAddressesDc;
	@Autowired
	private CollectionContainer<UserBank> userBanksDc;
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;
	@Autowired
	private TextField<String> payNetVerification;

	public String getTypeUser() {
		return typeUser;
	}

	public void setTypeUser(String typeUser) {
		this.typeUser = typeUser;
	}

	@Autowired
	private Environment environment;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		userProfileDl.load();

		userAddressesDl.setParameter("user1",userProfileDc.getItem());
		userAddressesDl.load();

		userBanksDl.setParameter("user1",userProfileDc.getItem());
		userBanksDl.load();
		getKYCDetails();
		dislayPaynetValidation();
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		try{
			addressLineOne.setValue(userAddressesDc.getMutableItems().get(0).getAddress1());
			addressLineTwo.setValue(userAddressesDc.getMutableItems().get(0).getAddress2());
			postcode.setValue(userAddressesDc.getMutableItems().get(0).getPostcode());
			state.setValue(userAddressesDc.getMutableItems().get(0).getState());
			if(userBanksDc.getMutableItems().size() != 0) {
				bankName.setValue(userBanksDc.getMutableItems().get(0).getBank().getName());
				bankAccount.setValue(userBanksDc.getMutableItems().get(0).getAccountNumber());
			}
		}catch (Exception ex){
			log.info("Incomplete User Data");
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.WARNING)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Incomplete User Data")
					.show();
		}
	}




	public void getKYCDetails() {

		UserTier userTierRegistration = this.dataManager.load(UserTier.class).query("select e from UserTier e " + "where e.user.username = :username " + "order by e.createdDate desc").parameter("username", userProfileDl.getContainer().getItem().getUsername()).one();

//		String baseUrl = "http://acquaintme.milradius.com:8080/api";
		String endpoint = "/rekognition/kycDetails";
		String queryParam1 = "reference=";
		String queryParam2 = userTierRegistration.getReference();

		String url = environment.getProperty("acquaintme") + endpoint + "?" + queryParam1 + queryParam2;

		try {
			URL apiUrl = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
			connection.setRequestMethod("GET");

			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = connection.getInputStream();
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = bufferedReader.readLine()) != null) {
					response.append(inputLine);
				}
				bufferedReader.close();

				// handle the response here
				Gson gson = new Gson();
				JsonObject jsonObject = gson.fromJson(response.toString(), JsonObject.class);
				String document = String.valueOf(jsonObject.get("documents").getAsJsonArray());
				List<JsonObject> jsonObjectSelfieList = new ArrayList<>();
				List<JsonObject> jsonObjectFrontImgList = new ArrayList<>();
				List<JsonObject> jsonObjectBackImgList = new ArrayList<>();
				JsonObject[] jsonObjectArray = gson.fromJson(document, JsonObject[].class);

				for (JsonObject jsonObject1 : jsonObjectArray) {
					String docName = jsonObject1.get("docName").getAsString();
					if (docName.equals("faceImg")) {
						jsonObjectSelfieList.add(jsonObject1);
					}
				}

				for (JsonObject jsonObject1 : jsonObjectArray) {
					String docName = jsonObject1.get("docName").getAsString();
					if (docName.equals("documentImgFront")) {
						jsonObjectFrontImgList.add(jsonObject1);
					}
				}

				for (JsonObject jsonObject1 : jsonObjectArray) {
					String docName = jsonObject1.get("docName").getAsString();
					if (docName.equals("documentImgBack")) {
						jsonObjectBackImgList.add(jsonObject1);
					}
				}

				String keySelfie = jsonObjectSelfieList.get(0).get("key").getAsString();
				String keyFrontImg = jsonObjectFrontImgList.get(0).get("key").getAsString();
				String keyBackImg = jsonObjectBackImgList.get(0).get("key").getAsString();
				String similarityPercent = String.valueOf(jsonObject.get("similarityPercent"));

				getOriginalImage(keySelfie);
				getKYCImage(keyFrontImg);

				similarityKYC.setValue(similarityPercent + "%");
			} else {  // handle the error response here
				notifications.create().withContentMode(ContentMode.HTML).withType(Notifications.NotificationType.WARNING).withPosition(Notifications.Position.MIDDLE_CENTER).withCaption("Images for KYC not found").show();
			}

		} catch (IOException e) {
//			log.error("Error", e);
		}
	}

	public void getOriginalImage(String key) throws IOException {
		originalImage.setSource(StreamResource.class).setStreamSupplier(() -> {
			try {
				return new ByteArrayInputStream(AmazonS3Bean.getObjectFromS3(key));
			} catch (IOException e) {
				throw new RuntimeException(e.getCause().getMessage());
			}
		});
	}

	public void getKYCImage(String key) throws IOException {
		kycImage.setSource(StreamResource.class).setStreamSupplier(() -> {
			try {
				return new ByteArrayInputStream(AmazonS3Bean.getObjectFromS3(key));
			} catch (IOException e) {
				throw new RuntimeException(e.getCause().getMessage());
			}
		});
	}

	@Install(to = "identificationNoField", subject = "formatter")
	private String identificationNoFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	@Install(to = "phoneNumberField", subject = "formatter")
	private String phoneNumberFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	private void dislayPaynetValidation(){
		try{
			IntegrationService integrationService = this.dataManager.load(IntegrationService.class)
					.query("select e from IntegrationService e where e.code = :code " +
							"and e.serviceName = :serviceName and e.createdBy = :createdBy")
					.parameter("code","ITR01/02")
					.parameter("serviceName","Account Enquiry")
					.parameter("createdBy", userProfileDc.getItem().getUsername())
					.one();

			if(integrationService.getSuccess().equals(true)){
				payNetVerification.setValue(integrationService.getSuccess().toString());
			}else{
				String jsongString  = integrationService.getRequestBody();
				Gson gson = new Gson();
				JsonObject jsonObject = gson.fromJson(jsongString, JsonObject.class);

				payNetVerification.setValue(jsonObject.get("ResponseMessage").getAsString());
			}
		}catch (Exception exception){
			log.info(exception.getMessage());
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.WARNING)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("PayNet Details of selected user could not be found")
					.show();
		}
	}
}