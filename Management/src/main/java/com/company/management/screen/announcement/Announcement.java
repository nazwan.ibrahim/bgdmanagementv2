package com.company.management.screen.announcement;

import com.company.management.screen.createannouncement.CreateAnnouncement;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.component.Button;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("Announcement")
@UiDescriptor("announcement.xml")
public class Announcement extends Screen {
    @Autowired
    private ScreenBuilders screenBuilders;
    @Autowired
    private CollectionLoader<com.company.management.entity.Announcement> announcementsDl;

    @Subscribe("announcementsTableCreateBtn")
    public void onAnnouncementsTableCreateBtnClick(Button.ClickEvent event) {
        logger.info("BUTTON create CLICKED");
        CreateAnnouncement createAnnouncement = screenBuilders.screen(this)
                .withScreenClass(CreateAnnouncement.class)
                .withAfterCloseListener(createAnnouncementAfterScreenCloseEvent -> {
                    announcementsDl.load();
                })
                .build();
        createAnnouncement.show();
    }


    private final Logger logger = LogManager.getLogger(Announcement.class);




}