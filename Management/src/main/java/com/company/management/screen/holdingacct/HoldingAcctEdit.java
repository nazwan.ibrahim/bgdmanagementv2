package com.company.management.screen.holdingacct;

import com.company.management.api.ManagementController;
import com.company.management.entity.wallet.*;
import io.jmix.core.DataManager;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.HasValue;
import io.jmix.ui.component.Label;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.HoldingAcct;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@UiController("HoldingAcct.edit")
@UiDescriptor("HoldingAccount.xml")
@EditedEntityContainer("holdingAcctDc")
public class HoldingAcctEdit extends Screen {
	@Autowired
	private CollectionLoader<AccountWalletView> accountWalletViewsDl;
	@Autowired
	private CollectionContainer<AccountWalletView> accountWalletViewsDc;
	@Autowired
	private Label<BigDecimal> currentGold;
	@Autowired
	private Label<BigDecimal> currentGoldPcs;
	@Autowired
	private TextField<BigDecimal> totalConverGram;
	@Autowired
	private DataManager dataManager;

	private BigDecimal countHolding = BigDecimal.ZERO;
	@Autowired
	private Label<BigDecimal> currentHoldingGold;
	@Autowired
	private Notifications notifications;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private TextField<Integer> toConvertPcs;

	private BigDecimal totalDiffGold = BigDecimal.ZERO;


	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		accountWalletViewsDl.setParameter("accountOwner1","BgdAdmin");
		accountWalletViewsDl.setParameter("code1","E02");
		accountWalletViewsDl.load();
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		currentGold.setValue(accountWalletViewsDc.getItems().get(0).getAvailableAmount());
		BigDecimal divide = accountWalletViewsDc.getItems().get(0).getAvailableAmount().divide(BigDecimal.valueOf(4.25), 6, RoundingMode.HALF_EVEN);
		currentGoldPcs.setValue(divide);
		loadHolding();
	}
	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		close(StandardOutcome.CLOSE);
	}

	private void loadHolding(){
		List<HoldingAcct> holdingAccts = this.dataManager.load(HoldingAcct.class).all().list();

		holdingAccts.forEach(holdingAcct -> {
			countHolding = countHolding.add(holdingAcct.getHolding());
		});
		currentHoldingGold.setValue(countHolding);
	}

	@Subscribe("toConvertPcs")
	public void onToConvertPcsValueChange(HasValue.ValueChangeEvent<Integer> event) {
		BigDecimal getValue = BigDecimal.valueOf(event.getValue());
		assert getValue != null;
		BigDecimal totalConvert = getValue.multiply(BigDecimal.valueOf(4.25));
		totalConverGram.setValue(totalConvert);
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {

		BigDecimal toConverPcs = BigDecimal.valueOf(toConvertPcs.getValue());
		BigDecimal convertGram = totalConverGram.getValue();
		BigDecimal currentTradable = accountWalletViewsDc.getItems().get(0).getAvailableAmount();
		BigDecimal diffGoldGram = convertGram.subtract(currentTradable);
		if(toConverPcs.compareTo(currentGoldPcs.getValue()) < 0){
			notifications.create(Notifications.NotificationType.ERROR)
					.withCaption("Cannot Convert Less Than Current Tradable Account (pcs)")
					.show();
		}else if(diffGoldGram.compareTo(countHolding)> 0){
			notifications.create(Notifications.NotificationType.ERROR)
					.withCaption("Not Enough Current Holding Account")
					.show();
		}else{
//			HoldingAcct holdingAcct = this.dataManager.create(HoldingAcct.class);
//			String reference = managementController.generateSeqNo("HC","6");
//			holdingAcct.setReference_Num(reference);
//			holdingAcct.setTradable(diffGoldGram);
//			holdingAcct.setHolding(diffGoldGram.negate());

			List<HoldingAcct> holdingAccts = this.dataManager.load(HoldingAcct.class)
					.query("select e from HoldingAcct e " +
							"where e.holding > 0")
					.list();

			holdingAccts.forEach(e -> {
				if(e.getHolding().compareTo(diffGoldGram) <= 0) {
					e.setHolding(BigDecimal.ZERO);
					e.setTradable(e.getTradable().add(e.getHolding()));

					//calculate new diffGoldGram
					 totalDiffGold = diffGoldGram.subtract(e.getHolding());
				} else {
					e.setHolding(e.getHolding().subtract(diffGoldGram));
					e.setTradable(e.getTradable().add(diffGoldGram));

					//calculate new diffGoldGram
					// diffGoldGram = 0
				}
				this.dataManager.save(e);
			});

			String queryaccountBgd = "select a from AccountBgd a " +
					"where a.accOwner = :accOwner1 " +
					"and a.walletType.code = :walletTypeCode1";
			AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
					.query(queryaccountBgd).parameter("accOwner1", "BgdAdmin").parameter("walletTypeCode1", "E02").one();

			String querytransStatus = "select t from TransStatus t " +
					"where t.code = :code1";
			TransStatus transStatus = this.dataManager.load(TransStatus.class)
					.query(querytransStatus).parameter("code1", "G01")
					.one();

			TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
			transactionAcct.setAccount(accountBgd);
			transactionAcct.setTransType(type("F01"));
			String reference1 = managementController.generateSeqNo("HC","6");
			transactionAcct.setReference(reference1);
			transactionAcct.setAmount(diffGoldGram);
			transactionAcct.setTransStatus(transStatus);
			transactionAcct.setCreatedBy(accountBgd.getAccOwner());
			transactionAcct.setCreatedDate(new Date());

//			this.dataManager.save(holdingAcct);
			this.dataManager.save(transactionAcct);

			notifications.create(Notifications.NotificationType.HUMANIZED)
					.withCaption("Successful Convert into BGD")
					.show();
			close(StandardOutcome.CLOSE);
		}
	}
	private TransactionType type(String code){
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
		return this.dataManager.load(TransactionType.class)
				.query(query).parameter("code1", code)
				.one();
	}
}