package com.company.management.screen.costrevocerymanagement;

import com.amazonaws.util.IOUtils;
import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.Resources;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.email.EmailAttachment;
import io.jmix.email.EmailInfo;
import io.jmix.email.EmailInfoBuilder;
import io.jmix.email.Emailer;
import io.jmix.security.role.assignment.RoleAssignmentRepository;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.action.list.RemoveAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.screen.*;
import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@UiController("CostRevoceryManagement.edit")
@UiDescriptor("cost-revocery-management-edit.xml")
@EditedEntityContainer("costRevoceryManagementDc")
public class CostRevoceryManagementEdit extends StandardEditor<CostRevoceryManagement> {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(CostRevoceryManagementEdit.class);
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Named("uploadFileTable.remove")
	private RemoveAction<CostRecoveryManagementDoc> uploadFileTableRemove;
	@Autowired
	private TextField<BigDecimal> valueTaxField;
	@Autowired
	private EntityComboBox<RtType> uomTaxFieldId;
	@Autowired
	private CollectionLoader<RtType> rtTypesUomDl;
	@Autowired
	private CollectionLoader<RtType> rtTypesTaxDl;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private CollectionLoader<CostRecoveryManagementDoc> costRecoveryManagementDocsDl;
	@Autowired
	private DateField<Date> efectiveDateField;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private TemporaryStorage temporaryStorage;

	CostRecoveryManagementDoc costRecoveryManagementDoc;
	@Autowired
	private CollectionContainer<CostRecoveryManagementDoc> costRecoveryManagementDocsDc;
	@Autowired
	private InstanceContainer<CostRevoceryManagement> costRevoceryManagementDc;
	@Autowired
	private Notifications notifications;
	@Autowired
	private TextField<BigDecimal> minFieldId;
	@Autowired
	private TextField<BigDecimal> maxFieldId;
	@Autowired
	private TextArea<String> remarksFieldId;
	@Autowired
	private RoleAssignmentRepository roleAssignmentRepository;
	@Autowired
	protected Resources resources;
	@Autowired
	private Emailer emailer;

	@Subscribe
	public void onBeforeShow(final BeforeShowEvent event) {
		rtTypesUomDl.setParameter("codeJ01", "J01");
		rtTypesUomDl.setParameter("codeJ04", "J04");
		rtTypesUomDl.load();

		rtTypesTaxDl.setParameter("codeTax", "TAX");
		rtTypesTaxDl.load();

		costRecoveryManagementDocsDl.setParameter("costRecoveryManagement1", getEditedEntity());
		costRecoveryManagementDocsDl.load();

		efectiveDateField.setRangeStart(timeSource.currentTimestamp());
	}

	@Subscribe
	public void onInit(final InitEvent event) {
		uploadMultipleFile();
	}

	@Subscribe
	public void onAfterShow(final AfterShowEvent event) {
		RtType statusPending = this.dataManager.load(RtType.class)
				.query("select e from RtType e " +
						"where e.code = :code")
				.parameter("code", "G05").one();

		CostRevoceryManagement costRevoceryManagement = this.dataManager.create(CostRevoceryManagement.class);
		costRevoceryManagement.setProduct(costRevoceryManagementDc.getItem().getProduct());
		costRevoceryManagement.setType(costRevoceryManagementDc.getItem().getType());
		costRevoceryManagement.setCharge(costRevoceryManagementDc.getItem().getCharge());
		costRevoceryManagement.setUom(costRevoceryManagementDc.getItem().getUom());
		costRevoceryManagement.setMinRM(costRevoceryManagementDc.getItem().getMinRM());
		costRevoceryManagement.setMaxRM(costRevoceryManagementDc.getItem().getMaxRM());
		costRevoceryManagement.setTypeTax(costRevoceryManagementDc.getItem().getTypeTax());
		costRevoceryManagement.setValueTax(costRevoceryManagementDc.getItem().getValueTax());
		costRevoceryManagement.setUomTax(costRevoceryManagementDc.getItem().getUomTax());
		costRevoceryManagement.setEfectiveDate(costRevoceryManagementDc.getItem().getEfectiveDate());
		costRevoceryManagement.setRequestBy(currentAuthentication.getUser().getUsername());
		costRevoceryManagement.setRequestDate(new Timestamp(System.currentTimeMillis()));
		costRevoceryManagement.setStatus(statusPending);

		costRevoceryManagementDc.setItem(costRevoceryManagement);

		if(costRevoceryManagement.getUom().getCode().equals("J01")) {
			minFieldId.setValue(null);
			minFieldId.setEditable(false);

			maxFieldId.setValue(null);
			maxFieldId.setEditable(false);
		} else {
			minFieldId.setEditable(true);
			maxFieldId.setEditable(true);
		}

		if(costRevoceryManagement.getTypeTax().getCode().equals("TAX00")) {
			valueTaxField.setValue(BigDecimal.ZERO);
			valueTaxField.setEditable(false);

//			uomTaxFieldId.setValue(null);
			uomTaxFieldId.setEditable(false);
		} else {
			valueTaxField.setEditable(true);
			uomTaxFieldId.setEditable(true);
		}
	}
	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				costRecoveryManagementDoc = this.dataManager.create(CostRecoveryManagementDoc.class);
				costRecoveryManagementDoc.setCostRecoveryManagement(costRevoceryManagementDc.getItem());
				costRecoveryManagementDoc.setName(fileName);
				costRecoveryManagementDoc.setFile_upload(fileRef);
				costRecoveryManagementDoc.setAction(loadStatus("ACT01"));

				costRecoveryManagementDocsDc.getMutableItems().add(costRecoveryManagementDoc);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}

	@Install(to = "uploadFileTable.deleteFileUpload", subject = "columnGenerator")
	private Component uploadFileTableDeleteFileUploadColumnGenerator(final CostRecoveryManagementDoc costRecoveryManagementDoc) {
		Button btn = uiComponents.create(Button.class);
		btn.setCaption("Delete");
		btn.addClickListener(clickEvent -> {
			uploadFileTableRemove.execute();
		});
		return btn;
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(final Button.ClickEvent event) {
		dialogs.createOptionDialog()
				.withCaption("Revenue Update Confirmation")
				.withContentMode(ContentMode.HTML)
				.withMessage("Click Confirm or Cancel button to proceed.")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Confirm")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("Cancel")
				)
				.show();
	}

	private void saveData(){
		if(!efectiveDateField.isEmpty() &&!remarksFieldId.isEmpty() && !costRecoveryManagementDocsDc.getMutableItems().isEmpty()) {
			costRevoceryManagementDc.getItem().setRequestBy(currentAuthentication.getUser().getUsername());
			costRevoceryManagementDc.getItem().setRequestDate(new Timestamp(System.currentTimeMillis()));
			this.dataManager.save(costRevoceryManagementDc.getItem());
			costRecoveryManagementDocsDc.getMutableItems().forEach(costRecoveryManagementDoc -> {
				this.dataManager.save(costRecoveryManagementDoc);
			});
			roleAssignmentRepository.getAllAssignments().forEach(roleAssignment -> {
				if(roleAssignment.getRoleCode().equals("BGDCheckerRole")){
					try{
						InputStream resourceAsStream = resources.getResourceAsStream("image/bursa_old_logo.svg");
						byte[] bytes = new byte[0];

						bytes = IOUtils.toByteArray(resourceAsStream);

						EmailAttachment emailAtt = new EmailAttachment(bytes,
								"bursa_old_logo.svg", "logoId");
						CostRevoceryManagement newsItem = getEditedEntity();
						EmailInfo emailInfo = EmailInfoBuilder.create()
								.setAddresses(roleAssignment.getUsername())
								.setSubject("Approval Required for Change Request")
								.setFrom("supportadmin@milradius.com.my")
								.setBody("Please Approve: "+newsItem.getType().getName()+ " \n" +
										"For Product :"+newsItem.getProduct().getName() )
//                                .setAttachments(emailAtt)
								.build();
//                        emailer.sendEmailAsync(emailInfo);
						emailer.sendEmail(emailInfo);
					}catch (Exception ex){
						log.error(ex.getMessage());
					}
				}
			});
			notifications.create()
					.withCaption("Notification has been sent to Checker Email")
					.withType(Notifications.NotificationType.HUMANIZED)
					.show();
			closeWithDiscard();
		}else{
			notifications.create()
					.withCaption("Please Fill All Mandatory Fields")
					.withType(Notifications.NotificationType.ERROR)
					.show();
			disableCommitActions();
		}
	}

	private RtType loadStatus(String code){
		return this.dataManager.load(RtType.class)
				.query("select r from RtType r " +
						"where r.code = :code1")
				.parameter("code1", code)
				.one();
	}

	@Install(to = "uploadFileTable.file_upload", subject = "columnGenerator")
	private Component uploadFileTableFile_uploadColumnGenerator(final CostRecoveryManagementDoc costRecoveryManagementDoc) {
		if (costRecoveryManagementDoc.getFile_upload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(costRecoveryManagementDoc.getFile_upload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(costRecoveryManagementDoc.getFile_upload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Subscribe("taxFieldId")
	public void onTaxFieldIdValueChange(final HasValue.ValueChangeEvent<RtType> event) {
		if(event.getValue().getCode().equals("TAX00")){
			valueTaxField.setEditable(false);
			valueTaxField.setValue(BigDecimal.ZERO);
			uomTaxFieldId.setEditable(false);
//			uomTaxFieldId.setValue(null);
		}else{
			valueTaxField.setEditable(true);

		}
	}
}