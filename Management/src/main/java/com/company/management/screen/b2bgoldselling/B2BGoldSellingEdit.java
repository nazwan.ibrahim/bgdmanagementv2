package com.company.management.screen.b2bgoldselling;

import io.jmix.ui.screen.*;
import com.company.management.entity.B2BGoldSelling;

@UiController("B2BGoldSelling.edit")
@UiDescriptor("b2b-gold-selling-edit.xml")
@EditedEntityContainer("b2BGoldSellingDc")
public class B2BGoldSellingEdit extends StandardEditor<B2BGoldSelling> {
}