package com.company.management.screen.currency;

import io.jmix.ui.screen.*;
import com.company.management.entity.Currency;

@UiController("Currency.browse")
@UiDescriptor("currency-browse.xml")
@LookupComponent("table")
public class CurrencyBrowse extends MasterDetailScreen<Currency> {
}