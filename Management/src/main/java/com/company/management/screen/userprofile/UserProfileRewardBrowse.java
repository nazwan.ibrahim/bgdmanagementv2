package com.company.management.screen.userprofile;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.UserProfile;


@UiController("UserProfileReward.browse")
@UiDescriptor("user-profile-reward-browse.xml")
@LookupComponent("userProfilesTable")
public class UserProfileRewardBrowse extends StandardLookup<UserProfile> {

}