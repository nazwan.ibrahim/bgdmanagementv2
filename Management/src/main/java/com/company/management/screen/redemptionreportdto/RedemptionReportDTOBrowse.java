package com.company.management.screen.redemptionreportdto;

import com.company.management.entity.wallet.RedeemInfo;
import io.jmix.core.DataManager;
import io.jmix.core.LoadContext;
import io.jmix.core.entity.KeyValueEntity;
import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.RedemptionReportDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

@UiController("RedemptionReportDTO.browse")
@UiDescriptor("redemption-report-dto-browse.xml")
@LookupComponent("redemptionReportDToesTable")
public class RedemptionReportDTOBrowse extends StandardLookup<RedemptionReportDTO> {
    @Autowired
    private DataManager dataManager;

    @Install(to = "redemptionReportDToesDl", target = Target.DATA_LOADER)
    private List<RedemptionReportDTO> redemptionReportDToesDlLoadDelegate(LoadContext<RedemptionReportDTO> loadContext) {
        // Here you can load entities from an external store
        List<RedeemInfo> redeemInfos = this.dataManager.load(RedeemInfo.class)
                .query("select e from RedeemInfo e " +
                        "where e.transaction.transStatus.code = :code")
                .parameter("code", "G01")
                .list();

        List<RedemptionReportDTO> redemptionReportDTOS = this.dataManager.load(RedemptionReportDTO.class).all().list();
        redeemInfos.forEach(e -> {

        });




        return Collections.emptyList();
    }
}