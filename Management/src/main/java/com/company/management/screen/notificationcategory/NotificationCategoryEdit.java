package com.company.management.screen.notificationcategory;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationCategory;

@UiController("NotificationCategory.edit")
@UiDescriptor("notification-category-edit.xml")
@EditedEntityContainer("notificationCategoryDc")
public class NotificationCategoryEdit extends StandardEditor<NotificationCategory> {
}