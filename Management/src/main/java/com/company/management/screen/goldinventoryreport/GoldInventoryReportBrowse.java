package com.company.management.screen.goldinventoryreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.GoldInventoryReport;

@UiController("GoldInventoryReport.browse")
@UiDescriptor("gold-inventory-report-browse.xml")
@LookupComponent("goldInventoryReportsTable")
public class GoldInventoryReportBrowse extends StandardLookup<GoldInventoryReport> {
}