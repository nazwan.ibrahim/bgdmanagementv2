package com.company.management.screen.transactionhistoriesviewadmin;

import com.company.management.entity.PurchaseHistoriesView;
import io.jmix.ui.component.TabSheet;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import org.springframework.beans.factory.annotation.Autowired;


@UiController("TransactionHistoriesViewAdminB2BSupplier.browse")
@UiDescriptor("transaction-histories-view-admin-b2bSupplier-browse.xml")
@LookupComponent("transactionHistoriesViewAdminsTable")
public class TransactionHistoriesViewAdminB2BSupplierBrowse extends StandardLookup<PurchaseHistoriesView> {
	@Autowired
	private TabSheet freeTab;
	@Autowired
	private CollectionLoader<PurchaseHistoriesView> purchaseHistoriesViewsDl;
	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		if(freeTab.getSelectedTab().equals("freeAccount")) {
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F06");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
			purchaseHistoriesViewsDl.setFirstResult(20);
		}else{
			purchaseHistoriesViewsDl.setParameter("transactionCode1", "F05");
			purchaseHistoriesViewsDl.setParameter("accOwner1", "AceAdmin");
			purchaseHistoriesViewsDl.load();
			purchaseHistoriesViewsDl.setFirstResult(20);
		}
	}

	@Subscribe("freeTab")
	public void onFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("freeAccount")){
			purchaseHistoriesViewsDl.setParameter("transactionCode1","F06");
			purchaseHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
			purchaseHistoriesViewsDl.load();
		}else {
			purchaseHistoriesViewsDl.setParameter("transactionCode1","F05");
			purchaseHistoriesViewsDl.setParameter("accOwner1","AceAdmin");
			purchaseHistoriesViewsDl.load();
		}
	}
}