package com.company.management.screen.goldpricehistory;

import com.company.management.entity.GoldPriceHistory;
import com.sun.star.util.DateTime;
import io.jmix.core.Metadata;
import io.jmix.core.TimeSource;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import liquibase.repackaged.org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;


@UiController("GoldPriceHistoryFragment")
@UiDescriptor("gold-price-history-fragment.xml")
public class GoldPriceHistoryFragment extends ScreenFragment {
	@Autowired
	private CollectionLoader<GoldPriceHistory> goldPriceHistoryDl;

	@Subscribe
	public void onInit(InitEvent event) {
		goldPriceHistoryDl.load();
		List<GoldPriceHistory> items = new ArrayList<>();
//		Date startDate = DateUtils.addDays(timeSource.currentTimestamp(), -DAYS_COUNT);
//		for (int i = 0; i < DAYS_COUNT; i++) {
//			items.add(generateDateValueVolume(DateUtils.addDays(startDate, i), i));
//		}
//		goldPriceHistoryDc.setItems(items);
	}

//	private GoldPriceHistory generateDateValueVolume(Date dateTime, int i) {
//		Long value = Math.round(random.nextDouble() * (20 + i)) + 20 + i;
//		Long volume = Math.round(random.nextDouble() * (20 + i)) + i;
//		return dateValueVolume(dateTime, value, volume);
//	}
//
//	private GoldPriceHistory dateValueVolume(Date dateTime, Long value, Long volume) {
//		GoldPriceHistory dateValueVolume = metadata.create(GoldPriceHistory.class);
//		dateValueVolume.setDateTime(dateTime);
//		dateValueVolume.setBuyPrice(BigDecimal.valueOf(value));
//		dateValueVolume.setSellPrice(BigDecimal.valueOf(volume));
//		return dateValueVolume;
//	}
}