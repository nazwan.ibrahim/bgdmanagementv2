package com.company.management.screen.notificationcategory;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.NotificationCategory;

@UiController("NotificationCategory.browse")
@UiDescriptor("notification-category-browse.xml")
@LookupComponent("notificationCategoriesTable")
public class NotificationCategoryBrowse extends StandardLookup<NotificationCategory> {
}