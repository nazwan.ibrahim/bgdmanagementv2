package com.company.management.screen.userprofile;

import com.company.management.app.ActivityLogBean;
import com.company.management.app.BgdIDBean;
import com.company.management.entity.*;
import com.company.management.entity.registrationdb.*;
import com.company.management.entity.registrationdb.UserEmployment;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.service.CipherEncryptionManagement;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.Metadata;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;

import io.jmix.ui.upload.TemporaryStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.company.management.bean.*;
import org.springframework.core.env.Environment;

import java.io.*;
import java.util.*;

@UiController("UserProfile.edit")
@UiDescriptor("user-profile-edit.xml")
@EditedEntityContainer("userProfileDc")
public class UserProfileEdit extends StandardEditor<UserProfile> {
	private static final Logger log = LoggerFactory.getLogger(UserProfileEdit.class);
	@Autowired
	private CollectionLoader<ActivationStatus> activationStatusesDl;
	@Autowired
	private DataManager dataManager;
	private String typeUser;
	@Autowired
	private CollectionLoader<UserAddress> userAddressesDl;
	@Autowired
	private InstanceContainer<UserProfile> userProfileDc;
	@Autowired
	private TextField<String> addressLineOneResi;
	@Autowired
	private CollectionContainer<UserAddress> userAddressesDc;
	@Autowired
	private TextField<String> addressLineTwoResi;
	@Autowired
	private TextField<String> postcodeResi;
	@Autowired
	private TextField<String> stateResi;
	@Autowired
	private TextField<String> townResi;
	@Autowired
	private TextField countryDataResi;
	@Autowired
	private CollectionLoader<UserBank> userBanksDl;
	@Autowired
	private TextField<String> bankName;
	@Autowired
	private TextField<String> bankAccount;
	@Autowired
	private CollectionContainer<UserBank> userBanksDc;
	@Autowired
	private HBoxLayout idDetails;
	@Autowired
	private VBoxLayout country;
	@Autowired
	private VBoxLayout add1;
	@Autowired
	private VBoxLayout add2;
	@Autowired
	private HBoxLayout post;
	@Autowired
	private HBoxLayout stateCountry;
	@Autowired
	private HBoxLayout bankFN;
	@Autowired
	private VBoxLayout bankAN;
	@Autowired
	private CipherEncryptionManagement cipherEncryptionManagement;
//	@Autowired
//	private TextField<String> payNetVerification;
	@Autowired
	private TextArea<String> ambankResponse;
	@Autowired
	private CollectionLoader<UserTier> userTiersDl;
	@Autowired
	private EntityComboBox<KycStatus> approvalStatusEntity;
	@Autowired
	private CollectionLoader<KycStatus> kycStatusesDl;
	@Autowired
	private RadioButtonGroup<KycStatus> approveCheckBox;
//	@Autowired
//	private TextField<String> statuskyc;
	@Autowired
	private CollectionLoader<KycStatus> kycStatusesDl1;
	@Autowired
	private CollectionContainer<KycStatus> kycStatusesDc1;
	@Autowired
	private CollectionContainer<KycStatus> kycStatusesDc;
	@Autowired
	private CollectionContainer<KycStatus> kycStatusesDc2;
	@Autowired
	private TextField<String> emailVerified;
	@Autowired
	private HBoxLayout statusKYCAAANNN;
	@Autowired
	private TemporaryStorage temporaryStorage;
	@Autowired
	private FileMultiUploadField fileUpload;
	@Autowired
	private InstanceContainer<ActivityLog> activityLogDc;
	@Autowired
	private CollectionContainer<ActivityLogFile> activityLogFilesDc;
	@Autowired
	private ActivityLogBean activityLogBean;
	@Autowired
	private TextArea<String> remarksFieldId;
	@Autowired
	private CollectionLoader<ActivityLogFile> activityLogFilesDl;
	@Autowired
	private TextField addressLineTwoMail;
	@Autowired
	private TextField addressLineOneMail;
	@Autowired
	private TextField postcodeMail;
	@Autowired
	private TextField stateMail;
	@Autowired
	private TextField townMail;
	@Autowired
	private TextField countryDataMail;
	@Autowired
	private HBoxLayout homeNoLine;
	@Autowired
	private VBoxLayout residentialAddressContainer;
	@Autowired
	private VBoxLayout mailingAddressContainer;
	@Autowired
	private VBoxLayout employmentDetailsContainer;
	@Autowired
	private VBoxLayout bpfwsContainer;
	@Autowired
	private VBoxLayout bankInformationContainer;
	@Autowired
	private VBoxLayout bgdAccountInformationContainer;
	@Autowired
	private VBoxLayout onboardingApprovalContainer;
	@Autowired
	private VBoxLayout emailStatus;
	@Autowired
	private VBoxLayout remarksContainer;
//	@Autowired
//	private DateField<String> dobField;
	@Autowired
	private CollectionLoader<UserEmployment> userEmploymentDl;
	@Autowired
	private CollectionContainer<UserEmployment> userEmploymentDc;
	@Autowired
	private TextField employmentDetails;
	@Autowired
	private TextField employerName;
	@Autowired
	private TextField position;
	@Autowired
	private TextField natureBusiness;
	@Autowired
	private TextField countryTaxResidence;



	@Autowired
	private CollectionLoader<FinancialSource> financialSourcesDl;
	@Autowired
	private CollectionLoader<StatutoryDetail> statutoryDetailsDl;
	@Autowired
	private CollectionContainer<FinancialSource> financialSourcesDc;
	@Autowired
	private TextField purposeAccountTransaction;
	@Autowired
	private CollectionContainer<StatutoryDetail> statutoryDetailsDc;
	@Autowired
	private TextField expectedVolume;
	@Autowired
	private TextField expectedNumber;
	@Autowired
	private TextField sourceFunds;
	@Autowired
	private TextField sourceWealth;
//	@Autowired
//	private CollectionLoader<AccountBgd> accountBgdDl;
//	@Autowired
//	private CollectionContainer<AccountBgd> accountBgdDc;
	@Autowired
	private TextField bgdID;
	@Autowired
	private TextField tier;
	@Autowired
	private CollectionContainer<UserTier> userTiersDc;
	@Autowired
	private TextField accountStatus;
	@Autowired
	private TextField identificationNoField;
	@Autowired
	private TextField<String> phoneNumberField;
	@Autowired
	private CollectionLoader<UserVerification> userVerificationDl;
	@Autowired
	private CollectionContainer<UserVerification> userVerificationDc;
	@Autowired
	private TextField kycStatus;
	@Autowired
	private TextField nameScreeningStatus;
	@Autowired
	private TextField riskProfilingStatus;
	@Autowired
	private TextField otherSourceFunds;
	@Autowired
	private TextField otherSourceWealth;
	@Autowired
	private TextField tierStatus;
	@Autowired
	private TextField createdAccountDate;
	@Autowired
	private TextField closingAccountDate;

	@Autowired
	private CollectionLoader<FatcaCrsDeclaration> fatcaCRSDl;
	@Autowired
	private CollectionContainer<FatcaCrsDeclaration> fatcaCRSDc;
	@Autowired
	private Metadata metadata;
	@Autowired
	private VBoxLayout fatcaCRSContainer;
	@Autowired
	private TextField businessName;
	@Autowired
	private TextField previousEmployerName;

	public String getTypeUser() {
		return typeUser;
	}

	public void setTypeUser(String typeUser) {
		this.typeUser = typeUser;
	}
	@Autowired
	private HBoxLayout verificationBox;
	@Autowired
	private InstanceLoader<UserProfile> userProfileDl;

	@Autowired
	private AmazonS3Bean AmazonS3Bean;
//	@Autowired
//	private Image originalImage;
//	@Autowired
//	private Image kycImage;
//	@Autowired
//	private Label<String> similarityKYC;
//	@Autowired
//	private Label<BigDecimal> kycInfo;
//	@Autowired
//	private HBoxLayout imageKYC;
	@Autowired
	private Notifications notifications;

	@Autowired
	private Environment environment;

	@Autowired
	private BgdIDBean bgdIDBean;
	@Autowired
	private CollectionContainer<FatcaCRSDTO> fatcaCRSDTODc;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) throws IOException {
		if(typeUser.equals("freeAccount")){
			hideElement();
		}else {
			try {
				userProfileDl.load();
				userAddressesDl.setParameter("user1", userProfileDc.getItem());
				userAddressesDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userBanksDl.setParameter("user1", userProfileDc.getItem());
				userBanksDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userTiersDl.setParameter("user1", userProfileDc.getItem());
				userTiersDl.setParameter("currentTier1", true);
				userTiersDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				kycStatusesDl.setParameter("code1", "G12");
				kycStatusesDl1.setParameter("code1", "G10");
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userEmploymentDl.setParameter("user1", userProfileDc.getItem());
				userEmploymentDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}try {
				userEmploymentDc.getMutableItems().get(0).getPosition().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}try {
				userEmploymentDc.getMutableItems().get(0).getNatureOfBusiness().getCode();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				statutoryDetailsDl.setParameter("user1", userProfileDc.getItem());
				statutoryDetailsDl.load();
				statutoryDetailsDc.getMutableItems().get(0).getTransactionPurposeType().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				financialSourcesDl.setParameter("user1", userProfileDc.getItem());
				financialSourcesDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}try {
				financialSourcesDc.getMutableItems().get(0).getSourceFundType().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}try {
				financialSourcesDc.getMutableItems().get(0).getSourceWealthType().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			//Hide Other Sources of Funds/Wealth
			try {
				if (financialSourcesDc.getMutableItems().get(0).getSourceFundType().getName().equals("Others"))
					otherSourceFunds.setVisible(true);
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}try {
				if (financialSourcesDc.getMutableItems().get(0).getSourceWealthType().getName().equals("Others"))
					otherSourceWealth.setVisible(true);
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				activityLogDc.setItem(this.dataManager.create(ActivityLog.class));
				activityLogFilesDl.setParameter("activityLog1", activityLogDc.getItem());
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userVerificationDl.setParameter("user1", userProfileDc.getItem());
				userVerificationDl.load();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userVerificationDc.getMutableItems().get(0).getKycResult().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userVerificationDc.getMutableItems().get(0).getNsResult().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				userVerificationDc.getMutableItems().get(0).getCrpResult().getName();
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
			try {
				BgdIDDTO bgdIDBean1 = bgdIDBean.getBgdID(userProfileDc.getItem().getUsername());
				bgdIDBean1.getBgdID();

				getScreenData().loadAll();
			} catch (Exception ex) {
			log.error(ex.getMessage());
			}
		}

	}
	@Subscribe
	public void onAfterShow(AfterShowEvent event) {

		try {
			fatcaCRSDl.setParameter("user1", userProfileDc.getItem());
			fatcaCRSDl.load();

			List<String[]> list1 = new ArrayList<>();
			List<String[]> list2 = new ArrayList<>();
			List<String[]> list3 = new ArrayList<>();
			List<String[]> list4 = new ArrayList<>();
			List<String[]> data = new ArrayList<>();
			List<FatcaCRSDTO> fatcaCRSDTOS = new ArrayList<>();
			try {
				fatcaCRSDc.getItems().forEach(e -> {
					if(e.getDeclaration().getCode().equals("DCLRN01")){
						String[] str1 = {e.getAgreed().toString(),e.getDeclaration().getName()};
						list1.add(str1);
					}
					if(e.getDeclaration().getCode().equals("DCLRN02")){
						String[] str2 = {e.getAgreed().toString(),e.getDeclaration().getName()};
						list2.add(str2);
					}
					if(e.getDeclaration().getCode().equals("DCLRN03")){
						String[] str3 = {e.getAgreed().toString(),e.getDeclaration().getName()};
						list3.add(str3);
					}
					if(e.getDeclaration().getCode().equals("DCLRN04")){
						String[] str4 = {e.getAgreed().toString(),e.getDeclaration().getName()};
						list4.add(str4);
					}
				});
				data.add(list1.get(0));
				data.add(list2.get(0));
				data.add(list3.get(0));
				data.add(list4.get(0));
				for(String[] str : data){
					FatcaCRSDTO fatcaCRSDTO = metadata.create(FatcaCRSDTO.class);
					fatcaCRSDTO.setAgreed(Boolean.parseBoolean(str[0]));
					fatcaCRSDTO.setDeclaration(str[1]);
					fatcaCRSDTOS.add(fatcaCRSDTO);
				}
				fatcaCRSDTODc.setItems(fatcaCRSDTOS);
			}catch (Exception ex){
				log.info(ex.getMessage());
			}
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
		uploadMultipleFile();
		emailVerified.setValue(getuserTier().getUser().getEmailVerified().toString());
		try{
			if(getuserTier().getKycStatus().getCode().equals("G12")){
				approvalStatusEntity.setOptionsContainer(kycStatusesDc);
				String querykycStatuses = "select e from KycStatus e where e.code = :code1 or e.code = :code2";
				List<KycStatus> kycStatuses = this.dataManager.load(KycStatus.class)
						.query(querykycStatuses)
						.parameter("code1", "G09")
						.parameter("code2", "G10")
						.list();
				kycStatusesDc2.setItems(kycStatuses);
			}else{
				approvalStatusEntity.setOptionsContainer(kycStatusesDc);
				String querykycStatuses = "select e from KycStatus e where e.code = :code1";
				List<KycStatus> kycStatuses = this.dataManager.load(KycStatus.class)
						.query(querykycStatuses)
						.parameter("code1", "G10")
						.list();
				kycStatusesDc2.setItems(kycStatuses);
			}
		}catch (Exception ex){
			log.info("Incomplete User Data : "+ex.getMessage());
		}

		try{
//			statuskyc.setValue(getuserTier().getKycStatus().getName());
			if(userAddressesDc.getMutableItems().size() != 0) {
				List<UserAddress> userAddressResi = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L01")).toList();
				if (userAddressResi.size() > 0) {
					addressLineOneResi.setValue(userAddressesDc.getMutableItems().get(0).getAddress1());
					addressLineTwoResi.setValue(userAddressesDc.getMutableItems().get(0).getAddress2());
					postcodeResi.setValue(userAddressesDc.getMutableItems().get(0).getPostcode());
					stateResi.setValue(userAddressesDc.getMutableItems().get(0).getState());
					townResi.setValue(userAddressesDc.getMutableItems().get(0).getTown());
					countryDataResi.setValue(userAddressesDc.getMutableItems().get(0).getCountry());
				}

				List<UserAddress> userAddressMail = userAddressesDc.getItems().stream().filter(f -> f.getAddType().getCode().equals("L03")).toList();
				if (userAddressMail.size() > 0) {
					addressLineOneMail.setValue(userAddressesDc.getMutableItems().get(0).getAddress1());
					addressLineTwoMail.setValue(userAddressesDc.getMutableItems().get(0).getAddress2());
					postcodeMail.setValue(userAddressesDc.getMutableItems().get(0).getPostcode());
					stateMail.setValue(userAddressesDc.getMutableItems().get(0).getState());
					townMail.setValue(userAddressesDc.getMutableItems().get(0).getTown());
					countryDataMail.setValue(userAddressesDc.getMutableItems().get(0).getCountry());
				}
			}

				//USER TIER
				try {
					tier.setValue(userTiersDc.getMutableItems().get(0).getTier().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					accountStatus.setValue(userProfileDc.getItem().getActStatus().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					identificationNoField.setValue(userProfileDc.getItem().getIdentificationNo());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}

				//USER VERIFICATION
					try {
						kycStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getKycResult().getName());
					} catch (Exception ex) {
						log.error(ex.getMessage());
					}
					try {
						tierStatus.setValue(userVerificationDc.getMutableItems().get(0).getOveralVerificationResult().getName());
					} catch (Exception ex) {
						log.error(ex.getMessage());
					}
					try {
						nameScreeningStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getNsResult().getName());
					} catch (Exception ex) {
						log.error(ex.getMessage());
					}
					try {
						riskProfilingStatus.setValue(userTiersDc.getMutableItems().get(0).getVerification().getCrpResult().getName());
					} catch (Exception ex) {
						log.error(ex.getMessage());
					}
			//BGD ID
				try {
					bgdID.setValue(bgdIDBean.getBgdID(userProfileDc.getItem().getUsername()).getBgdID());
				} catch (Exception ex) {
					log.error(ex.getMessage());
				}

			//USER BANKS
				try {
					bankName.setValue(userBanksDc.getMutableItems().get(0).getBank().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					bankAccount.setValue(userBanksDc.getMutableItems().get(0).getAccountNumber());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}

			//USER EMPLOYMENT
				try {
					employmentDetails.setValue(userEmploymentDc.getMutableItems().get(0).getEmploymentStatus().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					employerName.setValue(userEmploymentDc.getMutableItems().get(0).getEmployerName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					businessName.setValue(userEmploymentDc.getMutableItems().get(0).getBusinessName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					previousEmployerName.setValue(userEmploymentDc.getMutableItems().get(0).getPreviousEmployerName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					position.setValue(userEmploymentDc.getMutableItems().get(0).getPosition().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					natureBusiness.setValue(userEmploymentDc.getMutableItems().get(0).getNatureOfBusiness().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					countryTaxResidence.setValue(userEmploymentDc.getMutableItems().get(0).getCountryOftaxResidence());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
			try {
				if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("EMPL")){ // Employed
					//set visible true
					employerName.setVisible(true);
					position.setVisible(true);
					natureBusiness.setVisible(true);
					countryTaxResidence.setVisible(true);
				}
				else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("SLEMPL")) { // Self - Employed
					businessName.setVisible(true);
					position.setVisible(true);
					natureBusiness.setVisible(true);
					countryTaxResidence.setVisible(true);
				}
				else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("UNEMPL")) { // Unemployed
					countryTaxResidence.setVisible(true);
				}
				else if (userEmploymentDc.getItem().getEmploymentStatus().getCode().equals("RETIRED")) { // Retired
					previousEmployerName.setVisible(true);
					position.setVisible(true);
					natureBusiness.setVisible(true);
					countryTaxResidence.setVisible(true);
				}
			}catch (Exception ex) {
				log.error(ex.getMessage());
			}

				//STATUTORY DETAILS
				try {
					purposeAccountTransaction.setValue(statutoryDetailsDc.getMutableItems().get(0).getTransactionPurposeType().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					expectedVolume.setValue(statutoryDetailsDc.getMutableItems().get(0).getFrequencyMonthlyTransaction().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					expectedNumber.setValue(statutoryDetailsDc.getMutableItems().get(0).getTotalMonthlyTransaction().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}

			//FINANCIAL SOURCES
				try {
					sourceFunds.setValue(financialSourcesDc.getMutableItems().get(0).getSourceFundType().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					sourceWealth.setValue(financialSourcesDc.getMutableItems().get(0).getSourceWealthType().getName());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					otherSourceFunds.setValue(financialSourcesDc.getMutableItems().get(0).getOtherSourceOfFund());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
				try {
					otherSourceWealth.setValue(financialSourcesDc.getMutableItems().get(0).getOtherSourceOfWealth());
				}catch (Exception ex){
					log.info("Incomplete User Data : "+ex.getMessage());
				}
			//USER PROFILE
			try {
				createdAccountDate.setValue(userProfileDc.getItem().getApproveDate());
			}catch (Exception ex){
			log.info("Incomplete User Data : "+ex.getMessage());
			}
			try {
				closingAccountDate.setValue(userProfileDc.getItem().getCloseDate());
			}catch (Exception ex){
			log.info("Incomplete User Data : "+ex.getMessage());
			}
			try {
				phoneNumberField.setValue(userProfileDc.getItem().getPhoneNumber());
			}catch (Exception ex){
			log.info("Incomplete User Data : "+ex.getMessage());
			}
			//FATCA & CRS
//			try {
//				usCitizenBox.setValue(fatcaCRSDc.getItem().getAgreed());
//			}catch (Exception ex){
//				log.info("Incomplete User Data : "+ex.getMessage());
//			}
//			try {
//				usCitizenText.setValue(fatcaCRSDc.getItem().getDeclaration().g);
//			}catch (Exception ex){
//				log.info("Incomplete User Data : "+ex.getMessage());
//			}
		}catch (Exception ex){
			log.info("Incomplete User Data : "+ex.getMessage());
		}
	}
//	public void getKYCDetails() {
//
//		String queryuserTierRegistration = "select e from UserTier e " +
//				"where e.user.username = :username and e.currentTier = :currentTier " +
//				"order by e.createdDate desc";
//		UserTier userTierRegistration = this.dataManager.load(UserTier.class)
//				.query(queryuserTierRegistration)
//				.parameter("username", userProfileDl.getContainer().getItem().getUsername())
//				.parameter("currentTier",true)
//				.one();
//
//
////		String baseUrl = "http://acquaintme.milradius.com:8080/api";
//		String endpoint = "/rekognition/kycDetails";
//		String queryParam1 = "reference=";
//		String queryParam2 = userTierRegistration.getReference();
//
//		String url = environment.getProperty("acquaintme") + endpoint + "?" + queryParam1 + queryParam2;
//
//		try {
//			URL apiUrl = new URL(url);
//			HttpURLConnection connection = (HttpURLConnection) apiUrl.openConnection();
//			connection.setRequestMethod("GET");
//
//			int responseCode = connection.getResponseCode();
//			if (responseCode == HttpURLConnection.HTTP_OK) {
//				InputStream inputStream = connection.getInputStream();
//				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//				String inputLine;
//				StringBuffer response = new StringBuffer();
//
//				while ((inputLine = bufferedReader.readLine()) != null) {
//					response.append(inputLine);
//				}
//				bufferedReader.close();
//
//				// handle the response here
//				Gson gson = new Gson();
//				JsonObject jsonObject = gson.fromJson(response.toString(), JsonObject.class);
//				String document = String.valueOf(jsonObject.get("documents").getAsJsonArray());
//				List<JsonObject> jsonObjectSelfieList = new ArrayList<>();
//				List<JsonObject> jsonObjectFrontImgList = new ArrayList<>();
//				List<JsonObject> jsonObjectBackImgList = new ArrayList<>();
//				JsonObject[] jsonObjectArray = gson.fromJson(document, JsonObject[].class);
//
//				for (JsonObject jsonObject1 : jsonObjectArray) {
//					String docName = jsonObject1.get("docName").getAsString();
//					if (docName.equals("faceImg")) {
//						jsonObjectSelfieList.add(jsonObject1);
//					}
//				}
//
//				for (JsonObject jsonObject1 : jsonObjectArray) {
//					String docName = jsonObject1.get("docName").getAsString();
//					if (docName.equals("documentImgFront")) {
//						jsonObjectFrontImgList.add(jsonObject1);
//					}
//				}
//
//				for (JsonObject jsonObject1 : jsonObjectArray) {
//					String docName = jsonObject1.get("docName").getAsString();
//					if (docName.equals("documentImgBack")) {
//						jsonObjectBackImgList.add(jsonObject1);
//					}
//				}
//
//				String keySelfie = jsonObjectSelfieList.get(0).get("key").getAsString();
//				String keyFrontImg =jsonObjectFrontImgList.get(0).get("key").getAsString();
//				String keyBackImg = jsonObjectBackImgList.get(0).get("key").getAsString();
//				String similarityPercent = String.valueOf(jsonObject.get("similarityPercent"));
//
//				getOriginalImage(keySelfie);
//				getKYCImage(keyFrontImg);
//
//				similarityKYC.setValue(similarityPercent+"%");
//
//			}else {  // handle the error response here
//				notifications.create()
//						.withContentMode(ContentMode.HTML)
//						.withType(Notifications.NotificationType.WARNING)
//						.withPosition(Notifications.Position.MIDDLE_CENTER)
//						.withCaption("Images for KYC not found")
//						.show();
//			}
//
//		} catch (IOException e) {
//			log.error("Error :", e.getMessage());
//		}
//	}
//	public void getOriginalImage(String key) throws IOException {
//		originalImage.setSource(StreamResource.class)
//				.setStreamSupplier(() -> {
//					try {
//						return new ByteArrayInputStream(AmazonS3Bean.getObjectFromS3(key));
//					} catch (IOException e) {
//						throw new RuntimeException(e.getCause().getMessage());
//					}
//				});
//	}
//
//	public void getKYCImage(String key) throws IOException {
//		kycImage.setSource(StreamResource.class)
//				.setStreamSupplier(() -> {
//					try {
//						return new ByteArrayInputStream(AmazonS3Bean.getObjectFromS3(key));
//					} catch (IOException e) {
//						throw new RuntimeException(e.getCause().getMessage());
//					}
//				});
//	}

	@Install(to = "identificationNoField", subject = "formatter")
	private String identificationNoFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	@Install(to = "phoneNumberField", subject = "formatter")
	private String phoneNumberFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	@Install(to = "homeNumberField", subject = "formatter")
	private String homeNumberFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	@Install(to = "officeNumberField", subject = "formatter")
	private String officeNumberFieldFormatter(String value) {
		return cipherEncryptionManagement.decrypt(value,environment.getProperty("secret_key"));
	}

	@Install(to = "bankAccount", subject = "formatter")
	private String bankAccountFormatter(final Object value) {
		return cipherEncryptionManagement.decrypt(value.toString(),environment.getProperty("secret_key"));
	}

//	@Install(to = "dobField", subject = "formatter")
//	private String dobFieldFormatter(final Object value) {
//		return cipherEncryptionManagement.decrypt(value.toString(),environment.getProperty("secret_key"));
//	}

	private void hideElement()
	{
//		kycInfo.setVisible(false);
//		imageKYC.setVisible(false);
		verificationBox.setVisible(false);
//		idDetails.setVisible(false);
		country.setVisible(false);
		add1.setVisible(false);
		add2.setVisible(false);
		post.setVisible(false);
		stateCountry.setVisible(false);
		bankFN.setVisible(false);
		bankAN.setVisible(false);
		statusKYCAAANNN.setVisible(false);
		homeNoLine.setVisible(false);
		emailStatus.setVisible(true);
		residentialAddressContainer.setVisible(false);
		mailingAddressContainer.setVisible(false);
		employmentDetailsContainer.setVisible(false);
		bpfwsContainer.setVisible(false);
		bankInformationContainer.setVisible(false);
		bgdAccountInformationContainer.setVisible(false);
		onboardingApprovalContainer.setVisible(false);
		remarksContainer.setVisible(false);
//		dobField.setVisible(false);
		fatcaCRSContainer.setVisible(false);

		verificationBox.setEnabled(false);
		idDetails.setEnabled(false);
		country.setEnabled(false);
		add1.setEnabled(false);
		add2.setEnabled(false);
		post.setEnabled(false);
		stateCountry.setEnabled(false);
		bankFN.setEnabled(false);
		bankAN.setEnabled(false);
		statusKYCAAANNN.setEnabled(false);
	}

//	private void dislayPaynetValidation(){
//		Gson gson = new Gson();
//		try{
//			String queryintegrationService = "select e from IntegrationService e where e.code = :code " +
//					"and e.serviceName = :serviceName and e.createdBy = :createdBy order by e.createdDate Desc";
//			IntegrationService integrationService = this.dataManager.load(IntegrationService.class)
//					.query(queryintegrationService)
//					.parameter("code","ITR01/02")
//					.parameter("serviceName","Account Enquiry")
//					.parameter("createdBy", userProfileDc.getItem().getUsername())
//					.one();
//
//			JsonObject jsonObject = gson.fromJson(integrationService.getResponseBody(), JsonObject.class);
//			if(integrationService.getSuccess().equals(true)) {
//				String creditorAccountName = jsonObject.get("creditorAccountName").toString();
//				String creditorAccountType = jsonObject.get("creditorAccountType").toString();
//				String lookUpReference = jsonObject.get("lookUpReference").toString();
////				payNetVerification.setValue(integrationService.getSuccess().toString());
//				ambankResponse.setValue("Creditor Account Name : " + creditorAccountName + "\nCreditor Account Type : " + creditorAccountType + "\nLookup Reference :" + lookUpReference);
//			}else {
////				payNetVerification.setValue(integrationService.getSuccess().toString());
//				ambankResponse.setValue("Ambank Validation Error for respective user.\n" +
//						"Status: No Active User");
//			}
//		}catch (Exception exception){
//			log.info(exception.getMessage());
//			notifications.create()
//					.withContentMode(ContentMode.HTML)
//					.withType(Notifications.NotificationType.WARNING)
//					.withPosition(Notifications.Position.MIDDLE_CENTER)
//					.withCaption("PayNet Details of selected user could not be found")
//					.show();
//		}
//	}

	private UserTier getuserTier(){
		String query = "select e from UserTier e " +
				"where e.user = :user " +
				"and e.currentTier = :currentTier1";
		return this.dataManager.load(UserTier.class)
				.query(query)
				.parameter("currentTier1", "")
				.parameter("user", userProfileDc.getItem())
				.parameter("currentTier1", true)
				.one();
	}

	private void saveStatus(){
		try{
			String queryuserTier = "select e from UserTier e where e.user = :user and e.currentTier = :currentTier";
			UserTier userTier = this.dataManager.load(UserTier.class)
					.query(queryuserTier)
					.parameter("user", userProfileDc.getItem())
					.parameter("currentTier", true)
					.one();

			if(!Objects.requireNonNull(approveCheckBox.getValue()).getCode().equals("G09")){//reject

				userTier.setCurrentTier(false);

				UserTier userTier1 = this.dataManager.create(UserTier.class);
				userTier1.setUser(userTier.getUser());
				userTier1.setCurrentTier(true);
				userTier1.setKycStatus(approveCheckBox.getValue());
				userTier1.setTier(tierType("T00"));
				this.dataManager.save(userTier);
				this.dataManager.save(userTier1);
			}else{
//				UserTier userTier = this.dataManager.load(UserTier.class)
//						.query("select e from UserTier e where e.user = :user")
//						.parameter("user", userProfileDc.getItem())
//						.one();

				userTier.setKycStatus(kycStatus("G09"));
				userTier.setCurrentTier(true);
				this.dataManager.save(userTier);

				String queryuserTierZero = "select e from UserTier e where e.user = :user and e.tier.code = :tierCode";
				List<UserTier> userTierZero = this.dataManager.load(UserTier.class)
						.query(queryuserTierZero)
						.parameter("user", userProfileDc.getItem())
						.parameter("tierCode","T00")
						.list();

				userTierZero.forEach(userTier1 -> {
					userTier1.setCurrentTier(false);
					this.dataManager.save(userTier1);
				});

			}
			activityLogBean.makerActivity("MDL04-01", "LOG04-01", "",
					"UserProfile", userProfileDc.getItem().getId().toString(), remarksFieldId.getValue(), activityLogFilesDc.getItems());
//			if(accountBgd.getAccOwner().equals("BgdAdmin")) {

		}catch (Exception ex){
			log.error("Data Unable to save due to :" + ex.getMessage());
		}
	}

	private TierType tierType(String code){
		String query = "select e from TierType e where e.code = :code";
		return this.dataManager.load(TierType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	private KycStatus kycStatus(String code){
		String query = "select e from KycStatus e where e.code = :code";
		return this.dataManager.load(KycStatus.class)
				.query(query)
				.parameter("code",code)
				.one();
	}
	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		saveStatus();
		close(StandardOutcome.CLOSE);
	}

	@Install(to = "approveCheckBox", subject = "optionCaptionProvider")
	private String approveCheckBoxOptionCaptionProvider(KycStatus kycStatus) {
		if(kycStatus.getCode().equals("G09")) {
			return "Approved";
		}	else {
			return "Reject";
		}
	}

	@Subscribe("sendEmail")
	public void onSendEmailClick(Button.ClickEvent event) {

	}

	private void uploadMultipleFile() {
		fileUpload.addQueueUploadCompleteListener(queueUploadCompleteEvent -> {
			for (Map.Entry<UUID, String> entry : fileUpload.getUploadsMap().entrySet()) {
				UUID fileId = entry.getKey();
				String fileName = entry.getValue();
				FileRef fileRef = temporaryStorage.putFileIntoStorage(fileId, fileName);
				ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
				activityLogFile.setActivityLog(activityLogDc.getItem());
				activityLogFile.setName(fileName);
				activityLogFile.setFileUpload(fileRef);
				activityLogFile.setAction(rtType("ACT01"));

				activityLogFilesDc.getMutableItems().add(activityLogFile);
			}

			notifications.create()
					.withCaption("Uploaded files: " + fileUpload.getUploadsMap().values())
					.show();
		});
		fileUpload.addFileUploadErrorListener(queueFileUploadErrorEvent ->
				notifications.create()
						.withCaption("File upload error")
						.show());
	}
	private RtType rtType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

}
