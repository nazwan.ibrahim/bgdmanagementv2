package com.company.management.screen.transactionacct;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransactionAcct;

@UiController("AceWallet.edit")
@UiDescriptor("ace-wallet-edit.xml")
@EditedEntityContainer("transactionAcctDc")
public class AceWalletEdit extends StandardEditor<TransactionAcct> {
}