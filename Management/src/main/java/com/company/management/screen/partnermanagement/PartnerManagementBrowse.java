package com.company.management.screen.partnermanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.PartnerManagement;

@UiController("PartnerManagement.browse")
@UiDescriptor("partner-management-browse.xml")
@LookupComponent("partnerManagementsTable")
public class PartnerManagementBrowse extends StandardLookup<PartnerManagement> {
}