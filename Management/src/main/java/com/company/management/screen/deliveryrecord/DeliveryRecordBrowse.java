package com.company.management.screen.deliveryrecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecord;

@UiController("DeliveryRecord.browse")
@UiDescriptor("delivery-record-browse.xml")
@LookupComponent("deliveryRecordsTable")
public class DeliveryRecordBrowse extends StandardLookup<DeliveryRecord> {
}