package com.company.management.screen.partnermanagement;

import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.screen.userprofile.UserProfileEdit;
import io.jmix.core.DataManager;
import io.jmix.securitydata.entity.RoleAssignmentEntity;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Component;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.component.Table;
import io.jmix.ui.navigation.Route;
import io.jmix.ui.screen.*;
import com.company.management.entity.PartnerManagement;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("PartnerManagementV1.browse")
@UiDescriptor("partner-management-v1-browse.xml")
@LookupComponent("partnerManagementsTable")
@Route("users")
public class PartnerManagementV1Browse extends StandardLookup<PartnerManagement>{
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(PartnerManagementV1Browse.class);
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private GroupTable<PartnerManagement> partnerManagementsTable;


	private void goToEditPage(PartnerManagement partnerManagement) {
		PartnerManagementV1Edit partnerManagementV1Edit = screenBuilders.screen(this)
				.withScreenClass(PartnerManagementV1Edit.class)
				.build();
		partnerManagementV1Edit.setEntityToEdit(partnerManagement);
		partnerManagementV1Edit.show();
	}

	@Subscribe("partnerManagementsTable.create")
	public void onPartnerManagementsTableCreate(Action.ActionPerformedEvent event) {
		goToEditPage(this.dataManager.create(PartnerManagement.class));
	}

	@Subscribe("partnerManagementsTable.edit")
	public void onPartnerManagementsTableEdit(Action.ActionPerformedEvent event) {
		goToEditPage(partnerManagementsTable.getSingleSelected());
	}

//	@Install(to = "partnerManagementsTable.role", subject = "columnGenerator")
//	private Component partnerManagementsTableRoleColumnGenerator(PartnerManagement partnerManagement) {
//		if (partnerManagement.getUser().getUsername() == null) {
//			return new Table.PlainTextCell("");
//		} else {
//			try {
//				RoleAssignmentEntity roleAssignmentEntity = this.dataManager.load(RoleAssignmentEntity.class)
//						.query("select e from RoleAssignmentEntity e where e.username = :username")
//						.parameter("username", partnerManagement.getUser().getUsername())
//						.one();
//				return new Table.PlainTextCell(roleAssignmentEntity.getRoleType().toString());
//				return new Table.PlainTextCell("");
//			} catch (Exception ex) {
//				log.info(ex.getCause().getMessage().toString());
//				return new Table.PlainTextCell("");
//			}
			//return new Table.PlainTextCell("");
//		}
//	}
}