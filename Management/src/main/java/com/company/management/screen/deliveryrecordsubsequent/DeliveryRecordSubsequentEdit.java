package com.company.management.screen.deliveryrecordsubsequent;

import io.jmix.ui.screen.*;
import com.company.management.entity.DeliveryRecordSubsequent;

@UiController("DeliveryRecordSubsequent.edit")
@UiDescriptor("delivery-record-subsequent-edit.xml")
@EditedEntityContainer("deliveryRecordSubsequentDc")
public class DeliveryRecordSubsequentEdit extends StandardEditor<DeliveryRecordSubsequent> {
}