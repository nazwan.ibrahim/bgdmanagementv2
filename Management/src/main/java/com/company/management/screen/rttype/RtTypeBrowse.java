package com.company.management.screen.rttype;

import com.company.management.entity.RtTypeGroup;
import com.company.management.entity.SupportDoc;
import com.company.management.entity.TierManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.RtType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

@UiController("RtType.browse")
@UiDescriptor("rt-type-browse.xml")
@LookupComponent("rtTypesTable")
public class RtTypeBrowse extends StandardLookup<RtType> {

}