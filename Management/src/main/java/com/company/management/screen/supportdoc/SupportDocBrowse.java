package com.company.management.screen.supportdoc;

import com.company.management.entity.TierManagement;
import io.jmix.core.DataManager;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.DataContext;
import io.jmix.ui.screen.*;
import com.company.management.entity.SupportDoc;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("SupportDoc.browse")
@UiDescriptor("support-doc-browse.xml")
@LookupComponent("supportDocsTable")
public class SupportDocBrowse extends StandardLookup<SupportDoc> {

	private TierManagement tierManagement;
	@Autowired
	private GroupTable<SupportDoc> supportDocsTable;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private DataContext dataContext;

	public void setParentDataContext(DataContext parentDataContext) {
		dataContext.setParent(parentDataContext);
	}

	public TierManagement getTierManagement() {
		return tierManagement;
	}

	public void setTierManagement(TierManagement tierManagement) {
		this.tierManagement = tierManagement;
	}

	@Subscribe("selectDoc")
	public void onSelectDocClick(Button.ClickEvent event) {

		SupportDoc supportDoc = this.dataManager.create(SupportDoc.class);
		supportDoc.setTierManagement(getTierManagement());
		supportDoc.setType(supportDocsTable.getSingleSelected().getType());
		dataManager.save(supportDoc);

//		dataManager.save(supportDoc);
		close(StandardOutcome.CLOSE);

	}


}