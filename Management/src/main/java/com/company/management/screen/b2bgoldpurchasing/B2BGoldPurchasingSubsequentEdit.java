package com.company.management.screen.b2bgoldpurchasing;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;
import com.company.management.entity.wallet.AccountBgd;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionAcct;
import com.company.management.entity.wallet.TransactionType;
import com.google.common.collect.Sets;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.core.Messages;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.datatoolsui.screen.entityinspector.EntityInspectorBrowser;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.icon.Icons;
import io.jmix.ui.icon.JmixIcon;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@UiController("B2BGoldPurchasingSubsequent.edit")
@UiDescriptor("b2b-gold-purchasing-subsequent-edit.xml")
@EditedEntityContainer("b2BGoldPurchasingDc")
public class B2BGoldPurchasingSubsequentEdit extends StandardEditor<B2BGoldPurchasing> {

	private static final Logger log = LoggerFactory.getLogger(B2BGoldPurchasingSubsequentEdit.class);
	@Autowired
	private Notifications notifications;
	@Autowired
	private Button commitAndCloseBtn;
	@Autowired
	private VBoxLayout uploadFileZone;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private ButtonsPanel addButton;
	@Autowired
	private Icons icons;
	@Autowired
	private Messages messages;

	public String getTypeTransaction() {
		return typeTransaction;
	}

	public void setTypeTransaction(String typeTransaction) {
		this.typeTransaction = typeTransaction;
	}

	private String typeTransaction;
	@Autowired
	private InstanceLoader<B2BGoldPurchasing> b2BGoldPurchasingDl;
	@Autowired
	private CollectionLoader<B2BGoldPurchasingProduct> b2BGoldPurchasingProductsDl;
	@Autowired
	private InstanceContainer<B2BGoldPurchasing> b2BGoldPurchasingDc;
	@Autowired
	private CollectionLoader<B2BGoldPurchasingDoc> b2BGoldPurchasingDocsDl;
	@Autowired
	private Button commit;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<B2BGoldPurchasingProduct> b2BGoldPurchasingProductsDc;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private TimeSource timeSource;
	@Autowired
	private ManagementController managementController;
	private final BigDecimal total = BigDecimal.ZERO;

	private BigDecimal totalNotSave = BigDecimal.ZERO;

	private final BigDecimal totalSave = BigDecimal.ZERO;
	@Autowired
	private Dialogs dialogs;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		b2BGoldPurchasingDl.load();
		b2BGoldPurchasingProductsDl.setParameter("b2bGoldPurchasing1", b2BGoldPurchasingDc.getItem());
		b2BGoldPurchasingProductsDl.load();
		b2BGoldPurchasingDocsDl.setParameter("b2bGoldPurchasing1", b2BGoldPurchasingDc.getItem());
		b2BGoldPurchasingDocsDl.load();
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		addImport();
//		if(typeTransaction.equals("purchase")){
//			showDownloadFile();
//		}
		try {
			if (getEditedEntity().getStatus().getCode().equals("G01")) {
				commitAndCloseBtn.setEnabled(false);
				commitAndCloseBtn.setVisible(false);
				uploadFileZone.setVisible(false);
				uploadFileZone.setEnabled(false);
				commit.setEnabled(false);
				commit.setVisible(false);
			}
		} catch (Exception ex) {
			log.info("status Code Null due to create new object : " + ex.getMessage());
		}
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> checkPO()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private RtType loadType(String code) {
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	private void saveUploadDoc() {
		B2BGoldPurchasingDoc b2BGoldPurchasingDoc = this.dataManager.create(B2BGoldPurchasingDoc.class);
		b2BGoldPurchasingDoc.setB2bGoldPurchasing(b2BGoldPurchasingDc.getItem());
		b2BGoldPurchasingDoc.setFileUpload(uploadPurchaseForm.getValue());
		b2BGoldPurchasingDoc.setName(uploadPurchaseForm.getFileName());
		b2BGoldPurchasingDoc.setCreatedBy(currentAuthentication.getAuthentication().getName());
		b2BGoldPurchasingDoc.setCreatedDate(timeSource.currentTimestamp());
		this.dataManager.save(b2BGoldPurchasingDoc);
	}


	@Subscribe("b2BGoldSellingProductsTable.add")
	public void onB2BGoldSellingProductsTableAdd(Action.ActionPerformedEvent event) {
		B2BGoldPurchasingProduct b2BGoldPurchasingProduct = this.dataManager.create(B2BGoldPurchasingProduct.class);
		b2BGoldPurchasingProduct.setB2bGoldPurchasing(b2BGoldPurchasingDc.getItem());
		b2BGoldPurchasingProduct.setType(loadType("GT03"));
		b2BGoldPurchasingProduct.setQuantity(1);
		b2BGoldPurchasingProduct.setPurity(BigDecimal.valueOf(999.9));
		b2BGoldPurchasingProductsDc.getMutableItems().add(b2BGoldPurchasingProduct);
	}

	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> checkPO()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveData() {
		b2BGoldPurchasingProductsDc.getItems().forEach(b2BGoldPurchasingProduct -> {
			if (b2BGoldPurchasingProduct.getCode() != null) {
				Integer qty = b2BGoldPurchasingProduct.getQuantity();
				BigDecimal goldGram = BigDecimal.valueOf(1000);
				BigDecimal calculate = goldGram.multiply(BigDecimal.valueOf(qty));
				totalNotSave = totalNotSave.add(calculate);
				getEditedEntity().setPhysical_stock(totalNotSave);
			}
		});

		if (typeTransaction.equals("purchase")) {
			getEditedEntity().setType(loadType("ITF/03"));
			getEditedEntity().setPhysical_stock(totalNotSave);
		} else {
			getEditedEntity().setType(loadType("ITF/04"));
			getEditedEntity().setPhysical_stock(totalNotSave.negate());
		}

		this.dataManager.save(getEditedEntity());

		b2BGoldPurchasingProductsDc.getItems().forEach(e -> {
			this.dataManager.save(e);
		});

		if (uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		if (getEditedEntity().getStatus().getCode().equals("G01")) {
			addtogoldWallet();
		}
		close(StandardOutcome.DISCARD);
	}

	private void checkPO() {
//		Pattern regex = Pattern.compile("[~!@#$%^&*()+_=-{}?<>.,;'|]");
		Pattern regex = Pattern.compile("[$&+,:;=?@#|'<>.^*()%!-]");
		Matcher matcherDesc = regex.matcher(getEditedEntity().getDescription());
		Matcher matcherPO = regex.matcher(getEditedEntity().getPo_No());

		if(matcherDesc.find()){
			notifications.create(Notifications.NotificationType.ERROR)
					.withCaption("Description contain special character")
					.show();
		} else if (getEditedEntity().getPo_No() == null || matcherPO.find()) {
			notifications.create().withCaption("PO No. is required and cannot contain special character").withType(Notifications.NotificationType.ERROR).show();
		} else {
			saveData();
		}
	}

	@Install(to = "b2BGoldSellingProductsTable.sub_total", subject = "valueProvider")
	private Object b2BGoldSellingProductsTableSub_totalValueProvider(B2BGoldPurchasingProduct b2BGoldPurchasingProduct) {
		BigDecimal price = b2BGoldPurchasingProduct.getPrice();
		return price;
	}

	private void addtogoldWallet() {
		String queryaccountBgd = "select a from AccountBgd a " +
				"where a.accOwner = :accOwner1 " +
				"and a.walletType.code = :walletTypeCode1";
		AccountBgd accountBgd = this.dataManager.load(AccountBgd.class)
				.query(queryaccountBgd).parameter("accOwner1", "AceAdmin").parameter("walletTypeCode1", "E02").one();

		TransactionAcct transactionAcct = this.dataManager.create(TransactionAcct.class);
		String querytransStatus = "select t from TransStatus t " +
				"where t.code = :code1";
		TransStatus transStatus = this.dataManager.load(TransStatus.class)
				.query(querytransStatus).parameter("code1", "G01")
				.one();

		transactionAcct.setAccount(accountBgd);

		transactionAcct.setTransType(type("F01"));
		String reference = managementController.generateSeqNo("BGDF01", "6");
		transactionAcct.setReference(reference);
		if (typeTransaction.equals("purchase")) {
			transactionAcct.setAmount(totalNotSave);
		} else {
			transactionAcct.setAmount(totalNotSave.negate());
		}

		transactionAcct.setTransStatus(transStatus);
		transactionAcct.setCreatedBy(accountBgd.getAccOwner());
		transactionAcct.setCreatedDate(new Date());

		this.dataManager.save(transactionAcct);
	}

	private TransactionType type(String code) {
		String query = "select t from TransactionType t " +
				"where t.code = :code1";
		return this.dataManager.load(TransactionType.class)
				.query(query).parameter("code1", code)
				.one();
	}

	@Install(to = "b2BGoldPurchasingDocsTable.fileUpload", subject = "columnGenerator")
	private Component b2BGoldPurchasingDocsTableFileUploadColumnGenerator(B2BGoldPurchasingDoc b2BGoldPurchasingDoc) {
		if (b2BGoldPurchasingDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(b2BGoldPurchasingDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(b2BGoldPurchasingDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	private void addImport() {
		FileStorageUploadField importUpload = this.uiComponents.create(FileStorageUploadField.class);
		importUpload.setPermittedExtensions(Sets.newHashSet(".json", ".zip", ".xls", ".xlsx"));
		importUpload.setUploadButtonIcon(this.icons.get(JmixIcon.UPLOAD));
		importUpload.setUploadButtonCaption(this.messages.getMessage(EntityInspectorBrowser.class, "import"));
		importUpload.addFileUploadSucceedListener((fileUploadSucceedEvent) -> {
			XSSFWorkbook workbook = null;
			try {
				workbook = new XSSFWorkbook(Objects.requireNonNull(importUpload.getFileContent()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			XSSFSheet worksheet = workbook.getSheetAt(0);

			for (int index = 0; index < 101; index++) {
				if (index > 0) {
					B2BGoldPurchasingProduct b2BGoldPurchasingProduct = dataManager.create(B2BGoldPurchasingProduct.class);

					XSSFRow row = worksheet.getRow(index);
//					if(typeTransaction.equals("buy")){
					if (loadType(row.getCell(2).getStringCellValue()).getCode().equals("GT01")) {
						b2BGoldPurchasingProduct.setB2bGoldPurchasing(getEditedEntity());
						b2BGoldPurchasingProduct.setCode(row.getCell(0).getStringCellValue());
						b2BGoldPurchasingProduct.setSerial_number(row.getCell(1).getStringCellValue());

						b2BGoldPurchasingProduct.setType(loadType(row.getCell(2).getStringCellValue()));
						b2BGoldPurchasingProduct.setPurity(BigDecimal.valueOf(row.getCell(3).getNumericCellValue()));
						b2BGoldPurchasingProduct.setQuantity((int) row.getCell(4).getNumericCellValue());
						b2BGoldPurchasingProduct.setPrice(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()));


						b2BGoldPurchasingProductsDc.getMutableItems().add(b2BGoldPurchasingProduct);
					} else {
						notifications.create(Notifications.NotificationType.ERROR)
								.withCaption("Wrong Gold Type File Upload for Serial Number : " + row.getCell(1).getStringCellValue())
								.show();
					}
				}
			}
		});
		addButton.add(importUpload);
	}

	private void showDownloadFile() {
		Path path = Paths.get("fileformat/BGD Issuance Gold Bar (Supplier).xlsx");
//		if(typeTransaction.equals("purchase")){
//
//		}else{
//			path = Paths.get("fileformat/BGD Issuance Gold Dinar.xlsx");
//		}
		FileRef fileRef = new FileRef("fs", path.toString(), path.getFileName().toString());
		LinkButton linkButton = uiComponents.create(LinkButton.class);
		linkButton.setAlignment(Component.Alignment.BOTTOM_CENTER);
		linkButton.setCaption(path.getFileName().toString());
		linkButton.setIcon(this.icons.get(JmixIcon.DOWNLOAD));
		linkButton.setAction(new BaseAction("download")
				.withCaption(fileRef.getFileName())
				.withHandler(actionPerformedEvent ->
						downloader.download(fileRef)
				)
		);
		addButton.add(linkButton);
	}

	@Subscribe("uploadPurchaseForm")
	public void onUploadPurchaseFormValueChange(HasValue.ValueChangeEvent<FileRef> event) {
		try {
			String fileName = Objects.requireNonNull(event.getValue()).getFileName();
			// Check if the file name contains multiple extensions
			if (fileName.contains("..") || fileName.indexOf('.') != fileName.lastIndexOf('.')) {
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.WARNING)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Invalid file upload. Please use the correct file")
						.show();
				uploadPurchaseForm.clear();
			}
		} catch (Exception ex) {
			log.info(ex.getMessage());
		}
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDiscard();
	}
}