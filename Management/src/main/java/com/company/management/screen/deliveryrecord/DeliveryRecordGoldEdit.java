package com.company.management.screen.deliveryrecord;

import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.FileRef;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Objects;

@UiController("DeliveryRecordGold.edit")
@UiDescriptor("delivery-record-gold-edit.xml")
@EditedEntityContainer("deliveryRecordDc")
public class DeliveryRecordGoldEdit extends StandardEditor<DeliveryRecord> {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(DeliveryRecordGoldEdit.class);
	@Autowired
	private InstanceLoader<DeliveryRecord> deliveryRecordDl;
	@Autowired
	private CollectionLoader<PurchaseRecordProduct> purchaseRecordProductsDl;
	@Autowired
	private InstanceContainer<DeliveryRecord> deliveryRecordDc;
	@Autowired
	private CollectionLoader<PurchaseRecordSuppDoc> purchaseRecordSuppDocsDl;
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private CollectionLoader<DeliveryRecordDoc> deliveryRecordDocsDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private FileStorageUploadField uploadPurchaseForm;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private Button commit;
	@Autowired
	private Label<String> labelPurchase;
	@Autowired
	private GroupTable<PurchaseRecordProduct> purchaseRecordProductsTable;
	@Autowired
	private GroupTable<PurchaseRecordSuppDoc> fileUploadTable;
	@Autowired
	private Label labelDelivery;
	@Autowired
	private VBoxLayout uploadFileZone;
	@Autowired
	private GroupTable<DeliveryRecordDoc> fileUploadDelivery;
	@Autowired
	private TextField<String> location;
	@Autowired
	private TextField<String> destination;
	@Autowired
	private TextField<String> delireceiptNo;
	@Autowired
	private DateField<Date> dateReceipt;
	@Autowired
	private Button commitAndCloseBtn;
	@Autowired
	private Notifications notifications;

	public String getDisableElement() {
		return disableElement;
	}

	public void setDisableElement(String disableElement) {
		this.disableElement = disableElement;
	}

	private String disableElement;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		deliveryRecordDl.load();
		purchaseRecordProductsDl.setParameter("purchase_record1", deliveryRecordDc.getItem().getPurchaseRecord());
		purchaseRecordProductsDl.load();
		purchaseRecordSuppDocsDl.setParameter("purchase_record1", deliveryRecordDc.getItem().getPurchaseRecord());
		purchaseRecordSuppDocsDl.load();
		deliveryRecordDocsDl.setParameter("deliveryRecordPurchaseRecord1", deliveryRecordDc.getItem().getPurchaseRecord());
		deliveryRecordDocsDl.load();
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		if(getEditedEntity().getStatus().getCode().equals("G01")){
			commitAndCloseBtn.setVisible(false);
			commit.setVisible(false);
			commitAndCloseBtn.setEnabled(false);
			commit.setEnabled(false);
			uploadFileZone.setVisible(false);
		}
	}



	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(PurchaseRecordSuppDoc purchaseRecordSuppDoc) {
		if (purchaseRecordSuppDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(purchaseRecordSuppDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(purchaseRecordSuppDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	@Subscribe("commitAndCloseBtn")
	public void onCommitAndCloseBtnClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Delivery Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private RtType loadType(String code){
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	private void saveUploadDoc(){
		DeliveryRecordDoc deliveryRecordDoc = this.dataManager.create(DeliveryRecordDoc.class);
		deliveryRecordDoc.setDeliveryRecord(deliveryRecordDc.getItem());
		deliveryRecordDoc.setUploadFile(uploadPurchaseForm.getValue());
		deliveryRecordDoc.setName(uploadPurchaseForm.getFileName());
		this.dataManager.save(deliveryRecordDoc);
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Delivery Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveData(){
		dataManager.save(getEditedEntity());
		if(uploadPurchaseForm.getValue() != null) {
			saveUploadDoc();
		}
		closeWithDiscard();
	}

	@Install(to = "fileUploadDelivery.uploadFile", subject = "columnGenerator")
	private Component fileUploadDeliveryUploadFileColumnGenerator(DeliveryRecordDoc deliveryRecordDoc) {
		if (deliveryRecordDoc.getUploadFile() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(deliveryRecordDoc.getUploadFile().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(deliveryRecordDoc.getUploadFile())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}
	private void hideElement(){
		purchaseRecordProductsTable.setEnabled(false);
		labelPurchase.setVisible(false);
		fileUploadTable.setVisible(false);
		fileUploadTable.setEnabled(false);
		labelDelivery.setVisible(false);
		uploadFileZone.setVisible(false);
		fileUploadDelivery.setVisible(false);
		location.setEditable(false);
		destination.setEditable(false);
		delireceiptNo.setEditable(false);
		dateReceipt.setEditable(false);
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		closeWithDiscard();
	}

	@Subscribe("uploadPurchaseForm")
	public void onUploadPurchaseFormValueChange(HasValue.ValueChangeEvent<FileRef> event) {
		try {
			String fileName = Objects.requireNonNull(event.getValue()).getFileName();
			// Check if the file name contains multiple extensions
			if (fileName.contains("..") || fileName.indexOf('.') != fileName.lastIndexOf('.')) {
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.WARNING)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Invalid file upload. Please use the correct file")
						.show();
				uploadPurchaseForm.clear();
			}
		}catch (Exception ex){
			log.info(ex.getMessage());
		}
	}
}