package com.company.management.screen.reserverecord;

import com.company.management.entity.DeliveryRecordSubsequent;
import com.company.management.entity.ReserveRecordProduct;
import com.google.api.client.util.DateTime;
import io.jmix.ui.Notifications;
import io.jmix.ui.component.ContentMode;
import io.jmix.ui.component.DateField;
import io.jmix.ui.component.TextField;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.ReserveRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@UiController("ReserveRecordGoldBarInventory.edit")
@UiDescriptor("reserve-record-goldBar-inventory-edit.xml")
@EditedEntityContainer("reserveRecordDc")
public class ReserveRecordGoldBarInventoryEdit extends StandardEditor<ReserveRecord> {
	private static final Logger log = LoggerFactory.getLogger(ReserveRecordGoldBarInventoryEdit.class);
	@Autowired
	private CollectionLoader<DeliveryRecordSubsequent> deliveryRecordSubsequentsDl;
	@Autowired
	private CollectionLoader<ReserveRecordProduct> reserveRecordProductsDl;
	@Autowired
	private InstanceLoader<ReserveRecord> reserveRecordDl;
	@Autowired
	private InstanceContainer<ReserveRecord> reserveRecordDc;
	@Autowired
	private CollectionContainer<DeliveryRecordSubsequent> deliveryRecordSubsequentsDc;
	@Autowired
	private TextField<String> location;
	@Autowired
	private TextField<String> destination;
	@Autowired
	private TextField<String> delireceiptNo;
	@Autowired
	private DateField<Date> dateReceipt;
	@Autowired
	private Notifications notifications;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		reserveRecordDl.load();
		reserveRecordProductsDl.setParameter("reverse_record1",reserveRecordDc.getItem());
		reserveRecordProductsDl.load();
		deliveryRecordSubsequentsDl.setParameter("reserveRecord1",reserveRecordDc.getItem());
		deliveryRecordSubsequentsDl.load();
	}
	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		try{
			location.setValue(deliveryRecordSubsequentsDc.getItems().get(0).getLocation());
			destination.setValue(deliveryRecordSubsequentsDc.getItems().get(0).getDestination());
			delireceiptNo.setValue(deliveryRecordSubsequentsDc.getItems().get(0).getDeliveryReceipt());
			dateReceipt.setValue(deliveryRecordSubsequentsDc.getItems().get(0).getCreatedDate());
		} catch (Exception ex){
//			log.info(ex.getCause().getMessage());
			log.info("Incomplete User Data");
			notifications.create()
					.withContentMode(ContentMode.HTML)
					.withType(Notifications.NotificationType.WARNING)
					.withPosition(Notifications.Position.MIDDLE_CENTER)
					.withCaption("Incomplete Data for Delivery Record")
					.show();
		}
	}
}