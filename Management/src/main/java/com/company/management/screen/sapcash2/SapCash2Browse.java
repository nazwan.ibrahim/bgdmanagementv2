package com.company.management.screen.sapcash2;

import com.company.management.entity.SapFee2;
import com.company.management.screen.sapfee2.SapFee2Browse;
import io.jmix.core.TimeSource;
import io.jmix.ui.component.Button;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.screen.*;
import com.company.management.entity.SapCash2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.concurrent.atomic.AtomicInteger;

@UiController("SapCash2.browse")
@UiDescriptor("sap-cash2-browse.xml")
@LookupComponent("sapCash2sTable")
public class SapCash2Browse extends StandardLookup<SapCash2> {
	@Autowired
	private CollectionContainer<SapCash2> sapCash2sDc;
	private static final Logger log = LoggerFactory.getLogger(SapFee2Browse.class);
	@Autowired
	private TimeSource timeSource;
	BigDecimal sumAmountnSst = BigDecimal.ZERO;
	BigDecimal sumAllTrans = BigDecimal.ZERO;
	BigDecimal sumAllTrans1 = BigDecimal.ZERO;

	int incrementNum;
	AtomicInteger x=new AtomicInteger(1);
	@Subscribe("downloadBtn")
	public void onDownloadBtnClick(final Button.ClickEvent event) throws FileNotFoundException {
		String dateFormat = "yyyyMM";
		String dateFormatTxt = "yyyyMMdd";
		SimpleDateFormat dateSimpleformatter = new SimpleDateFormat(dateFormat);
		SimpleDateFormat dateSimpleformatterTxt = new SimpleDateFormat(dateFormatTxt);
		String path = String.format("C:\\Users\\amir\\Downloads\\CASH_%s.txt", dateSimpleformatter.format(timeSource.currentTimestamp()));
		BigDecimal totalAmount = BigDecimal.ZERO;
		BigDecimal totalSst = BigDecimal.ZERO;
//		String path="C:\\Users\\amir\\Downloads\\test.txt";
		BufferedWriter bw = new BufferedWriter (new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
		try (CSVPrinter csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT)) {
			sapCash2sDc.getMutableItems().forEach(a -> {
				String getDate = dateSimpleformatterTxt.format(a.getTransactionDate());
				String getCreatedDate = getDate != null ? getDate : "";
				String getTypeCode = a.getTypeFee() != null ? a.getTypeFee(): "";
				BigDecimal getAmount = a.getAmount() != null ? a.getAmount() : BigDecimal.ZERO;
				try {
					incrementNum = x.getAndIncrement();
					csvPrinter.printRecord(String.format("%06d", incrementNum),getCreatedDate,getTypeCode,getAmount.setScale(2, RoundingMode.UNNECESSARY));
				} catch (IOException e) {
//					log.error("IOException Error :"+e);
					log.info("Error While writing CSV ", e);
				}

			});
			for(SapCash2 sapCash2 : sapCash2sDc.getItems()){
				totalAmount = totalAmount.add(sapCash2.getAmount() != null ? sapCash2.getAmount() : BigDecimal.ZERO);
			}
			csvPrinter.printRecord(String.format("%06d", incrementNum),null,null,totalAmount.setScale(2,RoundingMode.UNNECESSARY));
		} catch (IOException e) {
			log.error("Error While writing CSV ", e);
		}
	}
}