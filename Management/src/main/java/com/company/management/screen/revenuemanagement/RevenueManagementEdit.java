package com.company.management.screen.revenuemanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.RevenueManagement;

@UiController("RevenueManagement.edit")
@UiDescriptor("revenue-management-edit.xml")
@EditedEntityContainer("revenueManagementDc")
public class RevenueManagementEdit extends StandardEditor<RevenueManagement> {
}