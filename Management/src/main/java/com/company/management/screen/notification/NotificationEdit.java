package com.company.management.screen.notification;

import io.jmix.ui.screen.*;
import com.company.management.entity.registrationdb.Notification;

@UiController("Notification.edit")
@UiDescriptor("notification-edit.xml")
@EditedEntityContainer("notificationDc")
public class NotificationEdit extends StandardEditor<Notification> {
}