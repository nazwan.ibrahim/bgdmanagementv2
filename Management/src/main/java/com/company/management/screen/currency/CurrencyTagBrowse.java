package com.company.management.screen.currency;

import io.jmix.ui.screen.*;
import com.company.management.entity.Currency;

@UiController("CurrencyTag.browse")
@UiDescriptor("currency-tag-browse.xml")
@LookupComponent("currenciesTable")
public class CurrencyTagBrowse extends StandardLookup<Currency> {


}