package com.company.management.screen.createannouncement;

import com.company.management.entity.Announcement;
import com.company.management.entity.registrationdb.NotificationDetail;
import com.company.management.service.NotificationService;
import io.jmix.core.DataManager;
import io.jmix.ui.Dialogs;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.TextArea;
import io.jmix.ui.component.TextField;
import io.jmix.ui.screen.Screen;
import io.jmix.ui.screen.Subscribe;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("CreateAnnouncement")
@UiDescriptor("create-announcement.xml")
public class CreateAnnouncement extends Screen {

    private final Logger logger = LogManager.getLogger(CreateAnnouncement.class);

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private TextArea bodyArea;

    @Autowired
    private TextField titleField;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private Button saveButton;
    @Autowired
    private DataManager dataManager;
    @Autowired
    private TextField dataUrl;

    @Subscribe("saveButton")
    public void onSaveButtonClick(Button.ClickEvent event) {
        if(bodyArea.getRawValue().equals("") || titleField.getRawValue().equals("")) {
            dialogs.createMessageDialog()
//                    .withCaption("Input validation")
                    .withMessage("Please key in title and body")
                    .show();

        }else{
            saveButton.setEnabled(false);
            dialogs.createOptionDialog()
                    .withCaption("Please confirm")
                    .withMessage("A push notification will be sent to all registered device")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES, Action.Status.PRIMARY)
                                    .withDescription("A push notification will be sent to all registered device")
                                    .withHandler(e -> {
                                        Announcement announcement = dataManager.create(Announcement.class);
                                        NotificationDetail notificationDetail = notificationService.createAnnouncement(bodyArea.getRawValue(), titleField.getRawValue(), dataUrl.getRawValue());
                                        if (notificationDetail != null) {
                                            Boolean createNotification = notificationService.createNotification(notificationDetail);
                                            if (createNotification) {
                                                notificationService.pushNotification(notificationDetail, "announcement");
                                                announcement.setNotificationDetail(notificationDetail);
                                                dataManager.save(announcement);
                                                titleField.setValue("");
                                                bodyArea.setValue("");


                                            } else {
                                                logger.info("Cant create notification for all user");
                                            }
                                        } else {
                                            logger.info("Cant Create Notification Detail");
                                        }
                                    }),
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .show();
            saveButton.setEnabled(true);

        }
    }

}