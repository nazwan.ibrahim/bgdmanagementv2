package com.company.management.screen.shariahrecordsubsequent;

import io.jmix.ui.screen.*;
import com.company.management.entity.ShariahRecordSubsequent;

@UiController("ShariahRecordSubsequent.edit")
@UiDescriptor("shariah-record-subsequent-edit.xml")
@EditedEntityContainer("shariahRecordSubsequentDc")
public class ShariahRecordSubsequentEdit extends StandardEditor<ShariahRecordSubsequent> {
}