package com.company.management.screen.transactionhistoriesview;

import com.company.management.entity.ActivityLog;
import com.company.management.screen.transactionacct.AdjustmentCashEdit;
import com.company.management.screen.transactionacct.AdjustmentGoldEdit;
import com.company.management.screen.transactionacct.TransferCashEdit;
import com.company.management.screen.transactionacct.TransferGoldEdit;
import io.jmix.core.DataManager;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.*;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.TransactionHistoriesView;
import io.jmix.ui.screen.LookupComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;

@UiController("InvestorWalletView.browse")
@UiDescriptor("investor-wallet-browse.xml")
@LookupComponent("transactionHistoriesViewsTable")
public class InvestorWalletBrowse extends StandardLookup<TransactionHistoriesView> {
	private static final Logger log = LoggerFactory.getLogger(InvestorWalletBrowse.class);
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;
	DecimalFormat df = new DecimalFormat("#,###,##0.000000");
	@Autowired
	private DataManager dataManager;
	@Autowired
	private Label<BigDecimal> totalCash;
	@Autowired
	private Label<String> goldBar;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private Table<TransactionHistoriesView> transactionAcctsTable;
	@Autowired
	private Table<TransactionHistoriesView> transactionHistoriesViewsTable;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsGoldDl;
	@Autowired
	private CollectionLoader<ActivityLog> activityLogsCashDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		setOnBeforeShow();
	}

	private void setOnBeforeShow(){
		transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
		transactionHistoriesViewsDl.setParameter("accOwner2","AceAdmin");
		transactionHistoriesViewsDl.setParameter("walletCode1","E01");
		transactionHistoriesViewsDl.load();

		activityLogsGoldDl.setParameter("moduleCode1","MDL03-D02-E02");
		activityLogsGoldDl.load();

		activityLogsCashDl.setParameter("moduleCode1","MDL03-D02-E01");
		activityLogsCashDl.load();
		try{
			getTotalCashInvestor();
			getTotalGoldInvestor();
		}catch (Exception ex){
			log.info(ex.toString());
		}
	}

	/** To change column status color **/
	@Install(to = "transactionAcctsTable", subject = "styleProvider")
	protected String transactionTableStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}



	@Install(to = "transactionHistoriesViewsTable", subject = "styleProvider")
	protected String transactionTableGoldStyleProvider(TransactionHistoriesView transactionHistoriesView, String property) {
		if (property == null){

		}else if (property.equals("status")){
			switch (transactionHistoriesView.getStatusCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Subscribe("freeTab")
	public void onFreeTabSelectedTabChange(TabSheet.SelectedTabChangeEvent event) {
		if(event.getSelectedTab().getName().equals("goldAccount")){
			transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
			transactionHistoriesViewsDl.setParameter("accOwner2","AceAdmin");
			transactionHistoriesViewsDl.setParameter("walletCode1","E02");
			transactionHistoriesViewsDl.load();
		}else{
			transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
			transactionHistoriesViewsDl.setParameter("accOwner2","AceAdmin");
			transactionHistoriesViewsDl.setParameter("walletCode1","E01");
			transactionHistoriesViewsDl.load();
		}
	}

	@Install(to = "transactionAcctsTable.amount", subject = "formatter")
	private String transactionAcctsTableAmountFormatter(Object object) {
		if(object != null){
			return df.format(object);
		}
		return (String) object;
	}

	@Install(to = "transactionHistoriesViewsTable.amount", subject = "formatter")
	private String transactionHistoriesViewsTableAmountFormatter(Object object) {
		if(object != null){
			return df.format(object);
		}
		return (String) object;
	}

	private void getTotalCashInvestor(){
		String query = "select SUM(e.availableAmount) from AccountWalletView e where e.accountOwner <> :accountOwner1 " +
				"and e.accountOwner <> :accountOwner2 and e.code = :code";
		BigDecimal totalCashInvestor = this.dataManager.loadValue(query,BigDecimal.class)
				.store("wallet")
				.parameter("accountOwner1","BgdAdmin")
				.parameter("accountOwner2","AceAdmin")
				.parameter("code","E01")
				.one();
		totalCash.setValue(totalCashInvestor);
	}

	private void getTotalGoldInvestor(){
		String query = "select SUM(e.availableAmount) from AccountWalletView e where e.accountOwner <> :accountOwner1 " +
				"and e.accountOwner <> :accountOwner2 and e.code = :code";
		BigDecimal totalGold = this.dataManager.loadValue(query, BigDecimal.class)
				.store("wallet")
				.parameter("accountOwner1","BgdAdmin")
				.parameter("accountOwner2","AceAdmin")
				.parameter("code","E02")
				.one();
		goldBar.setValue(df.format(totalGold));
	}

	@Subscribe("transferCash")
	public void onTransferCashClick(final Button.ClickEvent event) {
		TransferCashEdit transferCashEdit = screenBuilders.screen(this)
				.withScreenClass(TransferCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferCashEdit.setAccountOwner("Investor");
		transferCashEdit.show();
	}

	@Subscribe("adjustmentCash")
	public void onAdjustmentCashClick(final Button.ClickEvent event) {
		AdjustmentCashEdit adjustmentCashEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentCashEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentCashEdit.setAccountOwner("Investor");
		adjustmentCashEdit.show();
	}

	@Subscribe("transferGold")
	public void onTransferGoldClick(final Button.ClickEvent event) {
		TransferGoldEdit transferGoldEdit = screenBuilders.screen(this)
				.withScreenClass(TransferGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		transferGoldEdit.setAccountOwner("Investor");
		transferGoldEdit.show();
	}

	@Subscribe("adjustmentGold")
	public void onAdjustmentGoldClick(final Button.ClickEvent event) {
		AdjustmentGoldEdit adjustmentGoldEdit = screenBuilders.screen(this)
				.withScreenClass(AdjustmentGoldEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.withOpenMode(OpenMode.THIS_TAB)
				.build();
		adjustmentGoldEdit.setAccountOwner("Investor");
		adjustmentGoldEdit.show();
	}

	@Subscribe("transactionAcctsTable.view")
	public void onTransactionAcctsTableView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionAcctsTable.getSingleSelected()));
		screen.show();
	}
	@Subscribe("transactionHistoriesViewsTable.view")
	public void onTransactionHistoriesViewsTableView(final Action.ActionPerformedEvent event) {
		TransactionApproveViewEdit screen = screenBuilders.screen(this)
				.withScreenClass(TransactionApproveViewEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					setOnBeforeShow();
				})
				.build();
		screen.setEntityToEdit(Objects.requireNonNull(transactionHistoriesViewsTable.getSingleSelected()));
		screen.show();
	}

	@Install(to = "activityLogInvestorGoldTable", subject = "styleProvider")
	private String activityLogInvestorGoldTableStyleProvider(final ActivityLog entity, final String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (entity.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}

	@Install(to = "activityLogInvestorCashTable", subject = "styleProvider")
	private String activityLogInvestorCashTableStyleProvider(final ActivityLog entity, final String property) {
		if (property == null){

		}else if (property.equals("status.name")){
			switch (entity.getStatus().getCode()){
				case "G01", "G04","G06", "G09":
					return "green-status";
				case "G02", "G07", "G10", "G13", "G14", "G18", "G20", "G21", "G22", "G23":
					return "red-status";
				case "G04/02", "G05", "G05/01", "G05/02", "G08", "G11", "G12", "G16", "G17", "G18/02", "G19/02", "G20/02", "G21/02", "G22/02", "G23/02":
					return "yellow-status";
				case "G19", "G03", "G15":
					return "grey-status";
				case "G06/01":
					return "orange-status";
			}
		}
		return null;
	}


}