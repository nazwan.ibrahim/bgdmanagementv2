package com.company.management.screen.tierthreshold;

import io.jmix.ui.screen.*;
import com.company.management.entity.TierThreshold;

@UiController("TierThreshold.browse")
@UiDescriptor("tier-threshold-browse.xml")
@LookupComponent("tierThresholdsTable")
public class TierThresholdBrowse extends StandardLookup<TierThreshold> {
}