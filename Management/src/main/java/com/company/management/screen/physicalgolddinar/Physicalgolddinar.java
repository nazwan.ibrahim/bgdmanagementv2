package com.company.management.screen.physicalgolddinar;

import io.jmix.dashboardsui.annotation.DashboardWidget;
import io.jmix.ui.screen.ScreenFragment;
import io.jmix.ui.screen.UiController;
import io.jmix.ui.screen.UiDescriptor;

@UiController("Physicalgolddinar")
@UiDescriptor("physicalGoldDinar.xml")
@DashboardWidget(name = "Physicalgolddinar")
public class Physicalgolddinar extends ScreenFragment {
}