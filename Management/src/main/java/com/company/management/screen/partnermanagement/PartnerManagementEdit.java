package com.company.management.screen.partnermanagement;

import io.jmix.ui.screen.*;
import com.company.management.entity.PartnerManagement;

@UiController("PartnerManagement.edit")
@UiDescriptor("partner-management-edit.xml")
@EditedEntityContainer("partnerManagementDc")
public class PartnerManagementEdit extends StandardEditor<PartnerManagement> {
}