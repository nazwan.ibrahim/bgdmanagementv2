package com.company.management.screen.purchaserecord;

import com.company.management.api.ManagementController;
import com.company.management.entity.*;
import com.google.common.collect.Sets;
import io.jmix.core.*;
import io.jmix.datatoolsui.screen.entityinspector.EntityInspectorBrowser;
import io.jmix.ui.Dialogs;
import io.jmix.ui.Notifications;
import io.jmix.ui.UiComponents;
import io.jmix.ui.action.Action;
import io.jmix.ui.action.BaseAction;
import io.jmix.ui.action.DialogAction;
import io.jmix.ui.component.*;
import io.jmix.ui.download.Downloader;
import io.jmix.ui.icon.Icons;
import io.jmix.ui.icon.JmixIcon;
import io.jmix.ui.model.CollectionContainer;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.model.InstanceContainer;
import io.jmix.ui.model.InstanceLoader;
import io.jmix.ui.screen.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@UiController("BGDDemocratisation.edit")
@UiDescriptor("BGD-Democratisation-edit.xml")
@EditedEntityContainer("purchaseRecordDc")
public class BGDDemocratisationEdit extends StandardEditor<PurchaseRecord> {
	private static final Logger log = LoggerFactory.getLogger(BGDDemocratisationEdit.class);
	@Autowired
	private UiComponents uiComponents;
	@Autowired
	private Downloader downloader;
	@Autowired
	private CollectionLoader<PurchaseRecordProduct> purchaseRecordProductsDl;
	@Autowired
	private CollectionLoader<PurchaseRecordSuppDoc> purchaseRecordSuppDocsDl;
	@Autowired
	private InstanceContainer<PurchaseRecord> purchaseRecordDc;
	@Autowired
	private InstanceLoader<PurchaseRecord> purchaseRecordDl;
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CollectionContainer<PurchaseRecordProduct> purchaseRecordProductsDc;

	private String goldType;
	@Autowired
	private Button commit;
	@Autowired
	private GroupTable<PurchaseRecordProduct> purchaseRecordProductsTable;
	@Autowired
	private GroupTable<PurchaseRecordSuppDoc> fileUploadTable;
	@Autowired
	private HBoxLayout editActions;
	@Autowired
	private ButtonsPanel addButton;
	@Autowired
	private Label<String> labelRecord;
	@Autowired
	private Dialogs dialogs;
	@Autowired
	private FileStorageUploadField uploadDoc;
	@Autowired
	private VBoxLayout uploadFileZone;
	@Autowired
	private Notifications notifications;
	private BigDecimal physical = BigDecimal.ZERO;
	private BigDecimal tradable = BigDecimal.ZERO;
	@Autowired
	private ManagementController managementController;
	@Autowired
	private Label<String> labelGold;
	@Autowired
	private Button createDraft;
	@Autowired
	private Icons icons;
	@Autowired
	private Messages messages;
	@Autowired
	private FileStorageLocator fileStorageLocator;
	@Autowired
	private CollectionContainer<PurchaseRecordSuppDoc> purchaseRecordSuppDocsDc;

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	private String formCode;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		purchaseRecordDl.load();
		purchaseRecordProductsDl.setParameter("purchase_record1", purchaseRecordDc.getItem());
		purchaseRecordProductsDl.load();
		purchaseRecordSuppDocsDl.setParameter("purchase_record1", purchaseRecordDc.getItem());
		purchaseRecordSuppDocsDl.load();
		if (goldType.equals("disableALL")) {
			purchaseRecordProductsTable.setEditable(false);
			fileUploadTable.setVisible(false);
			editActions.setVisible(false);
			addButton.setVisible(false);
			labelRecord.setVisible(false);
			uploadDoc.setVisible(false);
			uploadFileZone.setVisible(false);
		}
	}

	@Subscribe
	public void onAfterShow(AfterShowEvent event) {
		addImport();
//		showDownloadFile();
		if (goldType.equals("goldBar")) {
			labelGold.setValue("Gold Bar Purchase Information");
		} else {
			labelGold.setValue("Gold Dinar Purchase Information");
		}

		try {
			if (getEditedEntity().getStatus().getCode().equals("G01") && getEditedEntity().getStatus().getCode() != null) {
				createDraft.setEnabled(false);
				commit.setEnabled(false);
				createDraft.setVisible(false);
				commit.setVisible(false);
				uploadFileZone.setVisible(false);
				addButton.setVisible(false);
				addButton.setEnabled(false);
			}
		} catch (Exception ex) {
			log.info(ex.getMessage());
		}
	}

	private void addImport() {
		FileStorageUploadField importUpload = this.uiComponents.create(FileStorageUploadField.class);
		importUpload.setPermittedExtensions(Sets.newHashSet(".xlsx"));
		importUpload.setUploadButtonIcon(this.icons.get(JmixIcon.UPLOAD));
		importUpload.setUploadButtonCaption(this.messages.getMessage(EntityInspectorBrowser.class, "import"));
		importUpload.addFileUploadSucceedListener((fileUploadSucceedEvent) -> {
			XSSFWorkbook workbook = null;
			try {
				workbook = new XSSFWorkbook(Objects.requireNonNull(importUpload.getFileContent()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			XSSFSheet worksheet = workbook.getSheetAt(0);

//			for (int index = 0; index < worksheet.getPhysicalNumberOfRows(); index++) {
			for (int index = 0; index < 101; index++) {
				if (index > 0) {
					PurchaseRecordProduct purchaseRecordProduct = dataManager.create(PurchaseRecordProduct.class);

					XSSFRow row = worksheet.getRow(index);
					if (goldType.equals("goldBar")) {
						if (loadType(row.getCell(2).getStringCellValue()).getCode().equals("GT01")) {
							purchaseRecordProduct.setPurchase_record(getEditedEntity());
							purchaseRecordProduct.setCode(row.getCell(0).getStringCellValue());
							purchaseRecordProduct.setSerial_number(row.getCell(1).getStringCellValue());

							purchaseRecordProduct.setType(loadType(row.getCell(2).getStringCellValue()));
							purchaseRecordProduct.setPurity(BigDecimal.valueOf(row.getCell(3).getNumericCellValue()));
							purchaseRecordProduct.setQuantity((int) row.getCell(4).getNumericCellValue());
							purchaseRecordProduct.setPrice(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()));


							purchaseRecordProductsDc.getMutableItems().add(purchaseRecordProduct);
						} else {
							notifications.create(Notifications.NotificationType.ERROR)
									.withCaption("Wrong Gold Type File Upload for Serial Number : " + row.getCell(1).getStringCellValue())
									.show();
						}
					} else {
						if (loadType(row.getCell(2).getStringCellValue()).getCode().equals("GT02")) {
							purchaseRecordProduct.setPurchase_record(getEditedEntity());
							purchaseRecordProduct.setCode(row.getCell(0).getStringCellValue());
							purchaseRecordProduct.setSerial_number(row.getCell(1).getStringCellValue());

							purchaseRecordProduct.setType(loadType(row.getCell(2).getStringCellValue()));
							purchaseRecordProduct.setPurity(BigDecimal.valueOf(row.getCell(3).getNumericCellValue()));
							purchaseRecordProduct.setQuantity((int) row.getCell(4).getNumericCellValue());
							purchaseRecordProduct.setPrice(BigDecimal.valueOf(row.getCell(5).getNumericCellValue()));


							purchaseRecordProductsDc.getMutableItems().add(purchaseRecordProduct);
						} else {
							notifications.create(Notifications.NotificationType.ERROR)
									.withCaption("Wrong Gold Type File Upload for Serial Number : " + row.getCell(1).getStringCellValue())
									.show();
						}
					}
				}
			}
		});
		addButton.add(importUpload);
	}

	@Install(to = "fileUploadTable.fileUpload", subject = "columnGenerator")
	private Component fileUploadTableFileUploadColumnGenerator(PurchaseRecordSuppDoc purchaseRecordSuppDoc) {
		if (purchaseRecordSuppDoc.getFileUpload() != null) {
			LinkButton linkButton = uiComponents.create(LinkButton.class);
			linkButton.setAction(new BaseAction("download")
					.withCaption(purchaseRecordSuppDoc.getFileUpload().getFileName())
					.withHandler(actionPerformedEvent ->
							downloader.download(purchaseRecordSuppDoc.getFileUpload())
					)
			);
			return linkButton;
		} else {
			return new Table.PlainTextCell("<empty>");
		}
	}

	@Subscribe("purchaseRecordProductsTable.add")
	public void onPurchaseRecordProductsTableAdd(Action.ActionPerformedEvent event) {
		PurchaseRecordProduct purchaseRecordProduct = this.dataManager.create(PurchaseRecordProduct.class);
		purchaseRecordProduct.setPurchase_record(getEditedEntity());
		if (goldType.equals("goldBar")) {
			purchaseRecordProduct.setType(loadType("GT01"));
		} else {
			purchaseRecordProduct.setType(loadType("GT02"));
		}
		purchaseRecordProduct.setPurity(BigDecimal.valueOf(999.9));
		purchaseRecordProduct.setQuantity(1);
		purchaseRecordProductsDc.getMutableItems().add(purchaseRecordProduct);
	}


	private RtType loadType(String code) {
		String query = "select e from RtType e where e.code =:code";
		return this.dataManager.load(RtType.class)
				.query(query)
				.parameter("code", code)
				.one();
	}

	@Subscribe("commit")
	public void onCommitClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G01"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveCompleteData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	private void saveCompleteData() {
		if (getEditedEntity().getPo_Num() == null) {
			notifications.create().withCaption("PO No. Required required").withType(Notifications.NotificationType.ERROR).show();
		} else {
			commitData();
		}
	}

	private void commitData() {
		if (goldType.equals("goldBar")) {
			getEditedEntity().setType(loadType("GT01"));
			String reference = managementController.generateSeqNo("PIB", "6");
			getEditedEntity().setReference_Num(reference);
		} else {
			getEditedEntity().setType(loadType("GT02"));
			String reference = managementController.generateSeqNo("PID", "6");
			getEditedEntity().setReference_Num(reference);
		}
		getEditedEntity().setIssuanceType(loadType(formCode));
//		this.dataManager.save(getEditedEntity());
		purchaseRecordProductsDc.getItems().forEach(purchaseRecordProduct -> {
			if (purchaseRecordProduct.getType().getCode().equals("GT01")) {

				BigDecimal quantity = BigDecimal.valueOf(1000);
				physical = physical.add(quantity);
				BigDecimal goldPcs = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = goldPcs.multiply(BigDecimal.valueOf(4.25));
				BigDecimal goldHolding = quantity.subtract(goldInGram);
				purchaseRecordProduct.setHolding_acct(goldHolding);
				purchaseRecordProduct.setTradable_gold(goldInGram);
				tradable = tradable.add(goldInGram);
				purchaseRecordProduct.setSub_total(purchaseRecordProduct.getPrice());
//				dataManager.save(purchaseRecordProduct);
//				}
			} else {
				BigDecimal dinarGram = BigDecimal.valueOf(4.25);
				Integer quantity = purchaseRecordProduct.getQuantity();
				BigDecimal goldInGram = dinarGram.multiply(BigDecimal.valueOf(quantity));
				physical = physical.add(goldInGram);
				purchaseRecordProduct.setHolding_acct(BigDecimal.ZERO);
				purchaseRecordProduct.setTradable_gold(goldInGram);
				tradable = tradable.add(goldInGram);
				purchaseRecordProduct.setSub_total(purchaseRecordProduct.getPrice());
//				dataManager.save(purchaseRecordProduct);
			}

		});
		getEditedEntity().setAcc_tradable(tradable);
		getEditedEntity().setPhysical_stock(physical);

		this.dataManager.save(getEditedEntity());

		purchaseRecordProductsDc.getItems().forEach(e -> {
			this.dataManager.save(e);
		});

		if (uploadDoc.getValue() != null) {
			saveDoc();
		}
		if (getEditedEntity().getStatus().getCode().equals("G01")) {
			createNewEntity();
		}
		closeWithDiscard();
	}

	@Install(to = "purchaseRecordProductsTable.sub_total", subject = "valueProvider")
	private Object purchaseRecordProductsTableSub_totalValueProvider(PurchaseRecordProduct purchaseRecordProduct) {
		BigDecimal price = purchaseRecordProduct.getPrice();
		return price;
	}

	@Install(to = "purchaseRecordProductsTable.tradable_gold", subject = "valueProvider")
	private Object purchaseRecordProductsTableTradable_goldValueProvider(PurchaseRecordProduct purchaseRecordProduct) {
		if (purchaseRecordProduct.getType().getCode().equals("GT01")) {
			if (purchaseRecordProduct.getPrice() != null) {
				BigDecimal quantity = BigDecimal.valueOf(1000);
				BigDecimal gold = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = gold.multiply(BigDecimal.valueOf(4.25));
				return goldInGram;
			}
			return BigDecimal.ZERO;
		} else {
			BigDecimal quantity = BigDecimal.valueOf(4.25);
			return quantity;
		}
	}

	@Install(to = "purchaseRecordProductsTable.holding_acct", subject = "valueProvider")
	private Object purchaseRecordProductsTableHolding_acctValueProvider(PurchaseRecordProduct purchaseRecordProduct) {
		if (purchaseRecordProduct.getType().getCode().equals("GT01")) {
			if (purchaseRecordProduct.getPrice() != null) {
				BigDecimal quantity = BigDecimal.valueOf(1000);
				BigDecimal gold = quantity.divide(BigDecimal.valueOf(4.25), 0, RoundingMode.HALF_DOWN);
				BigDecimal goldInGram = gold.multiply(BigDecimal.valueOf(4.25));
				BigDecimal goldHolding = quantity.subtract(goldInGram);
				return goldHolding;
			}
			return BigDecimal.ZERO;
		} else {
			BigDecimal quantity = BigDecimal.valueOf(4.25);
			return quantity;
		}
	}


	private void saveDoc() {
		PurchaseRecordSuppDoc purchaseRecordSuppDoc = this.dataManager.create(PurchaseRecordSuppDoc.class);
		purchaseRecordSuppDoc.setPurchase_record(getEditedEntity());
		purchaseRecordSuppDoc.setName(uploadDoc.getFileName());
		purchaseRecordSuppDoc.setFileUpload(uploadDoc.getValue());
		this.dataManager.save(purchaseRecordSuppDoc);
	}

	private void createNewEntity() {
		DeliveryRecord deliveryRecord = this.dataManager.create(DeliveryRecord.class);
		deliveryRecord.setPurchaseRecord(getEditedEntity());
		deliveryRecord.setStatus(loadType("G11"));

		ShariahRecord shariahRecord = this.dataManager.create(ShariahRecord.class);
		shariahRecord.setDeliveryRecord(deliveryRecord);
		shariahRecord.setStatus(loadType("G11"));

		this.dataManager.save(deliveryRecord);
		this.dataManager.save(shariahRecord);
	}

	@Subscribe("createDraft")
	public void onCreateDraftClick(Button.ClickEvent event) {
		getEditedEntity().setStatus(loadType("G11"));
		dialogs.createOptionDialog()
				.withCaption("Confirm Purchase Record Verification")
				.withContentMode(ContentMode.HTML)
				.withMessage("Please make sure all information is correct before proceeding.\n" +
						"Confirm to proceed?")
				.withActions(
						new DialogAction(DialogAction.Type.YES)
								.withPrimary(true)
								.withCaption("Yes")
								.withHandler(e -> saveCompleteData()),
						new DialogAction(DialogAction.Type.NO)
								.withCaption("No, Back to Edit")
				)
				.show();
	}

	@Subscribe("closeBtn")
	public void onCloseBtnClick(Button.ClickEvent event) {
		close(StandardOutcome.DISCARD);
	}


	private void showDownloadFile() {
		Path path;
		if (goldType.equals("goldBar")) {
			path = Paths.get("/fileformat/BGD Issuance Gold Bar.xlsx");
			File file = new File("fileformat/BGD Issuance Gold Bar.xlsx");
		} else {
			path = Paths.get("/fileformat/BGD Issuance Gold Dinar.xlsx");
			File file = new File("fileformat/BGD Issuance Gold Dinar.xlsx");
		}
		FileRef fileRef = new FileRef(fileStorageLocator.getByName("fs").toString(), path.toString(), path.getFileName().toString());

		LinkButton linkButton = uiComponents.create(LinkButton.class);
		linkButton.setAlignment(Component.Alignment.BOTTOM_CENTER);
		linkButton.setCaption(path.getFileName().toString());
		linkButton.setIcon(this.icons.get(JmixIcon.DOWNLOAD));
		linkButton.setAction(new BaseAction("download")
				.withCaption(fileRef.getFileName())
				.withHandler(actionPerformedEvent ->
						downloader.download(fileRef)
				)
		);
		addButton.add(linkButton);
	}
	@Subscribe("uploadDoc")
	public void onUploadDocValueChange(HasValue.ValueChangeEvent<FileRef> event) {
		try {
			String fileName = Objects.requireNonNull(event.getValue()).getFileName();
			// Check if the file name contains multiple extensions
			if (fileName.contains("..") || fileName.indexOf('.') != fileName.lastIndexOf('.')) {
				notifications.create()
						.withContentMode(ContentMode.HTML)
						.withType(Notifications.NotificationType.WARNING)
						.withPosition(Notifications.Position.MIDDLE_CENTER)
						.withCaption("Invalid file upload. Please use the correct file")
						.show();
				uploadDoc.clear();
			}
		}catch (Exception ex){
			log.info(ex.getMessage());
		}
	}

}
