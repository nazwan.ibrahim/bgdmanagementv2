package com.company.management.screen.unitofmeasure;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.UnitOfMeasure;

@UiController("UnitOfMeasure.edit")
@UiDescriptor("unit-of-measure-edit.xml")
@EditedEntityContainer("unitOfMeasureDc")
public class UnitOfMeasureEdit extends StandardEditor<UnitOfMeasure> {
}