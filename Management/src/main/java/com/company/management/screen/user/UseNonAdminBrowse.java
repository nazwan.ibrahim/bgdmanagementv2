package com.company.management.screen.user;

import com.company.management.screen.changepassword.ChangePassword;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.ui.ScreenBuilders;
import io.jmix.ui.Screens;
import io.jmix.ui.action.Action;
import io.jmix.ui.component.Button;
import io.jmix.ui.component.GroupTable;
import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

@UiController("UserNonAdmin.browse")
@UiDescriptor("user-nonAdmin-browse.xml")
@LookupComponent("usersTable")
public class UseNonAdminBrowse extends StandardLookup<User> {
	@Autowired
	private CollectionLoader<User> usersDl;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private ScreenBuilders screenBuilders;
	@Autowired
	private GroupTable<User> usersTable;
	@Autowired
	private Screens screens;
	boolean currentPasswordRequired = false;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		usersDl.setParameter("username1",currentAuthentication.getUser().getUsername());
		getScreenData().loadAll();
	}

	@Subscribe("usersTable.edit")
	public void onUsersTableEdit(Action.ActionPerformedEvent event) {
		UserEdit userEdit = screenBuilders.screen(this)
				.withScreenClass(UserEdit.class)
				.withAfterCloseListener(afterScreenCloseEvent -> {
					getScreenData().loadAll();
				})
				.build();
		userEdit.setEntityToEdit(Objects.requireNonNull(usersTable.getSingleSelected()));
		userEdit.setDisablePassword("disablePassword");
		userEdit.show();
	}

	@Subscribe("usersTableChangePasswordBtn")
	public void onUsersTableChangePasswordBtnClick(final Button.ClickEvent event) {
		screens.create(ChangePassword.class)
				.withUsername(Objects.requireNonNull(usersTable.getSingleSelected()).getUsername())
				.withCurrentPasswordRequired(currentPasswordRequired)
				.show();
	}
}