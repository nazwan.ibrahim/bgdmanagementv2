package com.company.management.screen.purchaserecord;

import io.jmix.ui.screen.*;
import com.company.management.entity.PurchaseRecord;

@UiController("PurchaseRecordGoldDinarStock.edit")
@UiDescriptor("purchase-record-goldDinar-Stock-edit.xml")
@EditedEntityContainer("purchaseRecordDc")
public class PurchaseRecordGoldDinarStockEdit extends StandardEditor<PurchaseRecord> {
}