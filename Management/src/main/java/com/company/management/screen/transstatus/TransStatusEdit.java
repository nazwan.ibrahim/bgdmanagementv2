package com.company.management.screen.transstatus;

import io.jmix.ui.screen.*;
import com.company.management.entity.wallet.TransStatus;

@UiController("TransStatus.edit")
@UiDescriptor("trans-status-edit.xml")
@EditedEntityContainer("transStatusDc")
public class TransStatusEdit extends StandardEditor<TransStatus> {
}