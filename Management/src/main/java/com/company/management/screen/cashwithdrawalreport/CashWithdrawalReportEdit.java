package com.company.management.screen.cashwithdrawalreport;

import io.jmix.ui.screen.*;
import com.company.management.entity.bgdreporting.CashWithdrawalReport;

@UiController("CashWithdrawalReport.edit")
@UiDescriptor("cash-withdrawal-report-edit.xml")
@EditedEntityContainer("cashWithdrawalReportDc")
public class CashWithdrawalReportEdit extends StandardEditor<CashWithdrawalReport> {
}