package com.company.management.screen.transactionhistoriesview;

import io.jmix.ui.model.CollectionLoader;
import io.jmix.ui.screen.*;
import com.company.management.entity.TransactionHistoriesView;
import org.springframework.beans.factory.annotation.Autowired;

@UiController("CashReportWithdraw.browse")
@UiDescriptor("cash-withdraw-browse.xml")
@LookupComponent("transactionHistoriesViewsTable")
public class CashReportWithdrawBrowse extends StandardLookup<TransactionHistoriesView> {
	@Autowired
	private CollectionLoader<TransactionHistoriesView> transactionHistoriesViewsDl;

	@Subscribe
	public void onBeforeShow(BeforeShowEvent event) {
		transactionHistoriesViewsDl.setParameter("transactionCode1","F02");
		transactionHistoriesViewsDl.setParameter("walletCode1","E01");
		transactionHistoriesViewsDl.setParameter("accOwner1","BgdAdmin");
		transactionHistoriesViewsDl.setParameter("accOwner2","AceAdmin");
		transactionHistoriesViewsDl.setParameter("statusCode1","G01");
		transactionHistoriesViewsDl.load();
	}



}