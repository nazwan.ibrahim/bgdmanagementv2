package com.company.management.security;

import io.jmix.security.model.EntityAttributePolicyAction;
import io.jmix.security.model.EntityPolicyAction;
import io.jmix.security.role.annotation.EntityAttributePolicy;
import io.jmix.security.role.annotation.EntityPolicy;
import io.jmix.security.role.annotation.ResourceRole;

import javax.annotation.Nonnull;

@Nonnull
@ResourceRole(name = "RestApiRole", code = "rest-api-role", scope = "API")
public interface RestApiRole {
	@EntityPolicy(entityName = "*", actions = {EntityPolicyAction.ALL})
	@EntityAttributePolicy(entityName = "*", attributes = "*", action = EntityAttributePolicyAction.MODIFY)
//    @ScreenPolicy(screenIds = "*")
//    @MenuPolicy(menuIds = "*")
//    @SpecificPolicy(resources = "*")
	void entity();
}