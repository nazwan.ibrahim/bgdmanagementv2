package com.company.management.security;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HeaderFilterConfiguration {
    @Bean
    public FilterRegistrationBean<HeaderFilter> cspFilterRegistration() {
        FilterRegistrationBean<HeaderFilter> registrationBean =
                new FilterRegistrationBean<>(cspFilter());
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public HeaderFilter cspFilter() {
        return new HeaderFilter();
    }
}
