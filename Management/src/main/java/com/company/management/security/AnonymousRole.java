package com.company.management.security;

import io.jmix.security.model.EntityAttributePolicyAction;
import io.jmix.security.model.EntityPolicyAction;
import io.jmix.security.role.annotation.EntityAttributePolicy;
import io.jmix.security.role.annotation.EntityPolicy;
import io.jmix.security.role.annotation.ResourceRole;
import io.jmix.security.role.annotation.SpecificPolicy;
import io.jmix.securityui.role.annotation.MenuPolicy;
import io.jmix.securityui.role.annotation.ScreenPolicy;

import javax.annotation.Nonnull;

@Nonnull
@ResourceRole(name = "AnonymousRole", code = "anonymous-role", scope = "API")
public interface AnonymousRole {
	String CODE = "anonymous-role";

//    @MenuPolicy(menuIds = {"NewRegistration", "RegistrationDetail", "PhoneVerification"})
//    @ScreenPolicy(screenIds = {"NewRegistration", "MainScreen", "LoginScreen", "RegistrationDetail", "PhoneVerification"})
//    void screens();
//
////    @EntityAttributePolicy(entityClass = User.class, attributes = "*", action = EntityAttributePolicyAction.MODIFY)
////    @EntityPolicy(entityClass = User.class, actions = EntityPolicyAction.ALL)
//
//    @EntityPolicy(entityName = "*", actions = {EntityPolicyAction.ALL})
//    @EntityAttributePolicy(entityName = "*", attributes = "*", action = EntityAttributePolicyAction.MODIFY)
//    void user();

	@EntityPolicy(entityName = "*", actions = {EntityPolicyAction.ALL})
	@EntityAttributePolicy(entityName = "*", attributes = "*", action = EntityAttributePolicyAction.MODIFY)
	@ScreenPolicy(screenIds = "*")
	@MenuPolicy(menuIds = "*")
	@SpecificPolicy(resources = "*")
	void fullAccess();

	//    @MenuPolicy(menuIds = {"anonymousScreen"})
//    @ScreenPolicy(screenIds = {"MainScreenSideMenu", "MyAnonymousScreen"})
//    void screens();
//
//    @MenuPolicy(menuIds = {"application"})
//    void commonMenus();
}