package com.company.management;

import com.google.common.base.Strings;
import io.jmix.core.FileStorage;
import io.jmix.localfs.LocalFileStorage;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@SpringBootApplication
public class ManagementApplication {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(ManagementApplication.class);
    @Autowired
    private Environment environment;

    public static void main(String[] args) {
//        SpringApplication.run(ManagementApplication.class, args);
        try {
            SpringApplication.run(ManagementApplication.class, args);
        } catch (Exception e) {
            log.error("Error", e);
        }
    }

    @Bean
    @Primary
    @ConfigurationProperties("main.datasource")
    DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    @ConfigurationProperties("main.datasource.hikari")
    DataSource dataSource(DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    @Bean
    FileStorage myFileStorage() {
        return new LocalFileStorage("myfs", "/opt/file-storage");
    }
    @EventListener
    public void printApplicationUrl(ApplicationStartedEvent event) {
        try {
            log.info("Application started at http://localhost:"
                    + environment.getProperty("local.server.port")
                    + Strings.nullToEmpty(environment.getProperty("server.servlet.context-path")));
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
    }
}
