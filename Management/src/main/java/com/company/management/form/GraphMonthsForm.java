package com.company.management.form;

import java.math.BigDecimal;

public class GraphMonthsForm {

	private String MonthName;

	private String Date;

	private BigDecimal Price;
	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public BigDecimal getPrice() {
		return Price;
	}

	public void setPrice(BigDecimal price) {
		Price = price;
	}
	public String getMonthName() {
		return MonthName;
	}

	public void setMonthName(String monthName) {
		MonthName = monthName;
	}
}
