package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProductPriceForm implements Serializable {
	private String code;

	private String name;
	private BigDecimal buyPrice;
	private BigDecimal sellPrice;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
}

