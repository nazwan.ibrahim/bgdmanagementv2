package com.company.management.form;

import java.math.BigDecimal;

public class ProductMarginForm {



	private String product;

	private String description;

	private String type;

	private String rtUom;

	private BigDecimal value;

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRtUom() {
		return rtUom;
	}

	public void setRtUom(String rtUom) {
		this.rtUom = rtUom;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}