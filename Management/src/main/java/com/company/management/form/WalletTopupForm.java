package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class WalletTopupForm implements Serializable {

	public String getAcctNumber() {
		return acctNumber;
	}

	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getWalletTypeCode() {
		return walletTypeCode;
	}

	public void setWalletTypeCode(String walletTypeCode) {
		this.walletTypeCode = walletTypeCode;
	}

	public String getRegTypeCode() {
		return regTypeCode;
	}

	public void setRegTypeCode(String regTypeCode) {
		this.regTypeCode = regTypeCode;
	}

	public String getUomCode() {
		return uomCode;
	}

	public void setUomCode(String uomCode) {
		this.uomCode = uomCode;
	}

	public String getTransactionMethodCode() {
		return transactionMethodCode;
	}

	public void setTransactionMethodCode(String transactionMethodCode) {
		this.transactionMethodCode = transactionMethodCode;
	}

	public String getTransactionTypeCode() {
		return transactionTypeCode;
	}

	public void setTransactionTypeCode(String transactionTypeCode) {
		this.transactionTypeCode = transactionTypeCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public List<FeesForm> getFeesForms() {
		return feesForms;
	}

	public void setFeesForms(List<FeesForm> feesForms) {
		this.feesForms = feesForms;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	private String acctNumber;
	private String amount;
	private String walletTypeCode;
	private String regTypeCode;
	private String uomCode;
	private String transactionMethodCode;
	private String transactionTypeCode;
	private String statusCode;
	private List<FeesForm> feesForms;
	private String totalAmount;
}
