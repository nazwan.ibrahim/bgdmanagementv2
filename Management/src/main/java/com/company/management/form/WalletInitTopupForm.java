package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class WalletInitTopupForm implements Serializable {
    private String acctNumber;
    private BigDecimal amount;
    private BigDecimal totalAmount;
    private String walletTypeCode;
    private String regTypeCode;
    private String uomCode;
    private String transactionMethodCode;
    private String transactionTypeCode;
    private String statusCode;
    private List<FeesForm> feesForms;

    private String channelCode;

    private String bicCode;

    private String currency;

    private String CustomerName;



    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getWalletTypeCode() {
        return walletTypeCode;
    }

    public void setWalletTypeCode(String walletTypeCode) {
        this.walletTypeCode = walletTypeCode;
    }

    public String getRegTypeCode() {
        return regTypeCode;
    }

    public void setRegTypeCode(String regTypeCode) {
        this.regTypeCode = regTypeCode;
    }

    public String getUomCode() {
        return uomCode;
    }

    public void setUomCode(String uomCode) {
        this.uomCode = uomCode;
    }

    public String getTransactionMethodCode() {
        return transactionMethodCode;
    }

    public void setTransactionMethodCode(String transactionMethodCode) {
        this.transactionMethodCode = transactionMethodCode;
    }

    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<FeesForm> getFeesForms() {
        return feesForms;
    }

    public void setFeesForms(List<FeesForm> feesForms) {
        this.feesForms = feesForms;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getBicCode() {
        return bicCode;
    }

    public void setBicCode(String bicCode) {
        this.bicCode = bicCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }
}
