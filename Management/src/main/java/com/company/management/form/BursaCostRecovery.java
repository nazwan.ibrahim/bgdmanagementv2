package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class BursaCostRecovery implements Serializable {
	private String type;

	private BigDecimal value;

	private String uom;

	private boolean active;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
