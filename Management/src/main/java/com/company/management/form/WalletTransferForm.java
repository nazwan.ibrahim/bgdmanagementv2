package com.company.management.form;

import com.company.management.entity.registrationdb.UserProfile;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class WalletTransferForm implements Serializable {

    private String acctNumberSender;
    private String acctNumberReceiver;
    private BigDecimal amount;
    private BigDecimal bursaSellPrice;
    private String statusCode;
    private String reason;
    private String reference;
//    private FeesForm feesForm;
    private List<FeesForm> feesForms;
    private UserProfile receiverProfile;

    public BigDecimal getBursaSellPrice() {
        return bursaSellPrice;
    }

    public void setBursaSellPrice(BigDecimal bursaSellPrice) {
        this.bursaSellPrice = bursaSellPrice;
    }




    public String getAcctNumberSender() {
        return acctNumberSender;
    }

    public void setAcctNumberSender(String acctNumberSender) {
        this.acctNumberSender = acctNumberSender;
    }

    public String getAcctNumberReceiver() {
        return acctNumberReceiver;
    }

    public void setAcctNumberReceiver(String acctNumberReceiver) {
        this.acctNumberReceiver = acctNumberReceiver;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount.setScale(6, RoundingMode.HALF_EVEN);
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

//    public FeesForm getFeesForm() {
//        return feesForm;
//    }
//
//    public void setFeesForm(FeesForm feesForm) {
//        this.feesForm = feesForm;
//    }

    public List<FeesForm> getFeesForms() {
        return feesForms;
    }

    public void setFeesForms(List<FeesForm> feesForms) {
        this.feesForms = feesForms;
    }

    public UserProfile getReceiverProfile() {
        return receiverProfile;
    }

    public void setReceiverProfile(UserProfile receiverProfile) {
        this.receiverProfile = receiverProfile;
    }
}
