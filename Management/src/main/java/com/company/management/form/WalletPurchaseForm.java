package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class WalletPurchaseForm implements Serializable {
    private BigDecimal quantity;
    private String type;
    private String inputType;
    private BigDecimal price;
    private BigDecimal netPrice;
    private BigDecimal totalPrice;
    private String priceRequestId;
    private String referenceId;

    private List<FeesForm> feesForms;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<FeesForm> getFeesForms() {
        return feesForms;
    }

    public void setFeesForms(List<FeesForm> feesForms) {
        this.feesForms = feesForms;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }

    public String getPriceRequestId() {
        return priceRequestId;
    }

    public void setPriceRequestId(String priceRequestId) {
        this.priceRequestId = priceRequestId;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
