package com.company.management.form;

import java.io.Serializable;
import java.util.List;

public class TierManagementForm implements Serializable {
	private String code;
	private String category;

	private List<TierRequirementDoc> tierRequirementDoc;

	private List<TierThresholdForm> tierThresholdForm;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<TierRequirementDoc> getTierRequirementDoc() {
		return tierRequirementDoc;
	}

	public void setTierRequirementDoc(List<TierRequirementDoc> tierRequirementDoc) {
		this.tierRequirementDoc = tierRequirementDoc;
	}

	public List<TierThresholdForm> getTierThresholdForm() {
		return tierThresholdForm;
	}

	public void setTierThresholdForm(List<TierThresholdForm> tierThresholdForm) {
		this.tierThresholdForm = tierThresholdForm;
	}
}
