package com.company.management.form;

import java.io.Serializable;

public class TierRequirementDoc implements Serializable {
	private String code;
	private String requirementDoc;

	public String getRequirementDoc() {
		return requirementDoc;
	}

	public void setRequirementDoc(String requirementDoc) {
		this.requirementDoc = requirementDoc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
