package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductPriceDetailForm implements Serializable {

	private String code;

	private BigDecimal buyPrice;
	private BigDecimal sellPrice;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}
}
