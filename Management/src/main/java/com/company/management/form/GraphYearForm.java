package com.company.management.form;

import java.math.BigDecimal;

public class GraphYearForm {
	public String getYearName() {
		return YearName;
	}

	public void setYearName(String yearName) {
		YearName = yearName;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public BigDecimal getPrice() {
		return Price;
	}

	public void setPrice(BigDecimal price) {
		Price = price;
	}

	private String YearName;

	private String Date;

	private BigDecimal Price;
}
