package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class TransferManagementForm implements Serializable {
	private String senderName;
	private BigDecimal senderAmount;
	private String uom;
	private String receiverName;
	private String reason;
	private String status;

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public BigDecimal getSenderAmount() {
		return senderAmount;
	}

	public void setSenderAmount(BigDecimal senderAmount) {
		this.senderAmount = senderAmount;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
