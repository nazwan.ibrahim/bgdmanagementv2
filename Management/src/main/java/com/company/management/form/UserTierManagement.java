package com.company.management.form;

import java.io.Serializable;
import java.util.Date;

public class UserTierManagement implements Serializable {

	private String reference;

	private String userAccountStatusCode;

	private String userAccountStatusName;

	private String userAccountStatusDesc;

	private String userEmail;

	private String userUsername;

	private String identificationNo;

	private String identificationCode;
	private String identificationName;

	private String identificationDesc;

	private String userFullName;

	private String userTierCode;

	private String userTierName;

	private String userTierDesc;

	private String userCreatedBy;

	private Date userCreatedDate;

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getUserAccountStatusCode() {
		return userAccountStatusCode;
	}

	public void setUserAccountStatusCode(String userAccountStatusCode) {
		this.userAccountStatusCode = userAccountStatusCode;
	}

	public String getUserAccountStatusName() {
		return userAccountStatusName;
	}

	public void setUserAccountStatusName(String userAccountStatusName) {
		this.userAccountStatusName = userAccountStatusName;
	}

	public String getUserAccountStatusDesc() {
		return userAccountStatusDesc;
	}

	public void setUserAccountStatusDesc(String userAccountStatusDesc) {
		this.userAccountStatusDesc = userAccountStatusDesc;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserUsername() {
		return userUsername;
	}

	public void setUserUsername(String userUsername) {
		this.userUsername = userUsername;
	}

	public String getIdentificationNo() {
		return identificationNo;
	}

	public void setIdentificationNo(String identificationNo) {
		this.identificationNo = identificationNo;
	}

	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	public String getIdentificationName() {
		return identificationName;
	}

	public void setIdentificationName(String identificationName) {
		this.identificationName = identificationName;
	}

	public String getIdentificationDesc() {
		return identificationDesc;
	}

	public void setIdentificationDesc(String identificationDesc) {
		this.identificationDesc = identificationDesc;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getUserTierCode() {
		return userTierCode;
	}

	public void setUserTierCode(String userTierCode) {
		this.userTierCode = userTierCode;
	}

	public String getUserTierName() {
		return userTierName;
	}

	public void setUserTierName(String userTierName) {
		this.userTierName = userTierName;
	}

	public String getUserTierDesc() {
		return userTierDesc;
	}

	public void setUserTierDesc(String userTierDesc) {
		this.userTierDesc = userTierDesc;
	}

	public String getUserCreatedBy() {
		return userCreatedBy;
	}

	public void setUserCreatedBy(String userCreatedBy) {
		this.userCreatedBy = userCreatedBy;
	}

	public Date getUserCreatedDate() {
		return userCreatedDate;
	}

	public void setUserCreatedDate(Date userCreatedDate) {
		this.userCreatedDate = userCreatedDate;
	}
}
