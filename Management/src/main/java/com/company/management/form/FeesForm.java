package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class FeesForm implements Serializable {
    private String feeType;
    private String feeTypeCode;
    private BigDecimal setupFees;
    private String setupUom;
    private String setupUomCode;
    private BigDecimal chargeFees;
    private String chargeUom;
    private String chargeUomCode;
    private BigDecimal minFees;
    private BigDecimal maxFees;
    private BigDecimal minTransaction;

    public BigDecimal getMinTransaction() {
        return minTransaction;
    }

    public void setMinTransaction(BigDecimal minTransaction) {
        this.minTransaction = minTransaction;
    }

    public BigDecimal getMaxFees() {
        return maxFees;
    }

    public void setMaxFees(BigDecimal maxFees) {
        this.maxFees = maxFees;
    }

    public BigDecimal getMinFees() {
        return minFees;
    }

    public void setMinFees(BigDecimal minFees) {
        this.minFees = minFees;
    }

    public BigDecimal getSetupFees() {
        return setupFees;
    }

    public void setSetupFees(BigDecimal setupFees) {
        this.setupFees = setupFees;
    }

    public String getSetupUom() {
        return setupUom;
    }

    public void setSetupUom(String setupUom) {
        this.setupUom = setupUom;
    }

    public BigDecimal getChargeFees() {
        return chargeFees;
    }

    public void setChargeFees(BigDecimal chargeFees) {
        this.chargeFees = chargeFees;
    }

    public String getChargeUom() {
        return chargeUom;
    }

    public void setChargeUom(String chargeUom) {
        this.chargeUom = chargeUom;
    }

    public String getSetupUomCode() {
        return setupUomCode;
    }

    public void setSetupUomCode(String setupUomCode) {
        this.setupUomCode = setupUomCode;
    }

    public String getChargeUomCode() {
        return chargeUomCode;
    }

    public void setChargeUomCode(String chargeUomCode) {
        this.chargeUomCode = chargeUomCode;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }

    public String getFeeTypeCode() {
        return feeTypeCode;
    }

    public void setFeeTypeCode(String feeTypeCode) {
        this.feeTypeCode = feeTypeCode;
    }
}
