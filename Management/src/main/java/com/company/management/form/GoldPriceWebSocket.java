package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class GoldPriceWebSocket implements Serializable {

	private String type;
	private BigDecimal sellprice;
	private BigDecimal buyprice;
	public GoldPriceWebSocket(final String type,final BigDecimal sellprice,final BigDecimal buyprice) {
		this.type = type;
		this.sellprice = sellprice;
		this.buyprice = buyprice;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public BigDecimal getSellprice() {
		return sellprice;
	}

	public void setSellprice(BigDecimal sellprice) {
		this.sellprice = sellprice;
	}

	public BigDecimal getBuyprice() {
		return buyprice;
	}

	public void setBuyprice(BigDecimal buyprice) {
		this.buyprice = buyprice;
	}

}
