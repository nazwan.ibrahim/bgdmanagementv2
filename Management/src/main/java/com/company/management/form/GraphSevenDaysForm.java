package com.company.management.form;

import java.io.Serializable;
import java.math.BigDecimal;

public class GraphSevenDaysForm implements Serializable {
	private String DateName;

	private String Date;

	private BigDecimal Price;

	public String getDateName() {
		return DateName;
	}

	public void setDateName(String dateName) {
		DateName = dateName;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public BigDecimal getPrice() {
		return Price;
	}

	public void setPrice(BigDecimal price) {
		Price = price;
	}
}
