package com.company.management.bgdreporting;

import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.core.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class RevenueFeesReport {

	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private DataManager dataManager;

	public void saveRevenueAndFees() throws ParseException {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		String date1 = "2023-07-14";
		String date2 = "2023-07-15";
		Date startDate = df.parse(date1);
		Date startDate2 = df.parse(date2);
//			Optional transactionAcct = Optional.of(this.dataManager.unconstrained().loadValue("select sum(ta.amount) from TransactionAcct ta " +
//							"inner join AccountBgd ab on ta.account_id = ab.id " +
//							"inner join RegType rt on ab.reg_type_id = rt.id " +
//							"inner join WalletType wt on ab.wallet_type_id = wt.id " +
//							"inner join TransactionType tt on ta.trans_type_id = tt.id " +
//							"inner join TransStatus ts ON ta.trans_status_id = ts.id " +
//							"where tt.code = :ttcode and wt.code = :wtcode and ts.code = :tscode and (rt.code = :rtcode1 or rt.code = :rtcode2) " +
//							"and ta.createdDate > :created_date1 and ta.createdDate < :created_date2", BigDecimal.class)
//					.store("wallet")
//					.parameter("ttcode","F01")
//					.parameter("wtcode","E01")
//					.parameter("tscode","G01")
//					.parameter("rtcode1","D02")
//					.parameter("rtcode2","D03")
//					.parameter("created_date1","2023-07-14")
//					.parameter("created_date2","2023-07-15")
//					.optional());

			TransactionAcct transactionAcct1 = this.dataManager.load(TransactionAcct.class)
					.query("select SUM(t.amount) from TransactionAcct t " +
							"where t.createdDate > :createdDate1 " +
							"and t.createdDate < :createdDate2")
					.parameter("createdDate1", startDate)
					.parameter("createdDate2", startDate2)
					.one();

		StringBuilder sb = new StringBuilder();
//		for (KeyValueEntity kvEntity : kvEntities) {
//			BigDecimal sum = kvEntity.getValue("sum");
//			BigDecimal plus;
//		}
//		return sb.toString();
	}
}
