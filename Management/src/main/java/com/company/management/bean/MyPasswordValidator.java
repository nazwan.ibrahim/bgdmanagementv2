package com.company.management.bean;

import com.company.management.entity.User;
import com.company.management.entity.registrationdb.PasswordDictionary;
import io.jmix.core.DataManager;
import io.jmix.securityui.password.PasswordValidationContext;
import io.jmix.securityui.password.PasswordValidationException;
import io.jmix.securityui.password.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyPasswordValidator implements PasswordValidator<User> {
	@Autowired
	private DataManager dataManager;

	@Override
	public void validate(PasswordValidationContext<User> context) throws PasswordValidationException {
		for(PasswordDictionary passwordDictionary : passwordDictionary()){
			if(context.getPassword().contains(passwordDictionary.getWord()))
				throw new PasswordValidationException("Please use Stronger Password");
		}

		if (context.getPassword().length() < 15)
			throw new PasswordValidationException("Password is too short, must be more than 15 characters");
		if(!context.getPassword().matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$!%^?&+=])(?=\\S+$).{8,}$")){
			throw new PasswordValidationException("Password must contain at least one uppercase letter, one lowercase letter, one digit, and one special character");
		}
	}
	private List<PasswordDictionary> passwordDictionary(){
		String query = "select e from PasswordDictionary e";
		return this. dataManager.load(PasswordDictionary.class)
				.query(query)
				.list();
	}
}