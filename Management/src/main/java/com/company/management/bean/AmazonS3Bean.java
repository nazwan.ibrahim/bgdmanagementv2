package com.company.management.bean;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

@Component
public class AmazonS3Bean {

	@Autowired
	private Environment environment;
	@Bean
	public AmazonS3 s3client() {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(Objects.requireNonNull(environment.getProperty("jmix.awsfs.access-key")), "MNdcOCOz9XzpqxfhDaIxU1vI2enH2qq7QmQg7uFM");
		return AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.withRegion(Regions.AP_SOUTHEAST_1)
				.build();
	}

	public byte[] getObjectFromS3(String key) throws IOException {
		S3Object s3object = s3client().getObject(new GetObjectRequest(environment.getProperty("jmix.awsfs.bucket"), key));
		InputStream inputStream = s3object.getObjectContent();
		byte[] bytes = IOUtils.toByteArray(inputStream);
		return bytes;
	}

	//reference to conver file
	public void displayS3File(String key) throws IOException {
		byte[] fileContent = getObjectFromS3(key);
	}

}

