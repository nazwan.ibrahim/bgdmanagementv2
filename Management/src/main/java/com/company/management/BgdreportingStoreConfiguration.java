package com.company.management;

import io.jmix.autoconfigure.data.JmixLiquibaseCreator;
import io.jmix.core.JmixModules;
import io.jmix.core.Resources;
import io.jmix.data.impl.JmixEntityManagerFactoryBean;
import io.jmix.data.impl.JmixTransactionManager;
import io.jmix.data.persistence.DbmsSpecifics;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class BgdreportingStoreConfiguration {

    @Bean
    @ConfigurationProperties("bgdreporting.datasource")
    DataSourceProperties bgdreportingDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "bgdreporting.datasource.hikari")
    DataSource bgdreportingDataSource(@Qualifier("bgdreportingDataSourceProperties") DataSourceProperties properties) {
        return properties.initializeDataSourceBuilder().build();
    }

    @Bean
    LocalContainerEntityManagerFactoryBean bgdreportingEntityManagerFactory(
            @Qualifier("bgdreportingDataSource") DataSource dataSource,
            JpaVendorAdapter jpaVendorAdapter,
            DbmsSpecifics dbmsSpecifics,
            JmixModules jmixModules,
            Resources resources) {
        return new JmixEntityManagerFactoryBean("bgdreporting", dataSource, jpaVendorAdapter, dbmsSpecifics, jmixModules, resources);
    }

    @Bean
    JpaTransactionManager bgdreportingTransactionManager(@Qualifier("bgdreportingEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JmixTransactionManager("bgdreporting", entityManagerFactory);
    }
//
//    @Bean
//    public SpringLiquibase bgdreportingLiquibase(io.jmix.data.impl.liquibase.LiquibaseChangeLogProcessor processor, @Qualifier("bgdreportingDataSource") DataSource dataSource) {
//        return JmixLiquibaseCreator.create(dataSource, new LiquibaseProperties(), processor, "bgdreporting");
//    }
}
