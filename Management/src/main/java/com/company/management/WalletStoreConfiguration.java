package com.company.management;

import io.jmix.core.JmixModules;
import io.jmix.core.Resources;
import io.jmix.data.impl.JmixEntityManagerFactoryBean;
import io.jmix.data.impl.JmixTransactionManager;
import io.jmix.data.persistence.DbmsSpecifics;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class WalletStoreConfiguration {

	@Bean
	@ConfigurationProperties("wallet.datasource")
	DataSourceProperties walletDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@ConfigurationProperties(prefix = "wallet.datasource.hikari")
	DataSource walletDataSource(@Qualifier("walletDataSourceProperties") DataSourceProperties properties) {
		return properties.initializeDataSourceBuilder().build();
	}

	@Bean
	LocalContainerEntityManagerFactoryBean walletEntityManagerFactory(
			@Qualifier("walletDataSource") DataSource dataSource,
			JpaVendorAdapter jpaVendorAdapter,
			DbmsSpecifics dbmsSpecifics,
			JmixModules jmixModules,
			Resources resources) {
		return new JmixEntityManagerFactoryBean("wallet", dataSource, jpaVendorAdapter, dbmsSpecifics, jmixModules, resources);
	}

	@Bean
	JpaTransactionManager walletTransactionManager(@Qualifier("walletEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		return new JmixTransactionManager("wallet", entityManagerFactory);
	}
}
