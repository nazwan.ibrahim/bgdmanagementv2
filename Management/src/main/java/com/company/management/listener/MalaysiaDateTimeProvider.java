package com.company.management.listener;


import org.springframework.data.auditing.DateTimeProvider;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAccessor;
import java.util.Optional;

public class MalaysiaDateTimeProvider implements DateTimeProvider {
    private final ZoneId malaysiaTimeZone = ZoneId.of("Asia/Kuala_Lumpur");

    @Override
    public Optional<TemporalAccessor> getNow() {
        return Optional.of(ZonedDateTime.now(malaysiaTimeZone));
    }
}
