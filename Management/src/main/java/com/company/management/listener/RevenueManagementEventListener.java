package com.company.management.listener;

import com.company.management.entity.RevenueManagement;
import com.company.management.service.PriceService;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
public class RevenueManagementEventListener {

    @Autowired
    private PriceService priceService;


    @EventListener
    public void onRevenueManagementLoading(EntityLoadingEvent<RevenueManagement> event) {

    }

    @EventListener
    public void onRevenueManagementSaving(EntitySavingEvent<RevenueManagement> event) {
        if(event.getEntity().getProduct().getCode().equals("BGD")) {
            if(event.getEntity().getType().getCode().equals("C05")) {
//                priceService.setBUY_MARGIN(event.getEntity());
                priceService.calculatedBGDPrice(event.getEntity(), event.getEntity().getUom(), null, null);
            } else if (event.getEntity().getType().getCode().equals("C06")) {
//                priceService.setSELL_MARGIN(event.getEntity());
                priceService.calculatedBGDPrice(null, null, event.getEntity(), event.getEntity().getUom());
            }
        }
    }

    @EventListener
    public void onRevenueManagementChangedBeforeCommit(EntityChangedEvent<RevenueManagement> event) {

    }

    @TransactionalEventListener
    public void onRevenueManagementChangedAfterCommit(EntityChangedEvent<RevenueManagement> event) {

    }
}