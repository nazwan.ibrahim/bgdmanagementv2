package com.company.management.listener;

import com.company.management.entity.Product;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;
public class ProductEventListener {

	@EventListener
	public void onProductLoading(EntityLoadingEvent<Product> event) {

	}

	@EventListener
	public void onProductSaving(EntitySavingEvent<Product> event) {

	}

	@EventListener
	public void onProductChangedBeforeCommit(EntityChangedEvent<Product> event) {

	}

	@TransactionalEventListener
	public void onProductChangedAfterCommit(EntityChangedEvent<Product> event) {

	}
}
