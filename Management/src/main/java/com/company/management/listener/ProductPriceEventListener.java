package com.company.management.listener;

import com.company.management.entity.ProductPrice;
import com.company.management.service.PriceService;
import com.company.management.service.RunnableService;
import io.jmix.core.DataManager;
import io.jmix.core.event.EntityChangedEvent;
import io.jmix.core.event.EntityLoadingEvent;
import io.jmix.core.event.EntitySavingEvent;
import io.jmix.core.security.SystemAuthenticator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class ProductPriceEventListener {

    @Autowired
    private DataManager dataManager;

    @Autowired
    private PriceService priceService;

    ExecutorService executor = Executors.newSingleThreadExecutor();
    @Autowired
    private SystemAuthenticator systemAuthenticator;


    @EventListener
    public void onProductPriceLoading(EntityLoadingEvent<ProductPrice> event) {

    }

    @EventListener
    public void onProductPriceSaving(EntitySavingEvent<ProductPrice> event) {

        if(event.getEntity().getProduct().getCode().equals("BGD") && event.getEntity().getType().getCode().equals("A01")) {
            priceService.calculatedBGDPrice(event.getEntity());

//            RunnableService runnableService = new RunnableService("").runnableService1(event.getEntity());
//            runnableService.run();

//            executor.execute(new Runnable() {
//                @Override
//                public void run() {
//                    systemAuthenticator.runWithSystem(() -> {
//                        priceService.calculatedBGDPrice(event.getEntity());
//                    });
//                }
//            });
        }

        priceService.savePriceHistories(event.getEntity());
//        executor.execute(new Runnable() {
//            @Override
//            public void run() {
//                systemAuthenticator.runWithSystem(() -> {
//                    priceService.savePriceHistories(event.getEntity());
//                });
//            }
//        });
    }

    @EventListener
    public void onProductPriceChangedBeforeCommit(EntityChangedEvent<ProductPrice> event) {

    }

    @TransactionalEventListener
    public void onProductPriceChangedAfterCommit(EntityChangedEvent<ProductPrice> event) {
//        ProductPrice productPrice = this.dataManager.load(ProductPrice.class).id(event.getEntityId()).one();
//        if(productPrice.getProduct().equals("BGD") && productPrice.getType().getCode().equals("A01")) {
//            priceService.setACE_PRICE(productPrice);
//        }
    }
}