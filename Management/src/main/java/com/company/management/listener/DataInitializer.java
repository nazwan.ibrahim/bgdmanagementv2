package com.company.management.listener;

import com.company.management.entity.*;
import com.company.management.entity.wallet.TransStatus;
import com.company.management.entity.wallet.TransactionType;
import com.company.management.websocket.BGDWebSocket;
import com.company.management.service.PriceService;
import com.company.management.websocket.WebSocketSaveValue;
import io.jmix.core.DataManager;
import io.jmix.core.SaveContext;
import io.jmix.core.security.Authenticated;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.websocket.DeploymentException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
public class DataInitializer {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(DataInitializer.class);
	@Autowired
	private DataManager dataManager;

//	@Autowired
	private List<RtType> rtTypeList;

	@Autowired
	private PriceService priceService;

	@Autowired
	private Environment environment;
	@Autowired
	private WebSocketSaveValue webSocketSaveValue;

	@EventListener
	@Authenticated
	public void onApplicationStarted(ApplicationStartedEvent event) throws DeploymentException, IOException, URISyntaxException {
		initProduct();
		initRT_Type();
		initTierManagement();
		initTransactionType();
		initTransStatus();

//		getSupplierGoldPrice();
		BGDWebSocketPrice();
//		initFeesManagement();

		Product productBGD = this.dataManager.load(Product.class)
				.query("select e from Product e where e.code = :code")
				.parameter("code", "BGD").one();
//		initRevenueManagement(productBGD);
//		initCostRecoveryManagement(productBGD);

		//load siap2
//		priceService.setACE_PRICE(priceService.loadProductPrice("A01"));
//		priceService.setBURSA_PRICE(priceService.loadProductPrice("A02"));
//		priceService.setBUY_MARGIN(priceService.loadRevenueManagement("C05"));
//		priceService.setSELL_MARGIN(priceService.loadRevenueManagement("C06"));
	}

	public void BGDWebSocketPrice() {
		try {
			BGDWebSocket bgdWebSocket = new BGDWebSocket(new URI(Objects.requireNonNull(environment.getProperty("bursagolduatapi"))), webSocketSaveValue);
			bgdWebSocket.connect();
		}catch (Exception ex){
			log.info(ex.getCause().getMessage());
		}
//		while(true){
//			bgdWebSocket.onMessage();
//		}
	}

	public void initProduct() {
		List<ProductGroup> groupList = this.dataManager.load(ProductGroup.class).all().list();
		List<Product> productList = this.dataManager.load(Product.class).all().list();

		if(groupList.stream().noneMatch(a -> a.getCode().equals("COMMODITY"))) {
			ProductGroup productGroup = this.dataManager.create(ProductGroup.class);
			productGroup.setCode("COMMODITY");
			productGroup.setName("Commodity Product");
			this.dataManager.save(productGroup);

			if(productList.stream().noneMatch(a -> a.getCode().equals("BGD"))) {
				Product product = this.dataManager.create(Product.class);
				product.setProductGroup(productGroup);
				product.setCode("BGD");
				product.setName("Gold Product");
				this.dataManager.save(product);

				ProductPrice bursaPrice = this.dataManager.create(ProductPrice.class);
				bursaPrice.setProduct(product);
				bursaPrice.setType(rtTypeList.stream().filter(f -> f.getCode().equals("A02")).toList().get(0));
				this.dataManager.save(bursaPrice);

				ProductPrice supplierPrice = this.dataManager.create(ProductPrice.class);
				supplierPrice.setProduct(product);
				supplierPrice.setType(rtTypeList.stream().filter(f -> f.getCode().equals("A01")).toList().get(0));
				this.dataManager.save(supplierPrice);
			}
		}



	}
	public void initRT_Type(){

		List<RtTypeGroup> groupList = this.dataManager.load(RtTypeGroup.class)
				.query("select e from RtTypeGroup e")
				.list();

		List<RtType> rtTypeList = this.dataManager.load(RtType.class).all().list();

		//MENU
		if(groupList.stream().noneMatch(a -> a.getCode().equals("MENU"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("MENU");
			group.setName("Menu");
			group.setDescription("Menu navigation for ADAM");
			this.dataManager.save(group);
			groupList.add(group);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU01");
			rtType.setName("Dashboard");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU02");
			rtType.setName("WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU02-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU02-01");
			rtType.setName("Bursa Wallet");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU02-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU02-02");
			rtType.setName("Supplier Wallet");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU02-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU02-03");
			rtType.setName("Investor Wallet");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU03");
			rtType.setName("ONBOARDING MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU03-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU03-01");
			rtType.setName("Individual Investor");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU04");
			rtType.setName("INVESTOR ACCOUNT MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU05");
			rtType.setName("TIER MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU06");
			rtType.setName("USER MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU07");
			rtType.setName("REDEMPTION MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU08"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU08");
			rtType.setName("TRANSFER MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09");
			rtType.setName("REWARD MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09-01");
			rtType.setName("Sign Up Reward");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09-02");
			rtType.setName("Referral Reward");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09-03");
			rtType.setName("Reward Transfer");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09-04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09-04");
			rtType.setName("Custom Campaign Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU09-10"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU09-10");
			rtType.setName("Reward Management Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU10"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU10");
			rtType.setName("REVENUE MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU10-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU10-01");
			rtType.setName("Revenue Configuration");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU10-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU10-02");
			rtType.setName("Cost Recovery Configuration");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU10-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU10-03");
			rtType.setName("Margin Configuration");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11");
			rtType.setName("EQUILIBRIUM MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-01");
			rtType.setName("Primary Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-01-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-01-01");
			rtType.setName("Purchase Record");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-01-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-01-02");
			rtType.setName("Delivery Record");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-01-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-01-03");
			rtType.setName("Shariah Verification Audit");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-01-04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-01-04");
			rtType.setName("Shariah Approval Record");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-02");
			rtType.setName("Subsequent Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-02-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-02-01");
			rtType.setName("ReserveRecord");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-02-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-02-02");
			rtType.setName("Delivery Record");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-02-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-02-03");
			rtType.setName("Shariah Verification Audit");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU11-02-04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU11-02-04");
			rtType.setName("Shariah Approval Record");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12");
			rtType.setName("REPORTING");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-01");
			rtType.setName("Investors Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-02");
			rtType.setName("Revenue & Fee Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-03");
			rtType.setName("Cash Topup Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-04");
			rtType.setName("Cash Withdrawal Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-05");
			rtType.setName("B2B Arrangement Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-06");
			rtType.setName("Equilibrium Syariah Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-07");
			rtType.setName("Gold Inventory Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-08"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-08");
			rtType.setName("Redemption Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-09"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-09");
			rtType.setName("Bursa Cash Wallet Statement Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-09-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-09-02");
			rtType.setName("Bursa Gold Wallet Statement Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-09-03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-09-03");
			rtType.setName("Investor Cash Wallet Statement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-09-04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-09-04");
			rtType.setName("Investor Gold Wallet Statement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU12-10"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU12-10");
			rtType.setName("Marketing Consent Report");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}
		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MENU13"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MENU13");
			rtType.setName("User Profile");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MENU")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//LOG = Log Activity
		if(groupList.stream().noneMatch(a -> a.getCode().equals("LOG"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("LOG");
			group.setName("Log Activity");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG03-F01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG03-F01");
			rtType.setName("Topup");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG03-F02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG03-F02");
			rtType.setName("Withdraw");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG03-F03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG03-F03");
			rtType.setName("Transfer");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG03-F11"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG03-F11");
			rtType.setName("Adjustment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG04-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG04-01");
			rtType.setName("OnBoarding Approval");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG07-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG07-01");
			rtType.setName("Replacement Redemption");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG08-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG08-01");
			rtType.setName("Configure Sign-Up Reward");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG08-02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG08-02");
			rtType.setName("Configure Referral Reward");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("LOG04-01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("LOG04-01");
			rtType.setName("Change Account Status");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("LOG")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//ES = Employee Status
		if(groupList.stream().noneMatch(a -> a.getCode().equals("ES"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("ES");
			group.setName("Employee Status");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("SLEMPL"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("SLEMPL");
			rtType.setName("Self-employed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ES")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RETIRED"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RETIRED");
			rtType.setName("Employed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ES")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("EMPL"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("EMPL");
			rtType.setName("Employed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ES")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("UNEMPL"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("UNEMPL");
			rtType.setName("Unemployed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ES")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//BGDRR = BGD Replacement Reason
		if(groupList.stream().noneMatch(a -> a.getCode().equals("BGDRR"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("BGDRR");
			group.setName("BGD Replacement Reason");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("DRR01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("DRR01");
			rtType.setName("Lost or Stolen");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("BGDRR")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("DRR02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("DRR02");
			rtType.setName("Damaged or Defective Item");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("BGDRR")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//ML = Module Log
		if(groupList.stream().noneMatch(a -> a.getCode().equals("MDL"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("MDL");
			group.setName("Module Log");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL01");
			rtType.setName("Login");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL02");
			rtType.setName("Logout");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03");
			rtType.setName("ALL ABOUT WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D01-E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D01-E01");
			rtType.setName("BURSA CASH WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D01-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D01-E02");
			rtType.setName("BURSA GOLD WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D02-E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D02-E01");
			rtType.setName("INVESTOR CASH WALLET (Individual)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D02-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D02-E02");
			rtType.setName("INVESTOR GOLD WALLET (Individual)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D02-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D02-E02");
			rtType.setName("INVESTOR GOLD WALLET (Individual)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D03-E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D03-E01");
			rtType.setName("INVESTOR CASH WALLET (Company)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D03-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D03-E02");
			rtType.setName("INVESTOR GOLD WALLET (Company)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D04-E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D04-E01");
			rtType.setName("SUPPLIER CASH WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D04-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D04-E02");
			rtType.setName("SUPPLIER GOLD WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D05-E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D05-E01");
			rtType.setName("AGENT CASH WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL03-D05-E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL03-D05-E02");
			rtType.setName("AGENT GOLD WALLET");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL04");
			rtType.setName("ALL ABOUT ONBOARDING");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL04-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL04-01");
			rtType.setName("ALL ABOUT ONBOARDING (INDIVIDUAL INVESTOR)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL04-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL04-02");
			rtType.setName("ALL ABOUT ONBOARDING (COMPANY INVESTOR)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL05");
			rtType.setName("ALL ABOUT USER MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06");
			rtType.setName("ALL ABOUT REVENUE MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-01");
			rtType.setName("ALL ABOUT REVENUE CONFIG");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-01-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-01-01");
			rtType.setName("Request Amend revenue");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-01-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-01-02");
			rtType.setName("Approve Amend revenue");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-02");
			rtType.setName("ALL ABOUT COST RECOVERY CONFIG");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-02-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-02-01");
			rtType.setName("Request Amend Cost Recovery");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-02-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-02-02");
			rtType.setName("Approve Amend Cost Recovery");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-03");
			rtType.setName("ALL ABOUT MARGIN CONFIG");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-03-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-03-01");
			rtType.setName("Request Amend Margin");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL06-03-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL06-03-02");
			rtType.setName("Approve Amend Margin");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL07"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL07");
			rtType.setName("ALL ABOUT TIER MANAGEMENT");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL08"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL08");
			rtType.setName("ALL ABOUT REWARD");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL08-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL08-01");
			rtType.setName("Sign Up Reward Management");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("MDL08-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("MDL08-02");
			rtType.setName("Referral Reward Management");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("MDL")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//ACT = Action Level
		if(groupList.stream().noneMatch(a -> a.getCode().equals("ACT"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("ACT");
			group.setName("Action Level");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACT01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACT01");
			rtType.setName("Request");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACT02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACT02");
			rtType.setName("Approve");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACT03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACT03");
			rtType.setName("Reject");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACT04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACT04");
			rtType.setName("Verify/Review");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//Source of Wealth = Wealth
		if(groupList.stream().noneMatch(a -> a.getCode().equals("WEALTH"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("WEALTH");
			group.setName("Source of Wealth");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH01");
			rtType.setName("Business ownership");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH02");
			rtType.setName("Employment and salary");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH03");
			rtType.setName("Inheritance and family wealth");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH04");
			rtType.setName("Intellectual property");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH05");
			rtType.setName("Investment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH06");
			rtType.setName("Legal settlement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH07"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH07");
			rtType.setName("Real estate or assets holdings");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH08"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH08");
			rtType.setName("Rental income");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH09"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH09");
			rtType.setName("Trust funds");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("WEALTH99"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("WEALTH99");
			rtType.setName("Others");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("WEALTH")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//Source of Funds
		if(groupList.stream().noneMatch(a -> a.getCode().equals("FUND"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("FUND");
			group.setName("Source of Funds");
			this.dataManager.save(group);
			groupList.add(group);
		}


		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND01");
			rtType.setName("Business revenue");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND02");
			rtType.setName("Employment income");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND03");
			rtType.setName("Family support");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND04");
			rtType.setName("Inheritance or gifts");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND05");
			rtType.setName("Investment, dividends or royalties income");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND06");
			rtType.setName("Legal settlement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND07"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND07");
			rtType.setName("Loan");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND08"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND08");
			rtType.setName("Pension funds");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND09"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND09");
			rtType.setName("Rental income");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND10"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND10");
			rtType.setName("Sale of property or assets");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND11"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND11");
			rtType.setName("Saving");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("FUND99"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("FUND99");
			rtType.setName("Others");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("FUND")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//TXF = Freq Monthly Transaction
		if(groupList.stream().noneMatch(a -> a.getCode().equals("TXF"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TXF");
			group.setName("Freq Monthly Transaction");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXF10BL"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXF10BL");
			rtType.setName("Below 10 transactions");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXF")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXF30BL"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXF30BL");
			rtType.setName("11-30 transactions");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXF")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXF30AB"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXF30AB");
			rtType.setName("More than 30 transactions");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXF")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//TXT = Total Monthly Transaction
		if(groupList.stream().noneMatch(a -> a.getCode().equals("TXT"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TXT");
			group.setName("Total Monthly Transaction");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXT01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXT01");
			rtType.setName("Below RM3000");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXT02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXT02");
			rtType.setName("RM3000 - RM10000");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXT03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXT03");
			rtType.setName("More Than RM10000");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//TXP = Transaction Purpose Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("TXP"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TXP");
			group.setName("Transaction Purpose Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXP01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXP01");
			rtType.setName("Investment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXP")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXP02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXP02");
			rtType.setName("Saving");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXP")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXP03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXP03");
			rtType.setName("Diversification");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXP")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXP04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXP04");
			rtType.setName("Wealth Preservation");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXP")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("TXP05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TXP05");
			rtType.setName("Retirement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TXP")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//VET = Verification Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("VET"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("VET");
			group.setName("Verification Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("VET01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("VET01");
			rtType.setName("Upgrade Tier1");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("VET")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("VET11"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("VET11");
			rtType.setName("Periodic Review");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("VET")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//ACK = Agreement Acknowledgement
		if(groupList.stream().noneMatch(a -> a.getCode().equals("ACK"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("ACK");
			group.setName("Agreement Acknowledgement");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACK01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACK01");
			rtType.setName("Terms and Conditions Agreement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACK")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("ACK02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ACK02");
			rtType.setName("Receive Marketing Promotion Agreement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ACK")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//RCS = Response Code
		if(groupList.stream().noneMatch(a -> a.getCode().equals("RCS"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("RCS");
			group.setName("Response Code");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCS001"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCS001");
			rtType.setName("OK");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCS")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCS002"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCS002");
			rtType.setName("Created");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCS")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCS003"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCS003");
			rtType.setName("Updated");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCS")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//RCE = Response Code
		if(groupList.stream().noneMatch(a -> a.getCode().equals("RCE"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("RCE");
			group.setName("Response Code");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE000"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE000");
			rtType.setName("Try");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE999"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE999");
			rtType.setName("Miscellaneous error");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE001"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE001");
			rtType.setName("Amount Exceed monthly transaction limit");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE002"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE002");
			rtType.setName("Insufficient Balance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE003"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE003");
			rtType.setName("Invalid Account");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("RCE004"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RCE004");
			rtType.setName("Data Not Found");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RCE")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//NC = Notification Category
		if(groupList.stream().noneMatch(a -> a.getCode().equals("NC"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("NC");
			group.setName("Notification Category");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("NC01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("NC01");
			rtType.setName("Activities");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("NC")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("NC02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("NC02");
			rtType.setName("Announcements");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("NC")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//CHT = Channel Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("CHT"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("CHT");
			group.setName("Channel Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("BA"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("BA");
			rtType.setName("Mobile Application");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CHT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("BW"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("BW");
			rtType.setName("Web Browser");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CHT")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//A = Price Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("A"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("A");
			group.setName("Price Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("A01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("A01");
			rtType.setName("Supplier Price");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("A")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("A02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("A02");
			rtType.setName("Bursa Price");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("A")).toList().get(0));
			this.dataManager.save(rtType);
			rtTypeList.add(rtType);
		}

		//B = Cost Recovery Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("B"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("B");
			group.setName("Cost Recovery Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B01");
			rtType.setName("Deposit Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B02");
			rtType.setName("Withdrawal Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B03");
			rtType.setName("Courier Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B04");
			rtType.setName("Takaful Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B05");
			rtType.setName("Minting Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("B06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("B06");
			rtType.setName("Packaging Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("B")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//C = Revenue Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("C"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("C");
			group.setName("Revenue Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C01");
			rtType.setName("Transaction Fee (Buy)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C02");
			rtType.setName("Transaction Fee (Sell)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

//		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C03"))){
//			RtType rtType = this.dataManager.create(RtType.class);
//			rtType.setCode("C03");
//			rtType.setName("Transfer Fee (Receive)");
//			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
//			this.dataManager.save(rtType);
//		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C04");
			rtType.setName("Transfer Fee (Sender)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C05");
			rtType.setName("Margin (Buy)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C06");
			rtType.setName("Margin (Sell)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C07"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C07");
			rtType.setName("Onboarding Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C08"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C08");
			rtType.setName("Ar Rahnu Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C09"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C09");
			rtType.setName("Qard Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("C10"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("C10");
			rtType.setName("Administrative Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("C")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//D = Registration Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("D"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("D");
			group.setName("Registration Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D01");
			rtType.setName("Platform Owner");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D02");
			rtType.setName("Individual Investor");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D03");
			rtType.setName("Company Investor");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D04");
			rtType.setName("Supplier");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D05");
			rtType.setName("Agent");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("D06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("D06");
			rtType.setName("Trustee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("D")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//E = Wallet Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("E"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("E");
			group.setName("Wallet Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("E01");
			rtType.setName("Cash");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("E")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("E02");
			rtType.setName("Asset (Gold)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("E")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("E03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("E03");
			rtType.setName("Asset (Dinar Coin)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("E")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//F = Transaction Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("F"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("F");
			group.setName("Transaction Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F01");
			rtType.setName("Deposit");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F02");
			rtType.setName("Withdraw");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F03");
			rtType.setName("Transfer");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F03/E02-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F03/E02-02");
			rtType.setName("Gold Receive");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F03/E01-01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F03/E01-01");
			rtType.setName("Cash Transfer");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F03/E01-02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F03/E01-02");
			rtType.setName("Cash Receive");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F04"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F04");
			rtType.setName("Redeem");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F05"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F05");
			rtType.setName("Bursa Store Buy");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F06"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F06");
			rtType.setName("Bursa Store Sell");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F07"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F07");
			rtType.setName("Marketplace Buy");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F08"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F08");
			rtType.setName("Marketplace Sell");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F09"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F09");
			rtType.setName("BursaStore Trade Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F09/01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F09/01");
			rtType.setName("Administrative Fee");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F10"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F10");
			rtType.setName("Carry Forward");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F09/02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F09/02");
			rtType.setName("SST");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F09/03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F09/03");
			rtType.setName("GST");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F09/03"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F09/03");
			rtType.setName("GST");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F13/E01"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F13/E01");
			rtType.setName("Cash Adjustment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(rtTypeList.stream().noneMatch(a -> a.getCode().equals("F13/E02"))){
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("F13/E02");
			rtType.setName("Gold Adjustment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("F")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//G = Status Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("G"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("G");
			group.setName("Status Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G01");
			rtType.setName("Completed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G02");
			rtType.setName("Failed");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G03");
			rtType.setName("Not Active");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G04");
			rtType.setName("Active");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G05");
			rtType.setName("Pending");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G05/01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G05/01");
			rtType.setName("Pending-Verification");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G05/02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G05/02");
			rtType.setName("Pending-Approval");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G06");
			rtType.setName("Approved");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G07");
			rtType.setName("Reject");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G08"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G08");
			rtType.setName("Processing");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G11"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G11");
			rtType.setName("Draft");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G12"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G12");
			rtType.setName("Pending DV");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("G15"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("G15");
			rtType.setName("Archived");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("G")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//H = Payment Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("H"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("H");
			group.setName("Payment Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("H01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("H01");
			rtType.setName("Online Banking");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("H")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("H02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("H02");
			rtType.setName("Credit Card & Debit Card");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("H")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//J = UOM Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("J"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("J");
			group.setName("UOM Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("J01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("J01");
			rtType.setName("RM");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("J02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("J02");
			rtType.setName("Gram");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("J03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("J03");
			rtType.setName("Dinar");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("J04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("J04");
			rtType.setName("%");
			rtType.setDescription("Percentage");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//K = Investor Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("K"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("K");
			group.setName("Investor Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("K01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("K01");
			rtType.setName("Individual");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("K02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("K02");
			rtType.setName("Company");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("J")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//L = Address Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("L"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("L");
			group.setName("Address Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L01");
			rtType.setName("Home");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L02");
			rtType.setName("Company");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L03");
			rtType.setName("Billing");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L04");
			rtType.setName("Shipping");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L05");
			rtType.setName("Warehouse");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("L06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("L06");
			rtType.setName("Vault");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("L")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//M = Frequent Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("M"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("M");
			group.setName("Frequent Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("M01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("M01");
			rtType.setName("Daily");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("M")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("M02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("M02");
			rtType.setName("Weekly");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("M")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("M03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("M03");
			rtType.setName("Monthly");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("M")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("M04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("M04");
			rtType.setName("Yearly");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("M")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//N = Company Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("N"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("N");
			group.setName("Company Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N01");
			rtType.setName("Sole Proprietry");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N02");
			rtType.setName("Partnership");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N03");
			rtType.setName("Private Limited Company (Sdn Bhd)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N04");
			rtType.setName("Company Limited by Guarantee (GLC)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N05");
			rtType.setName("Unlimited Company (Sdn)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N06");
			rtType.setName("Public Limited Company (Bhd)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N07");
			rtType.setName("Foreign Company");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("N08"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("N08");
			rtType.setName("Limited Liability Partnership (LLP)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("N")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//P = Document Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("P"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("P");
			group.setName("Document Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P01");
			rtType.setName("Front Mykad");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P02");
			rtType.setName("Back Mykad");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P03");
			rtType.setName("Passport");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P04");
			rtType.setName("MyTentera");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P05");
			rtType.setName("Police");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P06");
			rtType.setName("Selfie");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P07");
			rtType.setName("Utility Statement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("P08"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("P08");
			rtType.setName("Bank Statement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("P")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//Q = Security Question Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("Q"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("Q");
			group.setName("Security Question Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q01");
			rtType.setName("Cat Name");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q02");
			rtType.setName("Birth Place");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q03");
			rtType.setName("Mother's Maiden");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q04");
			rtType.setName("Favourite Pet");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q05");
			rtType.setName("High School");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q06");
			rtType.setName("Spouse First Meet Place");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("Q07"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("Q07");
			rtType.setName("Elementary School");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("Q")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//R = Company Category Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("R"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("R");
			group.setName("Company Category Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("R01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("R01");
			rtType.setName("SME");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("R")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("R02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("R02");
			rtType.setName("MNC");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("R")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//S = Trading Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("S"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("S");
			group.setName("Trading Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S01");
			rtType.setName("Limit order buy");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S02");
			rtType.setName("Limit order sell");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S03");
			rtType.setName("Market order buy");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S04");
			rtType.setName("Market order sell");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S05");
			rtType.setName("Marketplace buy");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S06");
			rtType.setName("Marketplace sell");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S11"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S11");
			rtType.setName("Zero Filled");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S12"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S12");
			rtType.setName("Partial Filled");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S13"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S13");
			rtType.setName("All Filled");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S14"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S14");
			rtType.setName("Cancelled");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S15"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S15");
			rtType.setName("No Volume");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S21"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S21");
			rtType.setName("Cash");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("S22"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("S22");
			rtType.setName("Quantity");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("S")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//T = Tier Type
		if(groupList.stream().noneMatch(a -> a.getCode().equals("T"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("T");
			group.setName("Tier Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("T01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("T01");
			rtType.setName("Tier 1");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("T")).toList().get(0));
			this.dataManager.save(rtType);
		}

//		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("T02"))) {
//			RtType rtType = this.dataManager.create(RtType.class);
//			rtType.setCode("T02");
//			rtType.setName("Tier 2");
//			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("T")).toList().get(0));
//			this.dataManager.save(rtType);
//		}

//		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("T03"))) {
//			RtType rtType = this.dataManager.create(RtType.class);
//			rtType.setCode("T03");
//			rtType.setName("Tier 3");
//			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("T")).toList().get(0));
//			this.dataManager.save(rtType);
//		}

		//CCY = Currency
		if(groupList.stream().noneMatch(a -> a.getCode().equals("CCY"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("CCY");
			group.setName("Currency");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("CCY01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("CCY01");
			rtType.setName("Malaysia Ringgit");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CCY")).toList().get(0));
			this.dataManager.save(rtType);

			Currency currency = this.dataManager.create(Currency.class);
			currency.setCurrency(rtType);
			currency.setAbbreviation("MYR");
			currency.setRate(BigDecimal.valueOf(1));
			currency.setPer100(false);
			currency.setRangeUp(BigDecimal.valueOf(0));
			currency.setRangeDown(BigDecimal.valueOf(0));
			this.dataManager.save(currency);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("CCY02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("CCY02");
			rtType.setName("Singapore Dollar");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CCY")).toList().get(0));
			this.dataManager.save(rtType);

			Currency currency = this.dataManager.create(Currency.class);
			currency.setCurrency(rtType);
			currency.setAbbreviation("SGD");
			currency.setRate(BigDecimal.valueOf(3.2478));
			currency.setPer100(false);
			currency.setRangeUp(BigDecimal.valueOf(0.3079));
			currency.setRangeDown(BigDecimal.valueOf(0.3079));
			this.dataManager.save(currency);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("CCY03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("CCY03");
			rtType.setName("Brunei Dollar");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CCY")).toList().get(0));
			this.dataManager.save(rtType);

			Currency currency = this.dataManager.create(Currency.class);
			currency.setCurrency(rtType);
			currency.setAbbreviation("BND");
			currency.setRate(BigDecimal.valueOf(3.2478));
			currency.setPer100(false);
			currency.setRangeUp(BigDecimal.valueOf(0.3079));
			currency.setRangeDown(BigDecimal.valueOf(0.3079));
			this.dataManager.save(currency);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("CCY04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("CCY04");
			rtType.setName("Indonesian Rupiah");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("CCY")).toList().get(0));
			this.dataManager.save(rtType);

			Currency currency = this.dataManager.create(Currency.class);
			currency.setCurrency(rtType);
			currency.setAbbreviation("IDR");
			currency.setRate(BigDecimal.valueOf(0.0285));
			currency.setPer100(true);
			currency.setRangeUp(BigDecimal.valueOf(3509));
			currency.setRangeDown(BigDecimal.valueOf(3509));
			this.dataManager.save(currency);
		}

		//TM = Transfer Motive
		if(groupList.stream().noneMatch(a -> a.getCode().equals("TM"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TM");
			group.setName("Transfer Motive");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TM01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TM01");
			rtType.setName("Gift");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TM")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TM02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TM02");
			rtType.setName("Charity");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TM")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TM03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TM03");
			rtType.setName("Fund");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TM")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("GT"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("GT");
			group.setName("Gold Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("GT01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("GT01");
			rtType.setName("Gold Bar (1kg)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("GT")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("GT02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("GT02");
			rtType.setName("Gold Dinar");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("GT")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("GT03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("GT03");
			rtType.setName("B2B Gold");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("GT")).toList().get(0));
			this.dataManager.save(rtType);

		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("TAX"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TAX");
			group.setName("TAX");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TAX00"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TAX00");
			rtType.setName("Exempt");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TAX")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TAX01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TAX01");
			rtType.setName("SST");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TAX")).toList().get(0));
			this.dataManager.save(rtType);
		}

//		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TAX01"))) {
//			RtType rtType = this.dataManager.create(RtType.class);
//			rtType.setCode("TAX02");
//			rtType.setName("GST");
//			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TAX")).toList().get(0));
//			this.dataManager.save(rtType);
//		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("RR"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("RR");
			group.setName("Redeem Reason");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR01");
			rtType.setName("Investment");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR02");
			rtType.setName("Collection");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR03");
			rtType.setName("Gift");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR04");
			rtType.setName("Savings");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR05"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR05");
			rtType.setName("Emergency Fund");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("RR06"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("RR06");
			rtType.setName("Charity Donation");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("RR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("BDR"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("BDR");
			group.setName("Bursa Dinar Reordering Rules");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("BDR01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("BDR01");
			rtType.setName("Pcs");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("BDR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("BDR02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("BDR02");
			rtType.setName("Day");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("BDR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("BDR03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("BDR03");
			rtType.setName("Gold Dinar (4.25g)");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("BDR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("ITF"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("ITF");
			group.setName("Issuance Type");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("ITF/01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ITF/01");
			rtType.setName("Primary Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ITF")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("ITF/02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ITF/02");
			rtType.setName("Subsequent Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ITF")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("ITF/03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ITF/03");
			rtType.setName("B2B Buy Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ITF")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("ITF/04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("ITF/04");
			rtType.setName("B2B Sell Issuance");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("ITF")).toList().get(0));
			this.dataManager.save(rtType);
		}

		//Trasnfer Reason
		if(groupList.stream().noneMatch(a -> a.getCode().equals("TR"))) {
			RtTypeGroup group = this.dataManager.create(RtTypeGroup.class);
			group.setCode("TR");
			group.setName("Transfer Reason");
			this.dataManager.save(group);
			groupList.add(group);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TR01"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TR01");
			rtType.setName("Refunds or Returns");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TR02"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TR02");
			rtType.setName("Reimbursement");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TR03"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TR03");
			rtType.setName("Purchase");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TR")).toList().get(0));
			this.dataManager.save(rtType);
		}

		if (rtTypeList.stream().noneMatch(a -> a.getCode().equals("TR04"))) {
			RtType rtType = this.dataManager.create(RtType.class);
			rtType.setCode("TR04");
			rtType.setName("Sale");
			rtType.setTypeGroup(groupList.stream().filter(f -> f.getCode().equals("TR")).toList().get(0));
			this.dataManager.save(rtType);
		}

	}

	public void initTierManagement() {
		List<TierManagement> tierManagements = this.dataManager.load(TierManagement.class).all().list();

		//Tier 1
		if(tierManagements.stream().noneMatch(a -> a.getCode().equals("T01"))) {
			TierManagement tierManagement = this.dataManager.create(TierManagement.class);
			tierManagement.setCode("T01");
			tierManagement.setName("Tier 1");
			this.dataManager.save(tierManagement);

			List<TierThreshold> thresholds = this.dataManager.load(TierThreshold.class)
					.query("select e from TierThreshold e where e.tierManagement = :tierManagement")
					.parameter("tierManagement", tierManagement).list();

			//deposit
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F01"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F01").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J01").one());
				this.dataManager.save(threshold);
			}

			//withdraw
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F02"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F02").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J01").one());
				this.dataManager.save(threshold);
			}

			//transfer
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F03"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F03").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J02").one());
				this.dataManager.save(threshold);
			}
		}

		//Tier 2
		if(tierManagements.stream().noneMatch(a -> a.getCode().equals("T02"))) {
			TierManagement tierManagement = this.dataManager.create(TierManagement.class);
			tierManagement.setCode("T02");
			tierManagement.setName("Tier 2");
			this.dataManager.save(tierManagement);

			List<TierThreshold> thresholds = this.dataManager.load(TierThreshold.class)
					.query("select e from TierThreshold e where e.tierManagement = :tierManagement")
					.parameter("tierManagement", tierManagement).list();

			//deposit
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F01"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F01").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J01").one());
				this.dataManager.save(threshold);
			}

			//withdraw
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F02"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F02").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J01").one());
				this.dataManager.save(threshold);
			}

			//transfer
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F03"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F03").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J02").one());
				this.dataManager.save(threshold);
			}
		}

		//Tier 3
		if(tierManagements.stream().noneMatch(a -> a.getCode().equals("T03"))) {
			TierManagement tierManagement = this.dataManager.create(TierManagement.class);
			tierManagement.setCode("T03");
			tierManagement.setName("Tier 3");
			this.dataManager.save(tierManagement);

			List<TierThreshold> thresholds = this.dataManager.load(TierThreshold.class)
					.query("select e from TierThreshold e where e.tierManagement = :tierManagement")
					.parameter("tierManagement", tierManagement).list();

			//deposit
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F01"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F01").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J02").one());
				this.dataManager.save(threshold);
			}

			//withdraw
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F02"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F02").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J02").one());
				this.dataManager.save(threshold);
			}

			//transfer
			if(thresholds.stream().noneMatch(a -> a.getType().getCode().equals("F03"))) {
				TierThreshold threshold = this.dataManager.create(TierThreshold.class);
				threshold.setTierManagement(tierManagement);
				threshold.setType(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "F03").one());
				threshold.setUom(this.dataManager.load(RtType.class)
						.query("select e from RtType e where e.code = :code")
						.parameter("code", "J02").one());
				this.dataManager.save(threshold);
			}
		}
	}

	public void initFeesManagement(){
		List<FeesManagement> feesManagementList = this.dataManager.load(FeesManagement.class).all().list();

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C01"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C01").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C02"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C02").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C03"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C03").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C04"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C04").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C05"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C05").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C06"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C06").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C07"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C07").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C08"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C08").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}

		if(feesManagementList.stream().noneMatch(a -> a.getType().getCode().equals("C09"))) {
			FeesManagement feesManagement = this.dataManager.create(FeesManagement.class);
			feesManagement.setType(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "C09").one());
			feesManagement.setUom(this.dataManager.load(RtType.class)
					.query("select e from RtType e where e.code = :code")
					.parameter("code", "J01").one());
			feesManagement.setCharge(BigDecimal.valueOf(0.00));
			feesManagement.setActive(false);
			this.dataManager.save(feesManagement);
		}
	}

	public void initRevenueManagement(Product product) {
		List<RtType> typeList = this.dataManager.load(RtType.class).all().list();
		List<RevenueManagement> revenueManagementList = this.dataManager.load(RevenueManagement.class).all().list();
		Map<String, String> map = new HashMap<>();
		map.put("C01","");
		map.put("C02","");
		map.put("C03","");
		map.put("C04","");
		map.put("C05","");
		map.put("C06","");
		map.put("C07","");
		map.put("C08","");
		map.put("C09","");
		map.put("C10","");

		SaveContext saveContext = new SaveContext();

		for(String key: map.keySet()) {
			if(revenueManagementList.stream().noneMatch(a -> a.getProduct().equals(product) && a.getType().getCode().equals(key))) {
				RevenueManagement revenueManagement = this.dataManager.create(RevenueManagement.class);
				revenueManagement.setProduct(product);
				revenueManagement.setType(typeList.stream().filter(f -> f.getCode().equals(key)).toList().get(0));
				revenueManagement.setUom(typeList.stream().filter(f -> f.getCode().equals("J04")).toList().get(0)); //UOM percentage (%)
				revenueManagement.setActive(true);
				this.dataManager.save(revenueManagement);
			}
		}
	}

	public void initCostRecoveryManagement(Product product) {
		List<RtType> typeList = this.dataManager.load(RtType.class).all().list();
		List<CostRevoceryManagement> costRevoceryManagementList = this.dataManager.load(CostRevoceryManagement.class).all().list();
		Map<String, String> map = new HashMap<>();
		map.put("B01","");
		map.put("B02","");
		map.put("B03","");
		map.put("B04","");
		map.put("B05","");

		SaveContext saveContext = new SaveContext();

		for(String key: map.keySet()) {
			if(costRevoceryManagementList.stream().noneMatch(a -> a.getProduct().equals(product) && a.getType().getCode().equals(key))) {
				CostRevoceryManagement costRevoceryManagement = this.dataManager.create(CostRevoceryManagement.class);
				costRevoceryManagement.setProduct(product);
				costRevoceryManagement.setType(typeList.stream().filter(f -> f.getCode().equals(key)).toList().get(0));
				costRevoceryManagement.setUom(typeList.stream().filter(f -> f.getCode().equals("J04")).toList().get(0)); //UOM percentage (%)
				costRevoceryManagement.setActive(true);
				this.dataManager.save(costRevoceryManagement);
			}
		}
	}

	private void initProductPrice(){
		ProductPrice productPrice = this.dataManager.create(ProductPrice.class);
		productPrice.setBuyPrice(BigDecimal.ZERO);
		productPrice.setSellPrice(BigDecimal.ZERO);


	}

	public void initTransactionType() {
		List<TransactionType> groupList = this.dataManager.load(TransactionType.class).all().list();

		if(groupList.stream().noneMatch(a -> a.getCode().equals("F03/E02-02"))) {
			TransactionType type = this.dataManager.create(TransactionType.class);
			type.setCode("F03/E02-02");
			type.setName("Gold Receive");
			type.setVersion(1);
			this.dataManager.save(type);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("F03/E01-01"))) {
			TransactionType type = this.dataManager.create(TransactionType.class);
			type.setCode("F03/E01-01");
			type.setName("Cash Transfer");
			type.setVersion(1);
			this.dataManager.save(type);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("F03/E01-02"))) {
			TransactionType type = this.dataManager.create(TransactionType.class);
			type.setCode("F03/E01-02");
			type.setName("Cash Receive");
			type.setVersion(1);
			this.dataManager.save(type);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("F13/E01"))) {
			TransactionType type = this.dataManager.create(TransactionType.class);
			type.setCode("F13/E01");
			type.setName("Cash Adjustment");
			type.setVersion(1);
			this.dataManager.save(type);
		}

		if(groupList.stream().noneMatch(a -> a.getCode().equals("F13/E02"))) {
			TransactionType type = this.dataManager.create(TransactionType.class);
			type.setCode("F13/E02");
			type.setName("Gold Adjustment");
			type.setVersion(1);
			this.dataManager.save(type);
		}
	}

	public void initTransStatus() {
		List<TransStatus> groupList = this.dataManager.load(TransStatus.class).all().list();

		if (groupList.stream().noneMatch(a -> a.getCode().equals("G05/02"))) {
			TransStatus type = this.dataManager.create(TransStatus.class);
			type.setCode("G05/02");
			type.setName("Pending-Approval");
			type.setVersion(1);
			this.dataManager.save(type);
		}
	}

}
