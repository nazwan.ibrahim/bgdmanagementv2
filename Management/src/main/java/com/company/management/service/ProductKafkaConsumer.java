package com.company.management.service;

import com.company.management.api.ProductController;
import io.jmix.core.EntitySerialization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class ProductKafkaConsumer {

//	private final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
	private final Logger logger = LoggerFactory.getLogger("API");
	private EntitySerialization entitySerialization;

	private ProductController productController;

	@Autowired
	private KafkaTemplate<String, Object> kafkaTemplate;

//	@KafkaListener(topics = "jmix-kafka-reference", groupId = "group_id1")
//	public void getProductPrice(String message) throws IOException{
//		try{
//			productController.getListProductPrice();
//		}catch (Exception ex){
//			logger.error(ex.getMessage());
//		}
//	}

	public void sendProductPrice() {
		try {
			this.kafkaTemplate.send("jmix-kafka-json", entitySerialization.objectToJson(productController.getListProductPrice()));
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}
}
