package com.company.management.service;

import com.company.management.form.FeesForm;
import com.company.management.form.WalletTopupForm;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class TopupWalletService {

	private static final Logger log = LoggerFactory.getLogger(TopupWalletService.class);
	@Autowired
	private Environment  environment;

//	private final HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
//			HttpClientBuilder.create().build());


//	public Integer topUpWallet(String amount, String token, String accountNo){
//		try {
//			RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
//
//			String url = Objects.requireNonNull(environment.getProperty("topupwallet"));
//			HttpHeaders headers = new HttpHeaders();
//
//			headers.setBearerAuth(token);
//			headers.setContentType(MediaType.APPLICATION_JSON);
//			headers.add("Accept","application/json");
//
//			WalletTopupForm walletTopupForm = new WalletTopupForm();
//			walletTopupForm.setAcctNumber(accountNo);
//			walletTopupForm.setAmount(amount);
//			walletTopupForm.setTransactionMethodCode("H01");
//			walletTopupForm.setTransactionTypeCode("F01");
//			walletTopupForm.setStatusCode("G01");
//
//			FeesForm feesForm = new FeesForm();
//			feesForm.setFeeType("Transaction Fee");
//			feesForm.setFeeTypeCode("F09");
//			feesForm.setSetupFees(BigDecimal.valueOf(5.5));
//			feesForm.setSetupUom("RM");
//			feesForm.setSetupUomCode("string");
//			feesForm.setChargeFees(BigDecimal.valueOf(5.5));
//			feesForm.setChargeUom("RM");
//			feesForm.setChargeUomCode("J01");
//
//			List<FeesForm> feesFormList = new ArrayList<>();
//			feesFormList.add(feesForm);
//			walletTopupForm.setFeesForms(feesFormList);
//
//
//			List<WalletTopupForm> walletTopupFormList = new ArrayList<>();
//			walletTopupFormList.add(walletTopupForm);
//			ObjectMapper objectMapper = new ObjectMapper();
//			String requestBody = objectMapper.writeValueAsString(walletTopupFormList);
//
//			HttpEntity<String> requestEntity = new HttpEntity<>(requestBody, headers);
//			log.info("request -> " +requestBody);
//
//			ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, String.class);
//
//			int statusCode = responseEntity.getStatusCodeValue();
//			String responseBody = responseEntity.getBody();
//
//			log.info("Status code: " + statusCode);
//			log.info("Response body: " + responseBody);
//
//			if(statusCode == 200){
//				return 200;
//			}
//			else{
//				return 400;
//			}
//		}catch (Exception ex){
//			log.info(ex.toString());
//			return 400;
//		}
//	}

}
