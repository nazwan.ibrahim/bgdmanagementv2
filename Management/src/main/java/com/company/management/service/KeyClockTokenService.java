package com.company.management.service;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@Component
public class KeyClockTokenService {

	private static final Logger log = org.slf4j.LoggerFactory.getLogger(KeyClockTokenService.class);
	@Autowired
	private Environment environment;

	public String getKeyCloakToken()
	{
		String username = environment.getProperty("keycloak_username");
		String password = environment.getProperty("keycloak_password");
		String client_id = environment.getProperty("keycloak_client_id");
		String authUrl = environment.getProperty("keycloak_url") +"/auth/realms/" +environment.getProperty("keycloak_realms")+ "/protocol/openid-connect/token";

		String formBody = "grant_type=password"
				+"&username="+username
				+"&password="+password
				+"&client_id="+client_id;
		OkHttpClient client = new OkHttpClient().newBuilder()
				.build();
		MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");

		okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType,formBody);
		okhttp3.Request request = new okhttp3.Request.Builder()
				.url(authUrl)
				.method("POST", body)
				.addHeader("Content-Type", "application/x-www-form-urlencoded")
				.build();
		log.info(request.toString());
		try{
			okhttp3.Response response = client.newCall(request).execute();
			log.info(response.toString());

			JSONObject bodyJSON = new JSONObject(response.body().string());
			String token = bodyJSON.getString("access_token");
			if (response.code()==200)
				return String.valueOf(token);
			else
				return null;
		}
		catch (Exception ex){
			log.error("Keycloak Token -> "+ex.getMessage());
			return ex.getMessage();
		}
	}

	public Map<String,Object> createKeyCloakUser(String username, String email, String password, String AUTH_TOKEN) {
		String createuserUrl = environment.getProperty("keycloak_url") +"/auth/admin/realms/bgdadmin/users";

		JSONObject jsonBody = new JSONObject();
		jsonBody.put("username",username);
		jsonBody.put("email",email);

		JSONArray credentialsArray = new JSONArray();
		JSONObject credentials = new JSONObject();
		credentials.put("type","password");
		credentials.put("value",password);
		credentialsArray.put(credentials);
		jsonBody.put("credentials",credentialsArray);
		jsonBody.put("enabled","true");

		JSONObject access = new JSONObject();
		access.put("manageGroupMembership", true);
		access.put("view", true);
		access.put("mapRoles", true);
		access.put("impersonate", false);
		access.put("manage", true);
		jsonBody.put("access",access);

		OkHttpClient client = new OkHttpClient().newBuilder()
				.build();
		MediaType JSON = MediaType.parse("application/json");

		okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBody.toString(),JSON);
		okhttp3.Request request = new okhttp3.Request.Builder()
				.url(createuserUrl)
				.method("POST", body)
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
				.build();

		Map<String,Object> res = new HashMap<>();
		try{
			okhttp3.Response response = client.newCall(request).execute();

			res.put("code",response.code());
			res.put("message",response.message());
			return res;
		}
		catch (Exception ex){
			log.error(("Keycloak API error: " + ex.getMessage()));
			res.put("code",400);
			res.put("message",ex.getMessage());
			return res;
		}
	}

	public String keycloakGetUserId(String username, String AUTH_TOKEN){

		String realm = "bgdadmin";
		String updateuserUrl = environment.getProperty("keycloak_url") +"/auth/admin/realms/" +environment.getProperty("keycloak_realms")+ "/users?username="+username;

		OkHttpClient client = new OkHttpClient().newBuilder()
				.build();
		MediaType JSON = MediaType.parse("application/json");
		okhttp3.Request request = new okhttp3.Request.Builder()
				.url(updateuserUrl)
				.get()
				.addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
				.build();
		log.info(request.toString());
		try{
			okhttp3.Response response = client.newCall(request).execute();
			log.info(response.toString());
			var StrBody = response.body().string();
			if (StrBody.startsWith("[") && StrBody.endsWith("]")){
				StrBody = StrBody.substring(1,StrBody.length()-1);
			}
			JSONObject bodyJSON = new JSONObject(StrBody);
			if (response.isSuccessful()){
				return bodyJSON.getString("id");
			}
			else
				return null;
		}
		catch (Exception ex){
			log.error(ex.getMessage());
			return null;
		}

	}
	public Map<String,Object> keycloakUpdatePassword(String id, String newPassword, String AUTH_TOKEN) {

		String realm = "bgdadmin";
		String updateuserUrl = environment.getProperty("keycloak_url") +"/auth/admin/realms/"+realm+"/users/"+id+"/reset-password";
		JSONObject jsonBody = new JSONObject();
		jsonBody.put("type","password");
		jsonBody.put("value",newPassword);
		jsonBody.put("temporary",false);
//
//        String jsonBody ="{"
//                +"\"type\": \"password\","
//                +"\"value\":\""+newPassword+"\","
//                +"\"temporary\":false"
//                +"}";

		OkHttpClient client = new OkHttpClient().newBuilder()
				.build();
		MediaType JSON = MediaType.parse("application/json");
		okhttp3.RequestBody body = okhttp3.RequestBody.create(jsonBody.toString(),JSON);
		okhttp3.Request request = new okhttp3.Request.Builder()
				.url(updateuserUrl)
				.method("PUT", body)
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization", "Bearer "+ AUTH_TOKEN)
				.build();

		Map<String,Object> res = new HashMap<>();
		try{
			okhttp3.Response response = client.newCall(request).execute();
			res.put("code",response.code());
			res.put("message",response.message());
			return res;
//            return String.valueOf(response.code());
		}
		catch (Exception ex){
			log.error("Keycloak API error: " + ex.getMessage());
			res.put("code",400);
			res.put("message",ex.getMessage());
			return res;
//            return ex.getMessage();
		}

	}

	public Map<String,Object> deleteUser(String username) {
		Map<String,Object> res = new HashMap<>();

		try {
			String bearerAuth = getKeyCloakToken();
			String ssoID = keycloakGetUserId(username, bearerAuth);

			String url = environment.getProperty("keycloak_url") +"/auth/admin/realms/" +environment.getProperty("keycloak_realms")+ "/users/" +ssoID;
			HttpHeaders headers = new HttpHeaders();
			headers.setBearerAuth(bearerAuth);

			HttpEntity<String> request = new HttpEntity<>("", headers);
			log.info(request.toString());

			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.DELETE, request, String.class);
			log.info(response.toString());

			if(response.getStatusCode().is2xxSuccessful()) {
				res.put("code", HttpStatus.OK);
				res.put("message", "");
			}

		} catch (HttpClientErrorException exHttp) {
			log.error(exHttp.getMessage());
			res.put("code", HttpStatus.BAD_GATEWAY);
			res.put("message", exHttp.getMessage());
		} catch (Exception ex) {
			log.error(ex.getMessage());
			res.put("code", HttpStatus.BAD_REQUEST);

		}

		return res;
	}
}
