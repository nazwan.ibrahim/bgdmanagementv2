package com.company.management.service;

import com.company.management.entity.*;
import com.company.management.entity.wallet.TransactionAcct;
import io.jmix.core.DataManager;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class BgdIssuanceService {
	private static final Logger log = org.slf4j.LoggerFactory.getLogger(BgdIssuanceService.class);
	@Autowired
	private DataManager dataManager;

	@Autowired
	private BgdIssuanceLoadRecord combinedDataDTO;

	public List<BgdIssuanceLoadRecord> getCombinedData() {
		List<BgdIssuanceLoadRecord> combinedDataList = new ArrayList<>();
		try{
			List<ShariahRecord> entity1List = getPurchaseRecord();
			List<ShariahRecordSubsequent> entity2List = getReserveRecord();
			List<TransactionAcct> entity3List = getTransactionAcct();



			for (ShariahRecord shariahRecord : entity1List) {
				BgdIssuanceLoadRecord combinedDataDTO = dataManager.create(BgdIssuanceLoadRecord.class);
				combinedDataDTO.setTimeStamp(shariahRecord.getDeliveryRecord().getCreatedDate() != null ? shariahRecord.getDeliveryRecord().getCreatedDate():Date.valueOf("-"));
				combinedDataDTO.setDescription(shariahRecord.getDeliveryRecord().getPurchaseRecord().getDescription() != null ? shariahRecord.getDeliveryRecord().getPurchaseRecord().getDescription():"-");
				try{
					PurchaseRecordProduct purchaseRecordProduct = this.dataManager.load(PurchaseRecordProduct.class)
							.query("select e from PurchaseRecordProduct e where e.purchase_record =:purchase_record")
							.parameter("purchase_record", shariahRecord.getDeliveryRecord().getPurchaseRecord())
							.one();
					combinedDataDTO.setCode(purchaseRecordProduct.getCode() != null ? purchaseRecordProduct.getCode():"-");
					combinedDataDTO.setGoldType(purchaseRecordProduct.getType().getName() != null ? purchaseRecordProduct.getType().getName():"-");

					combinedDataDTO.setTradable(purchaseRecordProduct.getTradable_gold() != null ? purchaseRecordProduct.getTradable_gold():BigDecimal.valueOf(Long.parseLong("-")));
					combinedDataDTO.setIssuanceNo(purchaseRecordProduct.getPurchase_record().getReference_Num());
				}catch (Exception ex){
//				log.info("ShariahRecord "+ex.getMessage());
					log.info("ShariahRecord " + ex.getMessage());
				}

				combinedDataList.add(combinedDataDTO);
			}

			for (ShariahRecordSubsequent shariahRecordSubsequent : entity2List) {
				BgdIssuanceLoadRecord combinedDataDTO = dataManager.create(BgdIssuanceLoadRecord.class);
				combinedDataDTO.setTimeStamp(shariahRecordSubsequent.getDeliveryRecord().getCreatedDate() != null ? shariahRecordSubsequent.getDeliveryRecord().getCreatedDate(): Date.valueOf("-"));
				combinedDataDTO.setDescription(shariahRecordSubsequent.getDeliveryRecord().getReserveRecord().getDescription() != null ? shariahRecordSubsequent.getDeliveryRecord().getReserveRecord().getDescription():"-");
				try{
					ReserveRecordProduct reserveRecordProduct = this.dataManager.load(ReserveRecordProduct.class)
							.query("select e from ReserveRecordProduct e where e.reverse_record =:reverse_record")
							.parameter("reverse_record", shariahRecordSubsequent.getDeliveryRecord().getReserveRecord())
							.one();
					combinedDataDTO.setCode(reserveRecordProduct.getCode() != null ? reserveRecordProduct.getCode():"-");
					combinedDataDTO.setGoldType(reserveRecordProduct.getType().getName() != null ? reserveRecordProduct.getType().getName():"-");

					combinedDataDTO.setTradable(reserveRecordProduct.getTradable_gold() != null ? reserveRecordProduct.getTradable_gold(): BigDecimal.valueOf(Long.parseLong("-")));
					combinedDataDTO.setIssuanceNo(reserveRecordProduct.getReverse_record().getReference_Num());
				}catch (Exception ex){
					log.info("ShariahRecordSubsequent "+ex.getMessage());
				}

				combinedDataList.add(combinedDataDTO);
			}

			for (TransactionAcct transactionAcct : entity3List) {
				BgdIssuanceLoadRecord combinedDataDTO = dataManager.create(BgdIssuanceLoadRecord.class);
				combinedDataDTO.setTimeStamp(transactionAcct.getCreatedDate() != null ? transactionAcct.getCreatedDate():null);
				combinedDataDTO.setDescription(transactionAcct.getTransType().getCode() != null ? transactionAcct.getTransType().getCode():null);
				combinedDataDTO.setCode(null);
				combinedDataDTO.setGoldType(transactionAcct.getTransType().getName() != null ? transactionAcct.getTransType().getName():null);
				combinedDataDTO.setIssuanceNo(transactionAcct.getReference());
				combinedDataDTO.setTradable(transactionAcct.getAmount() != null ? transactionAcct.getAmount():null);
				combinedDataList.add(combinedDataDTO);
			}

			return combinedDataList;
		}catch (Exception ex){
			log.error(ex.getMessage());
		}
		return combinedDataList;
	}

	private List<ShariahRecord> getPurchaseRecord(){
		return this.dataManager.load(ShariahRecord.class)
				.query("select e from ShariahRecord e where e.status.code = :code")
				.parameter("code","G01")
				.list();
	}

	private List<ShariahRecordSubsequent> getReserveRecord(){
		return this.dataManager.load(ShariahRecordSubsequent.class)
				.query("select e from ShariahRecordSubsequent e where e.status.code = :code")
				.parameter("code","G01")
				.list();
	}

	private List<TransactionAcct> getTransactionAcct(){
		return this.dataManager.load(TransactionAcct.class)
				.query("select distinct e from TransactionAcct e where e.transType.code = :transType")
				.parameter("transType","F04")
				.list();
	}
}
