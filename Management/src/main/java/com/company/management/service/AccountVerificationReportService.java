package com.company.management.service;

import com.company.management.entity.AccountVerificationReport;
import com.company.management.entity.registrationdb.UserAddress;
import com.company.management.entity.registrationdb.UserEmployment;
import com.company.management.entity.registrationdb.UserProfile;
import com.company.management.entity.registrationdb.UserVerification;
import com.company.management.entity.wallet.AccountBgd;
import io.jmix.core.DataManager;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountVerificationReportService {


	private static final Logger log = org.slf4j.LoggerFactory.getLogger(AccountVerificationReportService.class);
	@Autowired
	private DataManager dataManager;

	public List<AccountVerificationReport> getCombinedData(){
		List<AccountVerificationReport> combineDataList = new ArrayList<>();

		try{
			List<UserVerification> userVerificationsList = getUserVerification();

			for(UserVerification userVerification : userVerificationsList){
				AccountVerificationReport accountVerificationReport = this.dataManager.create(AccountVerificationReport.class);
				accountVerificationReport.setInvestorName(userVerification.getUser().getFullName() != null ? userVerification.getUser().getFullName():"-");
				accountVerificationReport.setDateOfBirth(userVerification.getUser().getBirthday()  != null ? userVerification.getUser().getBirthday():"-");
				accountVerificationReport.setTaxResidenceCountry(userEmployment(userVerification.getUser()).getCountryOftaxResidence()  != null ? userEmployment(userVerification.getUser()).getCountryOftaxResidence():"-");
				accountVerificationReport.setNationality(userVerification.getUser().getNationality() != null ? userVerification.getUser().getNationality():"-");
				accountVerificationReport.setResidentialAddress(userAddress(userVerification.getUser()).getAddress1().toString() + "\n" + userAddress(userVerification.getUser()).getAddress2().toString()  != null ? userAddress(userVerification.getUser()).getAddress1() + "\n" + userAddress(userVerification.getUser()).getAddress2():"-");
				accountVerificationReport.setResidentialPostcode(userAddress(userVerification.getUser()).getPostcode() != null ? userAddress(userVerification.getUser()).getPostcode():"-");
				accountVerificationReport.setTaxResidenceCountry(userAddress(userVerification.getUser()).getCountry() != null ? userAddress(userVerification.getUser()).getCountry():"-");
				accountVerificationReport.setContactNum(userVerification.getUser().getPhoneNumber() != null ? userVerification.getUser().getPhoneNumber():"-");
				accountVerificationReport.setBgdID(getaccountBgd(userVerification.getUser().getUsername()).getAcctNumber().toString() != null ? String.valueOf(getaccountBgd(userVerification.getUser().getUsername()).getAcctNumber()) :"-");
				accountVerificationReport.setAccountStatusEndYear(userVerification.getOveralVerificationResult().getName() != null ? userVerification.getOveralVerificationResult().getName():"-");
				accountVerificationReport.setAccountOpeningDate(userVerification.getUser().getApproveDate() != null ? userVerification.getUser().getApproveDate().toString():"-");
				if(userVerification.getUser().getCloseDate() != null){
					accountVerificationReport.setAccountClosingDate(userVerification.getUser().getCloseDate().toString());
				}else{
					accountVerificationReport.setAccountClosingDate("-");
				}
				accountVerificationReport.setReportingYear("-");
				accountVerificationReport.setAccountBalanceEndYear("-");
				accountVerificationReport.setAccountCurrency("-");
				accountVerificationReport.setGrossSaleRedemptionCurrency("-");
				combineDataList.add(accountVerificationReport);
			}

		}catch (Exception ex){
			log.info(ex.getMessage());
		}

		return combineDataList;
	}

	private List<UserVerification> getUserVerification(){
		return this.dataManager.load(UserVerification.class)
				.query("select u from UserVerification u")
				.list();
	}

	private UserAddress userAddress(UserProfile userID){
		return this.dataManager.load(UserAddress.class)
				.query("select u from UserAddress u " +
						"where u.user = :user1")
				.parameter("user1", userID)
				.one();
	}

	private UserEmployment userEmployment(UserProfile userID){
		return this.dataManager.load(UserEmployment.class)
				.query("select u from UserEmployment u " +
						"where u.user = :user1")
				.parameter("user1", userID)
				.one();
	}

	private AccountBgd getaccountBgd(String username){
		return this.dataManager.load(AccountBgd.class)
				.query("select a from AccountBgd a " +
						"where a.accOwner = :accOwner1 " +
						"and a.walletType.code = :walletTypeCode1")
				.parameter("accOwner1", username)
				.parameter("walletTypeCode1", "E02")
				.one();
	}
}
