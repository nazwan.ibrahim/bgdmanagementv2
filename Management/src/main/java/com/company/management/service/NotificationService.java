package com.company.management.service;

import com.company.management.entity.registrationdb.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.Authenticated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NotificationService {


    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    @Autowired
    private TimeSource timeSource;
    @Autowired
    private DataManager dataManager;

    @Autowired
    private Environment env;

    private NotificationCategory ANNOUNCEMENT_CATEGORY;

    private NotificationStatus UNREAD_STATUS;


    @ManagedOperation
    @Authenticated
    @EventListener
    public void onApplicationStarted(ApplicationStartedEvent event) {
        try {
            ANNOUNCEMENT_CATEGORY = getNotificationCategory("NC02");
            UNREAD_STATUS = getNotificationStatus("G16");
        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
    }

    public NotificationCategory getNotificationCategory(String code){
        String query1="select n from NotificationCategory n " +
                "where n.code = :code1";
        NotificationCategory notificationCategory = dataManager.load(NotificationCategory.class)
                .query(query1).parameter("code1", code).one();
        return notificationCategory;
    }

    public NotificationStatus getNotificationStatus(String code){
        String query1="select n from NotificationStatus n " +
                "where n.code = :code1";
        NotificationStatus notificationStatus = dataManager.load(NotificationStatus.class)
                .query(query1).parameter("code1", code).one();
        return notificationStatus;
    }



    public NotificationDetail createAnnouncement(String body, String title,String dataUrl){
        //create notifiation detail

        try {
            NotificationDetail notificationDetail = dataManager.create(NotificationDetail.class);
            notificationDetail.setTitle(title);
            notificationDetail.setBody(body);
            notificationDetail.setDataUrl(dataUrl);
            notificationDetail.setDataReference(null);
            notificationDetail.setTypeId(null);
            notificationDetail.setVersion(1);
            dataManager.save(notificationDetail);
            return notificationDetail;
        }catch (Exception e){
            logger.error("Error while creating notification detail");
            return null;
        }

    }

    public Boolean createNotification(NotificationDetail notificationDetail){
        // create notification for all user

        try {
            List<UserProfile> userProfileList = dataManager.load(UserProfile.class)
                    .query("select u from UserProfile u").list();
            logger.info("Size user: "+userProfileList.size());
            List<Notification> notificationList = new ArrayList<>();
            for (UserProfile u : userProfileList) {
                Notification newNotification = dataManager.create(Notification.class);
                newNotification.setDetailId(notificationDetail);
                newNotification.setCategoryId(ANNOUNCEMENT_CATEGORY);
                newNotification.setStatusId(UNREAD_STATUS);
                newNotification.setUserId(u);
                newNotification.setSendDateTime(timeSource.currentTimestamp());
                newNotification.setVersion(1);
                notificationList.add(newNotification);
            }

            notificationList.forEach(notification -> {
                dataManager.save(notification);
            });
            return true;
        }catch (Exception e){
            logger.error("ERROR when creating notification: "+ e);
            return false;
        }

    }



    public Boolean pushNotification(NotificationDetail notificationDetail,String topic){
        //call api push proxy

        try{
            // build body to call push api at proxy, this api will call firebase to sent notification
            Map<String, Object> mainBody = new HashMap<>();
            Map<String, String> notificationBody = new HashMap<>();
            notificationBody.put("title", notificationDetail.getTitle());
            notificationBody.put("body", notificationDetail.getBody());

            Map<String, String> dataBody = new HashMap<>();
            dataBody.put("categoryCode", "NC02");
            dataBody.put("typeCode", "");
            dataBody.put("dataUrl", "");
            dataBody.put("dataReference", "");


            mainBody.put("notification", notificationBody);
            mainBody.put("data", dataBody);
            mainBody.put("topic",topic); // for now cater for one specific topic

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            String pushUrl = env.getProperty("push_proxy_url");
            HttpEntity<Map> pushRequest = new HttpEntity<>(mainBody, headers);
            ResponseEntity<String> pushResponseString = restTemplate.exchange(pushUrl, HttpMethod.POST, pushRequest, String.class);

            Gson pushResponseGson = new Gson();
            JsonObject pushResponseJson = pushResponseGson.fromJson(pushResponseString.getBody(), JsonObject.class);

            logger.info("GSON:" + pushResponseJson.toString());
                return true;
            }catch(Exception e){
                logger.info("Error on Push notification: "+e.getMessage());
                return false;
            }

        }

}
