package com.company.management.service;

import com.company.management.entity.ProductPrice;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class RunnableService implements Runnable{
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(RunnableService.class);
    private String threadName;
//    @Autowired
    private PriceService priceService;

    public RunnableService(String threadName) {
        threadName = threadName;
        log.info("Creating " + threadName);
    }

    @Override
    public void run() {
        log.info("Running " + threadName);
    }

    public RunnableService runnableService1(ProductPrice supplierPrice) {
        priceService.calculatedBGDPrice(supplierPrice);
        return null;
    }


}
