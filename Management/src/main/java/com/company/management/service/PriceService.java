package com.company.management.service;

import com.company.management.entity.ProductPrice;
import com.company.management.entity.ProductPriceHistories;
import com.company.management.entity.RevenueManagement;
import com.company.management.entity.RtType;
import io.jmix.core.DataManager;
import io.jmix.core.security.Authenticated;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PriceService {
   // private static final Logger logger = LoggerFactory.getLogger(PriceService.class);
    @Autowired
    private DataManager dataManager;

    private ProductPrice ACE_PRICE;
    private ProductPrice BURSA_PRICE;
    private RevenueManagement BUY_MARGIN;
    private RevenueManagement SELL_MARGIN;
    private RtType BUY_MARGIN_UOM;
    private RtType SELL_MARGIN_UOM;
//
    public void setACE_PRICE(ProductPrice ACE_PRICE) {
        this.ACE_PRICE = ACE_PRICE;
    }
    public ProductPrice getACE_PRICE(){
        return ACE_PRICE;
    }
    public void setBURSA_PRICE(ProductPrice BURSA_PRICE) {
        this.BURSA_PRICE = BURSA_PRICE;
    }
    public ProductPrice getBURSA_PRICE() {
        return BURSA_PRICE;
    }
    public void setBUY_MARGIN(RevenueManagement BUY_MARGIN) {
        this.BUY_MARGIN = BUY_MARGIN;
    }
    public RevenueManagement getBUY_MARGIN() {
        return BUY_MARGIN;
    }
    public void setSELL_MARGIN(RevenueManagement SELL_MARGIN) {
        this.SELL_MARGIN = SELL_MARGIN;
    }
    public RevenueManagement getSELL_MARGIN() {
        return SELL_MARGIN;
    }
    public void setBUY_MARGIN_UOM(RtType BUY_MARGIN_UOM) {
        this.BUY_MARGIN_UOM = BUY_MARGIN_UOM;
    }
    public RtType getBUY_MARGIN_UOM() {
        return BUY_MARGIN_UOM;
    }
    public void setSELL_MARGIN_UOM(RtType SELL_MARGIN_UOM) {
        this.SELL_MARGIN_UOM = SELL_MARGIN_UOM;
    }
    public RtType getSELL_MARGIN_UOM() {
        return SELL_MARGIN_UOM;
    }

    @Authenticated
    @ManagedOperation
    @EventListener(ApplicationReadyEvent.class)
    public void onStart() {
        setACE_PRICE(loadProductPrice("A01"));
        setBURSA_PRICE(loadProductPrice("A02"));
        setBUY_MARGIN(loadRevenueManagement("C05"));
        setSELL_MARGIN(loadRevenueManagement("C06"));
        setBUY_MARGIN_UOM(loadUomType("C05"));
        setSELL_MARGIN_UOM(loadUomType("C06"));
//        ACE_PRICE = loadProductPrice("A01");
//        BURSA_PRICE = loadProductPrice("A02");
//        BUY_MARGIN = loadRevenueManagement("C05");
//        SELL_MARGIN = loadRevenueManagement("C06");
//        BUY_MARGIN_UOM = loadUomType("C05");
//        SELL_MARGIN_UOM = loadUomType("C06");
    }

    public ProductPrice loadProductPrice(String typeCode) {
        String stringQuery =  "select p from ProductPrice p " +
                "where p.product.code = :productCode1 " +
                "and p.type.code = :typeCode1";

        ProductPrice productPrice = this.dataManager.load(ProductPrice.class)
                .query(stringQuery)
                .parameter("productCode1", "BGD")
                .parameter("typeCode1", typeCode)
                .joinTransaction(true)
                .one();

        return productPrice;
    }

    public RevenueManagement loadRevenueManagement(String typeCode) {
        String stringQuery =  "select r from RevenueManagement r " +
                "where r.product.code = :productCode1 " +
                "and r.type.code = :typeCode1 " +
                "and r.active = :active";

        RevenueManagement revenueManagement = this.dataManager.load(RevenueManagement.class)
                .query(stringQuery)
                .parameter("productCode1", "BGD")
                .parameter("typeCode1", typeCode)
                .parameter("active", true).one();

//        List<RevenueManagement> margins = this.dataManager.load(RevenueManagement.class)
//                    .query("select r from RevenueManagement r " +
//                            "where r.product.code = :productCode1")
//                    .parameter("productCode1", "BGD")
//                    .list();
//
//        RevenueManagement Margin = margins.stream().filter(f -> f.getType().getCode().equals("C05")).toList().get(0);

        return revenueManagement;
    }

    public RtType loadUomType(String marginCode) {
        String stringQuery = "select r.uom from RevenueManagement r " +
                "where r.product.code = :productCode1 " +
                "and r.type.code = :typeCode1 " +
                "and r.active = :active";

        RtType uom = this.dataManager.load(RtType.class)
                .query(stringQuery)
                .parameter("productCode1", "BGD")
                .parameter("typeCode1", marginCode)
                .parameter("active", true)
                .one();

        return uom;
    }

    public void calculatedBGDPrice(ProductPrice supplierPrice) {
        try {
//            List<RevenueManagement> margins = this.dataManager.load(RevenueManagement.class)
//                    .query("select r from RevenueManagement r " +
//                            "where r.product.code = :productCode1")
//                    .parameter("productCode1", "BGD")
//                    .list();
//
//            RevenueManagement buyMargin = margins.stream().filter(f -> f.getType().getCode().equals("C05")).toList().get(0);
//            RevenueManagement sellMargin = margins.stream().filter(f -> f.getType().getCode().equals("C06")).toList().get(0);
//            calculatedBGDPrice(supplierPrice, buyMargin, sellMargin);
            setACE_PRICE(supplierPrice);
            calculatedBGDPrice(getACE_PRICE(), getBUY_MARGIN(), getBUY_MARGIN_UOM(), getSELL_MARGIN(), getSELL_MARGIN_UOM());
        }catch (Exception ex) {
         //   logger.error(ex.getMessage());
        }
    }

    public void calculatedBGDPrice(RevenueManagement buyMargin, RtType buyMarginUom, RevenueManagement sellMargin, RtType sellMarginUom) {
        try {

            if(buyMargin == null) {
                String stringQuery = "select r from RevenueManagement r " +
                        "where r.product.code = :productCode1 " +
                        "and r.type.code = :typeCode1 " +
                        "and r.active = :active";

                buyMargin = this.dataManager.load(RevenueManagement.class)
                        .query(stringQuery)
                        .parameter("productCode1", "BGD")
                        .parameter("typeCode1", "C05")
                        .parameter("active", true)
//                        .joinTransaction(false)
                        .one();
//                buyMargin = BUY_MARGIN;

                buyMarginUom = buyMargin.getUom();
            } else if (sellMargin == null) {
                String stringQuery = "select r from RevenueManagement r " +
                        "where r.product.code = :productCode1 " +
                        "and r.type.code = :typeCode1 " +
                        "and r.active = :active";

                sellMargin = this.dataManager.load(RevenueManagement.class)
                        .query(stringQuery)
                        .parameter("productCode1", "BGD")
                        .parameter("typeCode1", "C06")
                        .parameter("active", true)
//                        .joinTransaction(false)
                        .one();
//                sellMargin = SELL_MARGIN;

                sellMarginUom = sellMargin.getUom();
            }
            setBUY_MARGIN(buyMargin);
            setSELL_MARGIN(sellMargin);
            setBUY_MARGIN_UOM(buyMarginUom);
            setSELL_MARGIN_UOM(sellMarginUom);
//            List<ProductPrice> priceList = this.dataManager.load(ProductPrice.class)
//                    .query("select p from ProductPrice p " +
//                            "where p.product.code = :productCode1")
//                    .parameter("productCode1", "BGD")
//                    .list();
//
//            ProductPrice supplierPrice = priceList.stream().filter(f -> f.getType().getCode().equals("A01")).toList().get(0);
//            ProductPrice supplierPrice = getACE_PRICE();

            calculatedBGDPrice(getACE_PRICE(), getBUY_MARGIN(), getBUY_MARGIN_UOM(), getSELL_MARGIN(), getSELL_MARGIN_UOM());
        }catch (Exception ex) {
        //    logger.error(ex.getMessage());
        }
    }

    public void calculatedBGDPrice(ProductPrice supplierPrice, RevenueManagement buyMargin, RtType buyMarginUom, RevenueManagement sellMargin, RtType sellMarginUom) {
        try {
            String stringQuery = "select p from ProductPrice p " +
                    "where p.product.code = :productCode1";
            List<ProductPrice> priceList = this.dataManager.load(ProductPrice.class)
                    .query(stringQuery)
                    .parameter("productCode1", "BGD")
                    .list();

            ProductPrice bursaPrice = priceList.stream().filter(f -> f.getType().getCode().equals("A02")).toList().get(0);

//            ProductPrice bursaPrice = BURSA_PRICE;

            if(buyMargin.getActive()) {
                if (buyMarginUom.getCode().equals("J01")) { // if margin on RM
                    bursaPrice.setBuyPrice(supplierPrice.getBuyPrice().subtract(buyMargin.getCharge()));
                } else if (buyMarginUom.getCode().equals("J04")) { // if margin on Percentage
                    BigDecimal timesPrice = supplierPrice.getBuyPrice().multiply(buyMargin.getCharge().divide(new BigDecimal(100)));
                    BigDecimal actualPrice = supplierPrice.getBuyPrice();
                    BigDecimal minusPrice = actualPrice.subtract(timesPrice);
//                    bursaPrice.setBuyPrice(supplierPrice.getBuyPrice().subtract(supplierPrice.getBuyPrice().multiply(buyMargin.getCharge())));
                    bursaPrice.setBuyPrice(minusPrice);
                }
            }

            if(sellMargin.getActive()) {
                if (sellMarginUom.getCode().equals("J01")) { // if margin on RM
                    bursaPrice.setSellPrice(supplierPrice.getSellPrice().add(sellMargin.getCharge()));
                } else if(sellMarginUom.getCode().equals("J04")) {
                    // if margin on Percentage
                    BigDecimal timesPrice = supplierPrice.getSellPrice().multiply(sellMargin.getCharge().divide(new BigDecimal(100)));
                    BigDecimal actualPrice = supplierPrice.getSellPrice();
                    BigDecimal minusPrice = actualPrice.add(timesPrice).abs();
//                    bursaPrice.setSellPrice(supplierPrice.getSellPrice().add(supplierPrice.getSellPrice().multiply(sellMargin.getCharge())));
                    bursaPrice.setSellPrice(minusPrice);
                }
            }

            this.dataManager.save(bursaPrice);
            setBURSA_PRICE(bursaPrice);

            //produce new price to kafka


        } catch (Exception ex) {
    //        logger.error(ex.getMessage());
        }
    }

    public void savePriceHistories(ProductPrice productPrice) {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date priceDate = formatter.parse(formatter.format(productPrice.getLastModifiedDate()));

            String stringQuery = "select e from ProductPriceHistories e " +
                    "where e.productPrice = :productPrice " +
                    "and e.priceTS = :priceTS";

            List<ProductPriceHistories> historiesList = this.dataManager.load(ProductPriceHistories.class)
                    .query(stringQuery)
                    .parameter("productPrice", productPrice)
                    .parameter("priceTS", priceDate).list();

            ProductPriceHistories priceHistories = this.dataManager.create(ProductPriceHistories.class);
            if(historiesList.size() == 0) {
                priceHistories.setProductPrice(productPrice);
                priceHistories.setBuyPrice(productPrice.getBuyPrice());
                priceHistories.setSellPrice(productPrice.getSellPrice());
                priceHistories.setPriceTS(priceDate);
            } else {
                priceHistories = historiesList.get(0);
                priceHistories.setBuyPrice(productPrice.getBuyPrice());
                priceHistories.setSellPrice(productPrice.getSellPrice());
            }

            this.dataManager.save(priceHistories);
        } catch (Exception ex) {
       //     logger.error(ex.getMessage());
        }
    }  public void saveAcePrice(BigDecimal buyPrice, BigDecimal sellPrice) {
//        setACE_PRICE(loadProductPrice("A01"));
//        getACE_PRICE().setBuyPrice(buyPrice);
//        getACE_PRICE().setSellPrice(sellPrice);
        ProductPrice price = loadProductPrice("A01");
        price.setBuyPrice(buyPrice);
        price.setSellPrice(sellPrice);
        this.dataManager.save(price);
    }
}
