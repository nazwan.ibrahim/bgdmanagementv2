package com.company.management.app;

import com.amazonaws.util.IOUtils;
import com.company.management.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.email.EmailAttachment;
import io.jmix.email.EmailInfo;
import io.jmix.email.EmailInfoBuilder;
import io.jmix.security.role.assignment.RoleAssignmentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@Component
public class ActivityLogBean {
    static Logger logger = LogManager.getLogger(ActivityLogBean.class);
    @Autowired
    private DataManager dataManager;
    @Autowired
    private EmailBean emailBean;

    @Autowired
    private CurrentAuthentication currentAuthentication;
    @Autowired
    private RoleAssignmentRepository roleAssignmentRepository;
    @Autowired
    private TimeSource timeSource;
    private List<RtType> rtTypeList() {
        return this.dataManager.load(RtType.class).all().list();
    }

    public void makerActivity(String moduleCode, String activityCode, String description, String refEntityName, String refEntityData, String remarks, List<ActivityLogFile> activityLogFiles) {
        try {
            List<RtType> rtTypeList = rtTypeList();
            ActivityLog activityLog = this.dataManager.create(ActivityLog.class);
            activityLog.setModule(rtTypeList.stream().filter(f -> f.getCode().equals(moduleCode)).toList().get(0));
            activityLog.setActivity(rtTypeList.stream().filter(f -> f.getCode().equals(activityCode)).toList().get(0));
            activityLog.setRefEntity(refEntityName);
            activityLog.setRefData(refEntityData);
            activityLog.setRequestRemarks(remarks);
            activityLog.setRequestBy(currentAuthentication.getUser().getUsername());
            activityLog.setRequestDate(timeSource.currentTimestamp());
            activityLog.setStatus(rtTypeList.stream().filter(f -> f.getCode().equals("G05")).toList().get(0));
            activityLog.setDescription(description);

            this.dataManager.save(activityLog);

            activityLogFiles.forEach(e -> {
                try {
                    ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
                    activityLogFile.setActivityLog(activityLog);
                    activityLogFile.setName(e.getName());
                    activityLogFile.setFileUpload(e.getFileUpload());
                    activityLogFile.setAction(rtTypeList.stream().filter(f -> f.getCode().equals("ACT01")).toList().get(0));

                    this.dataManager.save(activityLogFile);
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            });
            emailReminderChecker(moduleCode, activityCode, activityLog);
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

     public void checkerActivity(UUID activityLogUUID, String remarks, String statusCode, List<ActivityLogFile> activityLogFiles) {
        try {
            List<RtType> rtTypeList = rtTypeList();
            ActivityLog activityLog = this.dataManager.load(ActivityLog.class).id(activityLogUUID).one();
            activityLog.setApproveRemarks(remarks);
            activityLog.setApproveBy(currentAuthentication.getUser().getUsername());
            activityLog.setApproveDate(timeSource.currentTimestamp());
            activityLog.setStatus(rtTypeList.stream().filter(f -> f.getCode().equals(statusCode)).toList().get(0));

            this.dataManager.save(activityLog);

            activityLogFiles.forEach(e -> {
                try {
                    ActivityLogFile activityLogFile = this.dataManager.create(ActivityLogFile.class);
                    activityLogFile.setActivityLog(activityLog);
                    activityLogFile.setName(e.getName());
                    activityLogFile.setFileUpload(e.getFileUpload());
                    activityLogFile.setAction(rtTypeList.stream().filter(f -> f.getCode().equals("ACT02")).toList().get(0));

                    this.dataManager.save(activityLogFile);
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                }
            });
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
     }

     public void emailReminderChecker(String moduleCode, String activityCode, ActivityLog activityLog) {
        String body = "Dear Admin," +
                "You have {revenue configuration} to be review/approve." +
                "Please login to BGD ADAM to proceed.";

         roleAssignmentRepository.getAllAssignments().forEach(roleAssignment -> {
             if(roleAssignment.getRoleCode().equals("BGDCheckerRole")){
                 try{
                     RtType type = this.dataManager.load(RtType.class)
                             .query("select e from RtType e " +
                                     "where e.code = :code")
                             .parameter("code", moduleCode)
                             .one();

                     User user = this.dataManager.load(User.class)
                             .query("select e from User e " +
                                     "where e.username = :username")
                             .parameter("username", roleAssignment.getUsername())
                             .one();

                     emailBean.generateEmailRemainderToChecker(user, type.getName(), type.getName(), "", activityLog);
//                     InputStream resourceAsStream = resources.getResourceAsStream("image/bursa_old_logo.svg");
//                     byte[] bytes = new byte[0];
//
//                     bytes = IOUtils.toByteArray(resourceAsStream);
//
//                     EmailAttachment emailAtt = new EmailAttachment(bytes,
//                             "bursa_old_logo.svg", "logoId");
//                     RevenueManagement newsItem = getEditedEntity();
//                     EmailInfo emailInfo = EmailInfoBuilder.create()
//                             .setAddresses(roleAssignment.getUsername())
//                             .setSubject("Approval Required for Request Change")
//                             .setFrom("supportadmin@milradius.com.my")
//                             .setBody("Please Approve: "+newsItem.getType().getName()+ " \n" +
//                                     "For Product :"+newsItem.getProduct().getName() )
////                                .setAttachments(emailAtt)
//                             .build();
////                        emailer.sendEmailAsync(emailInfo);
//                     emailer.sendEmail(emailInfo);
                 }catch (Exception ex){
                     logger.error(ex.getMessage());
                 }
             }
         });
     }
}