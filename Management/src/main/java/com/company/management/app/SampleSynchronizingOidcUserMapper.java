//package com.company.management.app;
//
//import com.company.management.entity.User;
//import io.jmix.core.UnconstrainedDataManager;
//import io.jmix.core.security.UserRepository;
//import io.jmix.oidc.claimsmapper.ClaimsRolesMapper;
//import io.jmix.oidc.usermapper.SynchronizingOidcUserMapper;
//import org.springframework.security.oauth2.core.oidc.user.OidcUser;
//import org.springframework.stereotype.Component;
//
//@Component
//public class SampleSynchronizingOidcUserMapper extends SynchronizingOidcUserMapper<User> {
//	public SampleSynchronizingOidcUserMapper(UnconstrainedDataManager dataManager,
//	                                         UserRepository userRepository,
//	                                         ClaimsRolesMapper claimsRolesMapper) {
//		super(dataManager, userRepository, claimsRolesMapper);
//		setSynchronizeRoleAssignments(true);
//	}
//
//	@Override
//	protected Class<User> getApplicationUserClass() {
//		return User.class;
//	}
//
//	@Override
//	protected void populateUserAttributes(OidcUser oidcUser, User jmixUser) {
//		jmixUser.setUsername(oidcUser.getPreferredUsername());
//		jmixUser.setEmail(oidcUser.getEmail());
//	}
//
//	@Override
//	protected String getOidcUserUsername(OidcUser oidcUser) {
//		return oidcUser.getPreferredUsername();
//	}
//}
