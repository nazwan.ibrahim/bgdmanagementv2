package com.company.management.app;

import com.company.management.entity.Product;
import com.company.management.entity.ProductManagement;
import io.jmix.core.DataManager;
import io.jmix.core.EntitySerialization;
import io.jmix.core.security.CurrentAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("Admin")
public class AdminApiController {
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CurrentAuthentication currentAuthentication;
	@Autowired
	private EntitySerialization entitySerialization;

	public Product getProduct(String productCode) {
		return this.dataManager.load(Product.class)
				.query("select e from Product e where e.code = :code")
				.parameter("code", productCode).one();
	}

//	public List<ProductPrice> getListProductPrice(String productCode, String priceCode) {
//		List<ProductPrice> productPrices = getProduct(productCode).getProductPrice();
//		if (Objects.equals(priceCode, "-1")) {
//			return productPrices;
//		}
//		return productPrices.stream().filter(f -> f.getTypeID().getCode().equals(priceCode)).collect(Collectors.toList());
//	}

	public ProductManagement getProductMargin(String productCode) {
		return this.dataManager.load(ProductManagement.class)
				.query("select e from ProductManagement e where e.product.code = :productCode " +
						"and e.type.code = :typeCode")
				.parameter("productCode", productCode)
				.parameter("typeCode", "B01").one();
	}

//	@PutMapping("/productPrice")
//	public ResponseEntity updateProductPrice(String productCode, @RequestBody ProductPriceForm form) {
//		try {
//			ProductPrice supplierPrice = getListProductPrice(productCode, "A01").get(0);
//			supplierPrice.setSellPrice(form.getSellPrice());
//			supplierPrice.setBuyPrice(form.getBuyPrice());
//
//			ProductManagement productMargin = getProductMargin(productCode);
//			ProductPrice investorPrice = getListProductPrice(productCode, "A02").get(0);
//
//			if(Objects.equals(productMargin.getUomID().getCode(), "%")) {
//				investorPrice.setBuyPrice(form.getSellPrice().add(form.getSellPrice().multiply(productMargin.getValue())));
//				investorPrice.setSellPrice(form.getBuyPrice().add(form.getBuyPrice().multiply(productMargin.getValue())));
//			} else {
//				investorPrice.setBuyPrice(form.getSellPrice().add(productMargin.getValue()));
//				investorPrice.setSellPrice(form.getBuyPrice().add(productMargin.getValue()));
//			}
//
//			this.dataManager.save(supplierPrice);
//			this.dataManager.save(investorPrice);
//
//			return ResponseEntity.ok("Product Price Updated");
//		} catch (Exception ex) {
//			return ResponseEntity.badRequest().body(ex.getMessage());
//		}
//	}

}