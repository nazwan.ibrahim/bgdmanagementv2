package com.company.management.app;

import com.company.management.ManagementApplication;
import com.company.management.entity.PurchaseRecordProduct;
import com.company.management.entity.wallet.*;
import io.jmix.core.DataManager;
import io.jmix.core.TimeSource;
import io.jmix.core.security.CurrentAuthentication;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;


@Component
public class AdminManagementProcess {

	private static final Logger log = org.slf4j.LoggerFactory.getLogger(ManagementApplication.class);
	@Autowired
	private DataManager dataManager;
	@Autowired
	private CurrentAuthentication currentAuthentication;

	@Autowired
	private TimeSource timeSource;

	public TransStatus getTransactionStatus(String code) {

		String stringQuery = "select e from TransStatus e where e.code = :code";
		return this.dataManager.load(TransStatus.class)
				.query(stringQuery)
				.parameter("code", code).one();
	}
	public ResponseEntity approvedWalletTransfer(TransferInfoView form) {
		try {

				TransferInfo transferInfo = this.dataManager.load(TransferInfo.class).id(form.getId()).one();
				transferInfo.setApprovedBy(currentAuthentication.getUser().getUsername());
                transferInfo.setApprovedDate(timeSource.currentTimestamp());

				String stringQuery = "select e from AccountHold e where e.transactionAcct.reference = :reference";
				List<AccountHold> accountHold = this.dataManager.load(AccountHold.class)
						.query(stringQuery)
						.parameter("reference", form.getReference())
						.list();

				accountHold.forEach(e -> {
					e.setAmount(BigDecimal.ZERO);

					TransactionAcct transactionAcct = e.getTransactionAcct();
//					transactionAcct.setTransStatus(getTransactionStatus("G06"));
					transactionAcct.setTransStatus(getTransactionStatus(form.getStatusCode()));

					this.dataManager.save(e);
					this.dataManager.save(transactionAcct);
					this.dataManager.save(transferInfo);
			});

			return ResponseEntity.ok("Approved Successes");
		} catch (Exception ex) {
			return ResponseEntity.badRequest().body(ex.getMessage());
		}
	}

//	@Scheduled(fixedDelay = 30000)
	public void redemptionDinar() {
		List<RedeemInfo> redeemInfos = this.dataManager.load(RedeemInfo.class).all().list();

		redeemInfos.forEach(e -> {
			try {
				String stringQuery = "select e from PurchaseRecordProduct e " +
						"where e.serial_number =:serial_number";

				PurchaseRecordProduct product = this.dataManager.load(PurchaseRecordProduct.class)
						.query(stringQuery)
						.parameter("serial_number", e.getSerialNumber().replaceAll("\\s", ""))
						.one();

				product.setIsRedeem(true);
			} catch (Exception ex) {
				log.error(ex.getMessage());
			}
		});
	}

}
