package com.company.management.app;

import com.company.management.entity.*;
import com.company.management.entity.registrationdb.UserProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import io.jmix.core.DataManager;
import io.jmix.core.EntitySerialization;
import io.jmix.core.EntitySerializationOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Component
@RestController
@RequestMapping("reward")
public class RewardController {
    static Logger logger = LogManager.getLogger(ActivityLogBean.class);
    @Autowired
    private DataManager dataManager;
    @Autowired
    private EntitySerialization entitySerialization;

    private List<RewardManagement> rewardManagementList() {
        return this.dataManager.load(RewardManagement.class).all().list();
    }

    private List<RewardManagement> rewardManagementByCode(String code) {

            return this.dataManager.load(RewardManagement.class)
                    .query("select e from RewardManagement e " +
                            "where e.campaignCode = :campaignCode")
                    .parameter("campaignCode", code)
                    .list();
    }

    @GetMapping("validateCode")
    public ResponseEntity validateCode(String code) {
        logger.info("Reward/validateCode: "+code);
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
        try {
            responseDTO.setSuccess(true);
            List<RewardManagement> rewardManagement = rewardManagementByCode(code);
            //for sign up reward
            if((long) rewardManagement.size() > 0 && rewardManagement.get(0).getCampaignCode().equals(code) && rewardManagement.get(0).getStatus().getCode().equals("G04")) {
                if(rewardManagement.get(0).getRedeemed() < rewardManagement.get(0).getMaxParticipant()) {
                    responseDTO.setMessage("valid");
                    return ResponseEntity.ok(responseDTO);
                }
            }

            //for referral reward
            // check referral code on user profile

            responseDTO.setMessage("Invalid or already expired");
            return ResponseEntity.badRequest().body(responseDTO);
        } catch (Exception ex) {
            logger.error(ex.getMessage());

            responseDTO.setSuccess(false);
            responseDTO.setMessage("");
            return ResponseEntity.internalServerError().body(responseDTO);
        }
    }

    @PostMapping("participant")
    public ResponseEntity addParticipant(@RequestBody RewardParticipantDTO form) {
        logger.info("Reward/participant: "+form.toString());
        ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
        try {
            RewardManagement rewardManagement = rewardManagementByCode(form.getRewardCode()).get(0);
            UserProfile userProfile = this.dataManager.load(UserProfile.class).id(UUID.fromString(form.getUserID())).one();
            if (rewardManagement.getRedeemed() < rewardManagement.getMaxParticipant()) {
                RewardParticipant rewardParticipant = this.dataManager.create(RewardParticipant.class);
                rewardParticipant.setReward(rewardManagement);
                rewardParticipant.setUserID(form.getUserID());
                rewardParticipant.setRedeemed(false);
                rewardParticipant.setCreatedBy(userProfile.getUsername());
                this.dataManager.save(rewardParticipant);

                responseDTO.setSuccess(true);
                responseDTO.setMessage("Ok");
                return ResponseEntity.ok(responseDTO);
            } else {
                responseDTO.setSuccess(false);
                responseDTO.setMessage("Exceed Limit");
                return ResponseEntity.badRequest().body("Exceed Limit");
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            responseDTO.setSuccess(false);
            responseDTO.setMessage(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }
    }

    @GetMapping("trackRewards")
    public ResponseEntity trackRewards(String userID) {
        logger.info("Reward/trackRewards: "+userID);
        try {
            List<RewardParticipant> rewardParticipants = this.dataManager.load(RewardParticipant.class)
                    .query("select e from RewardParticipant e " +
                            "where e.userID = :userID")
                    .parameter("userID", userID)
                    .list();

            UserProfile userProfile = this.dataManager.load(UserProfile.class).id(UUID.fromString(userID)).one();

            List<TransactionHistoriesView> transactionRewards = this.dataManager.load(TransactionHistoriesView.class)
                    .query("select e from TransactionHistoriesView e " +
                            "where e.accOwner = :accOwner " +
                            "and e.transactionCode like CONCAT(:rewardCode, '%')")
                    .parameter("accOwner", userProfile.getUsername())
                    .parameter("rewardCode", "F12")
                    .list();

//            TrackRewardDTO trackRewardDTO = this.dataManager.create(TrackRewardDTO.class);
//            trackRewardDTO.setSuccessful(rewardParticipants.stream().filter(f -> f.getRedeemed().equals(true)).toList().size());
//            trackRewardDTO.setPending(rewardParticipants.stream().filter(f -> !f.getRedeemed().equals(true)).toList().size());
//            trackRewardDTO.setTransaction(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(transactionRewards));

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("successful", rewardParticipants.stream().filter(f -> f.getRedeemed().equals(true)).toList().size());
            jsonObject.put("pending", rewardParticipants.stream().filter(f -> !f.getRedeemed().equals(true)).toList().size());
            jsonObject.put("transaction", transactionRewards);


            return ResponseEntity.ok(jsonObject.toMap());

        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ResponseDTO responseDTO = this.dataManager.create(ResponseDTO.class);
            responseDTO.setSuccess(false);
            responseDTO.setMessage(ex.getMessage());
            return ResponseEntity.badRequest().body(responseDTO);
        }

    }

}