package com.company.management.app;

import com.company.management.entity.BgdIDDTO;
import com.company.management.entity.wallet.AccountBgd;
import io.jmix.core.DataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BgdIDBean {

    @Autowired
    private DataManager dataManager;

    public BgdIDDTO getBgdID(String username) {
        BgdIDDTO bgdIDDTO = this.dataManager.create(BgdIDDTO.class);
        bgdIDDTO.setUserName(username);
        bgdIDDTO.setBgdID(getAccountBgds(username).stream().filter(f -> f.getWalletType().getCode().equals("E02")).toList().get(0).getAcctNumber());

        return bgdIDDTO;
    }

    private List<AccountBgd> getAccountBgds(String accountOwner) {
        return this.dataManager.load(AccountBgd.class)
                .query("select e from AccountBgd e " +
                        "where e.accOwner = :accOwner")
                .parameter("accOwner", accountOwner)
                .list();
    }
}