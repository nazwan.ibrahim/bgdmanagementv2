package com.company.management.app;

import com.company.management.api.EmailNotificationController;
import com.company.management.entity.ActivityLog;
import com.company.management.entity.User;
import io.jmix.core.DataManager;
import io.jmix.email.EmailException;
import io.jmix.email.EmailInfo;
import io.jmix.email.Emailer;
import io.jmix.emailtemplates.EmailTemplates;
import io.jmix.emailtemplates.entity.EmailTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import io.jmix.emailtemplates.entity.EmailTemplate;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/email")
//@Component
public class EmailBean {
    private static final Logger logger = LoggerFactory.getLogger(EmailBean.class);
    @Autowired
    private DataManager dataManager;
    @Autowired
    private Emailer emailer;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private Environment environment;
    @Inject
    EmailTemplates emailTemplates;
    private EmailInfo emailInfo;
    @Autowired
    private Environment env;

    @PostMapping("generateEmailCashWithdrawalConfirmation")
    public ResponseEntity generateEmailCashWithdrawalConfirmation(String emailTo, String name, Date date,
                                                                  BigDecimal withdrawalAmt, BigDecimal transactionFee, BigDecimal sstFee, BigDecimal totalAmt){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "cash_withdrawal_confirmation";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Cash Withdrawal Confirmation")
                    .build();

            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("withdrawal_amount", "RM" + formatNumber.format(withdrawalAmt));
            params.put("withdrawal_fee", "RM" + formatNumber.format(transactionFee));
            params.put("sst_amount", "RM" + formatNumber.format(sstFee));
            params.put("total_amount", "RM" + formatNumber.format(totalAmt));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email cash withdrawal confirmation sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("generateEmailCashTopupConfirmation")
    public ResponseEntity generateEmailCashTopupConfirmation(String emailTo, String name, Date date,
                                                             BigDecimal topupAmt, BigDecimal topupFee, BigDecimal sstFee, BigDecimal totalAmt){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "cash_topup_confirmation";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Cash Top-Up Confirmation")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("topup_amount", "RM" + formatNumber.format(topupAmt));
            params.put("topup_fee", "RM" + formatNumber.format(topupFee));
            params.put("sst_amount", "RM" + formatNumber.format(sstFee));
            params.put("total_amount_received", "RM" + formatNumber.format(totalAmt));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email cash top-up confirmation sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email cash top-up confirmation sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("generateEmailGoldPurchase")
    public ResponseEntity generateEmailGoldPurchase(String emailTo, String name, Date date, String refNo, String receiptNo,
                                                    BigDecimal pricePerG, BigDecimal quantity, BigDecimal totalAmt, BigDecimal purchaseFee, BigDecimal sstAmt, BigDecimal totalAmtPaid){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "gold_purchase";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Gold Purchase")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("reference_no", refNo);
            params.put("receipt_no", receiptNo);
            params.put("price_per_g", "RM" + pricePerG + "/g");
            params.put("quantity", quantity + "/g");
            params.put("total_amount", "RM" + formatNumber.format(totalAmt));
            params.put("purchase_fee", "RM" + formatNumber.format(purchaseFee));
            params.put("sst_amount", "RM" + formatNumber.format(sstAmt));
            params.put("total_amount_paid", "RM" + formatNumber.format(totalAmtPaid));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email gold purchase sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold purchase sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("generateEmailGoldSale")
    public ResponseEntity generateEmailGoldSale(String emailTo, String name, Date date, String refNo, String receiptNo, BigDecimal pricePerG, BigDecimal quantity, BigDecimal totalAmt, BigDecimal saleFee, BigDecimal sstAmt, BigDecimal totalAmtReceived){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "gold_sale";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Gold Sale")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("reference_no", refNo);
            params.put("receipt_no", receiptNo);
            params.put("price_per_g", "RM" + pricePerG + "/g");
            params.put("quantity", quantity + "/g");
            params.put("total_amount", "RM" + formatNumber.format(totalAmt));
            params.put("sale_fee", "RM" + formatNumber.format(saleFee));
            params.put("sst_amount", "RM" + formatNumber.format(sstAmt));
            params.put("total_amount_received", "RM" + formatNumber.format(totalAmtReceived));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email gold sale sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold sale sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("generateEmailGoldRedemption")
    public ResponseEntity generateEmailGoldRedemption(String emailTo, String name, Date date, String refNo,
                                                      String deliverTo, String phoneNo, String address1, String address2, String postcode, String city, String state,
                                                      String redemptionReason, Integer coins, BigDecimal courier_fee, BigDecimal minting_fee, BigDecimal takaful_fee, BigDecimal sst_amount, BigDecimal total_amount_paid){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "gold_redemption";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Gold Redemption")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("reference_no", refNo);
            params.put("deliver_to", deliverTo);
            params.put("phone_no", phoneNo);
            params.put("address_line1", address1);
            params.put("address_line2", address2);
            params.put("postcode", postcode);
            params.put("city", city);
            params.put("state", state);
            params.put("redemption_reason", redemptionReason);
            params.put("coins_redeemed", coins);
            params.put("courier_fee", "RM" + formatNumber.format(courier_fee));
            params.put("minting_fee", "RM" + formatNumber.format(minting_fee));
            params.put("takaful_fee", "RM" + formatNumber.format(takaful_fee));
            params.put("sst_amount", "RM" + formatNumber.format(sst_amount));
            params.put("total_amount_paid", "RM" + formatNumber.format(total_amount_paid));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email gold redemption sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold redemption sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("generateEmailGoldTransfer")
    public ResponseEntity generateEmailGoldTransfer(String emailTo, String name, Date date, String transferTo, String transferReason, BigDecimal pricePerG, BigDecimal quantity, BigDecimal transferFee, BigDecimal sstAmt, BigDecimal totalAmtPaid){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "gold_transfer";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Gold Transfer")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("transferred_to", transferTo);
            params.put("transfer_reason", transferReason);
            params.put("price_per_g", pricePerG);
            params.put("quantity", quantity);
            params.put("transfer_fee", "RM" + formatNumber.format(transferFee));
            params.put("sst_amount", "RM" + formatNumber.format(sstAmt));
            params.put("total_amount_paid", "RM" + formatNumber.format(totalAmtPaid));
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email gold transfer sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold transfer sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/goldReceived")
    public ResponseEntity generateEmailGoldReceived(String emailTo, String name, Date date, String receiveFrom, String transferReason, BigDecimal quantityReceived){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "gold_received";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Gold Received")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("receive_from", receiveFrom);
            params.put("transfer_reason", transferReason);
            params.put("quantity_received", quantityReceived);
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email gold received sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold received sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public ResponseEntity generateEmailRemainderToChecker(User users, String subject, String param1, String param2, ActivityLog activityLog) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
//            if(users.size() > 0) {
                String CREATED_TEMPLATE_CODE = "notify_checker";

//                String emailsTo = "";
//                users.forEach(e -> {
//                    String s = emailsTo + "," + e.getEmail();
//                });

                // - using email template - //
                EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                        .setTo(users.getEmail())
                        .setSubject(subject)
                        .build();
                Map<String, Object> params = new HashMap<String, Object>();
                params.put("name", users.getFirstName());
                params.put("param1", param1);
                params.put("param2", param2);
                params.put("createdBy", activityLog.getRequestBy());
                params.put("createdDate", dateFormat.format(activityLog.getRequestDate()));
                EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate, params);

                try {
                    emailer.sendEmail(emailInfo);
                    logger.info("Email checker_remainder sent to " + emailInfo.getAddresses());
                    return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email gold received sent to " + emailInfo.getAddresses() + "\"}");
                } catch (EmailException ex) {
                    logger.error(ex.getMessage());
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\"" + ex.getMessages().get(0) + "\"}");
                }
//                return ResponseEntity.ok("email send");
//            } else {
//                return ResponseEntity.ok("ok");
//            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
    public String generateEmailResetPassword(String emailTo, String newPassword){
        try{
            String CREATED_TEMPLATE_CODE = "reset_new_password";
            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Reset Password")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("newPassword", newPassword);
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);
            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email reset password sent to "+emailInfo.getAddresses());
                return "success";
//                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email reset password sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return "failure";
//                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return "failure";
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    @PostMapping("/rewardCredited")
    public ResponseEntity generateEmailRewardCredited(String emailTo, String name, String date, String promoCode, String rewardType, String rewardAmount){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "reward_credited";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Reward Credited")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
//            params.put("date", dateFormat.format(date));
            params.put("date", date);
            params.put("promo_code", promoCode);
            params.put("reward_type", rewardType);
            params.put("reward_amount", rewardAmount);
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email reward credited sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email reward credited sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("/receiveGold")
    public ResponseEntity generateEmailReceiveGold(String emailTo, String name, Date date, String bgd, String transferReason, BigDecimal amount){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "receive_gold";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Receive Gold")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("bgd", bgd);
            params.put("transfer_reason", transferReason);
            params.put("amount", amount);
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email receive gold sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email receive gold sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
    @PostMapping("/receiveCash")
    public ResponseEntity generateEmailReceiveCash(String emailTo, String name, Date date, String bgd, String transferReason, BigDecimal amount){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "receive_cash";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(emailTo)
                    .setSubject("Receive Cash")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("date", dateFormat.format(date));
            params.put("bgd", bgd);
            params.put("transfer_reason", transferReason);
            params.put("amount", amount);
            params.put("bgd_email", env.getProperty("bgd_email"));
            params.put("bgd_phone_no", env.getProperty("bgd_phone_no"));
            params.put("email", emailTo);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email receive cash sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email receive cash sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public ResponseEntity generateEmailAccountUpgradeSuccess(String username, String email){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "account_upgrade_success";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(email)
                    .setSubject("Successful Account Upgrade")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", username);
            params.put("email", email);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public ResponseEntity generateEmailAccountUpgradeFailed(String username, String email){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "account_upgrade_failed";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(email)
                    .setSubject("Unsuccessful Account Upgrade")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", username);
            params.put("email", email);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public ResponseEntity generateEmailAccountUpgradePending(String username, String email){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "account_upgrade_pending";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(email)
                    .setSubject("Pending Account Upgrade")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", username);
            params.put("email", email);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }

    public ResponseEntity generateEmailCreateAdminuser(String name, String email, String password){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss a");
        NumberFormat formatNumber = new DecimalFormat("###,##0.00");
        try{
            String CREATED_TEMPLATE_CODE = "create_admin_user";

            // - using email template - //
            EmailTemplate emailTemplate = emailTemplates.buildFromTemplate(CREATED_TEMPLATE_CODE)
                    .setTo(email)
                    .setSubject("Create Admin User")
                    .build();
            Map<String,Object> params = new HashMap<String,Object>();
            params.put("name", name);
            params.put("email", email);
            params.put("password", password);
            params.put("urlfb", env.getProperty("bgd_fb_url"));
            params.put("urlx", env.getProperty("bgd_x_url"));
            params.put("urlig", env.getProperty("bgd_instagram_url"));
            params.put("urllinkedin", env.getProperty("bgd_linkedin_url"));
            EmailInfo emailInfo = emailTemplates.generateEmail(emailTemplate,params);

            try {
                emailer.sendEmail(emailInfo);
                logger.info("Email sent to "+emailInfo.getAddresses());
                return ResponseEntity.status(HttpStatus.OK).body("{\"message\":\"Email sent to "+emailInfo.getAddresses()+"\"}");
            }
            catch(EmailException ex){
                logger.error(ex.getMessage());
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessages().get(0)+"\"}");
            }
        }catch(Exception ex){
            logger.error(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("{\"message\":\""+ex.getMessage()+"\"}");
        }
    }
}