package com.company.management.entity.bgdreporting;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Store(name = "bgdreporting")
@JmixEntity
@Table(name = "CASH_TOP_UP")
@Entity
public class CashTopUp {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@Column(name = "TIMESTAMP_")
	private String timestamp;

	@Column(name = "REFERENCE_NO")
	private String referenceNo;

	@InstanceName
	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "TOP_UP_CHANNEL")
	private String topUpChannel;

	@Column(name = "DEBITTING_ACC_PARTNER")
	private String debittingAccPartner;

	@Column(name = "ACCOUNT_OWNER")
	private String accountOwner;

	@Column(name = "BGD_ID")
	private String bgdID;

	@Column(name = "TOPUP_AMOUNT")
	private BigDecimal topupAmount;

	@Column(name = "FEE_AMOUNT", precision = 19, scale = 2)
	private BigDecimal feeAmount;

	@Column(name = "TAX", precision = 19, scale = 2)
	private BigDecimal tax;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public void setTopupAmount(BigDecimal topupAmount) {
		this.topupAmount = topupAmount;
	}

	public BigDecimal getTopupAmount() {
		return topupAmount;
	}

	public BigDecimal getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getBgdID() {
		return bgdID;
	}

	public void setBgdID(String bgdID) {
		this.bgdID = bgdID;
	}

	public String getAccountOwner() {
		return accountOwner;
	}

	public void setAccountOwner(String accountOwner) {
		this.accountOwner = accountOwner;
	}

	public String getDebittingAccPartner() {
		return debittingAccPartner;
	}

	public void setDebittingAccPartner(String debittingAccPartner) {
		this.debittingAccPartner = debittingAccPartner;
	}

	public String getTopUpChannel() {
		return topUpChannel;
	}

	public void setTopUpChannel(String topUpChannel) {
		this.topUpChannel = topUpChannel;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}