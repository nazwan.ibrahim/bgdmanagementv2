package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "email_sending_attachment")
@Entity
public class EmailSendingAttachment {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@Column(name = "content")
	private byte[] content;

	@Column(name = "content_id", length = 50)
	private String content1;

	@Column(name = "content_file")
	@Lob
	private String contentFile;

	@CreatedDate
	@Column(name = "create_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTs;

	@CreatedBy
	@Column(name = "created_by", length = 50)
	private String createdBy;

	@DeletedDate
	@Column(name = "delete_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteTs;

	@DeletedBy
	@Column(name = "deleted_by", length = 50)
	private String deletedBy;

	@Column(name = "disposition", length = 50)
	private String disposition;

	@JoinColumn(name = "message_id", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	private EmailSendingMessage message;

	@InstanceName
	@Column(name = "name", length = 500)
	private String name;

	@Column(name = "sys_tenant_id")
	private String sysTenant;

	@Column(name = "text_encoding", length = 50)
	private String textEncoding;


	@Column(name = "update_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTs;

	@Column(name = "updated_by", length = 50)
	private String updatedBy;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Date updateTs) {
		this.updateTs = updateTs;
	}

	public String getTextEncoding() {
		return textEncoding;
	}

	public void setTextEncoding(String textEncoding) {
		this.textEncoding = textEncoding;
	}

	public String getSysTenant() {
		return sysTenant;
	}

	public void setSysTenant(String sysTenant) {
		this.sysTenant = sysTenant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EmailSendingMessage getMessage() {
		return message;
	}

	public void setMessage(EmailSendingMessage message) {
		this.message = message;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeleteTs() {
		return deleteTs;
	}

	public void setDeleteTs(Date deleteTs) {
		this.deleteTs = deleteTs;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getContentFile() {
		return contentFile;
	}

	public void setContentFile(String contentFile) {
		this.contentFile = contentFile;
	}

	public String getContent1() {
		return content1;
	}

	public void setContent1(String content1) {
		this.content1 = content1;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}