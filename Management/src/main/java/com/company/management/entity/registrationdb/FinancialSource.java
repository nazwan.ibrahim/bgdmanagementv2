package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED,
        unmappedColumns = {"sourceof_fund_id", "source_of_wealth_id"})
@JmixEntity
@Store(name = "registration")
@Table(name = "financial_sources", indexes = {
        @Index(name = "IDX_FINANCIALSOU_SOURCEOFFUND", columnList = "source_of_fund_id"),
        @Index(name = "IDX_FINANCIALSOU_SOURCEOFWEAL", columnList = "source_of_wealth_id")
})
@Entity
public class FinancialSource {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "other_source_of_fund")
    private String otherSourceOfFund;

    @Column(name = "other_source_of_wealth ")
    private String otherSourceOfWealth;

    @JoinColumn(name = "source_of_fund_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SourceOfFundType sourceFundType;

    @JoinColumn(name = "source_of_wealth_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private SourceOfWealthType sourceWealthType;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @DeletedBy
    @Column(name = "deleted_by")
    private String deletedBy;

    @DeletedDate
    @Column(name = "deleted_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @LastModifiedBy
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserProfile user;

    @Version
    @Column(name = "version", nullable = false)
    private Integer version;

    public String getOtherSourceOfWealth() {
        return otherSourceOfWealth;
    }

    public void setOtherSourceOfWealth(String otherSourceOfWealth) {
        this.otherSourceOfWealth = otherSourceOfWealth;
    }

    public String getOtherSourceOfFund() {
        return otherSourceOfFund;
    }

    public void setOtherSourceOfFund(String otherSourceOfFund) {
        this.otherSourceOfFund = otherSourceOfFund;
    }

    public SourceOfWealthType getSourceWealthType() {
        return sourceWealthType;
    }

    public void setSourceWealthType(SourceOfWealthType sourceWealthType) {
        this.sourceWealthType = sourceWealthType;
    }

    public SourceOfFundType getSourceFundType() {
        return sourceFundType;
    }

    public void setSourceFundType(SourceOfFundType sourceFundType) {
        this.sourceFundType = sourceFundType;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}