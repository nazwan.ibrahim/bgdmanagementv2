package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.util.UUID;

@JmixEntity
public class AccountVerificationReport {
	@JmixGeneratedValue
	@JmixId
	private UUID id;

	private String investorName;

	private String dateOfBirth;

	private String taxResidenceCountry;

	private String nationality;

	private String residentialAddress;

	private String residentialPostcode;

	private String residentialCountry;

	private String contactNum;

	private String bgdID;

	private String accountStatusEndYear;

	private String accountOpeningDate;

	private String accountClosingDate;

	private String reportingYear;

	private String lastAccountZerorise;

	private String accountBalanceEndYear;

	private String accountCurrency;

	private String grossSaleRedemptionYear;

	private String grossSaleRedemptionCurrency;

	public String getLastAccountZerorise() {
		return lastAccountZerorise;
	}

	public void setLastAccountZerorise(String lastAccountZerorise) {
		this.lastAccountZerorise = lastAccountZerorise;
	}

	public String getGrossSaleRedemptionYear() {
		return grossSaleRedemptionYear;
	}

	public void setGrossSaleRedemptionYear(String grossSaleRedemptionYear) {
		this.grossSaleRedemptionYear = grossSaleRedemptionYear;
	}

	public String getGrossSaleRedemptionCurrency() {
		return grossSaleRedemptionCurrency;
	}

	public void setGrossSaleRedemptionCurrency(String grossSaleRedemptionCurrency) {
		this.grossSaleRedemptionCurrency = grossSaleRedemptionCurrency;
	}

	public String getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(String accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public String getAccountBalanceEndYear() {
		return accountBalanceEndYear;
	}

	public void setAccountBalanceEndYear(String accountBalanceEndYear) {
		this.accountBalanceEndYear = accountBalanceEndYear;
	}

	public String getReportingYear() {
		return reportingYear;
	}

	public void setReportingYear(String reportingYear) {
		this.reportingYear = reportingYear;
	}

	public String getAccountClosingDate() {
		return accountClosingDate;
	}

	public void setAccountClosingDate(String accountClosingDate) {
		this.accountClosingDate = accountClosingDate;
	}

	public String getAccountOpeningDate() {
		return accountOpeningDate;
	}

	public void setAccountOpeningDate(String accountOpeningDate) {
		this.accountOpeningDate = accountOpeningDate;
	}

	public String getAccountStatusEndYear() {
		return accountStatusEndYear;
	}

	public void setAccountStatusEndYear(String accountStatusEndYear) {
		this.accountStatusEndYear = accountStatusEndYear;
	}

	public String getBgdID() {
		return bgdID;
	}

	public void setBgdID(String bgdID) {
		this.bgdID = bgdID;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	public String getResidentialCountry() {
		return residentialCountry;
	}

	public void setResidentialCountry(String residentialCountry) {
		this.residentialCountry = residentialCountry;
	}

	public String getResidentialPostcode() {
		return residentialPostcode;
	}

	public void setResidentialPostcode(String residentialPostcode) {
		this.residentialPostcode = residentialPostcode;
	}

	public String getResidentialAddress() {
		return residentialAddress;
	}

	public void setResidentialAddress(String residentialAddress) {
		this.residentialAddress = residentialAddress;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getTaxResidenceCountry() {
		return taxResidenceCountry;
	}

	public void setTaxResidenceCountry(String taxResidenceCountry) {
		this.taxResidenceCountry = taxResidenceCountry;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getInvestorName() {
		return investorName;
	}

	public void setInvestorName(String investorName) {
		this.investorName = investorName;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}