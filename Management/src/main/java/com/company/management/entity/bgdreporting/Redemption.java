package com.company.management.entity.bgdreporting;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Store(name = "bgdreporting")
@Table(name = "REDEMPTION")
@Entity
public class Redemption {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@Column(name = "BGD_ID")
	private String bgdID;

	@Column(name = "CUSTOMER_NAME")
	private String customerName;

	@Column(name = "GOLD_CODE")
	private String goldCode;

	@Column(name = "GOLD_TYPE")
	private String goldType;

	@Column(name = "VAULT_WAREHOUSE_LOCATION")
	private String vaultWarehouseLocation;

	@Column(name = "GOLD_DELIVERY_LOCATION")
	private String goldDeliveryLocation;

	@Column(name = "GOLD_DELIVERY_PARTNERS")
	private String goldDeliveryPartners;

	@Column(name = "SERIAL_NO")
	private String serialNo;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "OUT_IN_PCS", precision = 19)
	private BigInteger outInPcs;

	@Column(name = "OUT_IN_WEIGH", precision = 19, scale = 2)
	private BigDecimal outInWeigh;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public BigDecimal getOutInWeigh() {
		return outInWeigh;
	}

	public void setOutInWeigh(BigDecimal outInWeigh) {
		this.outInWeigh = outInWeigh;
	}

	public BigInteger getOutInPcs() {
		return outInPcs;
	}

	public void setOutInPcs(BigInteger outInPcs) {
		this.outInPcs = outInPcs;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getGoldDeliveryPartners() {
		return goldDeliveryPartners;
	}

	public void setGoldDeliveryPartners(String goldDeliveryPartners) {
		this.goldDeliveryPartners = goldDeliveryPartners;
	}

	public String getGoldDeliveryLocation() {
		return goldDeliveryLocation;
	}

	public void setGoldDeliveryLocation(String goldDeliveryLocation) {
		this.goldDeliveryLocation = goldDeliveryLocation;
	}

	public String getVaultWarehouseLocation() {
		return vaultWarehouseLocation;
	}

	public void setVaultWarehouseLocation(String vaultWarehouseLocation) {
		this.vaultWarehouseLocation = vaultWarehouseLocation;
	}

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	public String getGoldCode() {
		return goldCode;
	}

	public void setGoldCode(String goldCode) {
		this.goldCode = goldCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBgdID() {
		return bgdID;
	}

	public void setBgdID(String bgdID) {
		this.bgdID = bgdID;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}