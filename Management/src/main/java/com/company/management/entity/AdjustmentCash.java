package com.company.management.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum AdjustmentCash implements EnumClass<String> {

	ADDITION("ADDITION"),
    DEDUCTION("DEDUCTION");

	private final String id;

	AdjustmentCash(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Nullable
	public static AdjustmentCash fromId(String id) {
		for (AdjustmentCash at : AdjustmentCash.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}
}