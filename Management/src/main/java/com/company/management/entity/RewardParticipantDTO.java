package com.company.management.entity;

import com.company.management.entity.registrationdb.UserProfile;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
public class RewardParticipantDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private String rewardCode;

    private String userID;

    private RewardManagement reward;

    private RewardParticipant rewardParticipant;

    private UserProfile userProfile;

    private BgdIDDTO bgdID;

    @Temporal(TemporalType.TIMESTAMP)
    private Date redeemed_date;

    private BigDecimal redeemed_amount;

    private String campaign_name;

    private String fullname;

    private Integer maxParticipant;

    private String bgdIdStr;

    public String getBgdIdStr() {
        return bgdIdStr;
    }

    public void setBgdIdStr(String bgdIdStr) {
        this.bgdIdStr = bgdIdStr;
    }

    public Integer getMaxParticipant() {
        return maxParticipant;
    }

    public void setMaxParticipant(Integer maxParticipant) {
        this.maxParticipant = maxParticipant;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCampaign_name() {
        return campaign_name;
    }

    public void setCampaign_name(String campaign_name) {
        this.campaign_name = campaign_name;
    }

    public BigDecimal getRedeemed_amount() {
        return redeemed_amount;
    }

    public void setRedeemed_amount(BigDecimal redeemed_amount) {
        this.redeemed_amount = redeemed_amount;
    }

    public Date getRedeemed_date() {
        return redeemed_date;
    }

    public void setRedeemed_date(Date redeemed_date) {
        this.redeemed_date = redeemed_date;
    }

    public BgdIDDTO getBgdID() {
        return bgdID;
    }

    public void setBgdID(BgdIDDTO bgdID) {
        this.bgdID = bgdID;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public RewardParticipant getRewardParticipant() {
        return rewardParticipant;
    }

    public void setRewardParticipant(RewardParticipant rewardParticipant) {
        this.rewardParticipant = rewardParticipant;
    }

    public RewardManagement getReward() {
        return reward;
    }

    public void setReward(RewardManagement reward) {
        this.reward = reward;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}