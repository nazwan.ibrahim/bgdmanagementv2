package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "bgdreporting")
@Table(name = "gold_inventory_report")
@Entity
public class GoldInventoryReport {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "accumulative_physical_stock_g", precision = 19, scale = 6)
    private BigDecimal accumulativePhysicalStockG;

    @InstanceName
    @Column(name = "description")
    private String description;

    @Column(name = "purchase_order_no")
    private String purchaseOrderNo;

    @Column(name = "delivery_no")
    private String deliveryNo;

    @Column(name = "timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "accumulative_tradable_bgd_g", precision = 19, scale = 6)
    private BigDecimal accumulativeTradableBgdG;

    @Column(name = "accumulative_tradable_bgd_unit", precision = 19, scale = 6)
    private BigDecimal accumulativeTradableBgdUnit;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "deleted_by")
    private String deletedBy;

    @Column(name = "deleted_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @Column(name = "gold_supplier")
    private String goldSupplier;

    @Column(name = "gold_type")
    private String goldType;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Column(name = "non_tradable_g", precision = 19, scale = 6)
    private BigDecimal nonTradableG;

    @Column(name = "physical_stock_g", precision = 19, scale = 6)
    private BigDecimal physicalStockG;

    @Column(name = "report_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date reportDate;

    @Column(name = "serial_no")
    private String serialNo;

    @Column(name = "status")
    private String status;

    @Column(name = "tradable_bgd_g", precision = 19, scale = 6)
    private BigDecimal tradableBgdG;

    @Column(name = "vault_location")
    private String vaultLocation;

    @Column(name = "version", nullable = false)
    private Integer version;

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getPurchaseOrderNo() {
        return purchaseOrderNo;
    }

    public void setPurchaseOrderNo(String purchaseOrderNo) {
        this.purchaseOrderNo = purchaseOrderNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getVaultLocation() {
        return vaultLocation;
    }

    public void setVaultLocation(String vaultLocation) {
        this.vaultLocation = vaultLocation;
    }

    public BigDecimal getTradableBgdG() {
        return tradableBgdG;
    }

    public void setTradableBgdG(BigDecimal tradableBgdG) {
        this.tradableBgdG = tradableBgdG;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public BigDecimal getPhysicalStockG() {
        return physicalStockG;
    }

    public void setPhysicalStockG(BigDecimal physicalStockG) {
        this.physicalStockG = physicalStockG;
    }

    public BigDecimal getNonTradableG() {
        return nonTradableG;
    }

    public void setNonTradableG(BigDecimal nonTradableG) {
        this.nonTradableG = nonTradableG;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getGoldType() {
        return goldType;
    }

    public void setGoldType(String goldType) {
        this.goldType = goldType;
    }

    public String getGoldSupplier() {
        return goldSupplier;
    }

    public void setGoldSupplier(String goldSupplier) {
        this.goldSupplier = goldSupplier;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public BigDecimal getAccumulativeTradableBgdUnit() {
        return accumulativeTradableBgdUnit;
    }

    public void setAccumulativeTradableBgdUnit(BigDecimal accumulativeTradableBgdUnit) {
        this.accumulativeTradableBgdUnit = accumulativeTradableBgdUnit;
    }

    public BigDecimal getAccumulativeTradableBgdG() {
        return accumulativeTradableBgdG;
    }

    public void setAccumulativeTradableBgdG(BigDecimal accumulativeTradableBgdG) {
        this.accumulativeTradableBgdG = accumulativeTradableBgdG;
    }

    public BigDecimal getAccumulativePhysicalStockG() {
        return accumulativePhysicalStockG;
    }

    public void setAccumulativePhysicalStockG(BigDecimal accumulativePhysicalStockG) {
        this.accumulativePhysicalStockG = accumulativePhysicalStockG;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}