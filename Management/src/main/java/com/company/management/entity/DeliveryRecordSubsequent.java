package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "DELIVERY_RECORD_SUBSEQUENT", indexes = {
		@Index(name = "IDX_DELIVERYRECO_RESERVERECOR", columnList = "RESERVE_RECORD_ID"),
		@Index(name = "IDX_DELIVERYRECORDSUBSE_STATUS", columnList = "STATUS_ID")
})
@Entity
public class DeliveryRecordSubsequent {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@InstanceName
	@JoinColumn(name = "RESERVE_RECORD_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private ReserveRecord reserveRecord;

	@Column(name = "LOCATION")
	private String location;

	@Column(name = "DESTINATION")
	private String destination;

	@Column(name = "DELIVERY_RECEIPT")
	private String deliveryReceipt;

	@JoinColumn(name = "STATUS_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType status;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public void setReserveRecord(ReserveRecord reserveRecord) {
		this.reserveRecord = reserveRecord;
	}

	public ReserveRecord getReserveRecord() {
		return reserveRecord;
	}

	public RtType getStatus() {
		return status;
	}

	public void setStatus(RtType status) {
		this.status = status;
	}

	public String getDeliveryReceipt() {
		return deliveryReceipt;
	}

	public void setDeliveryReceipt(String deliveryReceipt) {
		this.deliveryReceipt = deliveryReceipt;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}