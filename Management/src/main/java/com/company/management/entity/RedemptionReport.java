package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "bgdreporting")
@Table(name = "redemption_report")
@Entity
public class RedemptionReport {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "bgd_id")
    private String bgd;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "customer_name")
    private String customerName;

    @Column(name = "deleted_by")
    private String deletedBy;

    @Column(name = "deleted_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @InstanceName
    @Column(name = "description")
    private String description;

    @Column(name = "gold_code")
    private String goldCode;

    @Column(name = "gold_delivery_partner")
    private String goldDeliveryPartner;

    @Column(name = "gold_type")
    private String goldType;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Column(name = "out_piece")
    private Integer outPiece;

    @Column(name = "out_weight", precision = 19, scale = 2)
    private BigDecimal outWeight;

    @Column(name = "reference_no")
    private String referenceNo;

    @Column(name = "serial_no")
    private String serialNo;

    @Column(name = "status")
    private String status;

    @Column(name = "timestamp_")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(name = "vault_warehouse_location")
    private String vaultWarehouseLocation;

    @Column(name = "version", nullable = false)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getVaultWarehouseLocation() {
        return vaultWarehouseLocation;
    }

    public void setVaultWarehouseLocation(String vaultWarehouseLocation) {
        this.vaultWarehouseLocation = vaultWarehouseLocation;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public BigDecimal getOutWeight() {
        return outWeight;
    }

    public void setOutWeight(BigDecimal outWeight) {
        this.outWeight = outWeight;
    }

    public Integer getOutPiece() {
        return outPiece;
    }

    public void setOutPiece(Integer outPiece) {
        this.outPiece = outPiece;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getGoldType() {
        return goldType;
    }

    public void setGoldType(String goldType) {
        this.goldType = goldType;
    }

    public String getGoldDeliveryPartner() {
        return goldDeliveryPartner;
    }

    public void setGoldDeliveryPartner(String goldDeliveryPartner) {
        this.goldDeliveryPartner = goldDeliveryPartner;
    }

    public String getGoldCode() {
        return goldCode;
    }

    public void setGoldCode(String goldCode) {
        this.goldCode = goldCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBgd() {
        return bgd;
    }

    public void setBgd(String bgd) {
        this.bgd = bgd;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}