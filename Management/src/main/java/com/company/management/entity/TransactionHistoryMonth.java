package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "transaction_history_month")
@Entity
public class TransactionHistoryMonth {
    @Column(name = "acct_number")
    @Id
    private String acctNumber;

    @Column(name = "acc_owner")
    private String accOwner;

    @Column(name = "sumamount")
    private BigDecimal sumamount;

    @Column(name = "transaction")
    private String transaction;

    @Column(name = "transcode")
    private String transcode;

    @Column(name = "wallet")
    private String wallet;

    @Column(name = "walletcode")
    private String walletcode;

    public String getWalletcode() {
        return walletcode;
    }

    public void setWalletcode(String walletcode) {
        this.walletcode = walletcode;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getTranscode() {
        return transcode;
    }

    public void setTranscode(String transcode) {
        this.transcode = transcode;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public BigDecimal getSumamount() {
        return sumamount;
    }

    public void setSumamount(BigDecimal sumamount) {
        this.sumamount = sumamount;
    }

    public String getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(String accOwner) {
        this.accOwner = accOwner;
    }

    public String getAcctNumber() {
        return acctNumber;
    }

    public void setAcctNumber(String acctNumber) {
        this.acctNumber = acctNumber;
    }
}