package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DdlGeneration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Table(name = "oauth_refresh_token")
@Entity
public class OauthRefreshToken {
	@Column(name = "token_id", nullable = false)
	@Id
	private String token;

	@Column(name = "authentication")
	private byte[] authentication;

	@Column(name = "token")
	private byte[] token1;

	public byte[] getToken1() {
		return token1;
	}

	public void setToken1(byte[] token1) {
		this.token1 = token1;
	}

	public byte[] getAuthentication() {
		return authentication;
	}

	public void setAuthentication(byte[] authentication) {
		this.authentication = authentication;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}