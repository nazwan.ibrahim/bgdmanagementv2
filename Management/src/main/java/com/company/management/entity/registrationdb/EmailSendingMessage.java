package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "email_sending_message")
@Entity
public class EmailSendingMessage {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@Column(name = "address_bcc")
	@Lob
	private String addressBcc;

	@Column(name = "address_cc")
	@Lob
	private String addressCc;

	@Column(name = "address_from", length = 100)
	private String addressFrom;

	@Column(name = "address_to")
	@Lob
	private String addressTo;

	@Column(name = "attachments_name")
	@Lob
	private String attachmentsName;

	@Column(name = "attempts_limit")
	private Integer attemptsLimit;

	@Column(name = "attempts_made")
	private Integer attemptsMade;

	@Column(name = "body_content_type", length = 50)
	private String bodyContentType;

	@Column(name = "content_text")
	@Lob
	private String contentText;

	@Column(name = "content_text_file")
	@Lob
	private String contentTextFile;

	@Column(name = "create_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTs;

	@CreatedBy
	@Column(name = "created_by", length = 50)
	private String createdBy;

	@Column(name = "date_sent")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateSent;

	@Column(name = "deadline")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deadline;

	@DeletedDate
	@Column(name = "delete_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteTs;

	@DeletedBy
	@Column(name = "deleted_by", length = 50)
	private String deletedBy;

	@Column(name = "email_headers", length = 500)
	private String emailHeaders;

	@Column(name = "important")
	private Boolean important;

	@Column(name = "status")
	private Integer status;

	@Column(name = "subject", length = 500)
	private String subject;

	@Column(name = "sys_tenant_id")
	private String sysTenant;

	@LastModifiedDate
	@Column(name = "update_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTs;

	@LastModifiedBy
	@Column(name = "updated_by", length = 50)
	private String updatedBy;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Date updateTs) {
		this.updateTs = updateTs;
	}

	public String getSysTenant() {
		return sysTenant;
	}

	public void setSysTenant(String sysTenant) {
		this.sysTenant = sysTenant;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getImportant() {
		return important;
	}

	public void setImportant(Boolean important) {
		this.important = important;
	}

	public String getEmailHeaders() {
		return emailHeaders;
	}

	public void setEmailHeaders(String emailHeaders) {
		this.emailHeaders = emailHeaders;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getDeleteTs() {
		return deleteTs;
	}

	public void setDeleteTs(Date deleteTs) {
		this.deleteTs = deleteTs;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

	public Date getDateSent() {
		return dateSent;
	}

	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreateTs() {
		return createTs;
	}

	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getContentTextFile() {
		return contentTextFile;
	}

	public void setContentTextFile(String contentTextFile) {
		this.contentTextFile = contentTextFile;
	}

	public String getContentText() {
		return contentText;
	}

	public void setContentText(String contentText) {
		this.contentText = contentText;
	}

	public String getBodyContentType() {
		return bodyContentType;
	}

	public void setBodyContentType(String bodyContentType) {
		this.bodyContentType = bodyContentType;
	}

	public Integer getAttemptsMade() {
		return attemptsMade;
	}

	public void setAttemptsMade(Integer attemptsMade) {
		this.attemptsMade = attemptsMade;
	}

	public Integer getAttemptsLimit() {
		return attemptsLimit;
	}

	public void setAttemptsLimit(Integer attemptsLimit) {
		this.attemptsLimit = attemptsLimit;
	}

	public String getAttachmentsName() {
		return attachmentsName;
	}

	public void setAttachmentsName(String attachmentsName) {
		this.attachmentsName = attachmentsName;
	}

	public String getAddressTo() {
		return addressTo;
	}

	public void setAddressTo(String addressTo) {
		this.addressTo = addressTo;
	}

	public String getAddressFrom() {
		return addressFrom;
	}

	public void setAddressFrom(String addressFrom) {
		this.addressFrom = addressFrom;
	}

	public String getAddressCc() {
		return addressCc;
	}

	public void setAddressCc(String addressCc) {
		this.addressCc = addressCc;
	}

	public String getAddressBcc() {
		return addressBcc;
	}

	public void setAddressBcc(String addressBcc) {
		this.addressBcc = addressBcc;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}