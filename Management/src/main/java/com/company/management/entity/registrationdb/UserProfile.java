package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "user_")
@Entity
public class UserProfile {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@JoinColumn(name = "act_status_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private ActivationStatus actStatus;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "approve_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approveDate;

	@Column(name = "bgd_id")
	private String bgd;

	@Column(name = "birthday")
	private String birthday;

	@Column(name = "close_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date closeDate;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@DeletedBy
	@Column(name = "deleted_by")
	private String deletedBy;

	@DeletedDate
	@Column(name = "deleted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@Column(name = "device_id")
	private String device;

	@Column(name = "email")
	private String email;

	@Column(name = "email_verified")
	private Boolean emailVerified;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "home_number")
	private String homeNumber;

	@Column(name = "identification_expiry_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date identificationExpiryDate;

	@Column(name = "identification_issuance_country")
	private String identificationIssuanceCountry;

	@Column(name = "identification_no")
	private String identificationNo;

	@JoinColumn(name = "identification_type_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private Identification identificationType;

	@LastModifiedBy
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "nationality")
	private String nationality;

	@Column(name = "next_review_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date nextReviewDate;

	@Column(name = "office_number")
	private String officeNumber;

	@Column(name = "password")
	private String password;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "pin")
	private String pin;

	@Column(name = "pin_enable")
	private Boolean pinEnable;

	@Column(name = "reference")
	private String reference;

	@Column(name = "referral")
	private String referral;

	@Column(name = "security_phase")
	private String securityPhase;

	@Column(name = "time_zone_id")
	private String timeZone;

	@JoinColumn(name = "user_type_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private UserType userType;

	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "SSO_USER_ID")
	private UUID ssoUserID;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public UUID getSsoUserID() {
		return ssoUserID;
	}

	public void setSsoUserID(UUID ssoUserID) {
		this.ssoUserID = ssoUserID;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getSecurityPhase() {
		return securityPhase;
	}

	public void setSecurityPhase(String securityPhase) {
		this.securityPhase = securityPhase;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Boolean getPinEnable() {
		return pinEnable;
	}

	public void setPinEnable(Boolean pinEnable) {
		this.pinEnable = pinEnable;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}

	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}

	public Date getNextReviewDate() {
		return nextReviewDate;
	}

	public void setNextReviewDate(Date nextReviewDate) {
		this.nextReviewDate = nextReviewDate;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Identification getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(Identification identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentificationNo() {
		return identificationNo;
	}

	public void setIdentificationNo(String identificationNo) {
		this.identificationNo = identificationNo;
	}

	public String getIdentificationIssuanceCountry() {
		return identificationIssuanceCountry;
	}

	public void setIdentificationIssuanceCountry(String identificationIssuanceCountry) {
		this.identificationIssuanceCountry = identificationIssuanceCountry;
	}

	public Date getIdentificationExpiryDate() {
		return identificationExpiryDate;
	}

	public void setIdentificationExpiryDate(Date identificationExpiryDate) {
		this.identificationExpiryDate = identificationExpiryDate;
	}

	public String getHomeNumber() {
		return homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getEmailVerified() {
		return emailVerified;
	}

	public void setEmailVerified(Boolean emailVerified) {
		this.emailVerified = emailVerified;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBgd() {
		return bgd;
	}

	public void setBgd(String bgd) {
		this.bgd = bgd;
	}

	public Date getApproveDate() {
		return approveDate;
	}

	public void setApproveDate(Date approveDate) {
		this.approveDate = approveDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ActivationStatus getActStatus() {
		return actStatus;
	}

	public void setActStatus(ActivationStatus actStatus) {
		this.actStatus = actStatus;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}