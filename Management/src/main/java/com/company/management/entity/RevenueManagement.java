package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "REVENUE_MANAGEMENT", indexes = {
        @Index(name = "IDX_REVENUE_MANAGEMENT_PRODUCT", columnList = "PRODUCT_ID"),
        @Index(name = "IDX_REVENUE_MANAGEMENT_TYPE", columnList = "TYPE_ID"),
        @Index(name = "IDX_REVENUE_MANAGEMENT_UOM", columnList = "UOM_ID"),
        @Index(name = "IDX_REVENUEMANAGEMENT_TYPETAX", columnList = "TYPE_TAX_ID"),
        @Index(name = "IDX_REVENUE_MANAGEMENT_UOM_TAX", columnList = "UOM_TAX_ID"),
        @Index(name = "IDX_REVENUE_MANAGEMENT_STATUS", columnList = "STATUS_ID")
})
@Entity
public class RevenueManagement {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "PRODUCT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    @JoinColumn(name = "TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType type;

    @Column(name = "CHARGE", precision = 19, scale = 4)
    private BigDecimal charge;

    @JoinColumn(name = "UOM_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType uom;

    @Column(name = "MIN_RM", precision = 19, scale = 2)
    private BigDecimal minRM;

    @Column(name = "MAX_RM", precision = 19, scale = 2)
    private BigDecimal maxRM;

    @Column(name = "ACTIVE")
    private Boolean active;

    @JoinColumn(name = "TYPE_TAX_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType typeTax;

    @Column(name = "VALUE_TAX", precision = 19, scale = 2)
    private BigDecimal valueTax;

    @JoinColumn(name = "UOM_TAX_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType uomTax;

    @Column(name = "REQUEST_BY")
    private String requestBy;

    @Column(name = "REQUEST_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;

    @Column(name = "VERIFY_BY")
    private String verifyBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "VERIFY_DATE")
    private Date verifyDate;

    @Column(name = "APPROVED_BY")
    private String approvedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "APPROVED_DATE")
    private Date approvedDate;

    @Column(name = "EFECTIVE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date efectiveDate;

    @JoinColumn(name = "STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType status;

    @Column(name = "REMARKS")
    private String remarks;

    @Column(name = "REMARKS2")
    private String remarks2;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    public String getRemarks2() {
        return remarks2;
    }

    public void setRemarks2(String remarks2) {
        this.remarks2 = remarks2;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setVerifyDate(Date verifyDate) {
        this.verifyDate = verifyDate;
    }

    public Date getVerifyDate() {
        return verifyDate;
    }

    public BigDecimal getMaxRM() {
        return maxRM;
    }

    public void setMaxRM(BigDecimal maxRM) {
        this.maxRM = maxRM;
    }

    public BigDecimal getMinRM() {
        return minRM;
    }

    public void setMinRM(BigDecimal minRM) {
        this.minRM = minRM;
    }

    public Date getEfectiveDate() {
        return efectiveDate;
    }

    public void setEfectiveDate(Date efectiveDate) {
        this.efectiveDate = efectiveDate;
    }

    public String getVerifyBy() {
        return verifyBy;
    }

    public void setVerifyBy(String verifyBy) {
        this.verifyBy = verifyBy;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestBy() {
        return requestBy;
    }

    public void setRequestBy(String requestBy) {
        this.requestBy = requestBy;
    }

    public RtType getStatus() {
        return status;
    }

    public void setStatus(RtType status) {
        this.status = status;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public RtType getUomTax() {
        return uomTax;
    }

    public void setUomTax(RtType uomTax) {
        this.uomTax = uomTax;
    }

    public BigDecimal getValueTax() {
        return valueTax;
    }

    public void setValueTax(BigDecimal valueTax) {
        this.valueTax = valueTax;
    }

    public RtType getTypeTax() {
        return typeTax;
    }

    public void setTypeTax(RtType typeTax) {
        this.typeTax = typeTax;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public RtType getUom() {
        return uom;
    }

    public void setUom(RtType uom) {
        this.uom = uom;
    }

    public BigDecimal getCharge() {
        return charge;
    }

    public void setCharge(BigDecimal charge) {
        this.charge = charge;
    }

    public RtType getType() {
        return type;
    }

    public void setType(RtType type) {
        this.type = type;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}