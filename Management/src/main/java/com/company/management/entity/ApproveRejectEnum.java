package com.company.management.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum ApproveRejectEnum implements EnumClass<String> {

    APPROVE("Approve"),
    REJECT("Reject");

    private String id;

    ApproveRejectEnum(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ApproveRejectEnum fromId(String id) {
        for (ApproveRejectEnum at : ApproveRejectEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}