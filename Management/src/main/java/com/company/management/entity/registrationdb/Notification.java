package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "notification")
@Entity
public class Notification {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "category_id_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationCategory categoryId;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @DeletedBy
    @Column(name = "deleted_by")
    private String deletedBy;

    @DeletedDate
    @Column(name = "deleted_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @JoinColumn(name = "detail_id_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationDetail detailId;

    @LastModifiedBy
    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Column(name = "read_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date readDateTime;

    @Column(name = "send_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sendDateTime;

    @JoinColumn(name = "status_id_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NotificationStatus statusId;

    @JoinColumn(name = "user_id_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserProfile userId;

    @Version
    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UserProfile getUserId() {
        return userId;
    }

    public void setUserId(UserProfile userId) {
        this.userId = userId;
    }

    public NotificationStatus getStatusId() {
        return statusId;
    }

    public void setStatusId(NotificationStatus statusId) {
        this.statusId = statusId;
    }

    public Date getSendDateTime() {
        return sendDateTime;
    }

    public void setSendDateTime(Date sendDateTime) {
        this.sendDateTime = sendDateTime;
    }

    public Date getReadDateTime() {
        return readDateTime;
    }

    public void setReadDateTime(Date readDateTime) {
        this.readDateTime = readDateTime;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public NotificationDetail getDetailId() {
        return detailId;
    }

    public void setDetailId(NotificationDetail detailId) {
        this.detailId = detailId;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public NotificationCategory getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(NotificationCategory categoryId) {
        this.categoryId = categoryId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}