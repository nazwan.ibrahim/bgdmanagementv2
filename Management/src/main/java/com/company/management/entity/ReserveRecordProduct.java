package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "REVERSE_RECORD_PRODUCT", indexes = {
		@Index(name = "IDX_REVERSERECOR_REVERSERECOR", columnList = "REVERSE_RECORD_ID"),
		@Index(name = "IDX_REVERSERECORDPRODUCT_TYPE", columnList = "TYPE_ID")
})
@Entity
public class ReserveRecordProduct {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@InstanceName
	@JoinColumn(name = "REVERSE_RECORD_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private ReserveRecord reverse_record;

	@Column(name = "CODE")
	private String code;

	@Column(name = "SERIAL_NUMBER")
	private String serial_number;

	@JoinColumn(name = "TYPE_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType type;

	@Column(name = "PURITY", precision = 19, scale = 2)
	private BigDecimal purity;

	@Column(name = "QUANTITY")
	private Integer quantity;

	@Column(name = "PRICE", precision = 19, scale = 2)
	private BigDecimal price;

	@Column(name = "SUB_TOTAL", precision = 19, scale = 2)
	private BigDecimal sub_total;

	@Column(name = "TRADABLE_GOLD", precision = 19, scale = 6)
	private BigDecimal tradable_gold;

	@Column(name = "HOLDING_ACCT", precision = 19, scale = 6)
	private BigDecimal holding_acct;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public BigDecimal getHolding_acct() {
		return holding_acct;
	}

	public void setHolding_acct(BigDecimal holding_acct) {
		this.holding_acct = holding_acct;
	}

	public BigDecimal getTradable_gold() {
		return tradable_gold;
	}

	public void setTradable_gold(BigDecimal tradable_gold) {
		this.tradable_gold = tradable_gold;
	}

	public RtType getType() {
		return type;
	}

	public void setType(RtType type) {
		this.type = type;
	}

	public BigDecimal getSub_total() {
		return sub_total;
	}

	public void setSub_total(BigDecimal sub_total) {
		this.sub_total = sub_total;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPurity() {
		return purity;
	}

	public void setPurity(BigDecimal purity) {
		this.purity = purity;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ReserveRecord getReverse_record() {
		return reverse_record;
	}

	public void setReverse_record(ReserveRecord reverse_record) {
		this.reverse_record = reverse_record;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}