package com.company.management.entity;

import javax.persistence.Column;
import javax.persistence.PrePersist;
import java.time.ZoneId;
import java.util.Date;

public class BaseEntity {
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "LAST_MODIFIED_DATE")
    private Date lastModifiedDate;

    @Column(name = "DELETED_DATE")
    private Date deletedDate;

    // Define the old and new timezones that you want to convert between
//    private static final ZoneId OLD_ZONE = ZoneId.of("Asia/Kuala_Lumpur");
    private static final ZoneId zoneIdMalaysia = ZoneId.of("Asia/Kuala_Lumpur");

    // ...

    @PrePersist
    public void prePersist() {
//        ZonedDateTime oldDate = createdDate.toInstant().atZone(OLD_ZONE);
//        ZonedDateTime newDate = oldDate.withZoneSameInstant(NEW_ZONE);
        createdDate = Date.from(createdDate.toInstant().atZone(zoneIdMalaysia).toInstant());
        lastModifiedDate = Date.from(createdDate.toInstant().atZone(zoneIdMalaysia).toInstant());
        deletedDate = Date.from(deletedDate.toInstant().atZone(zoneIdMalaysia).toInstant());
    }
}
