package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.JmixEntity;

import java.util.UUID;

@JmixEntity
public class FatcaCRSDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    private Boolean agreed;

    private String declaration;

    public void setAgreed(Boolean agreed) {
        this.agreed = agreed;
    }

    public Boolean getAgreed() {
        return agreed;
    }

    public String getDeclaration() {
        return declaration;
    }

    public void setDeclaration(String declaration) {
        this.declaration = declaration;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}