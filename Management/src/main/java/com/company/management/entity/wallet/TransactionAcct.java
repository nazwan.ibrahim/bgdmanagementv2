package com.company.management.entity.wallet;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "wallet")
@Table(name = "transaction_acct")
@Entity
public class TransactionAcct {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@JoinColumn(name = "account_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private AccountBgd account;

	@Column(name = "amount", precision = 19, scale = 4)
	private BigDecimal amount;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@DeletedBy
	@Column(name = "deleted_by")
	private String deletedBy;

	@DeletedDate
	@Column(name = "deleted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@LastModifiedBy
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@Column(name = "price", precision = 19, scale = 2)
	private BigDecimal price;

	@Column(name = "reference_id")
	private String reference;

	@JoinColumn(name = "trans_method_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private TransMethod transMethod;

	@JoinColumn(name = "trans_status_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private TransStatus transStatus;

	@JoinColumn(name = "trans_type_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private TransactionType transType;

	@OneToMany(mappedBy = "transaction")
	private List<RedeemInfo> redeemInfo;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public List<RedeemInfo> getRedeemInfo() {
		return redeemInfo;
	}

	public void setRedeemInfo(List<RedeemInfo> redeemInfo) {
		this.redeemInfo = redeemInfo;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getReference() {
		return reference;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public TransactionType getTransType() {
		return transType;
	}

	public void setTransType(TransactionType transType) {
		this.transType = transType;
	}

	public TransStatus getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(TransStatus transStatus) {
		this.transStatus = transStatus;
	}

	public TransMethod getTransMethod() {
		return transMethod;
	}

	public void setTransMethod(TransMethod transMethod) {
		this.transMethod = transMethod;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public AccountBgd getAccount() {
		return account;
	}

	public void setAccount(AccountBgd account) {
		this.account = account;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}