package com.company.management.entity.bgdreporting;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "bgdreporting")
@Table(name = "back2_back_report")
@Entity
public class Back2BackReport {
    @JmixGeneratedValue
    @Column(name = "id", nullable = false)
    @Id
    private UUID id;

    @Column(name = "BGD_ID")
    private String bgdID;

    @Column(name = "buy_investor_g", precision = 19, scale = 6)
    private BigDecimal buyInvestorG;

    @Column(name = "buy_investor_rm", precision = 19, scale = 6)
    private BigDecimal buyInvestorRm;

    @Column(name = "buy_supplier_g", precision = 19, scale = 6)
    private BigDecimal buySupplierG;

    @Column(name = "buy_supplier_rm", precision = 19, scale = 6)
    private BigDecimal buySupplierRm;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "deleted_by")
    private String deletedBy;

    @Column(name = "deleted_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModifiedDate;

    @Column(name = "margin", precision = 19, scale = 6)
    private BigDecimal margin;

    @Column(name = "price_investor", precision = 19, scale = 2)
    private BigDecimal priceInvestor;

    @Column(name = "price_supplier", precision = 19, scale = 6)
    private BigDecimal priceSupplier;

    @Column(name = "reference_no")
    private String referenceNo;

    @Column(name = "sell_investor_g", precision = 19, scale = 6)
    private BigDecimal sellInvestorG;

    @Column(name = "sell_investor_rm", precision = 19, scale = 6)
    private BigDecimal sellInvestorRm;

    @Column(name = "sell_supplier_g", precision = 19, scale = 6)
    private BigDecimal sellSupplierG;

    @Column(name = "sell_supplier_rm", precision = 19, scale = 6)
    private BigDecimal sellSupplierRm;

    @Column(name = "timestamp_investor")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampInvestor;

    @Column(name = "timestamp_supplier")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestampSupplier;

    @Column(name = "version", nullable = false)
    private Integer version;

    public String getBgdID() {
        return bgdID;
    }

    public void setBgdID(String bgdID) {
        this.bgdID = bgdID;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Date getTimestampSupplier() {
        return timestampSupplier;
    }

    public void setTimestampSupplier(Date timestampSupplier) {
        this.timestampSupplier = timestampSupplier;
    }

    public Date getTimestampInvestor() {
        return timestampInvestor;
    }

    public void setTimestampInvestor(Date timestampInvestor) {
        this.timestampInvestor = timestampInvestor;
    }

    public BigDecimal getSellSupplierRm() {
        return sellSupplierRm;
    }

    public void setSellSupplierRm(BigDecimal sellSupplierRm) {
        this.sellSupplierRm = sellSupplierRm;
    }

    public BigDecimal getSellSupplierG() {
        return sellSupplierG;
    }

    public void setSellSupplierG(BigDecimal sellSupplierG) {
        this.sellSupplierG = sellSupplierG;
    }

    public BigDecimal getSellInvestorRm() {
        return sellInvestorRm;
    }

    public void setSellInvestorRm(BigDecimal sellInvestorRm) {
        this.sellInvestorRm = sellInvestorRm;
    }

    public BigDecimal getSellInvestorG() {
        return sellInvestorG;
    }

    public void setSellInvestorG(BigDecimal sellInvestorG) {
        this.sellInvestorG = sellInvestorG;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public BigDecimal getPriceSupplier() {
        return priceSupplier;
    }

    public void setPriceSupplier(BigDecimal priceSupplier) {
        this.priceSupplier = priceSupplier;
    }

    public BigDecimal getPriceInvestor() {
        return priceInvestor;
    }

    public void setPriceInvestor(BigDecimal priceInvestor) {
        this.priceInvestor = priceInvestor;
    }

    public BigDecimal getMargin() {
        return margin;
    }

    public void setMargin(BigDecimal margin) {
        this.margin = margin;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public BigDecimal getBuySupplierRm() {
        return buySupplierRm;
    }

    public void setBuySupplierRm(BigDecimal buySupplierRm) {
        this.buySupplierRm = buySupplierRm;
    }

    public BigDecimal getBuySupplierG() {
        return buySupplierG;
    }

    public void setBuySupplierG(BigDecimal buySupplierG) {
        this.buySupplierG = buySupplierG;
    }

    public BigDecimal getBuyInvestorRm() {
        return buyInvestorRm;
    }

    public void setBuyInvestorRm(BigDecimal buyInvestorRm) {
        this.buyInvestorRm = buyInvestorRm;
    }

    public BigDecimal getBuyInvestorG() {
        return buyInvestorG;
    }

    public void setBuyInvestorG(BigDecimal buyInvestorG) {
        this.buyInvestorG = buyInvestorG;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}