package com.company.management.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum StateList implements EnumClass<String> {

    JHR("Johor Darul Ta'zim"),
    KDH("Kedah Darul Aman"),
    KTN("Kelantan Darul Naim"),
    KUL("Kuala Lumpur"),
    LBN("Labuan"),
    MLK("Melaka"),
    NSN("Negeri Sembilan Darul Khusus"),
    PHG("Pahang Darul Makmur"),
    PNG("Penang"),
    PRK("Perak Darul Ridzuan"),
    PLS("Perlis Indera Kayangan"),
    PJY("Putrajaya"),
    SBH("Sabah"),
    SWK("Sarawak"),
    SGR("Selangor Darul Ehsan"),
    TRG("Terengganu Darul Iman");

	private final String id;

	StateList(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Nullable
	public static StateList fromId(String id) {
		for (StateList at : StateList.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}
}