package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "sap_fee_1")
@Entity
public class SapFee1 {
	@Column(name = "type_fee")
	@Lob
	@Id
	private String typeFee;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "sst")
	private BigDecimal sst;

	@Column(name = "transaction_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getSst() {
		return sst;
	}

	public void setSst(BigDecimal sst) {
		this.sst = sst;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTypeFee() {
		return typeFee;
	}

	public void setTypeFee(String typeFee) {
		this.typeFee = typeFee;
	}
}