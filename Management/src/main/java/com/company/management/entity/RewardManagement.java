package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "REWARD_MANAGEMENT", indexes = {
        @Index(name = "IDX_REWARDMANAGEMEN_REWARDTYPE", columnList = "REWARD_TYPE_ID"),
        @Index(name = "IDX_REWARD_MANAGEMENT_UOM", columnList = "UOM_ID"),
        @Index(name = "IDX_REWARD_MANAGEMENT_STATUS", columnList = "STATUS_ID")
})
@Entity
public class RewardManagement {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    @JoinColumn(name = "REWARD_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType rewardType;

    @Column(name = "CAMPAIGN_NAME")
    private String campaignName;

    @Column(name = "CAMPAIGN_CODE")
    private String campaignCode;

    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "MAX_PARTICIPANT")
    private Integer maxParticipant;

    @Column(name = "BENEFICIARIES")
    private String beneficiaries;

    @Column(name = "REWARD_AMOUNT", precision = 19, scale = 2)
    private BigDecimal rewardAmount;

    @JoinColumn(name = "UOM_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType uom;

    @Column(name = "METHOD_")
    private String method;

    @JoinColumn(name = "STATUS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType status;

    @Column(name = "REDEEMED")
    private Integer redeemed;

    @Column(name = "REDEEMED_AMOUNT", precision = 19, scale = 2)
    private BigDecimal redeemedAmount;

    @OneToMany(mappedBy = "reward")
    private List<RewardCriteria> rewardCriteria;

    public BigDecimal getRedeemedAmount() {
        return redeemedAmount;
    }

    public void setRedeemedAmount(BigDecimal redeemedAmount) {
        this.redeemedAmount = redeemedAmount;
    }

    public Integer getRedeemed() {
        return redeemed;
    }

    public void setRedeemed(Integer redeemed) {
        this.redeemed = redeemed;
    }

    public List<RewardCriteria> getRewardCriteria() {
        return rewardCriteria;
    }

    public void setRewardCriteria(List<RewardCriteria> rewardCriteria) {
        this.rewardCriteria = rewardCriteria;
    }

    public RtType getStatus() {
        return status;
    }

    public void setStatus(RtType status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public RtType getUom() {
        return uom;
    }

    public void setUom(RtType uom) {
        this.uom = uom;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getBeneficiaries() {
        return beneficiaries;
    }

    public void setBeneficiaries(String beneficiaries) {
        this.beneficiaries = beneficiaries;
    }

    public Integer getMaxParticipant() {
        return maxParticipant;
    }

    public void setMaxParticipant(Integer maxParticipant) {
        this.maxParticipant = maxParticipant;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public RtType getRewardType() {
        return rewardType;
    }

    public void setRewardType(RtType rewardType) {
        this.rewardType = rewardType;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}