package com.company.management.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum MyIcon implements EnumClass<String> {

    ;

    private final String id;

    MyIcon(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static MyIcon fromId(String id) {
        for (MyIcon at : MyIcon.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}