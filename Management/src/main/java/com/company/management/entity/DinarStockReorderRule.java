package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@JmixEntity
@Table(name = "DINAR_STOCK_REORDER_RULE", indexes = {
		@Index(name = "IDX_DINARSTOCKREORDERRULE_TYPE", columnList = "TYPE_ID"),
		@Index(name = "IDX_DINARSTOCKREORDERRU_MINUOM", columnList = "MIN_UOM_ID"),
		@Index(name = "IDX_DINARSTOCKREORDERRU_MAXUOM", columnList = "MAX_UOM_ID"),
		@Index(name = "IDX_DINARSTOCKREORDERRU_DAYUOM", columnList = "DAY_UOM_ID")
})
@Entity
public class DinarStockReorderRule {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@JoinColumn(name = "TYPE_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType type;

	@Column(name = "CODE")
	private String code;

	@Column(name = "LOCATION")
	private String location;

	@Column(name = "MIN_QUANTITY")
	private Integer minQuantity;

	@JoinColumn(name = "MIN_UOM_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType minUOM;

	@Column(name = "MAX_QUANTITY")
	private Integer maxQuantity;

	@JoinColumn(name = "MAX_UOM_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType maxUOM;

	@Column(name = "DAY_TO_PURCHASE")
	private Integer dayToPurchase;

	@JoinColumn(name = "DAY_UOM_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType dayUOM;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public RtType getDayUOM() {
		return dayUOM;
	}

	public void setDayUOM(RtType dayUOM) {
		this.dayUOM = dayUOM;
	}

	public RtType getMaxUOM() {
		return maxUOM;
	}

	public void setMaxUOM(RtType maxUOM) {
		this.maxUOM = maxUOM;
	}

	public RtType getMinUOM() {
		return minUOM;
	}

	public void setMinUOM(RtType minUOM) {
		this.minUOM = minUOM;
	}

	public Integer getDayToPurchase() {
		return dayToPurchase;
	}

	public void setDayToPurchase(Integer dayToPurchase) {
		this.dayToPurchase = dayToPurchase;
	}

	public Integer getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public Integer getMinQuantity() {
		return minQuantity;
	}

	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public RtType getType() {
		return type;
	}

	public void setType(RtType type) {
		this.type = type;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}