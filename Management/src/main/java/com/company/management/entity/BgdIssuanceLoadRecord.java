package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.JmixProperty;
import org.springframework.stereotype.Component;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

@Component
@JmixEntity(annotatedPropertiesOnly = true)
public class BgdIssuanceLoadRecord {
	@Temporal(TemporalType.DATE)
	@JmixProperty
	private Date timeStamp;

	@JmixProperty
	private String description;

	@JmixProperty
	private String code;

	@JmixProperty
	private String goldType;

	@JmixProperty
	private String issuanceNo;

	@JmixProperty
	private BigDecimal tradable;

	public BigDecimal getTradableUnit() {
		return tradableUnit;
	}

	public void setTradableUnit(BigDecimal tradableUnit) {
		this.tradableUnit = tradableUnit;
	}

	private BigDecimal tradableUnit;

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	public String getIssuanceNo() {
		return issuanceNo;
	}

	public void setIssuanceNo(String issuanceNo) {
		this.issuanceNo = issuanceNo;
	}

	public BigDecimal getTradable() {
		return tradable;
	}

	public void setTradable(BigDecimal tradable) {
		this.tradable = tradable;
	}
}
