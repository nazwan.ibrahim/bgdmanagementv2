package com.company.management.entity.wallet;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "wallet")
@Table(name = "account_hold")
@Entity
public class AccountHold {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@JoinColumn(name = "account_bgd_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private AccountBgd accountBgd;

	@Column(name = "amount", precision = 19, scale = 2)
	private BigDecimal amount;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@DeletedBy
	@Column(name = "deleted_by")
	private String deletedBy;

	@DeletedDate
	@Column(name = "deleted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@JoinColumn(name = "transaction_acct_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private TransactionAcct transactionAcct;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public TransactionAcct getTransactionAcct() {
		return transactionAcct;
	}

	public void setTransactionAcct(TransactionAcct transactionAcct) {
		this.transactionAcct = transactionAcct;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public AccountBgd getAccountBgd() {
		return accountBgd;
	}

	public void setAccountBgd(AccountBgd accountBgd) {
		this.accountBgd = accountBgd;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}