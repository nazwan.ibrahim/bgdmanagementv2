package com.company.management.entity.bgdreporting;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.entity.annotation.JmixId;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@JmixEntity
public class RedemptionReportDTO {
    @JmixGeneratedValue
    @JmixId
    private UUID id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @InstanceName
    private String description;

    private String referenceNo;

    private String bgdID;

    private String investorName;

    private String goldCode;

    private String goldType;

    private String location;

    private String deliveryPartner;

    private String serialNo;

    private String status;

    private Integer outPS;

    private BigDecimal outGram;

    public BigDecimal getOutGram() {
        return outGram;
    }

    public void setOutGram(BigDecimal outGram) {
        this.outGram = outGram;
    }

    public Integer getOutPS() {
        return outPS;
    }

    public void setOutPS(Integer outPS) {
        this.outPS = outPS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getDeliveryPartner() {
        return deliveryPartner;
    }

    public void setDeliveryPartner(String deliveryPartner) {
        this.deliveryPartner = deliveryPartner;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGoldType() {
        return goldType;
    }

    public void setGoldType(String goldType) {
        this.goldType = goldType;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getGoldCode() {
        return goldCode;
    }

    public void setGoldCode(String goldCode) {
        this.goldCode = goldCode;
    }

    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public String getBgdID() {
        return bgdID;
    }

    public void setBgdID(String bgdID) {
        this.bgdID = bgdID;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}