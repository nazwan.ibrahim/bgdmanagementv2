package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "purchase_histories_view")
@Entity
public class PurchaseHistoriesView {
	@JmixGeneratedValue
	@Column(name = "uuid", nullable = false)
	@Id
	private UUID id;

	@Column(name = "acc_owner")
	private String accOwner;

	@Column(name = "bgd_id")
	private String bgd;

	@Column(name = "cash_amount")
	private BigDecimal cashAmount;

	@Column(name = "cash_uom")
	private String cashUom;

	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "gold_amount")
	private BigDecimal goldAmount;

	@Column(name = "gold_uom")
	private String goldUom;

	@Column(name = "price")
	private BigDecimal price;

	@Column(name = "reference")
	private String reference;

	@Column(name = "status")
	private String status;

	@Column(name = "status_code")
	private String statusCode;

	@Column(name = "transaction_code")
	private String transactionCode;

	public String getTransactionCode() {
		return transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getGoldUom() {
		return goldUom;
	}

	public void setGoldUom(String goldUom) {
		this.goldUom = goldUom;
	}

	public BigDecimal getGoldAmount() {
		return goldAmount;
	}

	public void setGoldAmount(BigDecimal goldAmount) {
		this.goldAmount = goldAmount;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCashUom() {
		return cashUom;
	}

	public void setCashUom(String cashUom) {
		this.cashUom = cashUom;
	}

	public BigDecimal getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(BigDecimal cashAmount) {
		this.cashAmount = cashAmount;
	}

	public String getBgd() {
		return bgd;
	}

	public void setBgd(String bgd) {
		this.bgd = bgd;
	}

	public String getAccOwner() {
		return accOwner;
	}

	public void setAccOwner(String accOwner) {
		this.accOwner = accOwner;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}