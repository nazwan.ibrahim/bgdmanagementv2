package com.company.management.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "PURCHASE_RECORD", indexes = {
		@Index(name = "IDX_PURCHASE_RECORD_TYPE", columnList = "ISSUANCE_TYPE_ID"),
		@Index(name = "IDX_PURCHASE_RECORD_STATUS", columnList = "STATUS_ID"),
		@Index(name = "IDX_PURCHASERECOR_ISSUANCETYPE", columnList = "ISSUANCE_TYPE_ID")
})
@Entity
public class PurchaseRecord {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@Column(name = "REFERENCE_NUM")
	private String reference_Num;

	@Column(name = "DESCRIPTION")
	private String description;

	@InstanceName
	@Column(name = "PO_NUM")
	private String po_Num;

	@JoinColumn(name = "TYPE_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType type;

	@Column(name = "SUPPLIER")
	private String supplier;

	@Column(name = "PHYSICAL_STOCK", precision = 19, scale = 6)
	private BigDecimal physical_stock;

	@Column(name = "ACC_TRADABLE", precision = 19, scale = 6)
	private BigDecimal acc_tradable;

	@JoinColumn(name = "ISSUANCE_TYPE_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType issuanceType;

	@Column(name = "DATE_")
	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	@JoinColumn(name = "STATUS_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private RtType status;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@OneToMany(mappedBy = "purchase_record")
	private List<PurchaseRecordProduct> product;

	public RtType getIssuanceType() {
		return issuanceType;
	}

	public void setIssuanceType(RtType issuanceType) {
		this.issuanceType = issuanceType;
	}

	public String getReference_Num() {
		return reference_Num;
	}

	public void setReference_Num(String reference_Num) {
		this.reference_Num = reference_Num;
	}

	public List<PurchaseRecordProduct> getProduct() {
		return product;
	}

	public void setProduct(List<PurchaseRecordProduct> product) {
		this.product = product;
	}

	public BigDecimal getAcc_tradable() {
		return acc_tradable;
	}

	public void setAcc_tradable(BigDecimal acc_tradable) {
		this.acc_tradable = acc_tradable;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public RtType getStatus() {
		return status;
	}

	public void setStatus(RtType status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getPhysical_stock() {
		return physical_stock;
	}

	public void setPhysical_stock(BigDecimal physical_stock) {
		this.physical_stock = physical_stock;
	}

	public RtType getType() {
		return type;
	}

	public void setType(RtType type) {
		this.type = type;
	}

	public String getPo_Num() {
		return po_Num;
	}

	public void setPo_Num(String po_Num) {
		this.po_Num = po_Num;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}