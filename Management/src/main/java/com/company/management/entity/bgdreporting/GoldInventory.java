package com.company.management.entity.bgdreporting;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Store(name = "bgdreporting")
@JmixEntity
@Table(name = "GOLD_INVENTORY")
@Entity
public class GoldInventory {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@Column(name = "TIMESTAMP_")
	private String timestamp;

	@InstanceName
	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "PURCHASE_ORDER")
	private String purchaseOrderNo;

	@Column(name = "DELIVERY_RECEIPT_NO")
	private String deliveryReceiptNo;

	@Column(name = "GOLD_CODE")
	private String goldCode;

	@Column(name = "GOLD_TYPE")
	private String goldType;

	@Column(name = "GOLD_SUPPLIER")
	private String goldSupplier;

	@Column(name = "VAULT_LOCATION")
	private String vaultLocation;

	@Column(name = "SERIAL_NO")
	private String serialNo;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "PHYSICAL_STOCK_IN_GRAM", precision = 19, scale = 2)
	private BigDecimal physicalStockInGram;

	@Column(name = "ACC_PHYSICAL_STOCKIN_GRAM", precision = 19, scale = 2)
	private BigDecimal accPhysicalStockInGram;

	@Column(name = "TRADABLE_BGD_IN_GRAM", precision = 19, scale = 2)
	private BigDecimal tradableBGDInGram;

	@Column(name = "ACC_TRADABLE_BGD_IN_GRAM", precision = 19, scale = 2)
	private BigDecimal accTradableBGDInGram;

	@Column(name = "ACC_TRADABLE_BGD_IN_UNIT")
	private BigDecimal accTradableBGDInUnit;

	@Column(name = "NON_TRADABLE_IN_GRAM", precision = 19, scale = 2)
	private BigDecimal nonTradableInGram;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public BigDecimal getNonTradableInGram() {
		return nonTradableInGram;
	}

	public void setNonTradableInGram(BigDecimal nonTradableInGram) {
		this.nonTradableInGram = nonTradableInGram;
	}

	public void setAccTradableBGDInUnit(BigDecimal accTradableBGDInUnit) {
		this.accTradableBGDInUnit = accTradableBGDInUnit;
	}

	public BigDecimal getAccTradableBGDInUnit() {
		return accTradableBGDInUnit;
	}

	public BigDecimal getAccTradableBGDInGram() {
		return accTradableBGDInGram;
	}

	public void setAccTradableBGDInGram(BigDecimal accTradableBGDInGram) {
		this.accTradableBGDInGram = accTradableBGDInGram;
	}

	public BigDecimal getTradableBGDInGram() {
		return tradableBGDInGram;
	}

	public void setTradableBGDInGram(BigDecimal tradableBGDInGram) {
		this.tradableBGDInGram = tradableBGDInGram;
	}

	public BigDecimal getAccPhysicalStockInGram() {
		return accPhysicalStockInGram;
	}

	public void setAccPhysicalStockInGram(BigDecimal accPhysicalStockInGram) {
		this.accPhysicalStockInGram = accPhysicalStockInGram;
	}

	public BigDecimal getPhysicalStockInGram() {
		return physicalStockInGram;
	}

	public void setPhysicalStockInGram(BigDecimal physicalStockInGram) {
		this.physicalStockInGram = physicalStockInGram;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getVaultLocation() {
		return vaultLocation;
	}

	public void setVaultLocation(String vaultLocation) {
		this.vaultLocation = vaultLocation;
	}

	public String getGoldSupplier() {
		return goldSupplier;
	}

	public void setGoldSupplier(String goldSupplier) {
		this.goldSupplier = goldSupplier;
	}

	public String getGoldType() {
		return goldType;
	}

	public void setGoldType(String goldType) {
		this.goldType = goldType;
	}

	public String getGoldCode() {
		return goldCode;
	}

	public void setGoldCode(String goldCode) {
		this.goldCode = goldCode;
	}

	public String getDeliveryReceiptNo() {
		return deliveryReceiptNo;
	}

	public void setDeliveryReceiptNo(String deliveryReceiptNo) {
		this.deliveryReceiptNo = deliveryReceiptNo;
	}

	public String getPurchaseOrderNo() {
		return purchaseOrderNo;
	}

	public void setPurchaseOrderNo(String purchaseOrderNo) {
		this.purchaseOrderNo = purchaseOrderNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}