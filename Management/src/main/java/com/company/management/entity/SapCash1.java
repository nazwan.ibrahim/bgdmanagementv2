package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "sap_cash_1")
@Entity
public class SapCash1 {
	@Column(name = "type_fee")
	@Lob
	@Id
	private String typeFee;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "transaction_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTypeFee() {
		return typeFee;
	}

	public void setTypeFee(String typeFee) {
		this.typeFee = typeFee;
	}
}