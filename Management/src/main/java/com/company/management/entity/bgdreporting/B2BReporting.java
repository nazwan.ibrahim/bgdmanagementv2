package com.company.management.entity.bgdreporting;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.NumberFormat;
import io.jmix.core.metamodel.annotation.Store;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Store(name = "bgdreporting")
@JmixEntity
@Table(name = "B2B_REPORTING")
@Entity
public class B2BReporting {
	@JmixGeneratedValue
	@Column(name = "ID", nullable = false)
	@Id
	private UUID id;

	@Column(name = "REFERENCE_NO")
	private String referenceNo;

	@Column(name = "TIMESTAMP_")
	private String investorTimestamp;

	@Column(name = "INVESTOR_PRICE_PER_GRAM", precision = 19, scale = 2)
	private BigDecimal investorPricePerGram;

	@NumberFormat(pattern = "#0.000000")
	@Column(name = "INVESTOR_BUY_IN_GRAM", precision = 19, scale = 6)
	private BigDecimal investorBuyInGram;

	@Column(name = "INVESTOR_BUY_IN_RM", precision = 19, scale = 2)
	private BigDecimal investorBuyInRM;

	@NumberFormat(pattern = "#0.000000")
	@Column(name = "INVESTOR_SELL_IN_GRAM", precision = 19, scale = 6)
	private BigDecimal investorSellInGram;

	@Column(name = "INVESTOR_SELL_IN_RM", precision = 19, scale = 2)
	private BigDecimal investorSellInRM;

	@Column(name = "SUPPLIER_TIMESTAMP")
	private String supplierTimestamp;

	@Column(name = "SUPPLIER_PRICE_PER_GRAM", precision = 19, scale = 2)
	private BigDecimal supplierPricePerGram;

	@NumberFormat(pattern = "#0.000000")
	@Column(name = "SUPPLIER_BUY_IN_GRAM", precision = 19, scale = 2)
	private BigDecimal supplierBuyInGram;

	@Column(name = "SUPPLIER_BUY_IN_RM", precision = 19, scale = 2)
	private BigDecimal supplierBuyInRM;

	@NumberFormat(pattern = "#0.000000")
	@Column(name = "SUPPLIER_SELL_IN_GRAM", precision = 19, scale = 6)
	private BigDecimal supplierSellInGram;

	@Column(name = "SUPPLIER_SELL_IN_RM", precision = 19, scale = 2)
	private BigDecimal supplierSellInRM;

	@Column(name = "MARGIN", precision = 19, scale = 2)
	private BigDecimal margin;

	@Column(name = "VERSION", nullable = false)
	@Version
	private Integer version;

	@CreatedBy
	@Column(name = "CREATED_BY")
	private String createdBy;

	@CreatedDate
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedBy
	@Column(name = "LAST_MODIFIED_BY")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "LAST_MODIFIED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@DeletedBy
	@Column(name = "DELETED_BY")
	private String deletedBy;

	@DeletedDate
	@Column(name = "DELETED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getSupplierSellInRM() {
		return supplierSellInRM;
	}

	public void setSupplierSellInRM(BigDecimal supplierSellInRM) {
		this.supplierSellInRM = supplierSellInRM;
	}

	public BigDecimal getSupplierSellInGram() {
		return supplierSellInGram;
	}

	public void setSupplierSellInGram(BigDecimal supplierSellInGram) {
		this.supplierSellInGram = supplierSellInGram;
	}

	public BigDecimal getSupplierBuyInRM() {
		return supplierBuyInRM;
	}

	public void setSupplierBuyInRM(BigDecimal supplierBuyInRM) {
		this.supplierBuyInRM = supplierBuyInRM;
	}

	public BigDecimal getSupplierBuyInGram() {
		return supplierBuyInGram;
	}

	public void setSupplierBuyInGram(BigDecimal supplierBuyInGram) {
		this.supplierBuyInGram = supplierBuyInGram;
	}

	public BigDecimal getInvestorBuyInRM() {
		return investorBuyInRM;
	}

	public void setInvestorBuyInRM(BigDecimal investorBuyInRM) {
		this.investorBuyInRM = investorBuyInRM;
	}

	public BigDecimal getSupplierPricePerGram() {
		return supplierPricePerGram;
	}

	public void setSupplierPricePerGram(BigDecimal supplierPricePerGram) {
		this.supplierPricePerGram = supplierPricePerGram;
	}

	public String getSupplierTimestamp() {
		return supplierTimestamp;
	}

	public void setSupplierTimestamp(String supplierTimestamp) {
		this.supplierTimestamp = supplierTimestamp;
	}

	public BigDecimal getInvestorSellInRM() {
		return investorSellInRM;
	}

	public void setInvestorSellInRM(BigDecimal investorSellInRM) {
		this.investorSellInRM = investorSellInRM;
	}

	public BigDecimal getInvestorSellInGram() {
		return investorSellInGram;
	}

	public void setInvestorSellInGram(BigDecimal investorSellInGram) {
		this.investorSellInGram = investorSellInGram;
	}

	public BigDecimal getInvestorBuyInGram() {
		return investorBuyInGram;
	}

	public void setInvestorBuyInGram(BigDecimal investorBuyInGram) {
		this.investorBuyInGram = investorBuyInGram;
	}

	public BigDecimal getInvestorPricePerGram() {
		return investorPricePerGram;
	}

	public void setInvestorPricePerGram(BigDecimal investorPricePerGram) {
		this.investorPricePerGram = investorPricePerGram;
	}

	public String getInvestorTimestamp() {
		return investorTimestamp;
	}

	public void setInvestorTimestamp(String investorTimestamp) {
		this.investorTimestamp = investorTimestamp;
	}

	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}