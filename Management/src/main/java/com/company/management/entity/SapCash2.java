package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "sap_cash_2")
@Entity
public class SapCash2 {
	@JmixGeneratedValue
	@Column(name = "id")
	@Id
	private Long id;

	@Column(name = "amount")
	private BigDecimal amount;

	@Column(name = "transaction_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;

	@Column(name = "type_fee")
	@Lob
	private String typeFee;

	public String getTypeFee() {
		return typeFee;
	}

	public void setTypeFee(String typeFee) {
		this.typeFee = typeFee;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}