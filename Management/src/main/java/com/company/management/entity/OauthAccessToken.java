package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DdlGeneration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Table(name = "oauth_access_token")
@Entity
public class OauthAccessToken {
	@Column(name = "authentication_id", nullable = false)
	@Id
	private String authentication;

	@Column(name = "authentication")
	private byte[] authentication1;

	@Column(name = "client_id")
	private String client;

	@Column(name = "refresh_token")
	private String refreshToken;

	@Column(name = "token_id", nullable = false)
	private String token;

	@Column(name = "token")
	private byte[] token1;

	@Column(name = "user_name")
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public byte[] getToken1() {
		return token1;
	}

	public void setToken1(byte[] token1) {
		this.token1 = token1;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public byte[] getAuthentication1() {
		return authentication1;
	}

	public void setAuthentication1(byte[] authentication1) {
		this.authentication1 = authentication1;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}
}