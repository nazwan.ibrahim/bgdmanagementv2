package com.company.management.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DbView;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@DbView
@JmixEntity
@Store(name = "wallet")
@Table(name = "admin_fee_users")
@Entity
public class AdminFeeUser {
    @Column(name = "acc_owner")
    @Id
    private String accOwner;

    @Column(name = "cash_amount")
    private BigDecimal cashAmount;

    @Column(name = "gold_amount")
    private BigDecimal goldAmount;

    public BigDecimal getGoldAmount() {
        return goldAmount;
    }

    public void setGoldAmount(BigDecimal goldAmount) {
        this.goldAmount = goldAmount;
    }

    public BigDecimal getCashAmount() {
        return cashAmount;
    }

    public void setCashAmount(BigDecimal cashAmount) {
        this.cashAmount = cashAmount;
    }

    public String getAccOwner() {
        return accOwner;
    }

    public void setAccOwner(String accOwner) {
        this.accOwner = accOwner;
    }
}