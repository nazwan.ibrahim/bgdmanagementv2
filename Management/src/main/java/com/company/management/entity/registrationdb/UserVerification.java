package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "user_verification")
@Entity
public class UserVerification {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@JoinColumn(name = "assessment_status_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private AssessmentStatus assessmentStatus;

	@Column(name = "cad_id")
	private UUID cad;

	@Column(name = "cad_application_id")
	private Integer cadApplication;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@JoinColumn(name = "crp_result_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private CrpResult crpResult;

	@DeletedBy
	@Column(name = "deleted_by")
	private String deletedBy;

	@DeletedDate
	@Column(name = "deleted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@Column(name = "ekyc_url")
	@Lob
	private String ekycUrl;

	@Column(name = "gbg_ref")
	private String gbgRef;

	@JoinColumn(name = "kyc_result_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private KycStatus kycResult;

	@LastModifiedBy
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@JoinColumn(name = "ns_result_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private NameScreeningResult nsResult;

	@JoinColumn(name = "overal_verification_result_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private OveralVerificationResult overalVerificationResult;

	@JoinColumn(name = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private UserProfile user;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UserProfile getUser() {
		return user;
	}

	public void setUser(UserProfile user) {
		this.user = user;
	}

	public OveralVerificationResult getOveralVerificationResult() {
		return overalVerificationResult;
	}

	public void setOveralVerificationResult(OveralVerificationResult overalVerificationResult) {
		this.overalVerificationResult = overalVerificationResult;
	}

	public NameScreeningResult getNsResult() {
		return nsResult;
	}

	public void setNsResult(NameScreeningResult nsResult) {
		this.nsResult = nsResult;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public KycStatus getKycResult() {
		return kycResult;
	}

	public void setKycResult(KycStatus kycResult) {
		this.kycResult = kycResult;
	}

	public String getGbgRef() {
		return gbgRef;
	}

	public void setGbgRef(String gbgRef) {
		this.gbgRef = gbgRef;
	}

	public String getEkycUrl() {
		return ekycUrl;
	}

	public void setEkycUrl(String ekycUrl) {
		this.ekycUrl = ekycUrl;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public CrpResult getCrpResult() {
		return crpResult;
	}

	public void setCrpResult(CrpResult crpResult) {
		this.crpResult = crpResult;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCadApplication() {
		return cadApplication;
	}

	public void setCadApplication(Integer cadApplication) {
		this.cadApplication = cadApplication;
	}

	public UUID getCad() {
		return cad;
	}

	public void setCad(UUID cad) {
		this.cad = cad;
	}

	public AssessmentStatus getAssessmentStatus() {
		return assessmentStatus;
	}

	public void setAssessmentStatus(AssessmentStatus assessmentStatus) {
		this.assessmentStatus = assessmentStatus;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}