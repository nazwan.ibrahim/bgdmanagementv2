package com.company.management.entity;

import io.jmix.core.metamodel.datatype.impl.EnumClass;

import javax.annotation.Nullable;


public enum IndividualApprovedAccount implements EnumClass<String> {

    APPROVED("APPROVED"),
    REJECT("REJECT");

	private final String id;

	IndividualApprovedAccount(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Nullable
	public static IndividualApprovedAccount fromId(String id) {
		for (IndividualApprovedAccount at : IndividualApprovedAccount.values()) {
			if (at.getId().equals(id)) {
				return at;
			}
		}
		return null;
	}
}