package com.company.management.entity.registrationdb;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "otp")
@Entity
public class Otp {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@Column(name = "otp")
	private Integer otp;

	@Column(name = "user_id", nullable = false)
	private UUID user;

	@Column(name = "valid_date_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validDateTime;

	public Date getValidDateTime() {
		return validDateTime;
	}

	public void setValidDateTime(Date validDateTime) {
		this.validDateTime = validDateTime;
	}

	public UUID getUser() {
		return user;
	}

	public void setUser(UUID user) {
		this.user = user;
	}

	public Integer getOtp() {
		return otp;
	}

	public void setOtp(Integer otp) {
		this.otp = otp;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}