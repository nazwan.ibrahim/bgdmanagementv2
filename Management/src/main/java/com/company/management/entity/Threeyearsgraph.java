package com.company.management.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.data.DbView;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@DbView
@JmixEntity
@Table(name = "threeyearsgraph")
@Entity
public class Threeyearsgraph {
	@JmixGeneratedValue
	@Column(name = "uuid")
	@Id
	private UUID id;

	@Column(name = "buy_price")
	private BigDecimal buyPrice;

	@Column(name = "price_ts")
	@Temporal(TemporalType.TIMESTAMP)
	private Date priceTs;

	@Column(name = "sell_price")
	private BigDecimal sellPrice;

	public BigDecimal getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(BigDecimal sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Date getPriceTs() {
		return priceTs;
	}

	public void setPriceTs(Date priceTs) {
		this.priceTs = priceTs;
	}

	public BigDecimal getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(BigDecimal buyPrice) {
		this.buyPrice = buyPrice;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}