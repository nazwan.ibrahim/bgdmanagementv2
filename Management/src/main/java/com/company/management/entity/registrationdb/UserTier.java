package com.company.management.entity.registrationdb;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import io.jmix.core.metamodel.annotation.Store;
import io.jmix.data.DdlGeneration;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@DdlGeneration(value = DdlGeneration.DbScriptGenerationMode.DISABLED)
@JmixEntity
@Store(name = "registration")
@Table(name = "user_tier", indexes = {
		@Index(name = "IDX_USER_TIER_VERIFICATION", columnList = "verification_id")
})
@Entity
public class UserTier {
	@JmixGeneratedValue
	@Column(name = "id", nullable = false)
	@Id
	private UUID id;

	@CreatedBy
	@Column(name = "created_by")
	private String createdBy;

	@CreatedDate
	@Column(name = "created_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@DeletedBy
	@Column(name = "deleted_by")
	private String deletedBy;

	@DeletedDate
	@Column(name = "deleted_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@JoinColumn(name = "kyc_status_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private KycStatus kycStatus;

	@JoinColumn(name = "verification_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private UserVerification verification;

	@LastModifiedBy
	@Column(name = "last_modified_by")
	private String lastModifiedBy;

	@LastModifiedDate
	@Column(name = "last_modified_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	@Column(name = "reference")
	private String reference;

	@JoinColumn(name = "tier_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private TierType tier;

	@JoinColumn(name = "user_id")
	@ManyToOne(fetch = FetchType.LAZY)
	private UserProfile user;

	@Column(name = "CURRENT_TIER")
	private Boolean currentTier;

	@Version
	@Column(name = "version", nullable = false)
	private Integer version;

	public UserVerification getVerification() {
		return verification;
	}

	public void setVerification(UserVerification verification) {
		this.verification = verification;
	}

	public Boolean getCurrentTier() {
		return currentTier;
	}

	public void setCurrentTier(Boolean currentTier) {
		this.currentTier = currentTier;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public UserProfile getUser() {
		return user;
	}

	public void setUser(UserProfile user) {
		this.user = user;
	}

	public TierType getTier() {
		return tier;
	}

	public void setTier(TierType tier) {
		this.tier = tier;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public KycStatus getKycStatus() {
		return kycStatus;
	}

	public void setKycStatus(KycStatus kycStatus) {
		this.kycStatus = kycStatus;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}
}