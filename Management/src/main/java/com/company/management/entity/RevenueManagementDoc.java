package com.company.management.entity;

import io.jmix.core.FileRef;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "REVENUE_MANAGEMENT_DOC", indexes = {
        @Index(name = "IDX_REVENUEMANAG_REVENUEMANAG", columnList = "REVENUE_MANAGEMENT_ID"),
        @Index(name = "IDX_REVENUEMANAGEMENTDO_ACTION", columnList = "ACTION_ID")
})
@Entity
public class RevenueManagementDoc {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    @JoinColumn(name = "REVENUE_MANAGEMENT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RevenueManagement revenueManagement;

    @InstanceName
    @Column(name = "NAME")
    private String name;

    @Column(name = "FILE_UPLOAD", length = 1024)
    private FileRef file_upload;

    @JoinColumn(name = "ACTION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RtType action;

    public RtType getAction() {
        return action;
    }

    public void setAction(RtType action) {
        this.action = action;
    }

    public FileRef getFile_upload() {
        return file_upload;
    }

    public void setFile_upload(FileRef file_upload) {
        this.file_upload = file_upload;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RevenueManagement getRevenueManagement() {
        return revenueManagement;
    }

    public void setRevenueManagement(RevenueManagement revenueManagement) {
        this.revenueManagement = revenueManagement;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}